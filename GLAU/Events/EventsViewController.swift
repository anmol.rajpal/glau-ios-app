//
//  EventsViewController.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 23/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit
class EventsViewController: UIViewController {
    let formatter = DateFormatter()
    let calendar = Calendar.current
    
//    let components: DateComponents = [
//        .day,
//        .hour,
//        .minute,
//        .second
//    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        self.navigationController?.navigationBar.isHidden = true
        setUpViews()
        setUpConstraints()
        let timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(printTime), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func printTime() {
        let launchDateString:String = "15/01/2019 12:00:00"
        let startTime = Date()
        if let endTime = getDateFromString(dateString: launchDateString, dateFormat: NSObject.DateFormatType.dateWithTimeType1) {
            let timeDifference = calendar.dateComponents([.day, .hour, .minute, .second], from: startTime, to: endTime)
            if startTime < endTime {
                daysLabel.text = String(timeDifference.day ?? 0)
                hoursLabel.text = String(timeDifference.hour ?? 0)
                minutesLabel.text = String(timeDifference.minute ?? 0)
                secondsLabel.text = String(timeDifference.second ?? 0)
            }
        } else {
            daysLabel.text = String(0)
            hoursLabel.text = String(0)
            minutesLabel.text = String(0)
            secondsLabel.text = String(0)
        }
        
    }
    let countDownLabel:UILabel = {
        let dateString = "5/10/2018"
        let launchDate:Date = dateString.convertToDate(dateString: dateString)
        print(launchDate)
//        let label = CountdownLabel(frame: CGRect.zero, date: launchDate)
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 50, weight: .light)
//        label.animationType = .Evaporate
//        label.start()
        return label
    }()
    
    let testLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .light)
        return label
    }()
    
    let imageView:UIImageView = {
        let view = UIImageView(image: UIImage(named: "confetti-wallpaper"))
        view.frame = CGRect.zero
        view.clipsToBounds = true
        return view
    }()
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let view = UIVisualEffectView(effect: blurEffect)
//        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.text = "for iOS launching in..."
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.ultraLight)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        return label
    }()
    let glasoLabel:UILabel = {
        let label = UILabel()
        let glaAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 65, weight: UIFont.Weight.light),
            NSAttributedString.Key.foregroundColor : UIColor.glasoThemeColor
        ]
        let soAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 65, weight: UIFont.Weight.light),
            NSAttributedString.Key.foregroundColor : UIColor.orange
        ]
        let glaAttributedString = NSAttributedString(string: "G L A ", attributes: glaAttributes)
        let soAttributedString = NSAttributedString(string: "S O", attributes: soAttributes)
        let glasoString = NSMutableAttributedString()
        glasoString.append(glaAttributedString)
        glasoString.append(soAttributedString)
        label.attributedText = glasoString
//        label.textColor = .white
        
        //            label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    let daysContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let hoursContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let minutesContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let secondsContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let daysLabel:UILabel = {
        let label = UILabel()
        label.text = "18"
        label.font = UIFont.systemFont(ofSize: 45, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        return label
    }()
    let hoursLabel:UILabel = {
        let label = UILabel()
        label.text = "1"
        label.font = UIFont.systemFont(ofSize: 45, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        
        return label
    }()
    let minutesLabel:UILabel = {
        let label = UILabel()
        label.text = "8"
        label.font = UIFont.systemFont(ofSize: 45, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        return label
    }()
    let secondsLabel:UILabel = {
        let label = UILabel()
        label.text = "45"
        label.font = UIFont.systemFont(ofSize: 45, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        return label
    }()
    let daysTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "Days"
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        return label
    }()
    let hoursTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "Hours"
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        return label
    }()
    let minutesTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "Minutes"
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        return label
    }()
    let secondsTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "Seconds"
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        return label
    }()
    lazy var stackView:UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        return stackView
    }()
    func setUpViews() {
        
        view.insertSubview(imageView, at: 0)
        view.addSubview(glasoLabel)
        view.addSubview(messageLabel)
        view.addSubview(stackView)
//        view.addSubview(blurredEffectView)
        stackView.addArrangedSubview(daysContainerView)
        stackView.addArrangedSubview(hoursContainerView)
        stackView.addArrangedSubview(minutesContainerView)
        stackView.addArrangedSubview(secondsContainerView)
        
//        daysContainerView.insertSubview(blurredEffectView, at: 0)
        daysContainerView.addSubview(daysLabel)
        daysContainerView.addSubview(daysTitleLabel)
        
        hoursContainerView.addSubview(hoursLabel)
        hoursContainerView.addSubview(hoursTitleLabel)
        
        minutesContainerView.addSubview(minutesLabel)
        minutesContainerView.addSubview(minutesTitleLabel)
        
        secondsContainerView.addSubview(secondsLabel)
        secondsContainerView.addSubview(secondsTitleLabel)
        
        
        
        
        
        view.addSubview(countDownLabel)

        view.addSubview(testLabel)
        imageView.frame = self.view.bounds
//        blurredEffectView.frame = self.view.bounds
//        blurredEffectView.frame = daysContainerView.bounds
//        blurredEffectView.frame = hoursContainerView.bounds
//        blurredEffectView.frame = minutesContainerView.bounds
//        blurredEffectView.frame = secondsContainerView.bounds
    }
    func setUpConstraints() {
//        _ = countDownLabel.anchor(view.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 200, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        countDownLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        _ = testLabel.anchor(countDownLabel.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 200, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        testLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = stackView.anchorWithoutWH(top: nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 15, leftConstant: 10, bottomConstant: 0, rightConstant: 10)
        
        _ = glasoLabel.anchorWithoutWH(top: nil, left: view.leftAnchor, bottom: messageLabel.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 25, rightConstant: 15)
        glasoLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        _ = messageLabel.anchorWithoutWH(top: nil, left: view.leftAnchor, bottom: stackView.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 25, rightConstant: 15)
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
     
        //        daysTitleLabel.bottomAnchor.constraint(equalTo: daysContainerView.bottomAnchor, constant: -5).isActive = true
        daysLabel.topAnchor.constraint(equalTo: daysContainerView.topAnchor, constant: 10).isActive = true
        daysLabel.centerXAnchor.constraint(equalTo: daysContainerView.centerXAnchor).isActive = true
        daysTitleLabel.topAnchor.constraint(equalTo: daysLabel.bottomAnchor, constant: 10).isActive = true
        daysTitleLabel.centerXAnchor.constraint(equalTo: daysContainerView.centerXAnchor).isActive = true
        daysContainerView.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 10).isActive = true
        daysContainerView.bottomAnchor.constraint(equalTo: daysTitleLabel.bottomAnchor, constant: 10).isActive = true
//        daysContainerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        hoursLabel.topAnchor.constraint(equalTo: hoursContainerView.topAnchor, constant: 10).isActive = true
        hoursLabel.centerXAnchor.constraint(equalTo: hoursContainerView.centerXAnchor).isActive = true
        hoursTitleLabel.topAnchor.constraint(equalTo: hoursLabel.bottomAnchor, constant: 10).isActive = true
        hoursTitleLabel.centerXAnchor.constraint(equalTo: hoursContainerView.centerXAnchor).isActive = true
        hoursContainerView.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 10).isActive = true
        hoursContainerView.bottomAnchor.constraint(equalTo: hoursTitleLabel.bottomAnchor, constant: 10).isActive = true
        
        minutesLabel.topAnchor.constraint(equalTo: minutesContainerView.topAnchor, constant: 10).isActive = true
        minutesLabel.centerXAnchor.constraint(equalTo: minutesContainerView.centerXAnchor).isActive = true
        minutesTitleLabel.topAnchor.constraint(equalTo: minutesLabel.bottomAnchor, constant: 10).isActive = true
        minutesTitleLabel.centerXAnchor.constraint(equalTo: minutesContainerView.centerXAnchor).isActive = true
        minutesContainerView.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 10).isActive = true
        minutesContainerView.bottomAnchor.constraint(equalTo: minutesTitleLabel.bottomAnchor, constant: 10).isActive = true
        
        secondsLabel.topAnchor.constraint(equalTo: secondsContainerView.topAnchor, constant: 10).isActive = true
        secondsLabel.centerXAnchor.constraint(equalTo: secondsContainerView.centerXAnchor).isActive = true
        secondsTitleLabel.topAnchor.constraint(equalTo: secondsLabel.bottomAnchor, constant: 10).isActive = true
        secondsTitleLabel.centerXAnchor.constraint(equalTo: secondsContainerView.centerXAnchor).isActive = true
        secondsContainerView.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 10).isActive = true
        secondsContainerView.bottomAnchor.constraint(equalTo: secondsTitleLabel.bottomAnchor, constant: 10).isActive = true
    }
}
