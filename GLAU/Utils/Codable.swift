//
//  User.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 27/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import Foundation
struct User {
    static var rollNo:String?
    static var name:String?
    static var status: String?
    static var semester: String?
    static var course: String?
    static var resident: String?
}

struct AuthenticationInfo: Codable {
    let message : String?
    let result : String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result = "Result"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
    }
}
struct StudentDetailsCodable: Codable {
    let branch : String?
    let course : String?
    let cPI : String?
    let fatherName : String?
    let message : String?
    let name : String?
    let nature : String?
    let placed : String?
    let resident : String?
    let result : String?
    let rollNo : String?
    let section : String?
    let semester : String?
    let status : String?
    let email : String?
    
    enum CodingKeys: String, CodingKey {
        case branch = "Branch"
        case course = "Course"
        case cPI = "CPI"
        case fatherName = "FatherName"
        case message = "Message"
        case name = "Name"
        case nature = "Nature"
        case placed = "Placed"
        case resident = "Resident"
        case result = "Result"
        case rollNo = "RollNo"
        case section = "Section"
        case semester = "Semester"
        case status = "Status"
        case email = "Email"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        branch = try values.decodeIfPresent(String.self, forKey: .branch)
        course = try values.decodeIfPresent(String.self, forKey: .course)
        cPI = try values.decodeIfPresent(String.self, forKey: .cPI)
        fatherName = try values.decodeIfPresent(String.self, forKey: .fatherName)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        nature = try values.decodeIfPresent(String.self, forKey: .nature)
        placed = try values.decodeIfPresent(String.self, forKey: .placed)
        resident = try values.decodeIfPresent(String.self, forKey: .resident)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        rollNo = try values.decodeIfPresent(String.self, forKey: .rollNo)
        section = try values.decodeIfPresent(String.self, forKey: .section)
        semester = try values.decodeIfPresent(String.self, forKey: .semester)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        email = try values.decodeIfPresent(String.self, forKey: .email)
    }
}
struct AcademicDetails: Codable {
    let advisor : String?
    let advisorEmail : String?
    let attend : String?
    let endMark : String?
    let faculty : String?
    let facultyEmail : String?
    let held : String?
    let m1Mark : String?
    let m2Mark : String?
    let message : String?
    let percentage : String?
    let result : String?
    let subjectCode : String?
    let subjectName : String?
    let subjectType : String?
    let advisorContact : String?
    let facultyContact : String?
    
    enum CodingKeys: String, CodingKey {
        case advisor = "Advisor"
        case advisorEmail = "AdvisorEmail"
        case attend = "Attend"
        case endMark = "EndMark"
        case faculty = "Faculty"
        case facultyEmail = "FacultyEmail"
        case held = "Held"
        case m1Mark = "M1Mark"
        case m2Mark = "M2Mark"
        case message = "Message"
        case percentage = "Percentage"
        case result = "Result"
        case subjectCode = "SubjectCode"
        case subjectName = "SubjectName"
        case subjectType = "SubjectType"
        case advisorContact = "AdvisorContact"
        case facultyContact = "FacultyContact"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        advisor = try values.decodeIfPresent(String.self, forKey: .advisor)
        advisorEmail = try values.decodeIfPresent(String.self, forKey: .advisorEmail)
        attend = try values.decodeIfPresent(String.self, forKey: .attend)
        endMark = try values.decodeIfPresent(String.self, forKey: .endMark)
        faculty = try values.decodeIfPresent(String.self, forKey: .faculty)
        facultyEmail = try values.decodeIfPresent(String.self, forKey: .facultyEmail)
        held = try values.decodeIfPresent(String.self, forKey: .held)
        m1Mark = try values.decodeIfPresent(String.self, forKey: .m1Mark)
        m2Mark = try values.decodeIfPresent(String.self, forKey: .m2Mark)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        percentage = try values.decodeIfPresent(String.self, forKey: .percentage)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        subjectCode = try values.decodeIfPresent(String.self, forKey: .subjectCode)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        subjectType = try values.decodeIfPresent(String.self, forKey: .subjectType)
        advisorContact = try values.decodeIfPresent(String.self, forKey: .advisorContact)
        facultyContact = try values.decodeIfPresent(String.self, forKey: .facultyContact)
    }
}
struct TimeTableCodable : Codable {
    let details : String?
    let facultyName : String?
    let lectureNo : String?
    let message : String?
    let result : String?
    let subjectName : String?
    let timing : String?
    
    enum CodingKeys: String, CodingKey {
        case details = "Details"
        case facultyName = "FacultyName"
        case lectureNo = "LectureNo"
        case message = "Message"
        case result = "Result"
        case subjectName = "SubjectName"
        case timing = "Timing"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        details = try values.decodeIfPresent(String.self, forKey: .details)
        facultyName = try values.decodeIfPresent(String.self, forKey: .facultyName)
        lectureNo = try values.decodeIfPresent(String.self, forKey: .lectureNo)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        timing = try values.decodeIfPresent(String.self, forKey: .timing)
    }
}
struct AttendanceDetailsCodable : Codable {
    
    let details : String?
    let lectureDate : String?
    let message : String?
    let result : String?
    let sNo : String?
    
    init(details:String?, lectureDate:String?, message:String?, result:String?, sNo:String?) {
        self.details = details
        self.lectureDate = lectureDate
        self.message = message
        self.result = result
        self.sNo = sNo
    }
    enum CodingKeys: String, CodingKey {
        case details = "Details"
        case lectureDate = "LectureDate"
        case message = "Message"
        case result = "Result"
        case sNo = "SNo"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        details = try values.decodeIfPresent(String.self, forKey: .details)
        lectureDate = try values.decodeIfPresent(String.self, forKey: .lectureDate)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        sNo = try values.decodeIfPresent(String.self, forKey: .sNo)
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(details, forKey: .details)
        try container.encode(lectureDate, forKey: .lectureDate)
        try container.encode(message, forKey: .message)
        try container.encode(result, forKey: .result)
        try container.encode(sNo, forKey: .sNo)

    }
}
struct SemesterListCodable: Codable {
    let message : String?
    let result : String?
    let semester : String?
    let code : String?
    let name : String?
    let grade : String?
    let gradePoint : String?
    let credit : String?
    let sCredit : String?
    let sGP : String?
    let sPI : String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result = "Result"
        case semester = "Semester"
        case code = "Code"
        case name = "Name"
        case grade = "Grade"
        case gradePoint = "GradePoint"
        case credit = "Credit"
        case sCredit = "SCredit"
        case sGP = "SGP"
        case sPI = "SPI"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        semester = try values.decodeIfPresent(String.self, forKey: .semester)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        grade = try values.decodeIfPresent(String.self, forKey: .grade)
        gradePoint = try values.decodeIfPresent(String.self, forKey: .gradePoint)
        credit = try values.decodeIfPresent(String.self, forKey: .credit)
        sCredit = try values.decodeIfPresent(String.self, forKey: .sCredit)
        sGP = try values.decodeIfPresent(String.self, forKey: .sGP)
        sPI = try values.decodeIfPresent(String.self, forKey: .sPI)
    }
}
struct SemesterResultCodable: Codable {
    let message : String?
    let result : String?
    let semester : String?
    let code : String?
    let name : String?
    let grade : String?
    let gradePoint : String?
    let credit : String?
    let sCredit : String?
    let sGP : String?
    let sPI : String?
    let cPI : String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result = "Result"
        case semester = "Semester"
        case code = "Code"
        case name = "Name"
        case grade = "Grade"
        case gradePoint = "GradePoint"
        case credit = "Credit"
        case sCredit = "SCredit"
        case sGP = "SGP"
        case sPI = "SPI"
        case cPI = "CPI"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        semester = try values.decodeIfPresent(String.self, forKey: .semester)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        grade = try values.decodeIfPresent(String.self, forKey: .grade)
        gradePoint = try values.decodeIfPresent(String.self, forKey: .gradePoint)
        credit = try values.decodeIfPresent(String.self, forKey: .credit)
        sCredit = try values.decodeIfPresent(String.self, forKey: .sCredit)
        sGP = try values.decodeIfPresent(String.self, forKey: .sGP)
        sPI = try values.decodeIfPresent(String.self, forKey: .sPI)
        cPI = try values.decodeIfPresent(String.self, forKey: .cPI)
    }
}
struct FeedObject : Codable {
    
    let articleOfTheDay : ArticleOfTheDay?
    let date : String?
    let news : [News]?
    let quote : String?
    
    enum CodingKeys: String, CodingKey {
        case articleOfTheDay = "articleOfTheDay"
        case date = "date"
        case news = "news"
        case quote = "quote"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
//        articleOfTheDay = try? ArticleOfTheDay(from: decoder)
        articleOfTheDay = try values.decodeIfPresent(ArticleOfTheDay.self, forKey: .articleOfTheDay)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        news = try values.decodeIfPresent([News].self, forKey: .news)
        quote = try values.decodeIfPresent(String.self, forKey: .quote)
    }
}
struct News : Codable {
    
    let content : String?
    let publisher : String?
    
    enum CodingKeys: String, CodingKey {
        case content = "content"
        case publisher = "publisher"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        content = try values.decodeIfPresent(String.self, forKey: .content)
        publisher = try values.decodeIfPresent(String.self, forKey: .publisher)
    }
}
struct ArticleOfTheDay : Codable {
    
    let content : String?
    let publisher : String?
    let source : String?
    
    enum CodingKeys: String, CodingKey {
        case content = "content"
        case publisher = "publisher"
        case source = "source"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        content = try values.decodeIfPresent(String.self, forKey: .content)
        publisher = try values.decodeIfPresent(String.self, forKey: .publisher)
        source = try values.decodeIfPresent(String.self, forKey: .source)
    }
    
}

struct QuoteServiceCodable : Codable {
    let message : String?
    let quoteOfTheDay : QuoteOfTheDay?
    let result : String?
    let serviceStatus : String?
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case quoteOfTheDay = "quoteOfTheDay"
        case result = "result"
        case serviceStatus = "serviceStatus"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        quoteOfTheDay = try values.decodeIfPresent(QuoteOfTheDay.self, forKey: .quoteOfTheDay)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        serviceStatus = try values.decodeIfPresent(String.self, forKey: .serviceStatus)
    }
    struct QuoteOfTheDay : Codable {
        let quote : String?
        let quoteId : String?
        let quoter : String?
        enum CodingKeys: String, CodingKey {
            case quote = "quote"
            case quoteId = "quoteId"
            case quoter = "quoter"
        }
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            quote = try values.decodeIfPresent(String.self, forKey: .quote)
            quoteId = try values.decodeIfPresent(String.self, forKey: .quoteId)
            quoter = try values.decodeIfPresent(String.self, forKey: .quoter)
        }
    }
}
struct CombinedServicesCodable : Codable {
    
    let articlesService : ArticlesService?
    let factsService : FactsService?
    let message : String?
    let quotesService : QuotesService?
    let result : String?
    let riddlesService : RiddlesService?
    let tipsService : TipsService?
    
    enum CodingKeys: String, CodingKey {
        case articlesService = "articlesService"
        case factsService = "factsService"
        case message = "message"
        case quotesService = "quotesService"
        case result = "result"
        case riddlesService = "riddlesService"
        case tipsService = "tipsService"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        articlesService = try values.decodeIfPresent(ArticlesService.self, forKey: .articlesService)
        factsService = try values.decodeIfPresent(FactsService.self, forKey: .factsService)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        quotesService = try values.decodeIfPresent(QuotesService.self, forKey: .quotesService)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        riddlesService = try values.decodeIfPresent(RiddlesService.self, forKey: .riddlesService)
        tipsService = try values.decodeIfPresent(TipsService.self, forKey: .tipsService)
    }
    struct FactsService : Codable {
        
        let factOfTheDay : FactOfTheDay?
        let message : String?
        let result : String?
        let serviceDetails : ServiceDetail?
        
        enum CodingKeys: String, CodingKey {
            case factOfTheDay = "factOfTheDay"
            case message = "message"
            case result = "result"
            case serviceDetails = "serviceDetails"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            factOfTheDay = try values.decodeIfPresent(FactOfTheDay.self, forKey: .factOfTheDay)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            result = try values.decodeIfPresent(String.self, forKey: .result)
            serviceDetails = try values.decodeIfPresent(ServiceDetail.self, forKey: .serviceDetails)
        }
        struct ServiceDetail : Codable {
            
            let accessRoute : String?
            let serviceDescription : String?
            let serviceId : String?
            let serviceName : String?
            let serviceState : String?
            
            enum CodingKeys: String, CodingKey {
                case accessRoute = "accessRoute"
                case serviceDescription = "serviceDescription"
                case serviceId = "serviceId"
                case serviceName = "serviceName"
                case serviceState = "serviceState"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                accessRoute = try values.decodeIfPresent(String.self, forKey: .accessRoute)
                serviceDescription = try values.decodeIfPresent(String.self, forKey: .serviceDescription)
                serviceId = try values.decodeIfPresent(String.self, forKey: .serviceId)
                serviceName = try values.decodeIfPresent(String.self, forKey: .serviceName)
                serviceState = try values.decodeIfPresent(String.self, forKey: .serviceState)
            }
            
        }
        struct FactOfTheDay : Codable {
            
            let fact : String?
            let factId : String?
            let publisherDetails : PublisherDetail?
            let publishingDate : String?
            let publishingTime : String?
            let source : String?
            let timestamp : String?
            
            enum CodingKeys: String, CodingKey {
                case fact = "fact"
                case factId = "factId"
                case publisherDetails = "publisherDetails"
                case publishingDate = "publishingDate"
                case publishingTime = "publishingTime"
                case source = "source"
                case timestamp = "timestamp"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                fact = try values.decodeIfPresent(String.self, forKey: .fact)
                factId = try values.decodeIfPresent(String.self, forKey: .factId)
                publisherDetails = try values.decodeIfPresent(PublisherDetail.self, forKey: .publisherDetails)
                publishingDate = try values.decodeIfPresent(String.self, forKey: .publishingDate)
                publishingTime = try values.decodeIfPresent(String.self, forKey: .publishingTime)
                source = try values.decodeIfPresent(String.self, forKey: .source)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
            }
            struct PublisherDetail : Codable {
                
                let publisherEmail : String?
                let publisherId : String?
                let publisherName : String?
                let publisherPhone : String?
                let publisherRole : String?
                
                enum CodingKeys: String, CodingKey {
                    case publisherEmail = "publisherEmail"
                    case publisherId = "publisherId"
                    case publisherName = "publisherName"
                    case publisherPhone = "publisherPhone"
                    case publisherRole = "publisherRole"
                }
                
                init(from decoder: Decoder) throws {
                    let values = try decoder.container(keyedBy: CodingKeys.self)
                    publisherEmail = try values.decodeIfPresent(String.self, forKey: .publisherEmail)
                    publisherId = try values.decodeIfPresent(String.self, forKey: .publisherId)
                    publisherName = try values.decodeIfPresent(String.self, forKey: .publisherName)
                    publisherPhone = try values.decodeIfPresent(String.self, forKey: .publisherPhone)
                    publisherRole = try values.decodeIfPresent(String.self, forKey: .publisherRole)
                }
                
            }
        }
    }
    
    struct QuotesService : Codable {
        
        let message : String?
        let quoteOfTheDay : QuoteOfTheDay?
        let result : String?
        let serviceDetails : ServiceDetail?
        
        enum CodingKeys: String, CodingKey {
            case message = "message"
            case quoteOfTheDay = "quoteOfTheDay"
            case result = "result"
            case serviceDetails = "serviceDetails"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            quoteOfTheDay = try values.decodeIfPresent(QuoteOfTheDay.self, forKey: .quoteOfTheDay)
            result = try values.decodeIfPresent(String.self, forKey: .result)
            serviceDetails = try values.decodeIfPresent(ServiceDetail.self, forKey: .serviceDetails)
        }
        struct ServiceDetail : Codable {
            
            let accessRoute : String?
            let serviceDescription : String?
            let serviceId : String?
            let serviceName : String?
            let serviceState : String?
            
            enum CodingKeys: String, CodingKey {
                case accessRoute = "accessRoute"
                case serviceDescription = "serviceDescription"
                case serviceId = "serviceId"
                case serviceName = "serviceName"
                case serviceState = "serviceState"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                accessRoute = try values.decodeIfPresent(String.self, forKey: .accessRoute)
                serviceDescription = try values.decodeIfPresent(String.self, forKey: .serviceDescription)
                serviceId = try values.decodeIfPresent(String.self, forKey: .serviceId)
                serviceName = try values.decodeIfPresent(String.self, forKey: .serviceName)
                serviceState = try values.decodeIfPresent(String.self, forKey: .serviceState)
            }
        }
        struct QuoteOfTheDay : Codable {
            
            let publisherDetails : PublisherDetail?
            let publishingDate : String?
            let publishingTime : String?
            let quote : String?
            let quoteId : String?
            let quoter : String?
            let source : String?
            let timestamp : String?
            
            enum CodingKeys: String, CodingKey {
                case publisherDetails = "publisherDetails"
                case publishingDate = "publishingDate"
                case publishingTime = "publishingTime"
                case quote = "quote"
                case quoteId = "quoteId"
                case quoter = "quoter"
                case source = "source"
                case timestamp = "timestamp"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                publisherDetails = try values.decodeIfPresent(PublisherDetail.self, forKey: .publisherDetails)
                publishingDate = try values.decodeIfPresent(String.self, forKey: .publishingDate)
                publishingTime = try values.decodeIfPresent(String.self, forKey: .publishingTime)
                quote = try values.decodeIfPresent(String.self, forKey: .quote)
                quoteId = try values.decodeIfPresent(String.self, forKey: .quoteId)
                quoter = try values.decodeIfPresent(String.self, forKey: .quoter)
                source = try values.decodeIfPresent(String.self, forKey: .source)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
            }
            struct PublisherDetail : Codable {
                
                let publisherEmail : String?
                let publisherId : String?
                let publisherName : String?
                let publisherPhone : String?
                let publisherRole : String?
                
                enum CodingKeys: String, CodingKey {
                    case publisherEmail = "publisherEmail"
                    case publisherId = "publisherId"
                    case publisherName = "publisherName"
                    case publisherPhone = "publisherPhone"
                    case publisherRole = "publisherRole"
                }
                
                init(from decoder: Decoder) throws {
                    let values = try decoder.container(keyedBy: CodingKeys.self)
                    publisherEmail = try values.decodeIfPresent(String.self, forKey: .publisherEmail)
                    publisherId = try values.decodeIfPresent(String.self, forKey: .publisherId)
                    publisherName = try values.decodeIfPresent(String.self, forKey: .publisherName)
                    publisherPhone = try values.decodeIfPresent(String.self, forKey: .publisherPhone)
                    publisherRole = try values.decodeIfPresent(String.self, forKey: .publisherRole)
                }
                
            }
            
        }
    }
    
    struct ArticlesService : Codable {
        
        let articleOfTheDay : ArticleOfTheDay?
        let message : String?
        let result : String?
        let serviceDetails : ServiceDetail?
        
        enum CodingKeys: String, CodingKey {
            case articleOfTheDay = "articleOfTheDay"
            case message = "message"
            case result = "result"
            case serviceDetails = "serviceDetails"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            articleOfTheDay = try values.decodeIfPresent(ArticleOfTheDay.self, forKey: .articleOfTheDay)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            result = try values.decodeIfPresent(String.self, forKey: .result)
            serviceDetails = try values.decodeIfPresent(ServiceDetail.self, forKey: .serviceDetails)
        }
        struct ServiceDetail : Codable {
            
            let accessRoute : String?
            let serviceDescription : String?
            let serviceId : String?
            let serviceName : String?
            let serviceState : String?
            
            enum CodingKeys: String, CodingKey {
                case accessRoute = "accessRoute"
                case serviceDescription = "serviceDescription"
                case serviceId = "serviceId"
                case serviceName = "serviceName"
                case serviceState = "serviceState"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                accessRoute = try values.decodeIfPresent(String.self, forKey: .accessRoute)
                serviceDescription = try values.decodeIfPresent(String.self, forKey: .serviceDescription)
                serviceId = try values.decodeIfPresent(String.self, forKey: .serviceId)
                serviceName = try values.decodeIfPresent(String.self, forKey: .serviceName)
                serviceState = try values.decodeIfPresent(String.self, forKey: .serviceState)
            }
        }
        struct ArticleOfTheDay : Codable {
            
            let articleId : String?
            let author : String?
            let content : String?
            let publisherDetails : PublisherDetail?
            let publishingDate : String?
            let publishingTime : String?
            let source : String?
            let timestamp : String?
            let title : String?
            
            enum CodingKeys: String, CodingKey {
                case articleId = "articleId"
                case author = "author"
                case content = "content"
                case publisherDetails = "publisherDetails"
                case publishingDate = "publishingDate"
                case publishingTime = "publishingTime"
                case source = "source"
                case timestamp = "timestamp"
                case title = "title"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                articleId = try values.decodeIfPresent(String.self, forKey: .articleId)
                author = try values.decodeIfPresent(String.self, forKey: .author)
                content = try values.decodeIfPresent(String.self, forKey: .content)
                publisherDetails = try values.decodeIfPresent(PublisherDetail.self, forKey: .publisherDetails)
                publishingDate = try values.decodeIfPresent(String.self, forKey: .publishingDate)
                publishingTime = try values.decodeIfPresent(String.self, forKey: .publishingTime)
                source = try values.decodeIfPresent(String.self, forKey: .source)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
                title = try values.decodeIfPresent(String.self, forKey: .title)
            }
            struct PublisherDetail : Codable {
                
                let publisherEmail : String?
                let publisherId : String?
                let publisherName : String?
                let publisherPhone : String?
                let publisherRole : String?
                
                enum CodingKeys: String, CodingKey {
                    case publisherEmail = "publisherEmail"
                    case publisherId = "publisherId"
                    case publisherName = "publisherName"
                    case publisherPhone = "publisherPhone"
                    case publisherRole = "publisherRole"
                }
                
                init(from decoder: Decoder) throws {
                    let values = try decoder.container(keyedBy: CodingKeys.self)
                    publisherEmail = try values.decodeIfPresent(String.self, forKey: .publisherEmail)
                    publisherId = try values.decodeIfPresent(String.self, forKey: .publisherId)
                    publisherName = try values.decodeIfPresent(String.self, forKey: .publisherName)
                    publisherPhone = try values.decodeIfPresent(String.self, forKey: .publisherPhone)
                    publisherRole = try values.decodeIfPresent(String.self, forKey: .publisherRole)
                }
                
            }
        }
    }

    struct RiddlesService : Codable {
        
        let message : String?
        let result : String?
        let riddleOfTheDay : RiddleOfTheDay?
        let serviceDetails : ServiceDetail?
        
        enum CodingKeys: String, CodingKey {
            case message = "message"
            case result = "result"
            case riddleOfTheDay = "riddleOfTheDay"
            case serviceDetails = "serviceDetails"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            result = try values.decodeIfPresent(String.self, forKey: .result)
            riddleOfTheDay = try values.decodeIfPresent(RiddleOfTheDay.self, forKey: .riddleOfTheDay)
            serviceDetails = try values.decodeIfPresent(ServiceDetail.self, forKey: .serviceDetails)
        }
        struct ServiceDetail : Codable {
            
            let accessRoute : String?
            let serviceDescription : String?
            let serviceId : String?
            let serviceName : String?
            let serviceState : String?
            
            enum CodingKeys: String, CodingKey {
                case accessRoute = "accessRoute"
                case serviceDescription = "serviceDescription"
                case serviceId = "serviceId"
                case serviceName = "serviceName"
                case serviceState = "serviceState"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                accessRoute = try values.decodeIfPresent(String.self, forKey: .accessRoute)
                serviceDescription = try values.decodeIfPresent(String.self, forKey: .serviceDescription)
                serviceId = try values.decodeIfPresent(String.self, forKey: .serviceId)
                serviceName = try values.decodeIfPresent(String.self, forKey: .serviceName)
                serviceState = try values.decodeIfPresent(String.self, forKey: .serviceState)
            }
            
        }
        struct RiddleOfTheDay : Codable {
            
            let answer : String?
            let publisherDetails : PublisherDetail?
            let publishingDate : String?
            let publishingTime : String?
            let riddle : String?
            let riddleId : String?
            let source : String?
            let timestamp : String?
            
            enum CodingKeys: String, CodingKey {
                case answer = "answer"
                case publisherDetails = "publisherDetails"
                case publishingDate = "publishingDate"
                case publishingTime = "publishingTime"
                case riddle = "riddle"
                case riddleId = "riddleId"
                case source = "source"
                case timestamp = "timestamp"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                answer = try values.decodeIfPresent(String.self, forKey: .answer)
                publisherDetails = try values.decodeIfPresent(PublisherDetail.self, forKey: .publisherDetails)
                publishingDate = try values.decodeIfPresent(String.self, forKey: .publishingDate)
                publishingTime = try values.decodeIfPresent(String.self, forKey: .publishingTime)
                riddle = try values.decodeIfPresent(String.self, forKey: .riddle)
                riddleId = try values.decodeIfPresent(String.self, forKey: .riddleId)
                source = try values.decodeIfPresent(String.self, forKey: .source)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
            }
            struct PublisherDetail : Codable {
                
                let publisherEmail : String?
                let publisherId : String?
                let publisherName : String?
                let publisherPhone : String?
                let publisherRole : String?
                
                enum CodingKeys: String, CodingKey {
                    case publisherEmail = "publisherEmail"
                    case publisherId = "publisherId"
                    case publisherName = "publisherName"
                    case publisherPhone = "publisherPhone"
                    case publisherRole = "publisherRole"
                }
                
                init(from decoder: Decoder) throws {
                    let values = try decoder.container(keyedBy: CodingKeys.self)
                    publisherEmail = try values.decodeIfPresent(String.self, forKey: .publisherEmail)
                    publisherId = try values.decodeIfPresent(String.self, forKey: .publisherId)
                    publisherName = try values.decodeIfPresent(String.self, forKey: .publisherName)
                    publisherPhone = try values.decodeIfPresent(String.self, forKey: .publisherPhone)
                    publisherRole = try values.decodeIfPresent(String.self, forKey: .publisherRole)
                }
                
            }
        }
    }
    
    struct TipsService : Codable {
        
        let message : String?
        let result : String?
        let serviceDetails : ServiceDetail?
        let tipOfTheDay : TipOfTheDay?
        
        enum CodingKeys: String, CodingKey {
            case message = "message"
            case result = "result"
            case serviceDetails = "serviceDetails"
            case tipOfTheDay = "tipOfTheDay"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            result = try values.decodeIfPresent(String.self, forKey: .result)
            serviceDetails = try values.decodeIfPresent(ServiceDetail.self, forKey: .serviceDetails)
            tipOfTheDay = try values.decodeIfPresent(TipOfTheDay.self, forKey: .tipOfTheDay)
        }
        struct ServiceDetail : Codable {
            
            let accessRoute : String?
            let serviceDescription : String?
            let serviceId : String?
            let serviceName : String?
            let serviceState : String?
            
            enum CodingKeys: String, CodingKey {
                case accessRoute = "accessRoute"
                case serviceDescription = "serviceDescription"
                case serviceId = "serviceId"
                case serviceName = "serviceName"
                case serviceState = "serviceState"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                accessRoute = try values.decodeIfPresent(String.self, forKey: .accessRoute)
                serviceDescription = try values.decodeIfPresent(String.self, forKey: .serviceDescription)
                serviceId = try values.decodeIfPresent(String.self, forKey: .serviceId)
                serviceName = try values.decodeIfPresent(String.self, forKey: .serviceName)
                serviceState = try values.decodeIfPresent(String.self, forKey: .serviceState)
            }
            
        }
        struct TipOfTheDay : Codable {
            
            let publisherDetails : PublisherDetail?
            let publishingDate : String?
            let publishingTime : String?
            let source : String?
            let timestamp : String?
            let tip : String?
            let tipId : String?
            
            enum CodingKeys: String, CodingKey {
                case publisherDetails = "publisherDetails"
                case publishingDate = "publishingDate"
                case publishingTime = "publishingTime"
                case source = "source"
                case timestamp = "timestamp"
                case tip = "tip"
                case tipId = "tipId"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                publisherDetails = try values.decodeIfPresent(PublisherDetail.self, forKey: .publisherDetails)
                publishingDate = try values.decodeIfPresent(String.self, forKey: .publishingDate)
                publishingTime = try values.decodeIfPresent(String.self, forKey: .publishingTime)
                source = try values.decodeIfPresent(String.self, forKey: .source)
                timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
                tip = try values.decodeIfPresent(String.self, forKey: .tip)
                tipId = try values.decodeIfPresent(String.self, forKey: .tipId)
            }
            struct PublisherDetail : Codable {
                
                let publisherEmail : String?
                let publisherId : String?
                let publisherName : String?
                let publisherPhone : String?
                let publisherRole : String?
                
                enum CodingKeys: String, CodingKey {
                    case publisherEmail = "publisherEmail"
                    case publisherId = "publisherId"
                    case publisherName = "publisherName"
                    case publisherPhone = "publisherPhone"
                    case publisherRole = "publisherRole"
                }
                
                init(from decoder: Decoder) throws {
                    let values = try decoder.container(keyedBy: CodingKeys.self)
                    publisherEmail = try values.decodeIfPresent(String.self, forKey: .publisherEmail)
                    publisherId = try values.decodeIfPresent(String.self, forKey: .publisherId)
                    publisherName = try values.decodeIfPresent(String.self, forKey: .publisherName)
                    publisherPhone = try values.decodeIfPresent(String.self, forKey: .publisherPhone)
                    publisherRole = try values.decodeIfPresent(String.self, forKey: .publisherRole)
                }
                
            }
        }
    }
}


struct NewsServiceCodable : Codable {
    
    let message : String?
    let newsOfTheDay : [NewsOfTheDay]?
    let result : String?
    let serviceDetails : ServiceDetail?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case newsOfTheDay = "newsOfTheDay"
        case result = "result"
        case serviceDetails = "serviceDetails"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        newsOfTheDay = try values.decodeIfPresent([NewsOfTheDay].self, forKey: .newsOfTheDay)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        serviceDetails = try values.decodeIfPresent(ServiceDetail.self, forKey: .serviceDetails)
    }
    struct ServiceDetail : Codable {
        
        let accessRoute : String?
        let serviceDescription : String?
        let serviceId : String?
        let serviceName : String?
        let serviceState : String?
        
        enum CodingKeys: String, CodingKey {
            case accessRoute = "accessRoute"
            case serviceDescription = "serviceDescription"
            case serviceId = "serviceId"
            case serviceName = "serviceName"
            case serviceState = "serviceState"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            accessRoute = try values.decodeIfPresent(String.self, forKey: .accessRoute)
            serviceDescription = try values.decodeIfPresent(String.self, forKey: .serviceDescription)
            serviceId = try values.decodeIfPresent(String.self, forKey: .serviceId)
            serviceName = try values.decodeIfPresent(String.self, forKey: .serviceName)
            serviceState = try values.decodeIfPresent(String.self, forKey: .serviceState)
        }
        
    }
    struct NewsOfTheDay : Codable {
        
        let content : String?
        let newsId : String?
        let publisherDetails : PublisherDetail?
        let publishingDate : String?
        let publishingTime : String?
        let source : String?
        let timestamp : String?
        let title : String?
        
        enum CodingKeys: String, CodingKey {
            case content = "content"
            case newsId = "newsId"
            case publisherDetails = "publisherDetails"
            case publishingDate = "publishingDate"
            case publishingTime = "publishingTime"
            case source = "source"
            case timestamp = "timestamp"
            case title = "title"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            content = try values.decodeIfPresent(String.self, forKey: .content)
            newsId = try values.decodeIfPresent(String.self, forKey: .newsId)
            publisherDetails = try values.decodeIfPresent(PublisherDetail.self, forKey: .publisherDetails)
            publishingDate = try values.decodeIfPresent(String.self, forKey: .publishingDate)
            publishingTime = try values.decodeIfPresent(String.self, forKey: .publishingTime)
            source = try values.decodeIfPresent(String.self, forKey: .source)
            timestamp = try values.decodeIfPresent(String.self, forKey: .timestamp)
            title = try values.decodeIfPresent(String.self, forKey: .title)
        }
        struct PublisherDetail : Codable {
            
            let publisherEmail : String?
            let publisherId : String?
            let publisherName : String?
            let publisherPhone : String?
            let publisherRole : String?
            
            enum CodingKeys: String, CodingKey {
                case publisherEmail = "publisherEmail"
                case publisherId = "publisherId"
                case publisherName = "publisherName"
                case publisherPhone = "publisherPhone"
                case publisherRole = "publisherRole"
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                publisherEmail = try values.decodeIfPresent(String.self, forKey: .publisherEmail)
                publisherId = try values.decodeIfPresent(String.self, forKey: .publisherId)
                publisherName = try values.decodeIfPresent(String.self, forKey: .publisherName)
                publisherPhone = try values.decodeIfPresent(String.self, forKey: .publisherPhone)
                publisherRole = try values.decodeIfPresent(String.self, forKey: .publisherRole)
            }
            
        }

    }
}




struct StudentHierarchyCodable : Codable {
    let contact : String?
    let email : String?
    let message : String?
    let name : String?
    let post : String?
    let result : String?
    let subject : String?
    enum CodingKeys: String, CodingKey {
        case contact = "Contact"
        case email = "Email"
        case message = "Message"
        case name = "Name"
        case post = "Post"
        case result = "Result"
        case subject = "Subject"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        contact = try values.decodeIfPresent(String.self, forKey: .contact)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        post = try values.decodeIfPresent(String.self, forKey: .post)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        subject = try values.decodeIfPresent(String.self, forKey: .subject)
    }
}
struct EventCalendarCodable : Codable {
    let caption : String?
    let department : String?
    let desc : String?
    let end : String?
    let eventType : String?
    let message : String?
    let result : String?
    let start : String?
    let venue : String?
    enum CodingKeys: String, CodingKey {
        case caption = "Caption"
        case department = "Department"
        case desc = "Desc"
        case end = "End"
        case eventType = "EventType"
        case message = "Message"
        case result = "Result"
        case start = "Start"
        case venue = "Venue"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        caption = try values.decodeIfPresent(String.self, forKey: .caption)
        department = try values.decodeIfPresent(String.self, forKey: .department)
        desc = try values.decodeIfPresent(String.self, forKey: .desc)
        end = try values.decodeIfPresent(String.self, forKey: .end)
        eventType = try values.decodeIfPresent(String.self, forKey: .eventType)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        start = try values.decodeIfPresent(String.self, forKey: .start)
        venue = try values.decodeIfPresent(String.self, forKey: .venue)
    }
}
struct NotificationsServiceCodable : Codable {
    let message : String?
    let result : String?
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case result = "result"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
    }
}
