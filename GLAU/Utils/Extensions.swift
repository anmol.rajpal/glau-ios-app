//
//  Extensions.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 22/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
extension UIFont {
    func withTraits(_ traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
//        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))!
        return UIFont(descriptor: descriptor, size: 0)
    }
    
    func expanded() -> UIFont {
        return withTraits(.traitExpanded)
    }
    func condensed() -> UIFont {
        return withTraits(.traitCondensed)
    }
    func bold() -> UIFont {
        return withTraits(.traitBold)
    }
    
    func italic() -> UIFont {
        return withTraits(.traitItalic)
    }
    
    func boldItalic() -> UIFont {
        return withTraits(.traitBold, .traitItalic)
    }
    
}
extension UITableView {
    func reloadDataWithLayout() {
        self.reloadData()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.reloadData()
    }
    func reloadAndScrollToTop() {
        self.reloadData()
        self.layoutIfNeeded()
//        self.contentOffset = CGPoint(x: 0, y: -self.contentInset.top)
        self.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
    }
    func scrollToTop() {
        self.contentOffset = .zero
    }
    func scrollToBottom(animated: Bool) {
//        let y = contentSize.height - frame.size.height + contentInset.bottom
        setContentOffset(CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude), animated: animated)
    }
}
extension UICollectionView {
    func reloadDataWithLayout() {
        self.reloadData()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.reloadData()
    }
}

extension Date {
    func getCurrentDate() -> String {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let currentDateStr = dateFormatter.string(from: currentDate)
        return currentDateStr
    }
    /// Returns a Date with the specified amount of components added to the one it is called with
    func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date? {
        let components = DateComponents(year: years, month: months, day: days, hour: hours, minute: minutes, second: seconds)
        return Calendar.current.date(byAdding: components, to: self)
    }
    
    /// Returns a Date with the specified amount of components subtracted from the one it is called with
    func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date? {
        return add(years: -years, months: -months, days: -days, hours: -hours, minutes: -minutes, seconds: -seconds)
    }
}
extension String {
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}
extension NSObject {
    /// Date Format type
    enum DateFormatType: String {
        /// Time
        case time = "HH:mm:ss"
        
        /// Date with hours
        case dateWithTime = "yyyy-MM-dd HH:mm:ss"
        case dateWithTimeType1 = "dd/MM/yyyy hh:mm:ss"
        /// Date
        case date = "dd/MM/yyyy"
        case dateType1 = "dd MMM, yyyy"
        case dateType2 = "MMM d"
        case MMMMdEEEE = "MMMM d EEEE"
    }
    /// Convert String to Date
    func convertToDate(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatType.date.rawValue // Your date format
        let serverDate: Date = dateFormatter.date(from: dateString)! // according to date format your date string
        return serverDate
    }
    func getDateFromString(dateString:String?, dateFormat:DateFormatType) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.rawValue
        let date:Date? = dateFormatter.date(from: dateString!)
        return date
    }
    func getStringFromDate(date:Date, dateFormat:DateFormatType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.rawValue
        let dateString:String = dateFormatter.string(from: date)
        return dateString
    }
}
extension User {
    func clearUserData() {
        User.rollNo = nil
        User.name = nil
        User.course = nil
        User.semester = nil
        User.status = nil
        User.resident = nil
    }
}
extension SFSafariViewController {
    static func setupSafariVC(_ url:URL) -> SFSafariViewController {
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.preferredControlTintColor = UIColor.white
        safariViewController.preferredBarTintColor = UIColor.glauSafariViewControllerThemeColor
//        safariViewController.extendedLayoutIncludesOpaqueBars = true
        return(safariViewController)
    }
}
extension MFMailComposeViewController {
    
    static public func setupMailComposer(subject:String, body:String, recipients:String..., delegate:MFMailComposeViewControllerDelegate, controller:UIViewController) {
        guard MFMailComposeViewController.canSendMail() else {
            showSendMailErrorAlert(controller:controller)
            return
        }
        let mailComposer = configureMailComposer(subject: subject, body: body, recipients: recipients, delegate: delegate)
        mailComposer.modalPresentationStyle = .overFullScreen
        controller.present(mailComposer, animated: true, completion: nil)
    }
    
    internal static func configureMailComposer(subject:String, body:String, recipients:[String], delegate:MFMailComposeViewControllerDelegate) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = delegate
        mailComposerVC.setToRecipients(recipients)
        mailComposerVC.setSubject(subject)
        mailComposerVC.setMessageBody(body, isHTML: false)
        return mailComposerVC
    }
    
    internal static func showSendMailErrorAlert(controller:UIViewController)
    {
        UIAlertController.showAlert(alertTitle: "Could Not Send Email", message: "Your device could not send e-mail. Make sure you have an Active Internet Connection", alertActionTitle: "Ok", controller: controller)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            print("mail cancelled")
        case MFMailComposeResult.sent:
            print("mail sent")
        case MFMailComposeResult.failed:
            print("mail failed")
            UIAlertController.showAlert(alertTitle: "Could Not Send Email", message: "Fail to send your email. Make sure you have active Internet connection and setup Details correctly", alertActionTitle: "Ok", controller: self)
        case MFMailComposeResult.saved:
            print("mail saved to drafts")
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
extension UIAlertController {
    static public func showAlert(alertTitle:String, message: String, alertActionTitle:String, controller: UIViewController) {
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: alertActionTitle, style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
}
enum forDevice {
    case iPhoneX
    case normal
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func checkReachability() {
        NetworkManager.shared.reachability.whenUnreachable = { _ in
            self.showOfflineViewController()
        }
    }
    internal func showOfflineViewController() {
        let ovc = OfflineViewController()
        ovc.modalPresentationStyle = .overFullScreen
        DispatchQueue.main.async {
            self.present(ovc, animated: false, completion: nil)
            //            self.navigationController?.pushViewController(ovc, animated: true)
            //            self.navigationController?.popToViewController(ovc, animated: true)
        }
    }
    func setupNavBar() {
        setUpNavBarForDevice(.normal)
//        setUpNavBarTitle()
    }
    func setUpNavBarTitle() {
        switch tabBarController?.selectedIndex {
        case 0: navigationItem.title = "Home"
//        case 1: navigationItem.title = "Events"
        case 1: navigationItem.title = "Academics"
        case 2: navigationItem.title = "Preferences"
//        case 3: navigationItem.title = "Academics"
        default: break
        }
    }
    func setUpNavBarTitleAttributes(_ titleColor:UIColor,_ largeTitleColor:UIColor,_ largeTitleCheck:Bool) {
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: titleColor]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: largeTitleColor]
        navigationController?.navigationBar.prefersLargeTitles = largeTitleCheck
    }
    func setUpNavBarForDevice(_ device:forDevice) {
        switch device {
        case .iPhoneX:
            setUpNavBarTitleAttributes(.white, .white, true)
            //        navigationController?.navigationBar.backgroundColor = .glauThemeColor
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.barTintColor = UIColor.glauThemeColor
            navigationController?.navigationBar.tintColor = UIColor.white
            self.extendedLayoutIncludesOpaqueBars = true
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.compact)
            navigationController?.navigationBar.shadowImage = UIImage()
        default:
            setUpNavBarTitleAttributes(.black, .black, true)
            navigationController?.navigationBar.isHidden = false
            self.extendedLayoutIncludesOpaqueBars = true
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.compact)
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.tintColor = UIColor.glauThemeColor
        }
    }
}
extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
//        return topViewController?.preferredStatusBarStyle ?? .default
        if let topVC = viewControllers.last {
            return topVC.preferredStatusBarStyle
        }
        return .default
    }
}
extension UIColor {
    static let mainTextBlue = UIColor.rgb(r: 7, g: 71, b: 89)
    static let highlightColor = UIColor.rgb(r: 50, g: 199, b: 242)
    static let glauThemeColor = UIColor.rgb(r: 57, g: 90, b: 43)
    static let glasoThemeColor = UIColor.rgb(r: 0, g: 137, b: 123).withAlphaComponent(0.8)
    static let indianRedColor = UIColor.rgb(r: 205, g: 92, b: 92)
    static let crimsonRedColor = UIColor.rgb(r: 220, g: 20, b: 60)
    static let fireBrickRedColor = UIColor.rgb(r: 178, g: 34, b: 34)
    static let glauSafariViewControllerThemeColor = UIColor.rgb(r: 44, g: 95, b: 42)
    static let systemRedColor = UIColor.rgb(r: 255, g: 59, b: 48)
    static let systemGreenColor = UIColor.rgb(r: 76, g: 217, b: 100)
    static let systemYellowColor = UIColor.rgb(r: 255, g: 204, b: 0)
    static let systemOrangeColor = UIColor.rgb(r: 76, g: 149, b: 0)
    static let systemTealColor = UIColor.rgb(r: 90, g: 200, b: 250)
    static let systemBlueColor = UIColor.rgb(r: 0, g: 122, b: 255)
    static let systemPurpleColor = UIColor.rgb(r: 88, g: 86, b: 214)
    static let systemPinkColor = UIColor.rgb(r: 255, g: 45, b: 85)
    static var randomColor: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
    
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
var categories = ["Date", "Quote", "News", "Article"]
//extension HomeController:UITableViewDelegate {}
//extension HomeController:UITableViewDataSource {
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return categories[section]
//    }
//    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return categories.count
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategoryRowTableViewCell
//        return cell
//    }
//}


//extension UICollectionView {
//    func registerReusableCell<T: UICollectionViewCell>(_: T.Type) {
//        if let nib = T.nib {
//            self.register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
//        } else {
//            self.register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
//        }
//    }
//    func dequeueSupplementaryView<T: UICollectionReusableView>(kind: String, indexPath: IndexPath) -> T? {
//        return self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T
//    }
//    func registerSupplementaryView<T: UICollectionReusableView>(_ : T.Type, kind: String) {
//        if let nib = T.nib {
//            self.register(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.reuseIdentifier)
//        } else {
//            self.register(T.self, forSupplementaryViewOfKind: kind, withReuseIdentifier: T.reuseIdentifier)
//        }
//    }
//    func dequeueReusableCell<T: UICollectionViewCell>(indexPath: IndexPath) -> T? {
//        return self.dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath as IndexPath) as? T
//    }
//}




extension HomeController:UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DropdownGridMenuCell", for: indexPath) as! DropdownGridMenuCell
        
        let item = self.items[indexPath.row]
        if !item.text.isEmpty {
            cell.textLabel.text = item.text
        } else if item.attributedText.length > 0 {
            cell.textLabel.attributedText = item.attributedText
        }
        
        cell.toggleSelection(item: item)
        
        return cell
    }
}
extension HomeController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as!  DropdownGridMenuCell
        let item = self.items[indexPath.row]
        item.isSelected = !item.isSelected
        cell.toggleSelection(item: item)     
    }
}
extension HomeController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: panelView.frame.size.width / 4, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
    }
}


extension UIView {
    
    func anchorToTop(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil) {
        
        anchorWithConstantsToTop(top, left: left, bottom: bottom, right: right, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
    
    func anchorWithConstantsToTop(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0) {
        
        _ = anchor(top, left: left, bottom: bottom, right: right, topConstant: topConstant, leftConstant: leftConstant, bottomConstant: bottomConstant, rightConstant: rightConstant)
    }
    
    func anchor(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    func anchorWithoutHeightConstant(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }

        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    func anchorWithoutWH(top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        anchors.forEach({$0.isActive = true})
        return anchors
    }
}
//if UIApplication.shared.keyWindow?.traitCollection.forceTouchCapability == UIForceTouchCapability.available
//{
//    registerForPreviewing(with: self, sourceView: collectionView)
//}

//if let indexPath = collectionView.indexPathForItem(at: location), let cellAttributes = collectionView.layoutAttributesForItem(at: indexPath) {
//    previewingContext.sourceRect = cellAttributes.frame
//    return subjectDetailViewController
//}
