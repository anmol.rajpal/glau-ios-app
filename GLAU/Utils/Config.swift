//
//  config.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 27/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import Foundation
public enum ServiceType:String {
    case Authentication = "HelloWorld?"
    case StudentDetails = "GetStudentDetails?"
    case AcademicDetails = "StudentAttendenceWithMarks?"
    case TimeTable = "StudentTimeTable?"
    case AttendanceDetails = "StudentDetailAttendance?"
    case SemesterList = "course_New?"
    case SemesterResult = "result?"
    case StudentHierarchy = "StudentHierarchy?"
    case EventCalendar = "EventCalendar?"
}
struct Config {
    struct serviceConfig {
        static let timeoutInterval:TimeInterval = 16.0
        static let serviceKey = "TESTTSET"
        static let serviceType = "TEST"
        static let serviceBaseUri = "https://summer.glauniversity.in/Testing.asmx/"
        static let serviceHost = "https://summer.glauniversity.in/Testing.asmx/HelloWorld?"
        static let userAcademicsDetailsHostUrl = "https://summer.glauniversity.in/Testing.asmx/StudentAttendenceWithMarks?"
        static let serviceBaseParam = "&servicekey=\(serviceKey)&servicetype=\(serviceType)"
        static func getAuthenticationParamString(rollNo:String, password:String) -> String {
            return "rollno=\(rollNo)&password=\(password)\(serviceBaseParam)"
        }
        static func getStudentDetailsParamString(rollNo:String) -> String {
            return "rollno=\(rollNo)\(serviceBaseParam)"
        }
        static func getAcademicDetailsParamString(rollNo:String) -> String {
            return "roll=\(rollNo)\(serviceBaseParam)"
        }
        static func getAttendanceDetailsParamString(rollNo:String, subjectCode:String) -> String {
            return "roll=\(rollNo)&subcode=\(subjectCode)\(serviceBaseParam)"
        }
        static func getTimeTableParamString(rollNo:String, date:String) -> String {
            return "roll=\(rollNo)&date=\(date)\(serviceBaseParam)"
        }
        static func getSemesterListParamString(_ rollNo:String) -> String {
            return "roll=\(rollNo)\(serviceBaseParam)"
        }
        static func getSemesterResultParamString(_ rollNo:String, _ semester:String) -> String {
            return "roll=\(rollNo)&sem=\(semester)\(serviceBaseParam)"
        }
        static func getStudentHierarchyParamString(_ rollNo:String) -> String {
            return "roll=\(rollNo)\(serviceBaseParam)"
        }
        static func getEventCalendarParamString(date:String) -> String {
            return "DateTill=\(date)\(serviceBaseParam)"
        }
        static func getServiceHostUri(_ serviceType:ServiceType) -> String {
            switch serviceType {
                case .Authentication: return "\(serviceBaseUri)\(ServiceType.Authentication.rawValue)"
                case .StudentDetails: return "\(serviceBaseUri)\(ServiceType.StudentDetails.rawValue)"
                case .AcademicDetails: return "\(serviceBaseUri)\(ServiceType.AcademicDetails.rawValue)"
                case .TimeTable: return "\(serviceBaseUri)\(ServiceType.TimeTable.rawValue)"
                case .AttendanceDetails: return "\(serviceBaseUri)\(ServiceType.AttendanceDetails.rawValue)"
                case .SemesterList: return "\(serviceBaseUri)\(ServiceType.SemesterList.rawValue)"
                case .SemesterResult: return "\(serviceBaseUri)\(ServiceType.SemesterResult.rawValue)"
                case .StudentHierarchy: return "\(serviceBaseUri)\(ServiceType.StudentHierarchy.rawValue)"
                case .EventCalendar: return "\(serviceBaseUri)\(ServiceType.EventCalendar.rawValue)"
            }
        }
        struct QuoteServiceConfig {
            static let serviceBaseUri = "https://www.natovi.com/api/services/1/quotes/quoteOfTheDay"
        }
        struct NewsServiceConfig {
            static let serviceBaseUri = "https://www.natovi.com/api/services/3/news/newsOfTheDay"
        }
        struct CombinedServicesConfig {
            static let serviceBaseUri = "https://www.natovi.com/api/services/combined"
        }
        struct NotificationsServiceConfig {
            static let serviceUri = "https://www.natovi.com/api/glau/user-notifications/insert-token"
        }
    }
    struct AppConfig {
        static let authKey = "glau-app-auth-key"
    }
    static let appId = "1447024519"
    static let requiredAttendance:Float = 75.0
    static let passingMark:Int = 7
    static let googleApiKey = "AIzaSyDIkdWSI542Bzrmm6TpQgL165x2sNL5DuE"
}
