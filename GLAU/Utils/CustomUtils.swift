//
//  CustomUtils.swift
//  GLA University
//
//  Created by Anmol Rajpal on 28/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import Foundation
import UIKit
import SwiftyGif
func calculateFontForDeviceType() {
    
}
func getDeviceTypeByHeight() -> iPhoneDevices {
    let height = UIScreen.main.bounds.height
    switch height {
        case 480.0 : return .iPhone4s
        case 568.0 : return .iPhoneSE
        case 667.0 : return .iPhone8
        case 736.0 : return .iPhone8plus
        case 812.0 : return .iPhoneXS
        case 896.0 : return .iPhoneXSMax
        default: return .iPhoneXSMax
    }
}
func getHeightForDevice(device:iPhoneDevices) -> CGFloat {
    switch device {
        case .iPhone4s: return 480.0 //Iphone 3,4,SE => 3.5 inch
        case .iPhoneSE: return 568.0 //iphone 5, 5s => 4 inch
        case .iPhone8: return 667.0//iphone 6, 6s => 4.7 inch
        case .iPhone8plus: return 736.0 //iphone 6s+ 6+  => 5.5 inch
        case .iPhoneXS: return 812.0 //iphone XS, X => 5.8 inch
        case .iPhoneXSMax: return 896.0 //iphone XSMax, XR => 6.5 inch
    }
}
func getWidthForDevice(device:iPhoneDevices) -> CGFloat {
    switch device {
        case .iPhone4s: return 320.0 //Iphone 3,4,SE => 3.5 inch
        case .iPhoneSE: return 320.0 //iphone 5, 5s => 4 inch
        case .iPhone8: return 375.0//iphone 6, 6s => 4.7 inch
        case .iPhone8plus: return 414.0 //iphone 6s+ 6+  => 5.5 inch
        case .iPhoneXS: return 375.0 //iphone XS, X => 5.8 inch
        case .iPhoneXSMax: return 414.0 //iphone XSMax, XR => 6.5 inch
    }
}
func getTimeOfDay(from date:Date = Date()) -> TimeOfDay {
    let hour = Calendar.current.component(.hour, from: date)
    switch hour {
    case 5..<12: return .Morning
    case 12: return .Noon
    case 13..<17: return .Afternoon
    case 17..<20: return .Evening
    default: return .Night
    }
}
public enum loopCount:Int {
    case infinite = -1
}
func setUpAttendanceColor(attendance:Float) -> UIColor {
    if attendance > Config.requiredAttendance {
        return UIColor.systemGreenColor
    } else if attendance < Config.requiredAttendance {
        return UIColor.systemRedColor
    } else {
        return UIColor.systemYellowColor
    }
}
func getGifImageView(_ gifName:String, loopCount:Int) -> UIImageView {
    let gif = UIImage(gifName: gifName)
    let imageView = UIImageView(gifImage: gif, manager: .defaultManager, loopCount: loopCount)
    return imageView
}
func drawLineFromPointToPoint(startX: Int, toEndingX endX: Int, startingY startY: Int, toEndingY endY: Int, ofColor lineColor: UIColor, widthOfLine lineWidth: CGFloat, inView view: UIView) {
    
    let path = UIBezierPath()
    path.move(to: CGPoint(x: startX, y: startY))
    path.addLine(to: CGPoint(x: endX, y: endY))
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.path = path.cgPath
    shapeLayer.strokeColor = lineColor.cgColor
    shapeLayer.lineWidth = lineWidth
    
    view.layer.addSublayer(shapeLayer)
    
}
func getAttendanceStatus(from info:String?) -> AttendanceStatus? {
    if info?.contains(AttendanceStatus.Present.rawValue) ?? false {
        return AttendanceStatus.Present
    } else if info?.contains(AttendanceStatus.Absent.rawValue) ?? false {
        return AttendanceStatus.Absent
    } else if info?.contains(AttendanceStatus.Leave.rawValue) ?? false {
        return AttendanceStatus.Leave
    }
    return nil
}
//func getSectionDetails(from section:String) -> (section:String, rollNo:String) {
//    let result = section.split(separator: " ")
//    let groups = result.filter { $0 != "(" && $0 != ")" }
//    let s = String(groups[0])
//    let r = String(groups[1])
//    return (s, r)
//}
func getSectionDetails(from section:String?) -> (section:String?, rollNo:String?) {
    let result = section?.split(separator: " ")
    let groups = result?.filter { $0 != "(" && $0 != ")" }
    guard groups?.count == 2 else {
        return (section, nil)
    }
    let sec = String(groups?[0] ?? "")
    let rNo = String(groups?[1] ?? "")
    return (sec, rNo)
}
class HorizontalLine:UIView {
    init(color: UIColor) {
        super.init(frame: CGRect(x: 100, y: 100, width: 1, height: 1))
        self.backgroundColor = color
        self.layer.opacity = 0.5
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder hasn't been implemented.")
    }
}
class VerticalLine:UIView {
    init(color: UIColor) {
        super.init(frame: CGRect(x: 100, y: 100, width: 1, height: 1))
        self.backgroundColor = color
        self.layer.opacity = 0.5
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder hasn't been implemented.")
    }
}
class InsetLabel:UILabel {
    var topInset:CGFloat!
    var bottomInset:CGFloat!
    var leftInset:CGFloat!
    var rightInset:CGFloat!
    init(_ topInset:CGFloat, _ bottomInset:CGFloat, _ leftInset:CGFloat, _ rightInset:CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.topInset = topInset
        self.bottomInset = bottomInset
        self.leftInset = leftInset
        self.rightInset = rightInset
    }
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)))
    }
    override public var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
