//
//  Enums.swift
//  GLA University
//
//  Created by Anmol Rajpal on 10/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import Foundation
public enum CustomFonts:String {
    case Futura
    case FuturaCondensedMedium = "Futura-CondensedMedium"
    case BodoniBook = "BodoniSvtyTwoITCTT-Book"
    case Bodoni
}
public enum TimeOfDay {
    case Morning
    case Noon
    case Afternoon
    case Evening
    case Night
}
public enum AttendanceStatus:String {
    case Present = "P"
    case Absent = "A"
    case Leave = "L"
}
public enum SubjectType:String {
    case theory
    case practical
}
public enum ResultType:String {
    case Success
    case Failure
}
public enum UserType:String {
    case Student
    case Faculty
    case Parent
}
public enum httpMethod:String {
    case GET
    case POST
    case PUT
}
public enum ServiceError:Error {
    case Unknown
    case FailedRequest
    case InvalidResponse
}
public enum ServiceState:String {
    case Disabled
    case Maintenance
    case Normal
}
public enum iPhoneDevices {
    case iPhone4s
    case iPhoneSE
    case iPhone8
    case iPhone8plus
    case iPhoneXS
    case iPhoneXSMax
}

public enum iPadDevices {
    case iPad
    case iPadAir
}
public enum Header {
    enum headerName:String {
        case contentType = "Content-Type"
        case accept = "Accept"
    }
    enum contentType:String {
        case json = "application/json"
        case xml = "application/xml"
        case urlEncoded = "application/x-www-form-urlencoded"
    }
    enum accept:String {
        case json = "application/json"
        case jsonFormatted = "application/json;indent=2"
        case xml = "application/xml"
    }
}
