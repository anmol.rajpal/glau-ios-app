//
//  OfflineViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 10/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
import Foundation
class OfflineViewController: UIViewController {
    let network = NetworkManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        setUpConstraints()
        reCheckReachability()
    }
    let window = UIApplication.shared.keyWindow
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reCheckReachability()
    }
    func reCheckReachability() {
        network.reachability.whenReachable = { _ in
            self.showTabBarController()
        }
    }
    let tbc = TabBarController()
    func showTabBarController() {
//        DispatchQueue.main.async {
//        let ofc = OfflineViewController()
//        self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: false, completion: nil)
//            self.window?.rootViewController = self.tbc
//            self.show(self.tbc, sender: self)
//        }
    }
    let offlineImageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "no-wifi-100"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    let infoLabel:UILabel = {
        let label = UILabel()
        label.text = "You are Offline, Connect to the Internet"
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    func setUpViews() {
        view.backgroundColor = .white
        view.addSubview(offlineImageView)
        view.addSubview(infoLabel)
    }
    func setUpConstraints() {
        offlineImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = offlineImageView.anchor(nil, left: nil, bottom: view.centerYAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
        _ = infoLabel.anchorWithoutWH(top: offlineImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
        
    }
}
