//
//  Item.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
public class MenuItemButton {
    public var text:String
    public var image:UIImage
    
    public init(text: String, image: UIImage) {
        self.text = text
        self.image = image
        self.image = image.withRenderingMode(.alwaysTemplate)
    }
}
