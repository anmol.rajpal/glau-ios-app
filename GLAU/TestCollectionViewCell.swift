//
//  TestCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class TestCollectionViewCell: UICollectionViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let testLabel:UILabel = {
        let label = UILabel()
        label.text = "Test"
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    func setUpViews() {
        contentView.addSubview(testLabel)
        contentView.backgroundColor = .red
    }
    func setUpConstraints() {
        testLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        testLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
}
