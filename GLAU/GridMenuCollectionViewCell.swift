//
//  GridMenuCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 13/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
protocol GridMenuDelegate {
    func itemPressed(cell:GridMenuCollectionViewCell, indexPath:IndexPath)
}
class GridMenuCollectionViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    static let viewHeight:CGFloat = 250
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
        configure(collectionView: collectionView)
    }
    var delegate:GridMenuDelegate?
    let items: [DropdownGridMenuItem] = [
        
        DropdownGridMenuItem(text: "Map", image: UIImage(named: "map")!, selected: false),
        DropdownGridMenuItem(text: "Directory", image: UIImage(named: "directory")!, selected: false),
        DropdownGridMenuItem(text: "Resources", image: UIImage(named: "resources")!, selected: false),
        DropdownGridMenuItem(text: "Emergency", image: UIImage(named: "emergency")!, selected: false),
        DropdownGridMenuItem(text: "Dining", image: UIImage(named: "dining")!, selected: false),
        DropdownGridMenuItem(text: "Libraries", image: UIImage(named: "libraries")!, selected: false),
        DropdownGridMenuItem(text: "Ask", image: UIImage(named: "ask")!, selected: false),
        DropdownGridMenuItem(text: "Calendar", image: UIImage(named: "calendar")!, selected: false)
    ]
    let menuItems: [MenuItemButton] = [
        MenuItemButton(text: "Map", image: UIImage(named: "map")!),
        MenuItemButton(text: "Directory", image: UIImage(named: "directory")!),
        MenuItemButton(text: "Resources", image: UIImage(named: "resources")!),
        MenuItemButton(text: "Emergency", image: UIImage(named: "emergency")!),
        MenuItemButton(text: "Dining", image: UIImage(named: "dining")!),
        MenuItemButton(text: "Libraries", image: UIImage(named: "libraries")!),
        MenuItemButton(text: "Ask", image: UIImage(named: "ask")!),
        MenuItemButton(text: "Calendar", image: UIImage(named: "calendar")!)
    ]
    let imageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "blabla"))
        imageView.frame = CGRect.zero
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
//        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let view = UIVisualEffectView(effect: blurEffect)
        view.layer.cornerRadius = 15
        //        view.frame = self.view.bounds
        view.clipsToBounds = true
//        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = true
        cv.contentInsetAdjustmentBehavior = .always
        cv.clipsToBounds = true
        cv.alwaysBounceVertical = true
        
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.isHidden = false
        return cv
    }()
    
    
    func setUpViews() {
        contentView.addSubview(imageView)
        contentView.addSubview(collectionView)
        imageView.addSubview(blurredEffectView)
        blurredEffectView.frame = self.imageView.bounds
//        collectionView.frame = self.blurredEffectView.bounds
        //        view.addSubview(collectionView)
    }
    func setUpConstraints() {
        _ = imageView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = collectionView.anchor(imageView.topAnchor, left: imageView.leftAnchor, bottom: imageView.bottomAnchor, right: imageView.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        //        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        //        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //        _ = collectionView.anchorWithoutHeightConstant(nil, left: view.leftAnchor, bottom: panelView.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0)
        //        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    internal func configure(collectionView:UICollectionView) {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(DropdownGridMenuCell.self, forCellWithReuseIdentifier: NSStringFromClass(DropdownGridMenuCell.self))
        collectionView.register(MenuCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(MenuCollectionViewCell.self))
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(MenuCollectionViewCell.self), for: indexPath) as! MenuCollectionViewCell
        
        let item = self.menuItems[indexPath.row]
        cell.configureButton(item: item)
//        cell.menuButton.addTarget(self, action: #selector(potty), for: UIControl.Event.touchUpInside)
        cell.menuButton.addAction(for: UIControl.Event.touchUpInside) {
            self.delegate?.itemPressed(cell: self, indexPath: indexPath)
        }
//        if !item.text.isEmpty {
//            cell.textLabel.text = item.text
//        } else if item.attributedText.length > 0 {
//            cell.textLabel.attributedText = item.attributedText
//        }
//
//        cell.toggleSelection(item: item)
        
        return cell
    }
    @objc func potty() {
//        delegate?.itemPressed(cell: self, indexpath)
    }
    @objc func showMapViewController() {
        
    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as!  MenuCollectionViewCell
//        let item = self.items[indexPath.row]
//        item.isSelected = !item.isSelected
////        cell.toggleSelection(item: item)
//        delegate?.itemPressed(cell: self)
////        UIView.animate(withDuration: 1.0) {
////            item.isSelected = false
////            cell.toggleSelection(item: item)
////        }
////        UIView.animate(withDuration: 1.0, delay: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
////            item.isSelected = !item.isSelected
////            cell.toggleSelection(item: item)
////        }) { (true) in
////            item.isSelected = !item.isSelected
////            cell.toggleSelection(item: item)
////        }
//    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (imageView.frame.size.width/3)-5, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    }
    

}
class ClosureSleeve {
    let closure: ()->()
    
    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }
    
    @objc func invoke () {
        closure()
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControl.Event, _ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, String(format: "[%d]", arc4random()), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}
