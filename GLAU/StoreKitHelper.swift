//
//  StoreKitHelper.swift
//  GLAU
//
//  Created by Anmol Rajpal on 01/03/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import Foundation
import StoreKit
struct StoreKitHelper {
    static func incrementAppOpenedCount() { // called from appdelegate didfinishLaunchingWithOptions:
        guard var appLaunchCount = UserDefaults.standard.appLaunchCount else {
            UserDefaults.standard.appLaunchCount = 1
            return
        }
        appLaunchCount += 1
        UserDefaults.standard.appLaunchCount = appLaunchCount
    }
    static func checkAndAskForReview() { // call this whenever appropriate
        // this will not be shown everytime. Apple has some internal logic on how to show this.
        guard let appLaunchCount = UserDefaults.standard.appLaunchCount else {
            UserDefaults.standard.appLaunchCount = 1
            return
        }
        switch appLaunchCount {
        case 10,50:
            StoreKitHelper().requestReview()
        case _ where appLaunchCount%100 == 0 :
            StoreKitHelper().requestReview()
        default: break;
        }
        
    }
    fileprivate func requestReview() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            // Fallback on earlier versions
            // Try any other 3rd party or manual method here.
        }
    }
}
