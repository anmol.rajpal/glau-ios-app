//
//  NewsTableViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 10/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    let newsReusableCell = "NewsCollectionViewCell"
    override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }
    func setupViews() {
        let collectionView : UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
            cv.translatesAutoresizingMaskIntoConstraints = false
            cv.delegate = self
            cv.dataSource = self
            cv.clipsToBounds = true
            cv.showsHorizontalScrollIndicator = false
            cv.showsVerticalScrollIndicator = false
            cv.backgroundColor = .clear
            cv.isHidden = false
            cv.isPagingEnabled = true
            cv.register(NewsCollectionViewCell.self, forCellWithReuseIdentifier: newsReusableCell)
            return cv
        }()
        contentView.addSubview(collectionView)
        _ = collectionView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        collectionView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }

}
extension NewsTableViewCell : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: newsReusableCell, for: indexPath) as! NewsCollectionViewCell
//        _ = cell.contentView.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor).isActive = true
        return cell
    }
}
extension NewsTableViewCell : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsInView:CGFloat = 1
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsInView) - (3 * hardCodedPadding)
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}
