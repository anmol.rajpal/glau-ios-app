//
//  newsCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 08/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
    }

    func setUpViews() {
        let cellView:UIView = {
            let view = UIView(frame: CGRect(x: 15, y: 0, width: bounds.width - 15, height: bounds.height))
            view.backgroundColor = .red
            view.layer.cornerRadius = 15
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 1
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowRadius = 2
//            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        let newsLabel:UILabel = {
            let label = UILabel()
            label.textColor = .white
            label.text = "This is my first news."
            label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            label.lineBreakMode = .byWordWrapping
            label.numberOfLines = 0
            return label
        }()
        let sourceLabel:UILabel = {
            let label = UILabel()
            label.text = "- "+"Anmol Rajpal"
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
            label.numberOfLines = 1
            return label
        }()
        cellView.addSubview(newsLabel)
        cellView.addSubview(sourceLabel)
        contentView.addSubview(cellView)
//        _ = cellView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        cellView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
//        _ = newsLabel.anchor(cellView.topAnchor, left: cellView.leftAnchor, bottom: nil, right: cellView.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        _ = sourceLabel.anchor(nil, left: nil, bottom: cellView.bottomAnchor, right: cellView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 15, widthConstant: 0, heightConstant: 0)
    }
}
