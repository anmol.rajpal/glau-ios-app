//
//  viewCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 08/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//
import UIKit
class HomeCollectionViewCell: BaseRoundedCardCell {
    var homeViewController : HomeViewController!
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpCellView()
    }
    func setUpCellView() {
        let frame = CGRect(x: BaseRoundedCardCell.kInnerMargin, y: BaseRoundedCardCell.kInnerMargin, width: bounds.width - (2 * BaseRoundedCardCell.kInnerMargin), height: BaseRoundedCardCell.cellHeight - (2 * BaseRoundedCardCell.kInnerMargin))
        let cell = CellView(frame: frame)
        cell.homeViewController = self.homeViewController
//        let cell = CellViewController(frame: frame)
        contentView.addSubview(cell)
    }
}
