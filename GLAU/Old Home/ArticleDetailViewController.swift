//
//  ArticleDetailViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 11/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class ArticleDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        self.view.backgroundColor = .white
        setUpViews()
        setUpConstraints()
        view.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpViews() {
        articleContainerView.addSubview(titleLabel)
        articleContainerView.addSubview(contentLabel)
        articleContainerView.addSubview(sourceLabel)
        articleContainerView.addSubview(authorLabel)
        view.addSubview(articleContainerView)
    }
    let articleContainerView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.backgroundColor = .purple
        view.layer.cornerRadius = 15
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 3
        return view
    }()
    let titleLabel:UILabel = {
        let label = UILabel()
        label.text = "This is the title."
        label.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()
    let contentLabel:UILabel = {
        let label = UILabel()
        label.text = "sdasd sdf s df sd f sadf a f sdf as dfasdf asa df asf sadf  asdf sda fa sdf as df asdf as dfa sdf sdaf as df asdf as df  fads fa sd asg ds f asdfasdfasdf asdf asd fsad fas df as fasf"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .light)
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 2
        return label
    }()
    let authorLabel:UILabel = {
        let label = UILabel()
        label.text = "- "+"Anmol Rajpal"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        return label
    }()
    let sourceLabel:UILabel = {
        let label = UILabel()
        label.text = "Source: TOI"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        return label
    }()
    func setUpConstraints() {
        _ = articleContainerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        articleContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = titleLabel.anchor(articleContainerView.topAnchor, left: articleContainerView.leftAnchor, bottom: nil, right: articleContainerView.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = contentLabel.anchor(titleLabel.bottomAnchor, left: articleContainerView.leftAnchor, bottom: nil, right: articleContainerView.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = sourceLabel.anchor(nil, left: articleContainerView.leftAnchor, bottom: articleContainerView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = authorLabel.anchor(nil, left: nil, bottom: articleContainerView.bottomAnchor, right: articleContainerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 10, widthConstant: 0, heightConstant: 0)
    }
}
