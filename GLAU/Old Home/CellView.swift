//
//  CellView.swift
//  GLA University
//
//  Created by Anmol Rajpal on 08/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class CellView: UITableView {
    var homeViewController : HomeViewController!
    let articleDetailViewController = ArticleDetailViewController()
    let dateCell = "DateCell"
    let quoteCell = "QuoteCell"
    let newsCell = "NewsCell"
    let articleCell = "ArticleCell"
    let subjectsCell = "SubjectsCell"
    let riddleCell = "RiddleCell"
    let commonCell = "CommonCell"
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: .grouped)
        self.layer.cornerRadius = 15
        self.backgroundColor = .white
        self.dataSource = self
        self.delegate = self
        self.translatesAutoresizingMaskIntoConstraints = false
        //        self.rowHeight = 200
        self.allowsSelection = false
        
        self.separatorStyle = .none
        
        //        self.estimatedRowHeight = 100
        //        self.rowHeight = UITableViewAutomaticDimension
        
        self.register(CommonTableViewCell.self, forCellReuseIdentifier: commonCell)
        self.register(DateTableViewCell.self, forCellReuseIdentifier: dateCell)
        self.register(QuoteTableViewCell.self, forCellReuseIdentifier: quoteCell)
        self.register(NewsTableViewCell.self, forCellReuseIdentifier: newsCell)
        self.register(ArticleTableViewCell.self, forCellReuseIdentifier: articleCell)
        self.register(SubjectsTableViewCell.self, forCellReuseIdentifier: subjectsCell)
        self.register(RiddleTableViewCell.self, forCellReuseIdentifier: riddleCell)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension CellView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
            case 0: return 35
            case 1: return 140
            case 2: return 100
            case 3: return 140
            default: return 0
        }
    }
    
}
extension CellView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categories[section]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        print(indexPath)
        switch indexPath.section {
            case 0: let cell = tableView.dequeueReusableCell(withIdentifier: dateCell)
                    cell?.textLabel?.text = "Today"
                    cell?.textLabel?.font = UIFont.systemFont(ofSize: 30, weight: UIFont.Weight.heavy)
            
            return cell!
            case 1: let cell = tableView.dequeueReusableCell(withIdentifier: quoteCell) as! QuoteTableViewCell
            return cell
            case 2: let cell = tableView.dequeueReusableCell(withIdentifier: newsCell) as! NewsTableViewCell
            return cell
            case 3: let cell = tableView.dequeueReusableCell(withIdentifier: articleCell) as! ArticleTableViewCell
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.taps(_:)))
//            tap.delegate = homeViewController.self
            addGestureRecognizer(tap)
            return cell
            default: let cell = tableView.dequeueReusableCell(withIdentifier: commonCell)
            cell?.textLabel?.text = "LOL"
            return cell!
        }
    }

    @objc func taps(_ gestureRecognizer: UITapGestureRecognizer) {
        homeViewController.show(articleDetailViewController, sender: self)
    }
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: let homeViewController = HomeViewController()
                let articleDetailViewController = ArticleDetailViewController()
                homeViewController.show(articleDetailViewController, sender: self)
                print("Date Selected")
        case 1: let homeViewController = HomeViewController()
                let articleDetailViewController = ArticleDetailViewController()
                homeViewController.show(articleDetailViewController, sender: self)
                print("Quote Selected")
        case 2: let homeViewController = HomeViewController()
                let articleDetailViewController = ArticleDetailViewController()
                homeViewController.show(articleDetailViewController, sender: self)
                print("News Selected")
        case 3:
//                let articleDetailViewController = ArticleDetailViewController()
                let ar = ArticleDetailViewAnimationController()
                if let cell = tableView.cellForRow(at: indexPath) {
                    let tap = UITapGestureRecognizer(target: cell, action: #selector(self.taps(_:)))
//                    tap.delegate = homeViewController.self
                    addGestureRecognizer(tap)
                }
                
                
//        self.delegate = homeViewController
        
//                homeViewController.show(articleDetailViewController, sender: homeViewController)
//                homeViewController.navigationController?.pushViewController(articleDetailViewController, animated: true)
//        homeViewController.present(articleDetailViewController, animated: true, completion: nil)
//                self.homeViewController.show(articleDetailViewController, sender: self)
                print("Article selected")
        default: print("LOL")
        }
    }
 */
   
}
