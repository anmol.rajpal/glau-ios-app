//
//  TodaySectionHeader.swift
//  GLA University
//
//  Created by Anmol Rajpal on 08/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class TodaySectionHeader: UICollectionReusableView {
    internal static let viewHeight: CGFloat = 81
    
    internal static func dequeue(fromCollectionView collectionView: UICollectionView, ofKind kind: String, atIndexPath indexPath: IndexPath) -> TodaySectionHeader {
        let view: TodaySectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(TodaySectionHeader.self), for: indexPath) as! TodaySectionHeader
        return view
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
