//
//  QuoteTableViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 10/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit
import Foundation
class QuoteTableViewCell: UITableViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
    }
    func setUpViews() {
        let quoteView:UIView = {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            view.layer.cornerRadius = 15
            view.backgroundColor = UIColor.glauThemeColor
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 1
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowRadius = 2
            //        view.clipsToBounds = true
            return view
        }()
   
        let quoteLabel:UILabel = {
            let label = UILabel()
            let quotationMarkAttributes = [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 40, weight: UIFont.Weight.bold),
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            let quoteTextAttributes = [
                NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.light),
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            let quotationMarkAttributedString = NSMutableAttributedString(string: "\u{275D} ", attributes: quotationMarkAttributes)
            let quoteTextAttributedString = NSAttributedString(string: "This is the first quote. Good Luck for the rest of your day. Good day Ahead.", attributes: quoteTextAttributes)
            let quoteString = NSMutableAttributedString()
            quoteString.append(quotationMarkAttributedString)
            quoteString.append(quoteTextAttributedString)
            label.attributedText = quoteString
            label.textColor = .white
            
//            label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            label.lineBreakMode = .byWordWrapping
            label.numberOfLines = 0
            return label
        }()
        let quoterLabel:UILabel = {
            let label = UILabel()
            label.text = "- "+"Crappy Quoter"
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
            label.numberOfLines = 1
            return label
        }()
        
        quoteView.addSubview(quoteLabel)
        quoteView.addSubview(quoterLabel)
        contentView.addSubview(quoteView)
        
        _ = quoteView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 5, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        quoteView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        _ = quoteLabel.anchor(quoteView.topAnchor, left: quoteView.leftAnchor, bottom: nil, right: quoteView.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = quoterLabel.anchor(nil, left: nil, bottom: quoteView.bottomAnchor, right: quoteView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 15, widthConstant: 0, heightConstant: 0)
    }
}
