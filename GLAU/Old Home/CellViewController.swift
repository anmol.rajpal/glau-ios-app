////
////  CellViewController.swift
////  GLA University
////
////  Created by Anmol Rajpal on 11/09/18.
////  Copyright © 2018 Protomile. All rights reserved.
////
//
//import UIKit
//
//class CellViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
//    let dateCell = "DateCell"
//    let quoteCell = "QuoteCell"
//    let newsCell = "NewsCell"
//    let articleCell = "ArticleCell"
//    let subjectsCell = "SubjectsCell"
//    let riddleCell = "RiddleCell"
//    let commonCell = "CommonCell"
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//     convenience init(frame:CGRect) {
//        self.init(frame: frame)
//    }
//    /*
//    convenience init(frame: CGRect) {
//        self.init(frame:frame)
//        setUpTableView(frame)
//    }
//    */
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("Error. coder has not been implemented")
//    }
//    func setUpTableView(_ frame:CGRect) {
//        let tableView = UITableView(frame: frame, style: .grouped)
//        tableView.layer.cornerRadius = 15
//        tableView.backgroundColor = .white
//        tableView.dataSource = self
//        tableView.delegate = self
//        tableView.translatesAutoresizingMaskIntoConstraints = false
//        //        self.rowHeight = 200
//        //        self.allowsSelection = false
//        tableView.separatorStyle = .none
//        //        self.estimatedRowHeight = 100
//        //        self.rowHeight = UITableViewAutomaticDimension
//
//        tableView.register(CommonTableViewCell.self, forCellReuseIdentifier: commonCell)
//        tableView.register(DateTableViewCell.self, forCellReuseIdentifier: dateCell)
//        tableView.register(QuoteTableViewCell.self, forCellReuseIdentifier: quoteCell)
//        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: newsCell)
//        tableView.register(ArticleTableViewCell.self, forCellReuseIdentifier: articleCell)
//        tableView.register(SubjectsTableViewCell.self, forCellReuseIdentifier: subjectsCell)
//        tableView.register(RiddleTableViewCell.self, forCellReuseIdentifier: riddleCell)
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return categories.count
//    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return categories[section]
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        //        print(indexPath)
//        switch indexPath.section {
//        case 0: let cell = tableView.dequeueReusableCell(withIdentifier: dateCell)
//        cell?.textLabel?.text = "Today"
//        cell?.textLabel?.font = UIFont.systemFont(ofSize: 30, weight: UIFont.Weight.heavy)
//
//        return cell!
//        case 1: let cell = tableView.dequeueReusableCell(withIdentifier: quoteCell) as! QuoteTableViewCell
//        return cell
//        case 2: let cell = tableView.dequeueReusableCell(withIdentifier: newsCell) as! NewsTableViewCell
//        return cell
//        case 3: let cell = tableView.dequeueReusableCell(withIdentifier: articleCell) as! ArticleTableViewCell
//        return cell
//        default: let cell = tableView.dequeueReusableCell(withIdentifier: commonCell)
//        cell?.textLabel?.text = "LOL"
//        return cell!
//        }
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        switch indexPath.section {
//        case 0: let homeViewController = HomeViewController()
//        let articleDetailViewController = ArticleDetailViewController()
//        homeViewController.show(articleDetailViewController, sender: self)
//        print("Date Selected")
//        case 1: let homeViewController = HomeViewController()
//        let articleDetailViewController = ArticleDetailViewController()
//        homeViewController.show(articleDetailViewController, sender: self)
//        print("Quote Selected")
//        case 2: let homeViewController = HomeViewController()
//        let articleDetailViewController = ArticleDetailViewController()
//        homeViewController.show(articleDetailViewController, sender: self)
//        print("News Selected")
//        case 3: _ = HomeViewController()
//        let articleDetailViewController = ArticleDetailViewController()
//        self.navigationController?.pushViewController(articleDetailViewController, animated: true)
//        print("Article selected")
//        default: print("LOL")
//        }
//    }
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
//
