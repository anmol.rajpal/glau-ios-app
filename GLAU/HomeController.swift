//
//  HomeController.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 22/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    static var panelViewHeightConstraint:NSLayoutConstraint?
    static var panelViewUpdatedHeightConstraint:NSLayoutConstraint?
    static var imageViewHeightConstraint:NSLayoutConstraint?
    static var imageViewUpdatedHeightConstraint:NSLayoutConstraint?
    static var blurViewHeightConstraint:NSLayoutConstraint?
    static var blurViewUpdatedHeightConstraint:NSLayoutConstraint?
    static var collectionViewHeightConstraint: NSLayoutConstraint?
    static var collectionViewUpdatedHeightConstraint: NSLayoutConstraint?
    
    let viewTopConstant:CGFloat = 20
    let viewLeftConstant:CGFloat = 0
    let viewRightConstant:CGFloat = 0
    let viewBottomConstant:CGFloat = 0
    let viewHeightConstant:CGFloat = 200
    let updateHeightConstantMultiplier:CGFloat = 2.5
    
    var state = false
    let menuCell = "DropdownGridMenuCell"
    
    let items: [DropdownGridMenuItem] = [
        
        DropdownGridMenuItem(text: "Map", image: UIImage(named: "map")!, selected: false),
        DropdownGridMenuItem(text: "Directoy", image: UIImage(named: "directory")!, selected: false),
        DropdownGridMenuItem(text: "Resources", image: UIImage(named: "resources")!, selected: false),
        DropdownGridMenuItem(text: "Emergency", image: UIImage(named: "emergency")!, selected: false),
        DropdownGridMenuItem(text: "Dining", image: UIImage(named: "dining")!, selected: false),
        DropdownGridMenuItem(text: "Libraries", image: UIImage(named: "libraries")!, selected: false),
        DropdownGridMenuItem(text: "Ask", image: UIImage(named: "ask")!, selected: false),
        DropdownGridMenuItem(text: "Calendar", image: UIImage(named: "calendar")!, selected: false)
    ]
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async {
            self.setUpCpiLayer()
            self.setUpAttendanceLayer()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        view.addSubview(panelView)
        setupViews()
        setupConstraints()
        
        
        let feedJsonObjectStr = """
        [
            {
                "date": "060918",
                "quote":"abc",
                "news": [{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"}],
                "articleOfTheDay": {"content":"TOI", "publisher":"TOI", "source":"TOI"}
            },
            {
                "date": "050918",
                "quote":"abc",
                "news": [{"content":"def", "publisher":"t"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"}],
                "articleOfTheDay": {"content":"LOL", "publisher":"LOL", "source":"LOL"}
            },
            {
                "date": "040918",
                "quote":"abc",
                "news": [{"content":"ghi", "publisher":"u"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"}],
                "articleOfTheDay": {"content":"abc", "publisher":"s", "source":"a"}
            },
            {
                "date": "030918",
                "quote":"abc",
                "news": [{"content":"jkl", "publisher":"v"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"},{"content":"abc", "publisher":"s"}],
                "articleOfTheDay": {"content":"abc", "publisher":"s", "source":"a"}
            }
        ]
    """
        let jsonData = feedJsonObjectStr.data(using: .utf8)!
        do {
            // data we are getting from network request
            let decoder = JSONDecoder()
            _ = try decoder.decode([FeedObject].self, from: jsonData)
//            print("Date: \(response[0].date!)")
//            print("Quote: \(response[0].quote!)")
//            print("Article of the Day: Content: \(response[0].articleOfTheDay!.content!) \nPublisher: \(response[0].articleOfTheDay!.publisher!) \nSource: \(response[0].articleOfTheDay!.source!)")
//            print("News Content: \(response[0].news![0].content!) \nNews Publisher: \(response[0].news![0].publisher!)")
        } catch { print(error) }
//        print(Date().getCurrentDate())
    }
    func setupViews() {
        panelView.insertSubview(blurredEffectView, at: 1)
        panelView.insertSubview(imageView, at: 0)
        panelView.insertSubview(collectionView, at: 2)
        panelView.addSubview(greetingLabel)
        panelView.addSubview(cpiLabel)
        panelView.addSubview(attendanceLabel)
        panelView.addSubview(panelButton)
    }
    
    let panelView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.layer.cornerRadius = 15
        view.backgroundColor = UIColor.clear
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
//        view.clipsToBounds = true
        return view
    }()
    let imageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "blabla"))
        imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        return imageView
    }()
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let view = UIVisualEffectView(effect: blurEffect)
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        
        return view
    }()
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.alwaysBounceVertical = true
        cv.clipsToBounds = true
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.isHidden = false
        //        cv.bindFrameToSuperviewBounds()
        cv.register(DropdownGridMenuCell.self, forCellWithReuseIdentifier: menuCell)
        return cv
    }()
    let greetingLabel:UILabel = {
        let label = UILabel()
        label.text = "Good Evening, Anmol!"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 19, weight: .medium)
        return label
    }()
    let cpiLabel:UILabel = {
        let label = UILabel()
        label.text = "7.25"
        label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
//        label.layer.borderColor = UIColor.white.cgColor
//        label.layer.borderWidth = 1
//        label.layer.cornerRadius = 25
        return label
    }()
    let attendanceLabel:UILabel = {
        let label = UILabel()
        label.text = "87.56%"
        label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
//        label.layer.borderColor = UIColor.white.cgColor
//        label.layer.borderWidth = 1
//        label.layer.cornerRadius = 25
        return label
    }()
    let panelButton:UIButton = {
        
        let origImage = UIImage(named: "plus")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
//        button.setImage(tintedImage, for: .normal)
        button.tintColor = .white
        
        button.addTarget(self, action: #selector(expandMenu), for: .touchUpInside)
        return button
    }()
    

    @objc func expandMenu() {
        if state == false {
            enterTheStage()
            animateSubViews()
            changePanelButtonTintColor(to: .fireBrickRedColor)
            state = true
        } else {
//            self.collectionView.isHidden = true
            leaveTheStage()
            animateSubviewsToIdentity()
            changePanelButtonTintColor(to: .white)
            state = false
        }
    }
    
    
    func animateSubViews() {
        UICollectionView.animate(withDuration: 0.3) {
            HomeController.collectionViewHeightConstraint?.isActive = false
            HomeController.collectionViewUpdatedHeightConstraint?.isActive = true
            self.view.layoutIfNeeded()
        }
        UIView.animate(withDuration: 0.4) {
            HomeController.panelViewHeightConstraint?.isActive = false
            HomeController.blurViewHeightConstraint?.isActive = false
            HomeController.imageViewHeightConstraint?.isActive = false
            HomeController.panelViewUpdatedHeightConstraint?.isActive = true
            HomeController.blurViewUpdatedHeightConstraint?.isActive = true
            HomeController.imageViewUpdatedHeightConstraint?.isActive = true
            self.view.layoutIfNeeded()
        }
        UIButton.animate(withDuration: 0.2) {
            self.panelButton.transform = self.panelButton.transform.rotated(by: CGFloat(Double.pi/4))
        }
    }
    func animateSubviewsToIdentity() {
        UICollectionView.animate(withDuration: 0.3) {
            HomeController.collectionViewUpdatedHeightConstraint?.isActive = false
            HomeController.collectionViewHeightConstraint?.isActive = true
            self.view.layoutIfNeeded()
        }
        UIView.animate(withDuration: 0.4) {
            HomeController.panelViewUpdatedHeightConstraint?.isActive = false
            HomeController.blurViewUpdatedHeightConstraint?.isActive = false
            HomeController.imageViewUpdatedHeightConstraint?.isActive = false
            HomeController.panelViewHeightConstraint?.isActive = true
            HomeController.blurViewHeightConstraint?.isActive = true
            HomeController.imageViewHeightConstraint?.isActive = true
            self.view.layoutIfNeeded()
        }
        UIButton.animate(withDuration: 0.2, animations: {
            self.panelButton.transform = CGAffineTransform.identity
        })
    }
    func changePanelButtonTintColor(to:UIColor) {
        panelButton.tintColor = to
    }
    func transformItemAtIndex(_ index: Int, negative: Bool) -> CGAffineTransform {
        let maxTranslation = self.view.frame.width / 2
        let minTranslation = self.view.frame.width / 3
        var translation = maxTranslation - (maxTranslation - minTranslation) * (CGFloat(index) / CGFloat(self.items.count))
        if negative {
            translation = -translation
            return CGAffineTransform(translationX: 0, y: translation)
        } else {
            return CGAffineTransform(translationX: 0, y: -translation)
        }
    }
    func enterTheStage(_ completion: (() -> Swift.Void)? = nil) {
        for index in 0..<self.items.count {
            let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0))
            cell?.transform = transformItemAtIndex(index, negative: false)
            cell?.alpha = 0
        }
        //        self.collectionView.isHidden = false
        UIView.animate(withDuration: 0.25) {
            self.collectionView.backgroundView?.alpha = 1.0
        }
        
        for index in 0..<self.items.count {
            let delay = 0.02 * CGFloat(index)
            UIView.animate(withDuration: 0.8, delay: TimeInterval(delay), usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: UIView.AnimationOptions(rawValue: 0), animations: {
                let cell = self.collectionView.cellForItem(at: IndexPath(row: index, section: 0))
                cell?.transform = CGAffineTransform.identity
                cell?.alpha = 1.0
            }, completion: { _ in
                if index + 1 == self.items.count {
                    completion?()
                }
            })
        }
    }
    func leaveTheStage(_ completion: (() -> Swift.Void)? = nil) {
        for index in 0..<self.items.count {
            UIView.animate(withDuration: 0.3, delay: 0.02, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                let cell = self.collectionView.cellForItem(at: IndexPath(row: self.items.count - index - 1, section: 0))
                cell?.transform = self.transformItemAtIndex(index, negative: true)
                cell?.alpha = 0
                if index + 1 == self.items.count {
                    self.collectionView.backgroundView?.alpha = 0
                }
            }, completion: { _ in
                if index + 1 == self.items.count {
                    completion?()
                }
            })
        }
    }
    func setUpCpiLayer() {
        let cpiTrackLayer = CAShapeLayer()
        let cpiStrokeLayer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: cpiLabel.center, radius: 40, startAngle: -CGFloat.pi/2, endAngle: 2*CGFloat.pi, clockwise: true)
        
        cpiTrackLayer.path = circularPath.cgPath
        cpiTrackLayer.strokeColor = UIColor.lightGray.cgColor
        cpiTrackLayer.lineWidth = 5
        cpiTrackLayer.fillColor = UIColor.clear.cgColor
        panelView.layer.insertSublayer(cpiTrackLayer, at: 2)
        
        cpiStrokeLayer.path = circularPath.cgPath
        cpiStrokeLayer.strokeColor = UIColor.green.cgColor
        cpiStrokeLayer.lineWidth = 5
        cpiStrokeLayer.fillColor = UIColor.clear.cgColor
        cpiStrokeLayer.lineCap = CAShapeLayerLineCap.round
        cpiStrokeLayer.strokeEnd = 1
        panelView.layer.insertSublayer(cpiStrokeLayer, at: 3)
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 0.8
        basicAnimation.duration = 1
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        cpiStrokeLayer.add(basicAnimation, forKey: "key")
    }
    func setUpAttendanceLayer() {
        let attendanceTrackLayer:CAShapeLayer = CAShapeLayer()
        let attendanceStrokeLayer:CAShapeLayer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: attendanceLabel.center, radius: 40, startAngle: -CGFloat.pi/2, endAngle: 2*CGFloat.pi, clockwise: true)
        
        attendanceTrackLayer.path = circularPath.cgPath
        attendanceTrackLayer.strokeColor = UIColor.lightGray.cgColor
        attendanceTrackLayer.lineWidth = 5
        attendanceTrackLayer.fillColor = UIColor.clear.cgColor
        panelView.layer.insertSublayer(attendanceTrackLayer, at: 2)
        
        attendanceStrokeLayer.path = circularPath.cgPath
        attendanceStrokeLayer.strokeColor = UIColor.red.cgColor
        attendanceStrokeLayer.lineWidth = 5
        attendanceStrokeLayer.fillColor = UIColor.clear.cgColor
        attendanceStrokeLayer.lineCap = CAShapeLayerLineCap.round
        attendanceStrokeLayer.strokeEnd = 1
        panelView.layer.insertSublayer(attendanceStrokeLayer, at: 3)
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        //        basicAnimation.fromValue = -0.1
        basicAnimation.toValue = 0.9 - 0.1
        basicAnimation.duration = 1
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        attendanceStrokeLayer.add(basicAnimation, forKey: "key")
    }
    func setupConstraints() {
        HomeController.panelViewHeightConstraint = panelView.heightAnchor.constraint(equalToConstant: viewHeightConstant)
        HomeController.panelViewUpdatedHeightConstraint = panelView.heightAnchor.constraint(equalToConstant: viewHeightConstant*updateHeightConstantMultiplier)
        HomeController.imageViewHeightConstraint = imageView.heightAnchor.constraint(equalToConstant: viewHeightConstant)
        HomeController.imageViewUpdatedHeightConstraint = imageView.heightAnchor.constraint(equalToConstant: viewHeightConstant*updateHeightConstantMultiplier)
        HomeController.blurViewHeightConstraint = blurredEffectView.heightAnchor.constraint(equalToConstant: viewHeightConstant)
        HomeController.blurViewUpdatedHeightConstraint = blurredEffectView.heightAnchor.constraint(equalToConstant: viewHeightConstant*updateHeightConstantMultiplier)
        
        HomeController.collectionViewHeightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 0)
        HomeController.collectionViewUpdatedHeightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 300)
        
        
        _ = panelView.anchorWithoutHeightConstant(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: viewTopConstant, leftConstant: viewLeftConstant, bottomConstant: viewBottomConstant, rightConstant: viewRightConstant, widthConstant: 0)
        HomeController.panelViewHeightConstraint!.isActive = true
        panelView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = blurredEffectView.anchorWithoutHeightConstant(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: viewTopConstant, leftConstant: viewLeftConstant, bottomConstant: viewBottomConstant, rightConstant: viewRightConstant, widthConstant: 0)
        HomeController.blurViewHeightConstraint!.isActive = true
        blurredEffectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = imageView.anchorWithoutHeightConstant(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: viewTopConstant, leftConstant: viewLeftConstant, bottomConstant: viewBottomConstant, rightConstant: viewRightConstant, widthConstant: 0)
        HomeController.imageViewHeightConstraint!.isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        _ = collectionView.anchorWithoutHeightConstant(nil, left: view.leftAnchor, bottom: panelView.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0)
        HomeController.collectionViewHeightConstraint?.isActive = true
        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        
        _ = greetingLabel.anchor(panelView.topAnchor, left: panelView.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 50, leftConstant: 50, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cpiLabel.anchor(greetingLabel.bottomAnchor, left: panelView.leftAnchor, bottom: nil, right: nil, topConstant: 30, leftConstant: 50, bottomConstant: 0, rightConstant: 40, widthConstant: 60, heightConstant: 60)
        _ = panelButton.anchor(greetingLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        _ = attendanceLabel.anchor(greetingLabel.bottomAnchor, left: nil, bottom: nil, right: panelView.rightAnchor, topConstant: 30, leftConstant: 40, bottomConstant: 0, rightConstant: 50, widthConstant: 60, heightConstant: 60)
        
        panelButton.centerXAnchor.constraint(equalTo: panelView.centerXAnchor).isActive = true
    }
}
