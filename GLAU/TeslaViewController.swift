//
//  TeslaViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class TeslaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        setUpConstraints()
        createPanGestureRecognizer(targetView: view)
    }
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    func createPanGestureRecognizer(targetView: UIView) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        targetView.addGestureRecognizer(panGesture)
    }
    
    @objc func handleCancelEvent() {
        self.dismiss(animated: true, completion: nil)
    }
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let view = UIVisualEffectView(effect: blurEffect)
        view.clipsToBounds = true
        return view
    }()
    let cancelButton:UIButton = {
        let origImage = UIImage(named: "cancel")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = .systemPinkColor
        button.addTarget(self, action: #selector(handleCancelEvent), for: .touchUpInside)
        return button
    }()
    let gifImageView:UIImageView = {
        let imageView = getGifImageView("dogs", loopCount: loopCount.infinite.rawValue)
        imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        return imageView
    }()
    let infoView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        return view
    }()
    let oopsLabel:UILabel = {
        let label = UILabel()
        label.text = "Sorry"
        label.font = UIFont.systemFont(ofSize: 30, weight: .light)
        return label
    }()
    let infoLabel:UILabel = {
        let label = UILabel()
        label.text = "Your TimeTable isn't configured yet!"
        label.textColor = UIColor.systemRedColor
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        return label
    }()
    func setUpViews() {
        view.addSubview(blurredEffectView)
        view.addSubview(cancelButton)
        view.addSubview(infoView)
        infoView.addSubview(gifImageView)
        infoView.addSubview(oopsLabel)
        infoView.addSubview(infoLabel)
        blurredEffectView.frame = self.view.bounds
    }
    func setUpConstraints() {
        _ = cancelButton.anchor(view.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 50, heightConstant: 50)
        _ = infoView.anchorWithoutWH(top: nil, left: view.leftAnchor, bottom: infoLabel.bottomAnchor, right: view.rightAnchor, topConstant: 150, leftConstant: 15, bottomConstant: -50, rightConstant: 15)
        infoView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        infoView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        _ = gifImageView.anchor(infoView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 150, heightConstant: 150)
        gifImageView.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
        _ = oopsLabel.anchor(gifImageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        oopsLabel.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
        _ = infoLabel.anchor(oopsLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        infoLabel.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
        
    }
}
