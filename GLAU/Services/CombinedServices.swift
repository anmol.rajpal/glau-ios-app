//
//  CombinedServices.swift
//  GLAU
//
//  Created by Anmol Rajpal on 14/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

final class CombinedServices: NSObject {
    static let shared = CombinedServices()
    typealias CombinedServicesFetchCompletion = (CombinedServicesCodable?, ServiceError?) -> ()
    func fetch(completion: @escaping CombinedServicesFetchCompletion) {
        let serviceHost:String = Config.serviceConfig.CombinedServicesConfig.serviceBaseUri
        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        request.httpMethod = httpMethod.POST.rawValue
        request.setValue(Header.contentType.json.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.didFetchQuoteData(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    private func didFetchQuoteData(data: Data?, response: URLResponse?, error: Error?, completion: CombinedServicesFetchCompletion) {
        if let error = error {
            print(error.localizedDescription)
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                
                processQuoteData(data: data, completion: completion)
            } else {
                
                completion(nil, .FailedRequest)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processQuoteData(data: Data, completion: CombinedServicesFetchCompletion) {
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(CombinedServicesCodable.self, from: data)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
        }
    }
}
