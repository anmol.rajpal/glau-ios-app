//
//  EventCalendarService.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
import SwiftSoup
final class EventCalendarService: NSObject {
    static let shared = EventCalendarService()
    typealias EventCalendarFetchCompletion = ([EventCalendarCodable]?, ServiceError?) -> ()
    func fetch(date:String, completion: @escaping EventCalendarFetchCompletion) {
        let serviceHost:String = Config.serviceConfig.getServiceHostUri(ServiceType.EventCalendar)
        let paramString = Config.serviceConfig.getEventCalendarParamString(date: date).data(using: String.Encoding.ascii, allowLossyConversion: false)
        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        request.httpMethod = httpMethod.POST.rawValue
        request.setValue(Header.contentType.urlEncoded.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        request.httpBody = paramString
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.didFetchEventCalendar(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    private func didFetchEventCalendar(data: Data?, response: URLResponse?, error: Error?, completion: EventCalendarFetchCompletion) {
        if let error = error {
            print(error.localizedDescription)
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processEventCalendar(data: data, completion: completion)
            } else {
                completion(nil, .FailedRequest)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processEventCalendar(data: Data, completion: EventCalendarFetchCompletion) {
        var document: Document = Document.init("")
        do {
            let dataString:String = String(data: data, encoding: .utf8)!
            document = try SwiftSoup.parse(dataString)
            let jsonObjectString: String = try document.body()!.text();
            let dataObject = Data(jsonObjectString.utf8)
            let decoder = JSONDecoder()
            let response = try decoder.decode([EventCalendarCodable].self, from: dataObject)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
        }
    }
}
