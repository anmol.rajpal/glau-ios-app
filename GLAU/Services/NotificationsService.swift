//
//  NotificationsService.swift
//  GLAU
//
//  Created by Anmol Rajpal on 23/03/19.
//  Copyright © 2019 natovi. All rights reserved.
//
import UIKit
final class NotificationsService: NSObject {
    static let shared = NotificationsService()
    typealias DeviceTokenSendCompletion = (NotificationsServiceCodable?, ServiceError?) -> ()
    func send(uuid:String, deviceToken:String, completion: @escaping DeviceTokenSendCompletion) {
        let parameters = ["uuid":uuid, "deviceToken":deviceToken, "authKey":Config.AppConfig.authKey]
        let serviceHost:String = Config.serviceConfig.NotificationsServiceConfig.serviceUri
        let url = URL(string: serviceHost)!
//        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)

        request.httpMethod = httpMethod.POST.rawValue
        request.addValue(Header.contentType.json.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        request.addValue(Header.contentType.json.rawValue, forHTTPHeaderField: Header.headerName.accept.rawValue)
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch {
            print(error.localizedDescription)
            completion(nil, .Unknown)
        }
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.validateResponseData(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    private func validateResponseData(data: Data?, response: URLResponse?, error: Error?, completion: DeviceTokenSendCompletion) {
        if let error = error {
            print(error.localizedDescription)
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processResponseData(data: data, completion: completion)
            } else {
                completion(nil, .InvalidResponse)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processResponseData(data: Data, completion: DeviceTokenSendCompletion) {
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(NotificationsServiceCodable.self, from: data)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
        }
    }
}
