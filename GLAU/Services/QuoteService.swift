//
//  QuoteService.swift
//  GLA University
//
//  Created by Anmol Rajpal on 01/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import Foundation
final class QuoteService: NSObject {
    static let shared = QuoteService()
    typealias QuoteFetchCompletion = (QuoteServiceCodable?, ServiceError?) -> ()
    func fetch(completion: @escaping QuoteFetchCompletion) {
        let serviceHost:String = Config.serviceConfig.QuoteServiceConfig.serviceBaseUri
        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        request.httpMethod = httpMethod.POST.rawValue
        request.setValue(Header.contentType.json.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.didFetchQuoteData(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    private func didFetchQuoteData(data: Data?, response: URLResponse?, error: Error?, completion: QuoteFetchCompletion) {
        if let error = error {
            print(error.localizedDescription)
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                
                processQuoteData(data: data, completion: completion)
            } else {
                
                completion(nil, .FailedRequest)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processQuoteData(data: Data, completion: QuoteFetchCompletion) {
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(QuoteServiceCodable.self, from: data)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
        }
    }
}
