//
//  UserAuthenticationService.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 26/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import Foundation
import SwiftSoup

final class UserAuthenticationService: NSObject {
    static let shared = UserAuthenticationService()
    typealias UserAuthenticationCompletion = (AuthenticationInfo?, ServiceError?) -> ()
    func fetch(_ rollNo:String,_ password:String, completion: @escaping UserAuthenticationCompletion) {
        let serviceHost:String = Config.serviceConfig.getServiceHostUri(ServiceType.Authentication)
        let paramString = Config.serviceConfig.getAuthenticationParamString(rollNo: rollNo, password: password).data(using: String.Encoding.ascii, allowLossyConversion: false)
        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        request.httpMethod = httpMethod.POST.rawValue
        request.setValue(Header.contentType.urlEncoded.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        request.httpBody = paramString
        
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.didFetchUserData(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    // MARK: - Helper Methods
    
    private func didFetchUserData(data: Data?, response: URLResponse?, error: Error?, completion: UserAuthenticationCompletion) {
        if let _ = error {
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processUserData(data: data, completion: completion)
            } else {
                completion(nil, .InvalidResponse)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processUserData(data: Data, completion: UserAuthenticationCompletion) {
        var document: Document = Document.init("")
        do {
            let dataString:String = String(data: data, encoding: .utf8)!
            document = try SwiftSoup.parse(dataString)
            let jsonObjectString: String = try document.body()!.text();
            //          print(jsonObjectString)
            let dataObject = Data(jsonObjectString.utf8)
            //            let dataObject = getDataObjectFromDataString(dataString!)
            let decoder = JSONDecoder()
            let response = try decoder.decode(AuthenticationInfo.self, from: dataObject)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
        }
    }
}
