//
//  AttendanceDetailsService.swift
//  GLA University
//
//  Created by Anmol Rajpal on 09/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
import SwiftSoup
final class AttendanceDetailsService: NSObject {
    static let shared  = AttendanceDetailsService()
    typealias AttendanceDetailsFetchCompletion = ([AttendanceDetailsCodable]?, ServiceError?) -> ()
    func fetch(rollNo:String, subjectCode:String, completion: @escaping AttendanceDetailsFetchCompletion) {
        let serviceHost:String = Config.serviceConfig.getServiceHostUri(ServiceType.AttendanceDetails)
        let paramString = Config.serviceConfig.getAttendanceDetailsParamString(rollNo: rollNo, subjectCode: subjectCode).data(using: String.Encoding.ascii, allowLossyConversion: false)
        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        request.httpMethod = httpMethod.POST.rawValue
        request.setValue(Header.contentType.urlEncoded.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        request.httpBody = paramString
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.didFetchAttendanceDetails(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    private func didFetchAttendanceDetails(data: Data?, response: URLResponse?, error: Error?, completion: AttendanceDetailsFetchCompletion) {
        if let error = error {
            print(error.localizedDescription)
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processAttendanceDetails(data: data, completion: completion)
            } else {
                completion(nil, .FailedRequest)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processAttendanceDetails(data: Data, completion: AttendanceDetailsFetchCompletion) {
        var document: Document = Document.init("")
        do {
            let dataString:String = String(data: data, encoding: .utf8)!
            document = try SwiftSoup.parse(dataString)
            let jsonObjectString: String = try document.body()!.text();
            let dataObject = Data(jsonObjectString.utf8)
            let decoder = JSONDecoder()
            let response = try decoder.decode([AttendanceDetailsCodable].self, from: dataObject)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
        }
    }
}
