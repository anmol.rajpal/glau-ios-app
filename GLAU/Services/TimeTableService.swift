//
//  StudentTimeTableService.swift
//  GLA University
//
//  Created by Anmol Rajpal on 07/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import Foundation
import SwiftSoup
final class TimeTableService:NSObject {
    static let shared  = TimeTableService()
    typealias TimeTableFetchCompletion = ([TimeTableCodable]?, ServiceError?) -> ()
    func fetch(rollNo:String, date:String, completion: @escaping TimeTableFetchCompletion) {
        let serviceHost:String = Config.serviceConfig.getServiceHostUri(ServiceType.TimeTable)
        let paramString = Config.serviceConfig.getTimeTableParamString(rollNo: rollNo, date: date).data(using: String.Encoding.ascii, allowLossyConversion: false)
        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        request.httpMethod = httpMethod.POST.rawValue
        request.setValue(Header.contentType.urlEncoded.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        request.httpBody = paramString
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.didFetchSemesterResult(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    private func didFetchSemesterResult(data: Data?, response: URLResponse?, error: Error?, completion: TimeTableFetchCompletion) {
        if let error = error {
            print(error.localizedDescription)
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processSemesterResult(data: data, completion: completion)
            } else {
                completion(nil, .FailedRequest)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processSemesterResult(data: Data, completion: TimeTableFetchCompletion) {
        var document: Document = Document.init("")
        do {
            let dataString:String = String(data: data, encoding: .utf8)!
            document = try SwiftSoup.parse(dataString)
            let jsonObjectString: String = try document.body()!.text();
            let dataObject = Data(jsonObjectString.utf8)
            let decoder = JSONDecoder()
            let response = try decoder.decode([TimeTableCodable].self, from: dataObject)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
        }
    }
}
