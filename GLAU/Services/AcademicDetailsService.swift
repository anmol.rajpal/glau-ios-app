//
//  AcademicDetailsService.swift
//  GLA University
//
//  Created by Anmol Rajpal on 10/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
import SwiftSoup
final class AcademicDetailsService: NSObject {
    static let shared = AcademicDetailsService()
    typealias AcademicDetailsFetchCompletion = ([AcademicDetails]?, ServiceError?) -> ()
    func fetchAcademicDetails(_ rollNo:String, completion: @escaping AcademicDetailsFetchCompletion) {
        let serviceHost:String = Config.serviceConfig.getServiceHostUri(ServiceType.AcademicDetails)
        let paramString = Config.serviceConfig.getAcademicDetailsParamString(rollNo: rollNo).data(using: String.Encoding.ascii, allowLossyConversion: false)
        let request = NSMutableURLRequest(url: NSURL(string:serviceHost.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.serviceConfig.timeoutInterval)
        request.httpMethod = httpMethod.POST.rawValue
        request.setValue(Header.contentType.urlEncoded.rawValue, forHTTPHeaderField: Header.headerName.contentType.rawValue)
        request.httpBody = paramString
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            self.didFetchAcademicDetails(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    private func didFetchAcademicDetails(data: Data?, response: URLResponse?, error: Error?, completion: AcademicDetailsFetchCompletion) {
        if let error = error {
            print(error.localizedDescription)
            completion(nil, .FailedRequest)
        } else if let data = data,
            let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                
                processAcademicDetails(data: data, completion: completion)
            } else {
                
                completion(nil, .FailedRequest)
            }
        } else {
            completion(nil, .Unknown)
        }
    }
    private func processAcademicDetails(data: Data, completion: AcademicDetailsFetchCompletion) {
        var document: Document = Document.init("")
        do {
            let dataString:String = String(data: data, encoding: .utf8)!
            document = try SwiftSoup.parse(dataString)
            let jsonObjectString: String = try document.body()!.text();
            //          print(jsonObjectString)
            let dataObject = Data(jsonObjectString.utf8)
            //            let dataObject = getDataObjectFromDataString(dataString!)
            let decoder = JSONDecoder()
            let response = try decoder.decode([AcademicDetails].self, from: dataObject)
//            print(response.count)
            completion(response, nil)
        } catch {
            print(error)
            completion(nil, .InvalidResponse)
            
        }
    }
}
