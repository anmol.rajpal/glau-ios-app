//
//  HomeViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 13/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class NewHomeViewController: UIViewController {
    let network = NetworkManager.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setUpViews()
        setUpConstraints()
        configure(collectionView: collectionView)
        fetchQuoteData()
        checkReachability()
//        self.extendedLayoutIncludesOpaqueBars = true
    }
    
    func reload() {
        collectionView.reloadData()
        collectionView.scrollsToTop = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchQuoteData()
        checkReachability()
    }
    /*
    func checkReachability() {
        network.reachability.whenUnreachable = { _ in
            self.showOfflineViewController()
        }
    }
    func showOfflineViewController() {
        let ovc = OfflineViewController()
        ovc.modalPresentationStyle = .overFullScreen
        DispatchQueue.main.async {
            self.present(ovc, animated: false, completion: nil)
        }
    }
 */
    var quoteData:QuoteServiceCodable?
    
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.clipsToBounds = true
        cv.alwaysBounceVertical = true
        
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = true
        cv.backgroundColor = .clear
        cv.isHidden = true
        return cv
    }()
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel() {
        messageLabel.isHidden = true
        messageLabel.text = "Unstable Internet Connection"
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        if !collectionView.isDragging {
            fetchQuoteData()
        }
    }
    
    private func updateViews() {
        let isEmpty:Bool = quoteData?.quoteOfTheDay?.quote?.isEmpty ?? true
        collectionView.isHidden = isEmpty
        messageLabel.isHidden = !isEmpty
        retryButton.isHidden = !isEmpty
        if !isEmpty { collectionView.reloadData() }
    }
    private func fetchQuoteData() {
        QuoteService.shared.fetch() { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print(error?.localizedDescription ?? "Quote Service Error")
                    self.updateViews()
                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?.result
                let message = data?.message
                
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.quoteData = data
                        
//                        print(data)
//                        print(self.quoteData ?? "nil")
//                        
//                        print(self.quoteData?.result ?? "nil")
//                        print(self.quoteData?.quoteOfTheDay?.quote ?? "nil")
                    }
                    self.updateViews()
                    self.spinner.stopAnimating()
                default: break
                }
            }
        }
    }
    func setUpViews() {
        view.addSubview(collectionView)
        view.addSubview(spinner)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = collectionView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
//        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        _ = collectionView.anchorWithoutHeightConstant(nil, left: view.leftAnchor, bottom: panelView.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0)
//        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }

}
extension NewHomeViewController: GridMenuDelegate {
    func itemPressed(cell: GridMenuCollectionViewCell, indexPath: IndexPath) {
        switch indexPath.row {
            case 0:
                let mvc = TeslaViewController()
                mvc.modalPresentationStyle = .overFullScreen
                self.present(mvc, animated: true, completion: nil)
            default:  let tesla = TeslaViewController()
                tesla.modalPresentationStyle = .overFullScreen
                self.present(tesla, animated: true, completion: nil)
        }
    }
    
//    func itemPressed(cell: GridMenuCollectionViewCell) {
////        print(cell.collectionView.indexPath(for: cell))
////        print(cell.collectionView.indexPath(for: cell as GridMenuCollectionViewCell)?.row)
//
//
//    }
}
extension NewHomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    internal func configure(collectionView:UICollectionView) {
        collectionView.register(TodaySectionHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: NSStringFromClass(TodaySectionHeaderCell.self))
        collectionView.register(ReusableHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: NSStringFromClass(ReusableHeaderCell.self))
        collectionView.register(ReusableFooterCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: NSStringFromClass(ReusableFooterCell.self))
        collectionView.register(GridMenuCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(GridMenuCollectionViewCell.self))
        collectionView.register(TestCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(TestCollectionViewCell.self))
        collectionView.register(QuoteCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(QuoteCollectionViewCell.self))
        collectionView.register(NewsViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(NewsViewCell.self))
        collectionView.register(ArticleCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(ArticleCollectionViewCell.self))
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return 0
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(GridMenuCollectionViewCell.self), for: indexPath) as! GridMenuCollectionViewCell
            cell.delegate = self
            return cell
        } else if indexPath.section == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(QuoteCollectionViewCell.self), for: indexPath) as! QuoteCollectionViewCell
            let quote = quoteData?.quoteOfTheDay?.quote ?? ""
            let quoter = quoteData?.quoteOfTheDay?.quoter ?? ""
            cell.setUpQuoteData(quote: quote, quoter: quoter)
            return cell
        } else if indexPath.section == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(NewsViewCell.self), for: indexPath) as! NewsViewCell
            cell.delegate = self
            return cell
        } else if indexPath.section == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(ArticleCollectionViewCell.self), for: indexPath) as! ArticleCollectionViewCell
//            cell.delegate = self
            return cell
        } else {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(TestCollectionViewCell.self), for: indexPath)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: view.frame.width, height: GridMenuCollectionViewCell.viewHeight)
        } else if indexPath.section == 2 {
            let quote = quoteData?.quoteOfTheDay?.quote ?? ""
            let quoter = quoteData?.quoteOfTheDay?.quoter ?? ""
            if quote.isEmpty || quoter.isEmpty {
                return CGSize.zero
            } else {
                return CGSize(width: view.frame.width, height: QuoteCollectionViewCell.viewHeight)
            }
        } else if indexPath.section == 3 {
            return CGSize(width: view.frame.width, height: NewsViewCell.viewHeight)
        } else if indexPath.section == 4 {
            return CGSize(width: view.frame.width, height: ArticleCollectionViewCell.viewHeight)
        } else {
            return CGSize(width: view.frame.width, height: 120)
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if(kind == UICollectionView.elementKindSectionHeader) {
           
            if indexPath.section == 1 {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(TodaySectionHeaderCell.self), for: indexPath) as! TodaySectionHeaderCell
                return headerView
            } else if indexPath.section == 2 {
                 let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(ReusableHeaderCell.self), for: indexPath) as! ReusableHeaderCell
                header.headerLabel.text = "headstart".capitalized
                header.descriptionLabel.text = "Quote of the day".uppercased()
                return header
            } else if indexPath.section == 3 {
                 let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(ReusableHeaderCell.self), for: indexPath) as! ReusableHeaderCell
                header.headerLabel.text = "News".capitalized
                header.descriptionLabel.text = "News of the day".uppercased()
                return header
            } else if indexPath.section == 4 {
                 let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(ReusableHeaderCell.self), for: indexPath) as! ReusableHeaderCell
                header.headerLabel.text = "Article".capitalized
                header.descriptionLabel.text = "Article of the day".uppercased()
                return header
            }
        } else {
            if indexPath.section == 1 {
                let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(ReusableFooterCell.self), for: indexPath)
                return footerView
            }
        }
        fatalError()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize.zero
        } else if section == 1 {
            return CGSize(width: view.frame.width, height: TodaySectionHeaderCell.viewHeight)
        } else if section == 2 {
            let quote = quoteData?.quoteOfTheDay?.quote ?? ""
            let quoter = quoteData?.quoteOfTheDay?.quoter ?? ""
            if quote.isEmpty || quoter.isEmpty {
                return CGSize.zero
            } else {
                return CGSize(width: view.frame.width, height: ReusableHeaderCell.viewHeight)
            }
        } else {
            return CGSize(width: view.frame.width, height: ReusableHeaderCell.viewHeight)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 1 {
            return CGSize(width: self.view.frame.width, height: ReusableFooterCell.viewHeight)
        } else {
            return CGSize.zero
        }
    }
    
}
extension NewHomeViewController:NewsCellDelegate {
    func newsPressed(cell: NewsViewCell) {
        let newsDetailVC = NewsDetailViewController()
        self.show(newsDetailVC, sender: self)
    }
}
extension NewHomeViewController:ArticleCellDelegate {
    
}
