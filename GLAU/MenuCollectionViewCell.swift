//
//  MenuCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(menuButton)
        self.backgroundColor = UIColor.clear
        self.selectedBackgroundView = UIView()
        setUpConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    static let buttonInsets:UIEdgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 5, right: 15)
    let menuButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
//        button.contentEdgeInsets = buttonInsets
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .systemPinkColor
        button.layer.cornerRadius = 7
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .light)
//        button.titleLabel?.translatesAutoresizingMaskIntoConstraints = false
//        button.imageView?.translatesAutoresizingMaskIntoConstraints = false
//        button.imageView?.clipsToBounds = true
//        button.titleEdgeInsets.bottom = -10
//        button.centerVertically()
//        button.semanticContentAttribute = UISemanticContentAttribute
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        button.clipsToBounds = true
        return button
    }()
    internal func configureButton(item:MenuItemButton) {
        menuButton.setImage(item.image, for: UIControl.State.normal)
        menuButton.setTitle(item.text, for: UIControl.State.normal)
        menuButton.centerVertically()
//        menuButton.imageView?.centerXAnchor.constraint(equalTo: (menuButton.centerXAnchor)).isActive = true
        
//        menuButton.titleLabel?.text = item.text
    }
    func setUpConstraints() {
        _ = menuButton.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 3, leftConstant: 3, bottomConstant: 3, rightConstant: 3, widthConstant: 0, heightConstant: 0)
        menuButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        menuButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
//        let metrics = ["margin": 0]
//        let vfsV = "V:|-[button]-|"
//        let vfsH = "H:|-[button]-|"
//        let views = ["button": self.menuButton] as [String : Any]
//
//        // horizontal centering
//        for (_, view) in views as [String : Any] {
//            let constraint = NSLayoutConstraint(item: self.contentView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
//
//            self.contentView.addConstraint(constraint)
//        }
//
//        self.menuButton.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: NSLayoutConstraint.Axis.vertical)
//
//        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: vfsV, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: metrics, views: views))
//        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: vfsH, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: metrics, views: views))
    }
}
extension UIButton {
    
    func centerVertically(padding: CGFloat = 7.0) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
                return
        }
        
        let totalHeight = imageViewSize.height + titleLabelSize.height + padding
        
        self.imageEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: 0.0,
            right: -titleLabelSize.width
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: padding,
            left: -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height + padding),
            right: 0.0
        )
        
        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: titleLabelSize.height,
            right: 0.0
        )
    }
    
}
