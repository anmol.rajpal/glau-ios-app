//
//  ReusableHeaderCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class ReusableHeaderCell: UICollectionViewCell {
    internal static let viewHeight: CGFloat = 65
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let headerLabel:UILabel = {
        let label = UILabel()
        label.text = "HEADER"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 22, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    let descriptionLabel:UILabel = {
        let label = UILabel()
        label.text = "DESCRIPTION"
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 14, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(headerLabel)
        contentView.addSubview(descriptionLabel)
    }
    func setUpConstraints() {
        _ = headerLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = descriptionLabel.anchor(headerLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}
