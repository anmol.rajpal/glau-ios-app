//
//  AppDelegate.swift
//  GLAU
//
//  Created by Anmol Rajpal on 27/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var shortcutItemToProcess: UIApplicationShortcutItem?
    enum NotificationType {
        case News
    }
    public enum ShortcutType: String {
        case showTimeTable = "showTimeTable"
        case showAttendance = "switchToAcademicsTab"
        case showUserProfile = "showUserProfile"
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UNUserNotificationCenter.current().delegate = self
        window = UIWindow(frame: UIScreen.main.bounds)
        StoreKitHelper.incrementAppOpenedCount()
        
        window?.rootViewController = TabBarController()
        
        window?.makeKeyAndVisible()
        if let shortcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            shortcutItemToProcess = shortcutItem
        }
        registerForPushNotifications()
        let notificationOption = launchOptions?[.remoteNotification]
        if let notification = notificationOption as? [String: AnyObject],
            let newsItem = notification["newsItem"] as? [String: AnyObject],
            let newsId = newsItem["newsId"] as? String {
            self.handleNewsNotification(newsId: newsId)
        }
        return true
    }
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable: Any],
        fetchCompletionHandler completionHandler:
        @escaping (UIBackgroundFetchResult) -> Void
        ) {
        
        if let aps = userInfo["aps"] as? [String: AnyObject],
        let newsItem = userInfo["newsItem"] as? [String: AnyObject] {
            print(aps)
            print(newsItem)
        } else {
            print("failure")
            completionHandler(.failed)
            return
        }
//        NewsItem.makeNewsItem(aps)
    }

    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        shortcutItemToProcess = shortcutItem
//        completionHandler(handleShortcutItem(withShortcutItem: shortcutItem))
    }
    
    fileprivate func isLoggedIn() -> Bool {
        return UserDefaults.standard.isLoggedIn()
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if let shortcutItem = shortcutItemToProcess {
            handleShortcut(withShortcutItem: shortcutItem)
            shortcutItemToProcess = nil
        }
    }
    func handleShortcut(withShortcutItem item: UIApplicationShortcutItem) {
        
        guard let shortcutType = item.type.components(separatedBy: ".").last else { return }
        
        if let type = ShortcutType(rawValue: shortcutType) {
            guard let tabBarController = self.window?.rootViewController as? TabBarController else { return }
            if tabBarController.isLoaded {
                tabBarController.processShortcut(type: type)
            } else {
                tabBarController.shortcutType = type
                
            }
        }
        return
    }
    func handleNotification(withNotificationType type: NotificationType) {
        guard let tabBarController = self.window?.rootViewController as? TabBarController else { return }
        if tabBarController.isLoaded {
            tabBarController.processNotification(type: type)
        } else {
            tabBarController.notificationType = type
        }
        return
    }
    func handleShortcutItem(withShortcutItem item: UIApplicationShortcutItem) -> Bool {
        
        guard let shortcutType = item.type.components(separatedBy: ".").last else { return false }
        
        if let type = ShortcutType(rawValue: shortcutType) {
            if isLoggedIn() {
                guard let tabBarController = self.window?.rootViewController as? TabBarController else {
                    return false
                }
                switch type {
                case .showAttendance:
                    tabBarController.selectedIndex = 1
                    return true
                case .showTimeTable:
                    let timeTableVC = TimeTableViewController()
                    timeTableVC.modalPresentationStyle = .overFullScreen
                    tabBarController.selectedIndex = 0
                    tabBarController.selectedViewController?.present(timeTableVC, animated: true, completion: nil)
                    return true
                case .showUserProfile:
                    let profileVC = ProfileTableViewController()
//                    let preferencesVC = PreferencesTableViewController()
                    tabBarController.selectedIndex = 2
//                    tabBarController.selectedViewController = preferencesVC
//                    preferencesVC.show(profileVC, sender: self)
                    tabBarController.selectedViewController?.show(profileVC, sender: self)
                    return true
                }
            }
        }
        return false
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
//        UIApplication.shared.applicationIconBadgeNumber = 0;
//        print("will resign active")
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        UIApplication.shared.applicationIconBadgeNumber = 0;
//        print("background")
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//        print("will enter foreground")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
//        print("Device Token: \(token)")
        self.enableRemoteNotifications()
        self.forwardTokenToServer(token: token)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error.localizedDescription)")
        self.disableRemoteNotifications()
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//        UIApplication.shared.applicationIconBadgeNumber = 0;
//        print("will terminate")
    }
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] granted, error in
                
                print("Permission granted: \(granted)")
                guard granted else { return }
                self?.getNotificationSettings()
        }
    }
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
//            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    func enableRemoteNotifications() {
        
    }
    func disableRemoteNotifications() {
        
    }
    func forwardTokenToServer(token:String) {
        guard let uuid = UIDevice.current.identifierForVendor?.uuidString else {
            print("uuid not found")
            return
        }
        NotificationsService.shared.send(uuid: uuid, deviceToken: token) { (data, error) in
            if let error = error {
                print("error")
                switch error {
                case .FailedRequest:
                    DispatchQueue.main.async {
                        print("Request Failure")
//                        UIAlertController.showAlert(alertTitle: "Request Timed Out", message: "Please try again later", alertActionTitle: "Ok", controller: self)
                    }
                case .InvalidResponse:
                    DispatchQueue.main.async {
                        print("Invalid Response")
//                        UIAlertController.showAlert(alertTitle: "Invalid Response", message: "Please try again", alertActionTitle: "Ok", controller: self)
                    }
                case .Unknown:
                    DispatchQueue.main.async {
                        print("Unknown Error Occured")
//                        UIAlertController.showAlert(alertTitle: "Some Error Occured", message: "An unknown error occured. Please try again later.", alertActionTitle: "Ok", controller: self)
                    }
                }
            }
            if let data = data,
                let result = data.result,
                let message = data.message {
                print("Result => \(result) & Message => \(message)")
                if ResultType(rawValue: result) != .Success {
                    print("diabling notification features")
                    self.disableRemoteNotifications()
                }
            }
        }
    }
    func handleNewsNotification(newsId:String?) {
        guard let tabBarController = self.window?.rootViewController as? TabBarController else { return }
        if tabBarController.isLoaded {
            tabBarController.processNewsNotification(newsId: newsId)
        } else {
            tabBarController.newsId = newsId
        }
        return
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let newsItem = userInfo["newsItem"] as? [String: AnyObject],
            let newsId = newsItem["newsId"] as? String {
            self.handleNewsNotification(newsId: newsId)
        } else {
            print("failure")
            return
        }
        
        completionHandler()
    }
}
