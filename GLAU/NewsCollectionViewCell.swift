//
//  NewsCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
protocol NewsCellDelegate {
    func newsPressed(cell:NewsViewCell)
}
class NewsViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    static let viewHeight:CGFloat = 110
    private var indexOfCellBeforeDragging = 0
    private var itemWidth:CGFloat {
        let inset = calculateSectionInset()
        return collectionView.frame.width - inset * 2
    }
    let interItemSpacing:CGFloat = 10
    func calculateSectionInset() -> CGFloat {
        return 20
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
        configure(collectionView: collectionView)
    }
    var delegate:NewsCellDelegate?
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
//        cv.translatesAutoresizingMaskIntoConstraints = false
//        cv.contentInsetAdjustmentBehavior = .always
        cv.clipsToBounds = true
        cv.isPagingEnabled = false
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.isHidden = false
        return cv
    }()
    func setUpViews() {
        contentView.addSubview(collectionView)
    }
    func setUpConstraints() {
        _ = collectionView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        //        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        //        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //        _ = collectionView.anchorWithoutHeightConstant(nil, left: view.leftAnchor, bottom: panelView.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0)
        //        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    internal func configure(collectionView:UICollectionView) {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: NSStringFromClass(NewsCell.self))
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(NewsCell.self), for: indexPath) as! NewsCell
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        handleNews()
    }
    func handleNews() {
        delegate?.newsPressed(cell: self)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interItemSpacing
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: itemWidth, height: NewsCell.viewHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = calculateSectionInset()
        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    private func indexOfMajorCell() -> Int {
        let itemWidth = self.itemWidth
        let xContentOffset = collectionView.contentOffset.x
        let proportionalOffset = xContentOffset / itemWidth
        let index = Int(round(proportionalOffset))
        let numberOfItems = collectionView.numberOfItems(inSection: 0)
        let safeIndex = max(0, min(numberOfItems - 1, index))
        return safeIndex
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //*
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        
        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        
        // calculate conditions:
        let dataSourceCount = collectionView.numberOfItems(inSection: 0)
        let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        
        if didUseSwipeToSkipCell {
            
            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = (itemWidth + 10) * CGFloat(snapToIndex)
//            print(toValue)
            // Damping equal 1 => no oscillations => decay animation:
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)
           //*/
            
        /*
        // Stop scrollView sliding:
        //        targetContentOffset.pointee = scrollView.contentOffset
        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        
        // calculate conditions:
        let dataSourceCount = collectionView.numberOfItems(inSection: 0)
        let swipeVelocityThreshold: CGFloat = 0.9 // after some trail and error
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        
        if didUseSwipeToSkipCell {
            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = (itemWidth + interItemSpacing) * CGFloat(snapToIndex)
            targetContentOffset.pointee = CGPoint(x: toValue, y: 0)
            */
        } else {
            targetContentOffset.pointee = scrollView.contentOffset
            // This is a much better way to scroll to a cell:
            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}
