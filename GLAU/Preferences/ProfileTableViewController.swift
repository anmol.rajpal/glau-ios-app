//
//  ProfileTableViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 27/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class ProfileTableViewController: UIViewController {
    
    let cellId = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        
        setUpViews()
        setUpConstraints()
//        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: nil, action: nil)
        setUpTableView()
        fetchStudentDetails()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Profile"
        view.backgroundColor = .white
        tableView.backgroundColor = .white
    }
    
    func setUpTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadDataWithLayout()
    }
    let tableView:UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.alpha = 0
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.9)
        tv.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 0.0, right: 0.0)
        tv.rowHeight = UITableView.automaticDimension
        tv.estimatedRowHeight = 44
        return tv
    }()
    var studentDetails:StudentDetailsCodable?
    func setUpViews() {
        view.addSubview(spinner)
        view.addSubview(tableView)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = tableView.anchorWithoutWH(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel() {
        messageLabel.isHidden = true
        messageLabel.text = "Unstable Internet Connection"
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        fetchStudentDetails()
    }
    
    private func updateViews() {
        let isEmpty:Bool = studentDetails?.result?.isEmpty ?? true
        if isEmpty {
            UIView.animate(withDuration: 0.2) {
                self.tableView.alpha = 0
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.tableView.alpha = 1
            }
        }
        messageLabel.isHidden = !isEmpty
        retryButton.isHidden = !isEmpty
        if !isEmpty { tableView.reloadDataWithLayout() }
    }
    var dict:[String:String] = [:]
    var keys:Array = ["Univ. Roll No.", "Name", "Email", "Father's Name", "Semester", "Section", "Class Roll No.", "Course", "Branch", "CPI", "Status", "Nature", "Resident", "Placed"]
    private func fetchStudentDetails() {
        let rollNo = UserDefaults.standard.getRollNo()
        StudentDetailsService.shared.fetch(rollNo: rollNo) { (data, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                
                guard error == nil else {
                    print(error?.localizedDescription ?? "Some Error Occured")
                    self.updateViews()
                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?.result
                let message = data?.message
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.studentDetails = data
                        let sectionStr = data.section
                        let sectionDetails = getSectionDetails(from: sectionStr)
                        let section = sectionDetails.section
                        let rollNo = sectionDetails.rollNo
                        
                        self.dict = [
                            "Univ. Roll No." : self.studentDetails?.rollNo ?? "--",
                            "Name" : self.studentDetails?.name ?? "--",
                            "Email" : self.studentDetails?.email ?? "--",
                            "Father's Name" : self.studentDetails?.fatherName ?? "--",
                            "Semester" : self.studentDetails?.semester ?? "--",
                            "Section" : section == "No" ? "--" : section ?? "--",
                            "Class Roll No.": rollNo ?? "--",
                            "Course" : self.studentDetails?.course ?? "",
                            "Branch" : self.studentDetails?.branch ?? "--",
                            "CPI" : self.studentDetails?.cPI ?? "--",
                            "Status" : self.studentDetails?.status ?? "--",
                            "Nature" : self.studentDetails?.nature ?? "--",
                            "Resident" : self.studentDetails?.resident ?? "--",
                            "Placed" : self.studentDetails?.placed ?? "--"
                        ]
                    }
                    self.updateViews()
                    self.spinner.stopAnimating()
                default: break
                }
            })
        }
    }
  
    var keyList:[String] {
        get{
            return Array(dict.keys)
        }
    }
    func create(text:String, detailText:String, cell:UITableViewCell) -> UITableViewCell {
        cell.textLabel?.text = text
        cell.detailTextLabel?.text = detailText
        return cell
    }
    
    func setCpiColor(_ cpiString:String) -> UIColor {
        let cpi = Double(cpiString)!
        if cpi > 7.5 {return UIColor.systemGreenColor}
        else if cpi > 7.0 {return UIColor.glauThemeColor}
        else if cpi > 6.5 {return UIColor.systemYellowColor}
        else if cpi > 6.0 {return UIColor.systemOrangeColor}
        else {return UIColor.systemRedColor}
    }
    
}
extension ProfileTableViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(dict.count)
        return dict.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: cellId)
        }
        
        
        cell?.detailTextLabel?.numberOfLines = 0
        cell?.detailTextLabel?.lineBreakMode = .byWordWrapping
        cell?.selectionStyle = .none
        
        // ALLOW MANUAL CONSTRAINTS
        cell?.detailTextLabel?.translatesAutoresizingMaskIntoConstraints = false
        // TOP +15, BOTTOM -15, RIGHT -15
        cell?.detailTextLabel?.topAnchor.constraint(equalTo: (cell?.contentView.topAnchor)!, constant: 10).isActive = true
        cell?.detailTextLabel?.leftAnchor.constraint(equalTo: (cell?.textLabel?.rightAnchor)!, constant: 25).isActive = true
        cell?.detailTextLabel?.bottomAnchor.constraint(equalTo: (cell?.contentView.bottomAnchor)!, constant: -10).isActive = true
        cell?.detailTextLabel?.rightAnchor.constraint(equalTo: (cell?.contentView.rightAnchor)!, constant: -15).isActive = true
        let row = indexPath.row
        //        let rowKey = keyList[row]
        //        cell?.textLabel?.text = rowKey
        //        let rowValue = dict[rowKey]
        //        cell?.detailTextLabel?.text = rowValue
        //        cell?.textLabel?.text = keys[row]
        cell?.detailTextLabel?.text = dict[keys[row]]
        
        switch row {
        case 0:
            cell?.textLabel?.text = "Univ. Roll No."
            cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        case 1:
            cell?.textLabel?.text = "Name"
            cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        //cell?.detailTextLabel?.text = studentDetails?.name
        case 2:
            cell?.textLabel?.text = "Email"
        //cell?.detailTextLabel?.text = studentDetails?.fatherName
        case 3:
            cell?.textLabel?.text = "Father's Name"
        //cell?.detailTextLabel?.text = studentDetails?.fatherName
        case 4:
            cell?.textLabel?.text = "Semester"
        //cell?.detailTextLabel?.text = studentDetails?.semester
        case 5:
            cell?.textLabel?.text = "Section"
        //cell?.detailTextLabel?.text = studentDetails?.section
        case 6:
            cell?.textLabel?.text = "Class Roll No."
        case 7:
            cell?.textLabel?.text = "Course"
        //cell?.detailTextLabel?.text = studentDetails?.course
        case 8:
            cell?.textLabel?.text = "Branch"
            cell?.detailTextLabel?.text = studentDetails?.branch?.isEmpty ?? true ? "--" : studentDetails?.branch
        case 9:
            cell?.textLabel?.text = "CPI"
            //            let cpi = studentDetails?.cPI ?? "0"
            cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
            //cell?.detailTextLabel?.text = cpi
        //cell?.detailTextLabel?.textColor = setCpiColor(cpi)
        case 10:
            cell?.textLabel?.text = "Status"
        //cell?.detailTextLabel?.text = studentDetails?.status
        case 11:
            cell?.textLabel?.text = "Nature"
        //cell?.detailTextLabel?.text = studentDetails?.nature
        case 12:
            cell?.textLabel?.text = "Resident"
        //cell?.detailTextLabel?.text = studentDetails?.resident
        case 13:
            cell?.textLabel?.text = "Placed"
            //cell?.detailTextLabel?.text = studentDetails?.placed
            if studentDetails?.placed == "No" {
                cell?.detailTextLabel?.textColor = UIColor.systemRedColor
            } else {
                cell?.detailTextLabel?.textColor = UIColor.systemGreenColor
            }
        default: break
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
