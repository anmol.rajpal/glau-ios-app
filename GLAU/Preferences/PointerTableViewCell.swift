//
//  PointerTableViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 03/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class PointerTableViewCell: UITableViewCell {
    var delegate:PointerInfoCellDelegate?
    var cpi:Float? {
        didSet {
            setUpAttributedCpi(cpi: cpi ?? 0)
//            animateCpiShapeLayer(cpi: cpi ?? 0)
            setupCpiLayers(cpi: cpi ?? 0)
        }
    }
    var spi:Float? {
        didSet {
            setUpAttributedSpi(spi: spi ?? 0)
//            animateSpiShapeLayer(spi: spi ?? 0)
            setupSpiLayers(spi: spi ?? 0)
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpLayers()
        setUpConstraints()
//        setupCircleLayers()
        animateView()
        setupNotificationObservers()
    }
    func animateView() {
        delegate?.animateCpi(cell: self)
        delegate?.animateSpi(cell: self)
    }
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    @objc private func handleEnterForeground() {
        animatePulsatingLayer()
    }
    lazy var cpiLabel:UILabel = {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.sizeToFit()
        label.textColor = .black
        label.textAlignment = .center
//        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
//    let cpiTitleLabel:UILabel = {
//        let label = UILabel()
//        label.text = "cpi".uppercased()
//        label.font = UIFont.systemFont(ofSize: 13, weight: .heavy)
//        label.textColor = .darkGray
//        label.textAlignment = .center
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
    let spiLabel:UILabel = {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
//        label.numberOfLines = 1
        label.sizeToFit()
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
//        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
//    let spiTitleLabel:UILabel = {
//        let label = UILabel()
//        label.text = "spi".uppercased()
//        label.font = UIFont.systemFont(ofSize: 13, weight: .heavy)
//        label.textColor = .darkGray
//        label.textAlignment = .center
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
    let cpiContainerView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let spiContainerView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var cpiPulsatingLayer:CAShapeLayer = {
        let layer = createCpiCircleShapeLayer(strokeColor: .clear, fillColor: UIColor.systemGreenColor.withAlphaComponent(0.2))
        return layer
    }()
    lazy var cpiTrackLayer:CAShapeLayer = {
        let layer = createCpiCircleShapeLayer(strokeColor: UIColor.systemGreenColor.withAlphaComponent(0.2), fillColor: .clear)
        return layer
    }()
    lazy var cpiShapeLayer:CAShapeLayer = {
        let layer = createCpiCircleShapeLayer(strokeColor: UIColor.systemGreenColor, fillColor: .clear)
        layer.strokeEnd = 0.0
        return layer
    }()
    lazy var spiPulsatingLayer:CAShapeLayer = {
        let layer = createSpiCircleShapeLayer(strokeColor: .clear, fillColor: UIColor.systemGreenColor.withAlphaComponent(0.2))
        return layer
    }()
    lazy var spiTrackLayer:CAShapeLayer = {
        let layer = createSpiCircleShapeLayer(strokeColor: UIColor.systemGreenColor.withAlphaComponent(0.2), fillColor: .clear)
        return layer
    }()
    lazy var spiShapeLayer:CAShapeLayer = {
        let layer = createSpiCircleShapeLayer(strokeColor: UIColor.systemGreenColor, fillColor: .clear)
        layer.strokeEnd = 0.0
        return layer
    }()
    internal func setUpAttributedCpi(cpi:Float) {
        let cpiString = String(format:"%.2f", cpi)
        let group = cpiString.split(separator: ".")
        let valueString = group[0]
        let precisionString = group[1]
        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 40, weight: .light)])
        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 25, weight: .light)])
        let cpiTitleAttributedString = NSAttributedString(string: "cpi".uppercased(), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 13, weight: .heavy), NSAttributedString.Key.foregroundColor:UIColor.lightGray])
        let cpiAttributedString = NSMutableAttributedString()
        cpiAttributedString.append(valueAttributedString)
        cpiAttributedString.append(NSAttributedString(string: "."))
        cpiAttributedString.append(precisionAttributedString)
        cpiAttributedString.append(NSAttributedString(string: "\n"))
        cpiAttributedString.append(cpiTitleAttributedString)
        cpiLabel.attributedText = cpiAttributedString
    }
    internal func setUpAttributedSpi(spi:Float) {
        let spiString = String(format:"%.2f", spi)
        let group = spiString.split(separator: ".")
        let valueString = group[0]
        let precisionString = group[1]
        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 40, weight: .light)])
        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 25, weight: .light)])
        let spiTitleAttributedString = NSAttributedString(string: "spi".uppercased(), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 13, weight: .heavy), NSAttributedString.Key.foregroundColor:UIColor.lightGray])
        let spiAttributedString = NSMutableAttributedString()
        spiAttributedString.append(valueAttributedString)
        spiAttributedString.append(NSAttributedString(string: "."))
        spiAttributedString.append(precisionAttributedString)
        spiAttributedString.append(NSAttributedString(string: "\n"))
        spiAttributedString.append(spiTitleAttributedString)
        spiLabel.attributedText = spiAttributedString
    }
    private func createCpiCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        _ = cpiLabel.frame.size.height
        let cpiLabelHeight = cpiLabel.frame.height
        let radius = (cpiLabelHeight - (cpiLabelHeight / 6))
        let circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = (cpiLabelHeight / 6) + 3
        layer.fillColor = fillColor.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.position = cpiContainerView.center
        return layer
    }
    private func createSpiCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        let spiLabelHeight = spiLabel.frame.height
        let radius = (spiLabelHeight - (spiLabelHeight / 6))
//        let spiLabelHeight = spiLabel.frame.size.height + 30
        let circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = (spiLabelHeight / 6) + 3
        layer.fillColor = fillColor.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.position = spiContainerView.center
        return layer
    }
    private func setupCpiLayers(cpi:Float) {
        DispatchQueue.main.async {
            self.animateCpiPulsatingLayer()
            self.cpiShapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
            self.spiShapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
            self.animateCpiShapeLayer(cpi: cpi)
        }
    }
    private func setupSpiLayers(spi:Float) {
        DispatchQueue.main.async {
            self.animateSpiPulsatingLayer()
            self.cpiShapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
            self.spiShapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
            self.animateSpiShapeLayer(spi: spi)
        }
    }
    private func setupCircleLayers() {
        DispatchQueue.main.async {
            self.animatePulsatingLayer()
            self.cpiShapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
            self.spiShapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
//            self.animateShapeLayer()
//            self.animateSpiShapeLayer()
        }
    }
    private func animateCpiShapeLayer(cpi:Float) {
        let stroke = CABasicAnimation(keyPath: "strokeEnd")
        stroke.duration = 1.0
        stroke.fromValue = 0
        stroke.isRemovedOnCompletion = false
        stroke.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        cpiShapeLayer.strokeEnd = CGFloat(cpi / 10)
        cpiShapeLayer.add(stroke, forKey: "strokeEnd")
    }
    private func animateSpiShapeLayer(spi:Float) {
        let stroke = CABasicAnimation(keyPath: "strokeEnd")
        stroke.duration = 1.0
        stroke.fromValue = 0
        stroke.isRemovedOnCompletion = false
        stroke.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        spiShapeLayer.strokeEnd = CGFloat(spi / 10)
        spiShapeLayer.add(stroke, forKey: "spiStrokeEnd")
    }
    private func animateShapeLayer(cpi:Float?, spi:Float?) {
        let stroke = CABasicAnimation(keyPath: "strokeEnd")
        stroke.duration = 1.0
        stroke.fromValue = 0
        stroke.isRemovedOnCompletion = false
        stroke.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        cpiShapeLayer.strokeEnd = CGFloat(cpi ?? 0 / 10)
        cpiShapeLayer.add(stroke, forKey: "strokeEnd")
        spiShapeLayer.strokeEnd = CGFloat(spi ?? 0 / 10)
        spiShapeLayer.add(stroke, forKey: "spiStrokeEnd")
    }
    private func animateCpiPulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.3
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        cpiPulsatingLayer.add(animation, forKey: "pulsing")
    }
    private func animateSpiPulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.3
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        spiPulsatingLayer.add(animation, forKey: "pulsing")
    }
    private func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.3
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        cpiPulsatingLayer.add(animation, forKey: "pulsing")
        spiPulsatingLayer.add(animation, forKey: "pulsing")
    }
    func setUpLayers() {
        DispatchQueue.main.async {
            self.contentView.layer.addSublayer(self.cpiPulsatingLayer)
            self.contentView.layer.addSublayer(self.cpiTrackLayer)
            self.contentView.layer.addSublayer(self.cpiShapeLayer)
            self.contentView.layer.addSublayer(self.spiPulsatingLayer)
            self.contentView.layer.addSublayer(self.spiTrackLayer)
            self.contentView.layer.addSublayer(self.spiShapeLayer)
        }
    }
    func setUpViews() {
        
        cpiContainerView.addSubview(cpiLabel)
        spiContainerView.addSubview(spiLabel)
        contentView.addSubview(cpiContainerView)
        contentView.addSubview(spiContainerView)
        
        //cpiContainerView.addSubview(cpiTitleLabel)
        
        //spiContainerView.addSubview(spiTitleLabel)
    }
    func setUpConstraints() {
        _ = cpiContainerView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.centerXAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        _ = spiContainerView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.centerXAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
//        cpiContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
//        spiContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        
        cpiLabel.centerXAnchor.constraint(equalTo: cpiContainerView.centerXAnchor).isActive = true
        cpiLabel.centerYAnchor.constraint(equalTo: cpiContainerView.centerYAnchor).isActive = true
        spiLabel.centerYAnchor.constraint(equalTo: spiContainerView.centerYAnchor).isActive = true
        spiLabel.centerXAnchor.constraint(equalTo: spiContainerView.centerXAnchor).isActive = true
        cpiLabel.layoutIfNeeded()
        spiLabel.layoutIfNeeded()
        let safeMargin:CGFloat = cpiLabel.frame.size.height
        let heightConstraint  = spiContainerView.heightAnchor.constraint(equalToConstant: (safeMargin * 2) + 20)
        heightConstraint.priority = UILayoutPriority(999)
        heightConstraint.isActive = true
        
        
        
        
        
        
        
        /*
//        cpiLabel.layoutIfNeeded()
        cpiContainerView.layoutIfNeeded()
        let h = cpiContainerView.bounds.width / 4
//        let h:CGFloat = 40

        cpiLabel.topAnchor.constraint(equalTo: cpiContainerView.topAnchor, constant: h).isActive = true
        cpiContainerView.bottomAnchor.constraint(equalTo: cpiLabel.bottomAnchor, constant: h).isActive = true
        spiLabel.topAnchor.constraint(equalTo: spiContainerView.topAnchor, constant: h).isActive = true
        spiContainerView.bottomAnchor.constraint(equalTo: spiLabel.bottomAnchor, constant: h).isActive = true
//        cpiLabel.centerYAnchor.constraint(equalTo: cpiContainerView.centerYAnchor).isActive = true
//        spiLabel.centerYAnchor.constraint(equalTo: spiContainerView.centerYAnchor).isActive = true
        
        
        let cpiContainerBottomAnchor = cpiContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        cpiContainerBottomAnchor.priority = UILayoutPriority(999)
        cpiContainerBottomAnchor.isActive = true
        let spiContainerBottomAnchor = spiContainerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        spiContainerBottomAnchor.priority = UILayoutPriority(999)
        spiContainerBottomAnchor.isActive = true
        */
        /*
        cpiTitleLabel.topAnchor.constraint(equalTo: cpiLabel.bottomAnchor, constant: 10).isActive = true
        cpiTitleLabel.centerXAnchor.constraint(equalTo: cpiLabel.centerXAnchor).isActive = true
        spiTitleLabel.topAnchor.constraint(equalTo: spiLabel.bottomAnchor, constant: 10).isActive = true
        spiTitleLabel.centerXAnchor.constraint(equalTo: spiLabel.centerXAnchor).isActive = true
         */
//        cpiLabel.layoutIfNeeded()
//        spiLabel.layoutIfNeeded()
//            let safeMargin:CGFloat = cpiLabel.frame.size.height
//            print(safeMargin)
//        cpiLabel.topAnchor.constraint(equalTo: cpiContainerView.topAnchor, constant: safeMargin).isActive = true
//        spiLabel.topAnchor.constraint(equalTo: spiContainerView.topAnchor, constant: safeMargin).isActive = true
        
//        cpiLabel.bottomAnchor.constraint(equalTo: cpiContainerView.bottomAnchor, constant: -safeMargin).isActive = true
//        spiLabel.bottomAnchor.constraint(equalTo: spiContainerView.bottomAnchor, constant: -safeMargin).isActive = true
        
//        let bottomConstraint = cpiLabel.bottomAnchor.constraint(equalTo: cpiContainerView.bottomAnchor, constant: -safeMargin)
//        bottomConstraint.priority = UILayoutPriority(999)
//        bottomConstraint.isActive = true
//
//        let spibottomConstraint = spiLabel.bottomAnchor.constraint(equalTo: spiContainerView.bottomAnchor, constant: -safeMargin)
//        spibottomConstraint.priority = UILayoutPriority(999)
//        spibottomConstraint.isActive = true

        
        
        //        _ = cpiLabel.anchor(cpiContainerView.topAnchor, left: nil, bottom: cpiContainerView.bottomAnchor, right: nil, topConstant: 50, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        _ = spiLabel.anchor(spiContainerView.topAnchor, left: nil, bottom: spiContainerView.bottomAnchor, right: nil, topConstant: 50, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        
        /*
        cpiLabel.layoutIfNeeded()
        spiLabel.layoutIfNeeded()
        let cpiSafeMargin:CGFloat = cpiLabel.frame.size.width * 2
        let spiSafeMargin:CGFloat = spiLabel.frame.size.width * 2
        cpiLabel.topAnchor.constraint(equalTo: cpiContainerView.topAnchor, constant: cpiSafeMargin).isActive = true
//        cpiLabel.bottomAnchor.constraint(equalTo: cpiContainerView.bottomAnchor, constant: -safeMargin).isActive = true
        spiLabel.topAnchor.constraint(equalTo: spiContainerView.topAnchor, constant: spiSafeMargin).isActive = true
//        spiLabel.bottomAnchor.constraint(equalTo: spiContainerView.bottomAnchor, constant: -safeMargin).isActive = true
        let cpiBottomConstraint = cpiLabel.bottomAnchor.constraint(equalTo: cpiContainerView.bottomAnchor, constant: -cpiSafeMargin)
        cpiBottomConstraint.priority = UILayoutPriority(999)
        cpiBottomConstraint.isActive = true
        let spiBottomConstraint = spiLabel.bottomAnchor.constraint(equalTo: spiContainerView.bottomAnchor, constant: -spiSafeMargin)
        spiBottomConstraint.priority = UILayoutPriority(999)
        spiBottomConstraint.isActive = true
        */
    }
}
