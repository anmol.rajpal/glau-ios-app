//
//  SubjectsTableViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 03/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
enum Grade:String {
    case Aplus="A+",A,Bplus="B+",B,Cplus="C+",C,Dplus="D+",D,Eplus="E+",E,F
}
class SubjectsTableViewCell: UITableViewCell {
    var subjectData:SemesterResultCodable? {
        didSet {
            setUpSubjectsResult(subjectData: subjectData)
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpSubjectsResult(subjectData:SemesterResultCodable?) {
        subjectNameLabel.text = subjectData?.name?.capitalized
        subjectCodeLabel.text = subjectData?.code
        gradePointLabel.text = subjectData?.gradePoint
        creditLabel.text = subjectData?.credit
        gradeLabel.text = subjectData?.grade
        gradeLabel.backgroundColor = setUpGradeColor(grade: Grade(rawValue: subjectData?.grade ?? "") ?? .F)
    }
    func setUpGradeColor(grade:Grade) -> UIColor {
        switch grade {
//        case .Aplus: return UIColor.glauThemeColor
//        case .A: return UIColor.glauThemeColor
//        case .Bplus: return UIColor.systemGreenColor
//        case .B: return UIColor.systemTealColor
//        case .Cplus: return UIColor.systemOrangeColor
//        case .C: return UIColor.systemYellowColor
//        case .Dplus: return UIColor.systemPurpleColor
//        case .D: return UIColor.systemPinkColor
//        case .Eplus: return UIColor.systemRedColor
        case .E: return UIColor.systemPinkColor
        case .F: return UIColor.black
        default: return UIColor.systemBlueColor
        }
    }
    let subjectNameLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        return label
    }()
    let subjectCodeLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .gray
        return label
    }()
    let gradePointTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "GP - "
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .black
        return label
    }()
    let gradePointLabel:UILabel = {
        let label = InsetLabel(5, 5, 6, 6)
        label.text = "-"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .white
        label.clipsToBounds = true
        label.backgroundColor = UIColor.systemBlueColor
        label.layer.cornerRadius = 8
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    let creditTitleLabel:UILabel = {
        let label = UILabel()
        label.text = "Credit - "
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .black
        return label
    }()
    let creditLabel:UILabel = {
        let label = InsetLabel(5, 5, 6, 6)
        label.text = "-"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .white
        label.clipsToBounds = true
        label.backgroundColor = .gray
        label.layer.cornerRadius = 8
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    let gradeLabel:UILabel = {
        let label = InsetLabel(5, 5, 8, 8)
        label.text = "-"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.medium)
        label.textColor = .white
        label.clipsToBounds = true
        label.backgroundColor = .red
        label.layer.cornerRadius = 7
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    func setUpViews() {
        
        contentView.addSubview(subjectNameLabel)
        contentView.addSubview(subjectCodeLabel)
        contentView.addSubview(gradePointTitleLabel)
        contentView.addSubview(gradePointLabel)
        contentView.addSubview(creditTitleLabel)
        contentView.addSubview(creditLabel)
        contentView.addSubview(gradeLabel)
    }
    func setUpConstraints() {
        _ = subjectNameLabel.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: subjectCodeLabel.topAnchor, right: contentView.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 10, rightConstant: 10)
        _ = subjectCodeLabel.anchorWithoutWH(top: subjectNameLabel.bottomAnchor, left: contentView.leftAnchor, bottom: gradePointLabel.topAnchor, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 10, rightConstant: 0)
        _ = gradeLabel.anchorWithoutWH(top: nil, left: nil, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 10)
        _ = gradePointTitleLabel.anchorWithoutWH(top: subjectCodeLabel.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 10, rightConstant: 0)
        _ = gradePointLabel.anchorWithoutWH(top: subjectCodeLabel.bottomAnchor, left: gradePointTitleLabel.rightAnchor, bottom: contentView.bottomAnchor, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 10, rightConstant: 0)
        _ = creditTitleLabel.anchorWithoutWH(top: subjectCodeLabel.bottomAnchor, left: gradePointLabel.rightAnchor, bottom: contentView.bottomAnchor, right: creditLabel.leftAnchor, topConstant: 10, leftConstant: 20, bottomConstant: 10, rightConstant: 0)
        _ = creditLabel.anchorWithoutWH(top: subjectCodeLabel.bottomAnchor, left: nil, bottom: contentView.bottomAnchor, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 10, rightConstant: 0)
        gradeLabel.widthAnchor.constraint(equalToConstant: 45).isActive = true
        gradePointLabel.widthAnchor.constraint(equalToConstant: 30).isActive = true
        creditLabel.widthAnchor.constraint(equalToConstant: 30).isActive = true
    }
}
