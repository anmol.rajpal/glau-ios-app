//
//  SemesterResultViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 02/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class SemesterResultTableViewController: UIViewController {
    var semesterResult:[SemesterResultCodable]?
    var semester:String? {
        didSet {
            navigationItem.title = "Semester - \(semester ?? "")"
            fetchSemesterResult(semester: semester ?? "")
        }
    }
    var spi:Float?
    var cpi:Float?
    let pointerCell = "pointerCell"
    let subjectCell = "subjectCell"
    override func viewDidLoad() {
        super.viewDidLoad()
//        fetchSemesterResult()
        view.backgroundColor = .white
        setupNavBar()
        
        setUpViews()
        setUpConstraints()
        tableView.dataSource = self
        tableView.delegate = self
//        setUpTableView()
        
        tableView.reloadDataWithLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        fetchSemesterResult()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        reloadAnimation()
    }
    func reloadAnimation() {
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        tableView.layoutIfNeeded()
        tableView.endUpdates()
    }
    func setUpTableView() {
//        tableView.isHidden = true
//        tableView.backgroundColor = .white
        tableView.alpha = 0.0
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.9)
        tableView.register(PointerTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(PointerTableViewCell.self))
        tableView.register(SubjectsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(SubjectsTableViewCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
//        tableView.reloadDataWithLayout()
    }
    fileprivate func showTableView() {
        //        self.tableView.alpha = 0.0
        //        let initialScale:CGFloat = 0.0
        //        self.tableView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
//        tableView.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.tableView.isHidden = false
            self.tableView.alpha = 1.0
            self.tableView.layer.transform = CATransform3DIdentity
            
        }, completion: nil)
    }
    fileprivate func hideTableView() {
        //        self.infoView.alpha = 1.0
        //        let initialScale:CGFloat = 1.0
        //        self.infoView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//            tableView.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1.0)
            self.tableView.alpha = 0.0
        }, completion: nil)
    }
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel(message:String = "Cannot Connect to GLAU") {
        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        fetchSemesterResult(semester: semester ?? "")
    }
    
    private func updateViews() {
        let isEmpty = semesterResult?.isEmpty ?? true
        tableView.isHidden = isEmpty
        messageLabel.isHidden = !isEmpty
        retryButton.isHidden = !isEmpty
        if !isEmpty { tableView.reloadDataWithLayout() }
    }
    let tableView : UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.register(PointerTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(PointerTableViewCell.self))
        tv.register(SubjectsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(SubjectsTableViewCell.self))
        tv.translatesAutoresizingMaskIntoConstraints = false
//        tv.backgroundColor = .white
        tv.bounces = true
        tv.alwaysBounceVertical = true
        tv.clipsToBounds = true
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = true
//        tv.layer.cornerRadius = 7
//        tv.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.9)
        tv.isHidden = true
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.estimatedRowHeight = 44.0
        tv.rowHeight = UITableView.automaticDimension
        return tv
    }()
    func setUpViews() {
//        view.backgroundColor = .white
        view.addSubview(tableView)
        view.addSubview(spinner)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
        
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = tableView.anchorWithoutWH(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
    }
    private func fetchSemesterResult(semester:String) {
        
//        spinner.startAnimating()
        self.hideTableView()
        self.messageLabel.isHidden = true
        self.retryButton.isHidden = true
        let rollNo = UserDefaults.standard.getRollNo()
        SemesterResultService.shared.fetch(rollNo: rollNo, semester: semester) { (data, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
                
                guard error == nil else {
                    self.hideTableView()
                    self.messageLabel.isHidden = false
                    self.retryButton.isHidden = false
                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?[0].result
                let message = data?[0].message
                
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    self.hideTableView()
                    self.messageLabel.text = "Cannot load Results"
                    self.messageLabel.isHidden = false
                    self.retryButton.isHidden = false
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.semesterResult = data
                        self.spi = (data[0].sPI as NSString?)?.floatValue
                        self.cpi = (data[0].cPI as NSString?)?.floatValue
                        self.tableView.reloadDataWithLayout()
                        self.showTableView()
                        self.spinner.stopAnimating()
                    }
                   
                default: break
                }
            })
        }
    }
    
}
extension SemesterResultTableViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { return 1 } else { return semesterResult?.count ?? 0 }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(PointerTableViewCell.self), for: indexPath) as! PointerTableViewCell
            cell.selectionStyle = .none
            cell.cpi = self.cpi
            cell.spi = self.spi
            cell.layoutIfNeeded()
            //            let spi = semesterResult[0].sPI ?? "0"
            //            let spi = "7.25"
            //            cell.setUpAttributedSpi(spi: spi)
            //            cell.setUpAttributedCpi(cpi: cpi)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(SubjectsTableViewCell.self), for: indexPath) as! SubjectsTableViewCell
            cell.selectionStyle = .none
            let subjectsResultData = semesterResult?[indexPath.row]
            cell.subjectData = subjectsResultData
            return cell
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 { return "POINTER" } else { return "GRADES" }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
//        header.backgroundColor = .gray
        header.textLabel?.textColor = UIColor.glauThemeColor
        header.backgroundView?.backgroundColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)
        header.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
protocol PointerInfoCellDelegate {
    func animateCpi(cell:PointerTableViewCell)
    func animateSpi(cell:PointerTableViewCell)
}
extension SemesterResultTableViewController: PointerInfoCellDelegate {
    func animateCpi(cell: PointerTableViewCell) {
        DispatchQueue.main.async {
            //            cell.shapeLayer.strokeEnd = 0.0
            cell.layoutIfNeeded()
            cell.cpiShapeLayer.layoutIfNeeded()
            let minimumCpi:Float = 6.0
            UIView.animate(withDuration: 1.0) {
                //                cell.shapeLayer.strokeEnd = CGFloat(self.attendance / 100)
                if self.cpi ?? 0 >= minimumCpi {
                    cell.cpiPulsatingLayer.fillColor = UIColor.systemGreenColor.withAlphaComponent(0.2).cgColor
                    cell.cpiTrackLayer.strokeColor = UIColor.systemGreenColor.withAlphaComponent(0.2).cgColor
                    cell.cpiShapeLayer.strokeColor = UIColor.systemGreenColor.cgColor
                } else {
                    cell.cpiPulsatingLayer.fillColor = UIColor.systemPinkColor.withAlphaComponent(0.2).cgColor
                    cell.cpiTrackLayer.strokeColor = UIColor.systemPinkColor.withAlphaComponent(0.2).cgColor
                    cell.cpiShapeLayer.strokeColor = UIColor.systemPinkColor.cgColor
                }
                cell.contentView.layoutIfNeeded()
            }
        }
    }
    func animateSpi(cell: PointerTableViewCell) {
        DispatchQueue.main.async {
            //            cell.shapeLayer.strokeEnd = 0.0
            cell.layoutIfNeeded()
            cell.cpiShapeLayer.layoutIfNeeded()
            let minimumSpi:Float = 6.0
            UIView.animate(withDuration: 1.0) {
                //                cell.shapeLayer.strokeEnd = CGFloat(self.attendance / 100)
                
                if self.spi ?? 7 >= minimumSpi {
                    cell.spiPulsatingLayer.fillColor = UIColor.systemGreenColor.withAlphaComponent(0.2).cgColor
                    cell.spiTrackLayer.strokeColor = UIColor.systemGreenColor.withAlphaComponent(0.2).cgColor
                    cell.spiShapeLayer.strokeColor = UIColor.systemGreenColor.cgColor
                } else {
                    cell.spiPulsatingLayer.fillColor = UIColor.systemPinkColor.withAlphaComponent(0.2).cgColor
                    cell.spiTrackLayer.strokeColor = UIColor.systemPinkColor.withAlphaComponent(0.2).cgColor
                    cell.spiShapeLayer.strokeColor = UIColor.systemPinkColor.cgColor
                }
                cell.contentView.layoutIfNeeded()
            }
        }
    }
}
