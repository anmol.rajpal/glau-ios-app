//
//  PreferencesTableViewController
//  glau ios app
//
//  Created by Anmol Rajpal on 25/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
class PreferencesTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    let options = [["Profile","Result"],["Student Portal","My Result"],["Review on App Store","Report a Bug"],["Facebook","Twitter","Google Plus","Youtube"],["Contact Developer", "Contact GLA University"], ["Privacy Policy", "Terms of Service"], ["Log Out"]]
    var identities = [String]()
    var sections = [String]()
    let universityLinks = ["http://glauniversity.in:8083/EduwareHome/Home.aspx", "http://glauniversity.in:8083/FinalResults"]
    let feedbacklinks = ["http://glauniversity.in:8083/EduwareHome/Home.aspx#"]
    let followlinks = ["https://www.facebook.com/glauniv/","https://twitter.com/gla_mathura","https://plus.google.com/117725914128868840841","https://www.youtube.com/user/OfficialGLAuniv"]
    let contactlinks = ["https://www.natovi.com/apps/glau/support", "http://www.gla.ac.in/contact-us"]
    let aboutLinks = ["https://www.natovi.com/apps/glau/privacy-policy", "https://www.natovi.com/apps/glau/terms"]
    let cellId = "cell"
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkReachability()
        setupNavBar()
        self.navigationItem.title = "Preferences"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        checkReachability()
//        self.tableView.backgroundColor = UIColor.clear
        sections = ["ACCOUNT","PORTAL","FEEDBACK","FOLLOW","SUPPORT", "ABOUT", "ACCOUNT ACTIONS"]
        setupNavBar()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)

        identities = ["A","B","C","D"]
        //        tableView.estimatedRowHeight = 80.0
//                tableView.rowHeight = UITableViewAutomaticDimension
        // Change the color of the table view
//                tableView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.2)
        
        // Remove the separators of the empty rows
        tableView.tableFooterView = UIView(frame: CGRect.zero)
//        tableView.sectionIndexColor = UIColor.green
//        tableView.sectionIndexBackgroundColor = UIColor.green
        // Change the color of the separator
        tableView.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.9)
        
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return sections.count
    }

    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.glauThemeColor
        header.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
            case 0: return 2
            case 1: return 1
            case 2: return 2
            case 3: return 4
            case 4: return 2
            case 5: return 2
            case 6: return 1
            default: return 0
        }
    }

    var darkMode = true
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        // Configure the cell...
        cell.textLabel!.text = options[indexPath.section][indexPath.row]
//        if indexPath.section == 5 {
//            cell.selectionStyle = .none
//            let switchObj = UISwitch(frame: CGRect(x: 1, y: 1, width: 20, height: 20))
//            switchObj.addTarget(self, action: #selector(toggle(_:)), for: .valueChanged)
//            cell.accessoryView = switchObj
//        }
        
        return cell
    }
    
    @objc func toggle(_ sender:UISwitch) {
        if sender.isOn {
            print("Switch On")
            darkMode = true
            setNeedsStatusBarAppearanceUpdate()
        } else {
            print("Switch off")
            darkMode = false
            setNeedsStatusBarAppearanceUpdate()
        }
        
    }
    
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
            case 0:
                if indexPath.row == 0 {
                    let profileTableViewController = ProfileTableViewController()
                    self.navigationController?.pushViewController(profileTableViewController, animated: true)
                } else if indexPath.row == 1 {
                    let semesterListTVC = SemesterListTableViewController()
                    self.navigationController?.pushViewController(semesterListTVC, animated: true)
                }
            case 1:
                if let url = URL(string: universityLinks[indexPath.row])
                {
                    let safariVC = SFSafariViewController.setupSafariVC(url)
                    safariVC.modalPresentationStyle = .overFullScreen
                    safariVC.modalPresentationCapturesStatusBarAppearance = true
                    self.present(safariVC, animated: true, completion: nil)
                }
            case 2:
                if indexPath.row == 0 {
                    self.reviewApp(appId: Config.appId)
                }
                else if indexPath.row == 1 {
                    let mailComposeVC = configureMailComposeViewController()
                    guard MFMailComposeViewController.canSendMail() else {
                        self.showSendMailErrorAlert()
                        return
                    }
                    mailComposeVC.modalPresentationStyle = .overFullScreen
                    self.present(mailComposeVC, animated: true, completion: nil)
                }
            case 3:
                if let url = URL(string: followlinks[indexPath.row])
                {
                    let safariVC = SFSafariViewController.setupSafariVC(url)
                    safariVC.modalPresentationStyle = .overFullScreen
                    safariVC.modalPresentationCapturesStatusBarAppearance = true
                    self.present(safariVC, animated: true, completion: nil)
                }
            case 4:
                if let url = URL(string: contactlinks[indexPath.row])
                {
                    let safariVC = SFSafariViewController.setupSafariVC(url)
                    safariVC.modalPresentationStyle = .overFullScreen
                    safariVC.modalPresentationCapturesStatusBarAppearance = true
                    self.present(safariVC, animated: true, completion: nil)
                }
        case 5:
            if let url = URL(string: aboutLinks[indexPath.row])
            {
                let safariVC = SFSafariViewController.setupSafariVC(url)
                safariVC.modalPresentationStyle = .overFullScreen
                safariVC.modalPresentationCapturesStatusBarAppearance = true
                self.present(safariVC, animated: true, completion: nil)
            }
            case 6:
                let alertVC = UIAlertController(title: "Confirm Logout", message: "Are you sure you want to log out?", preferredStyle: UIAlertController.Style.alert)
                alertVC.addAction(UIAlertAction(title: "Log Out", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
//                    self.showLoginController()
                    self.signOut()
                })
                alertVC.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alertVC, animated: true, completion: nil)
            default: break
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
    func signOut() {
        let loginController = LoginController()
        UserDefaults.standard.setIsLoggedIn(value: false)
        UserDefaults.clearUserData()
        
        DispatchQueue.main.async {
            guard let tbc = self.tabBarController as? TabBarController else {
                print("uaaa")
                return
            }
            tbc.isLoaded = false
            tbc.present(loginController, animated: true, completion: {
                tbc.selectedViewController?.view.isHidden = true
                tbc.viewControllers = nil
            })
        }
    }
    func showLoginController() {
        let loginController = LoginController()
        UserDefaults.standard.setIsLoggedIn(value: false)
        UserDefaults.clearUserData()
        
        DispatchQueue.main.async {
            self.tabBarController?.present(loginController, animated: true, completion:  {
                self.tabBarController?.selectedViewController?.view.isHidden = true
                self.tabBarController?.viewControllers = nil
            })
        }
        
//        self.view.isHidden = true
//        self.tabBarController?.viewControllers = nil
//        self.tabBarController?.viewControllers?.removeAll()
//        DispatchQueue.main.async {
//            self.tabBarController?.selectedIndex = 0
////            self.tabBarController?.viewControllers = nil
////            self.tabBarController?.selectedViewController = nil
////            self.tabBarController?.removeFromParent()
//            self.tabBarController?.present(loginController, animated: false, completion: {
////                self.tabBarController?.viewControllers = nil
//                self.tabBarController?.viewControllers?.removeAll()
//            })
//        }
    }
    func reviewApp(appId : String) {
//        let urlStr = "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=\(appId)"    //Not working anymore, you have to scroll down to review section in this case
        
        let urlStr = "itms-apps://itunes.apple.com/app/id\(appId)?mt=8&action=write-review"
        guard let url = URL(string : urlStr) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    func handleSignOut() {
        DispatchQueue.main.async {
            
//            let indexPath = IndexPath(row: 0, section: 0)
//            self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
//            self.tabBarController?.selectedIndex = 0
            
            guard let window = UIApplication.shared.keyWindow else {
                return
            }
            UIView.transition(with: window, duration: 0.2, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                window.rootViewController = LoginController()
            }, completion: { completed in
                UserDefaults.standard.setIsLoggedIn(value: false)
                UserDefaults.clearUserData()
                
            })
//            UIApplication.shared.keyWindow?.rootViewController = TabBarController()
//            let loginController = LoginController()
//            self.tabBarController?.present(loginController, animated: true, completion: {
//                
//                
//            })
        }
    }
    func configureMailComposeViewController() -> MFMailComposeViewController
    {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients(["anmol.rajpal@me.com"])
        mailComposerVC.setSubject("Report a Bug")
        mailComposerVC.setMessageBody("", isHTML: false)
        return mailComposerVC
    }
    
    func showSendMailErrorAlert()
    {
        UIAlertController.showAlert(alertTitle: "Could Not Send Email", message: "Make sure you have setup Mail Accounts properly", alertActionTitle: "Ok", controller: self)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("mail cancelled")
        case MFMailComposeResult.sent.rawValue:
            print("mail sent")
        default: break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
