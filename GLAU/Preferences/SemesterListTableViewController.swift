//
//  SemesterListTableViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 02/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class SemesterListTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        navigationItem.title = "Semesters"
        setUpViews()
        setUpConstraints()
        setUpTableView()
        fetchSemesterList()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchSemesterList()
    }
    var semesterList:[SemesterListCodable]?
    var semester:String?
    func setUpTableView() {
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 10.0, right: 0.0)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.reloadData()
    }
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = tableView.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel() {
        messageLabel.isHidden = true
        messageLabel.text = "Unstable Internet Connection"
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        fetchSemesterList()
    }
    
    private func updateViews() {
        let hasData = semesterList?.count ?? 0 > 0
        tableView.isHidden = !hasData
        messageLabel.isHidden = hasData
        retryButton.isHidden = hasData
        if hasData { tableView.reloadData() }
    }
    func setUpViews() {
//        view.layoutIfNeeded()
        view.addSubview(spinner)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    private func fetchSemesterList() {
        let rollNo = UserDefaults.standard.getRollNo()
        SemesterListService.shared.fetch(rollNo: rollNo) { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    self.updateViews()
                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?[0].result
                let message = data?[0].message
                
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.semesterList = data
                    }
                    self.updateViews()
                    self.spinner.stopAnimating()
                default: break
                }
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return semesterList?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let semester = semesterList?[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = "Semester - \(semester?.semester ?? "")"
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let semesterResultTVC = SemesterResultTableViewController()
        let semesters = semesterList?[indexPath.row]
        semesterResultTVC.semester = semesters?.semester
        self.show(semesterResultTVC, sender: self)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
