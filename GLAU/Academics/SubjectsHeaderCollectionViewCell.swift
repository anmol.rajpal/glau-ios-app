//
//  SubjectsHeaderCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 08/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class SubjectsHeaderCollectionViewCell: UICollectionViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let headerLabel:UILabel = {
        let label = UILabel()
        label.text = "Subjects"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 23, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(headerLabel)
    }
    func setUpConstraints() {
        _ = headerLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
}
