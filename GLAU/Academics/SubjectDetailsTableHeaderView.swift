//
//  SubjectDetailsTableHeaderView.swift
//  GLAU
//
//  Created by Anmol Rajpal on 17/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit

class SubjectDetailsTableHeaderView: UITableViewHeaderFooterView {
    var headerTitle:String? {
        didSet {
            headerLabel.text = headerTitle ?? ""
        }
    }
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setUpViews()
//        setUpConstraints()
//    }
    override var frame: CGRect {
        get {
            return super.frame
        }
        set {
            if newValue.width == 0 { return }
            super.frame = newValue
        }
    }
    let headerLabel:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).bold()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let separatorLine = HorizontalLine(color: UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0))
    func setUpViews() {
        contentView.clipsToBounds = true
        contentView.backgroundColor = .white
        contentView.addSubview(separatorLine)
        contentView.addSubview(headerLabel)
//        layoutIfNeeded()
    }
    func setUpConstraints() {
        _ = separatorLine.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0.5)
//        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
//        headerLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15).isActive = true
//        headerLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 15).isActive = true
//        headerLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        _ = headerLabel.anchorWithoutWH(top: separatorLine.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
//        let rightConstraint = headerLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15)
//        rightConstraint.priority = UILayoutPriority(500)
//        rightConstraint.isActive = true
        let bottomConstraint = headerLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -2)
        bottomConstraint.priority = UILayoutPriority(750)
        bottomConstraint.isActive = true
        
    }
}
