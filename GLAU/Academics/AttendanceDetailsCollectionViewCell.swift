//
//  AttendanceDetailsCollectionViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit

class AttendanceDetailsCollectionViewCell: UICollectionViewCell {
    var attendanceDetails:AttendanceDetailsCodable? {
        didSet {
            setUpAttendanceDetails(details: attendanceDetails)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        setUpConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setAttributedDate(dateString:String?) -> String? {
        if let date = getDateFromString(dateString: dateString, dateFormat: NSObject.DateFormatType.dateType1) {
            let str = getStringFromDate(date: date, dateFormat: NSObject.DateFormatType.dateType2)
            return str
        }
        return nil
    }
    func setUpAttendanceDetails(details:AttendanceDetailsCodable?) {
        serialNoLabel.text = details?.sNo ?? ""
        dateLabel.text = setAttributedDate(dateString: details?.lectureDate) ?? ""
        detailsLabel.text = details?.details ?? ""
        if let attendanceStatus = getAttendanceStatus(from: details?.details) {
            contentView.backgroundColor = setUpColor(for: attendanceStatus)
        }
    }
    func setUpColor(for attendanceStatus:AttendanceStatus) -> UIColor {
        switch attendanceStatus {
            case .Present: return .systemGreenColor
            case .Absent: return .systemPinkColor
            case .Leave: return .gray
        }
    }
    let serialNoLabel:UILabel = {
        let label = UILabel()
        label.text = "1"
        label.textColor = .white
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        return label
    }()
    let dateLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.heavy)
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let detailsLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.textAlignment = .center
        return label
    }()
    func setUpViews() {
        contentView.layer.cornerRadius = 15
//        contentView.backgroundColor = UIColor.systemPinkColor
//        contentView.addSubview(serialNoLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(detailsLabel)
    }
    func setUpConstraints() {
//        serialNoLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
//        serialNoLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
//        serialNoLabel.widthAnchor.constraint(equalToConstant: 20).isActive = true
        dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
//        dateLabel.leftAnchor.constraint(equalTo: serialNoLabel.rightAnchor, constant: 10).isActive = true
        dateLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        dateLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
        detailsLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 5).isActive = true
        detailsLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        detailsLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
        detailsLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
//        contentView.bottomAnchor.constraint(equalTo: detailsLabel.bottomAnchor, constant: 10).isActive = true
    }
}
