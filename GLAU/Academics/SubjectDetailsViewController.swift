//
//  SubjectDetailsViewController.swift
//  GLAU
//
//  Created by Anmol Rajpal on 17/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit
import MessageUI
protocol SubjectDetailsViewControllerDelegate {
    func handleAction(_ controller: SubjectDetailsViewController, actionTitle: SubjectDetailsViewController.PreviewActionsTitle, for previewedController: UIViewController)
    func setupMailAction(subject:String, body:String, facultyEmail:String)
    func showFacultyDetailsVC(facultyName:String, facultyEmail:String, facultyContact:String)
    func callFaculty(phoneNo:String)
    func messageFaculty(phoneNo: String)
    func lodgeLeave()
    func feedbackFaculty(facultyName:String, facultyEmail:String)
}
class SubjectDetailsViewController: UITableViewController {
    
    enum PreviewActionsTitle:String {
        case mailFaculty = "Mail Faculty"
        case messageFaculty = "Message Faculty"
        case viewFacultyDetails = "Faculty Details"
        case callFaculty = "Call Faculty"
        case lodgeLeave = "Lodge Leave"
        case feedbackFaculty = "Feedback"
    }
    var delegate:SubjectDetailsViewControllerDelegate?
    var attendanceDetails = [AttendanceDetailsCodable]()
    var subjectName:String?
    var subjectCode:String?
    var faculty:String?
    var attendance:Float?
    var attendedLectures:Int?
    var heldLectures:Int?
    var mod1Marks:String?
    var mod2Marks:String?
    var mod3Marks:String?
    var subjectType:String?
    var facultyEmail:String?
    var facultyContact:String?
//    func setUpDetails(details:AcademicDetails?) {
    var details:AcademicDetails? {
        didSet {
            self.subjectName = details?.subjectName ?? "--"
            self.subjectCode = details?.subjectCode ?? "--"
            self.faculty = details?.faculty ?? "--"
            if let att = details?.percentage {
                self.attendance = (att as NSString).floatValue
            }
            if let attended = details?.attend {
                self.attendedLectures = (attended as NSString).integerValue
            }
            if let held = details?.held {
                self.heldLectures = (held as NSString).integerValue
            }
            
            
            self.mod1Marks = details?.m1Mark ?? "--"
            self.mod2Marks = details?.m2Mark ?? "--"
            self.mod3Marks = details?.endMark ?? "--"
            self.subjectType = details?.subjectType ?? "--"
            self.facultyEmail = details?.facultyEmail ?? "--"
            self.facultyContact = details?.facultyContact ?? ""
        }
        
    }
    var mailSubject:String {
        get {
            return "Regarding Subject \(self.subjectCode ?? "")"
        }
    }
    let mailBody = "Respected Sir/mam,\n"
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        setUpTableView()
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.view.layer.removeAllAnimations()
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.backgroundColor = .white
        setupNavBar()
        navigationItem.title = subjectCode
//        self.navigationController?.navigationBar.prefersLargeTitles = false
        checkReachability()
        setUpTableView()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkReachability()
        reloadAnimation()
//        navigationItem.title = subjectCode
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    func reloadAnimation() {
        let indexPath = IndexPath(row: 0, section: 1)
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        tableView.layoutIfNeeded()
        tableView.endUpdates()
    }
    private func handlePreviewAction(actionTitle: PreviewActionsTitle, for viewController: UIViewController) {
        delegate?.handleAction(self, actionTitle: actionTitle, for: viewController)
    }
    override var previewActionItems: [UIPreviewActionItem] {
        
        let viewFacultyDetailsAction = UIPreviewAction(title: PreviewActionsTitle.viewFacultyDetails.rawValue, style: .default) { (action, viewController) in
            self.delegate?.showFacultyDetailsVC(facultyName: self.faculty ?? "", facultyEmail: self.facultyEmail ?? "", facultyContact: self.facultyContact ?? "")
            self.handlePreviewAction(actionTitle: .viewFacultyDetails, for: viewController)
        }
        let mailFacultyAction = UIPreviewAction(title: PreviewActionsTitle.mailFaculty.rawValue, style: .default) { (action, viewController) in
            self.delegate?.setupMailAction(subject: self.mailSubject, body: self.mailBody, facultyEmail: self.facultyEmail ?? "")
            self.handlePreviewAction(actionTitle: .mailFaculty, for: viewController)
        }
        let callFacultyAction = UIPreviewAction(title: PreviewActionsTitle.callFaculty.rawValue, style: .default) { (action, viewController) in
            self.delegate?.callFaculty(phoneNo: self.facultyContact ?? "")
            self.handlePreviewAction(actionTitle: .callFaculty, for: viewController)
        }
        let messageFacultyAction = UIPreviewAction(title: PreviewActionsTitle.messageFaculty.rawValue, style: .default) { (action, viewController) in
            self.delegate?.messageFaculty(phoneNo: self.facultyContact ?? "")
//            self.handlePreviewAction(actionTitle: .callFaculty, for: viewController)
        }
//        let lodgeLeaveAction = UIPreviewAction(title: PreviewActionsTitle.lodgeLeave.rawValue, style: .default) { (action, viewController) in
//            self.delegate?.lodgeLeave()
//            self.handlePreviewAction(actionTitle: .lodgeLeave, for: viewController)
//        }
//        let feedbackFacultyAction = UIPreviewAction(title: PreviewActionsTitle.feedbackFaculty.rawValue, style: .default) { (action, viewController) in
//            self.delegate?.feedbackFaculty(facultyName: self.faculty, facultyEmail: self.facultyEmail)
//            self.handlePreviewAction(actionTitle: .feedbackFaculty, for: viewController)
//        }
        let previewActionItems:Array = [viewFacultyDetailsAction, mailFacultyAction, callFacultyAction, messageFacultyAction]
        return previewActionItems
    }
    func setUpTableView() {
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
//        tableView.separatorColor = .white
        tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(SubjectDetailsTableHeaderView.self, forHeaderFooterViewReuseIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self))
        tableView.register(SubjectInfoTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(SubjectInfoTableViewCell.self))
        tableView.register(TestAttendanceInfoTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TestAttendanceInfoTableViewCell.self))
        tableView.register(AttendanceDetailsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(AttendanceDetailsTableViewCell.self))
        tableView.register(LecturesInfoTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(LecturesInfoTableViewCell.self))
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
//        tableView.reloadDataWithLayout()
    }
    func canLeaveNextLecture(attended:Int, held:Int, requiredAttendance:Float = 75) -> Bool {
        return (Float(attended) * 100) / Float(held + 1) >= requiredAttendance
    }
    func numLecturesCanLeave(attended:Int, held:Int) -> Int {
        var count = 0, h = held
        while canLeaveNextLecture(attended: attended, held: h) {
            h = h + 1
            count = count + 1
        }
        return count
    }
    func numLecturesRequired(attended:Int, held:Int) -> Int {
        var count = 0, a = attended, h = held
        let requiredAttendance:Float = 75
        while (Float(a) * 100) / Float(h) < requiredAttendance {
            a = a + 1
            h = h + 1
            count = count + 1
        }
        return count
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3 { return 3 }
        if section == 1 { return 2 }
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(SubjectInfoTableViewCell.self), for: indexPath) as! SubjectInfoTableViewCell
            cell.delegate = self
            cell.subjectType = subjectType
            cell.subjectName = subjectName
            cell.facultyName = faculty
//            cell.subjectTypeLabel.text = subjectType.uppercased()
//            cell.subjectNameLabel.text = subjectName.capitalized
//            cell.facultyNameLabel.text = faculty.capitalized
            cell.selectionStyle = .none
            return cell
        case 1:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TestAttendanceInfoTableViewCell.self), for: indexPath) as! TestAttendanceInfoTableViewCell
    //            cell.attendanceLabel.backgroundColor = setUpAttendanceColor(attendance: attendance)
                cell.delegate = self
                cell.attendance = attendance
                cell.layoutIfNeeded()
//                cell.setupCircleLayers()
//                cell.animatePulsatingLayer()
                cell.selectionStyle = .none
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AttendanceDetailsTableViewCell.self), for: indexPath) as! AttendanceDetailsTableViewCell
                cell.selectionStyle = .none
                cell.subjectCode = subjectCode
//                cell.attendanceDetails = attendanceDetails
                return cell
            }
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(LecturesInfoTableViewCell.self), for: indexPath) as! LecturesInfoTableViewCell
            cell.selectionStyle = .none
            
            if attendance! >= Config.requiredAttendance {
                let n = numLecturesCanLeave(attended: attendedLectures ?? 0, held: heldLectures ?? 0)
                if n == 0 {
                    cell.suggestionLabel.text = "SUGGESTION: Do not leave your next lecture in order to maintain Minimum Required Attendance."
                } else if n == 1 {
                    cell.suggestionLabel.text = "You can leave only 1 lecture until 75%"
                } else {
                    cell.suggestionLabel.text = "You can leave upto \(n) lectures until 75%"
                }
            } else {
                let n = numLecturesRequired(attended: attendedLectures ?? 0, held: heldLectures ?? 0)
                cell.suggestionLabel.text = "IMPORTANT: You need to attend atleast next \(n) lectures in order to maintain Minimum Required Attendance."
            }
            cell.attendedLecturesValueLabel.text = String(attendedLectures!)
            cell.heldLecturesValueLabel.text = String(heldLectures!)
            return cell
        case 3:
            var cell = tableView.dequeueReusableCell(withIdentifier: "cellId")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "cellId")
            }
            if SubjectType(rawValue: subjectType?.lowercased() ?? "") == SubjectType.theory {
                cell?.detailTextLabel?.numberOfLines = 1
                cell?.selectionStyle = .none
                cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.heavy)
                // ALLOW MANUAL CONSTRAINTS
                cell?.detailTextLabel?.translatesAutoresizingMaskIntoConstraints = false
                // TOP +15, BOTTOM -15, RIGHT -15
                cell?.detailTextLabel?.topAnchor.constraint(equalTo: (cell?.contentView.topAnchor)!, constant: 10).isActive = true
                cell?.detailTextLabel?.leftAnchor.constraint(equalTo: (cell?.textLabel?.rightAnchor)!, constant: 25).isActive = true
                cell?.detailTextLabel?.bottomAnchor.constraint(equalTo: (cell?.contentView.bottomAnchor)!, constant: -10).isActive = true
                cell?.detailTextLabel?.rightAnchor.constraint(equalTo: (cell?.contentView.rightAnchor)!, constant: -25).isActive = true
                let row = indexPath.row
                switch row {
                case 0:
                    cell?.textLabel?.text = "MidTerm - I"
                    cell?.detailTextLabel?.text = mod1Marks
                case 1:
                    cell?.textLabel?.text = "MidTerm - II"
                    cell?.detailTextLabel?.text = mod2Marks
                case 2:
                    cell?.textLabel?.text = "EndTerm"
                    cell?.detailTextLabel?.text = mod3Marks
                default: break
                }
            } else {
                cell?.isHidden = true
            }
            
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
//        case 1: if indexPath.row == 0 { return 170 } else { return 90 }
        case 1: if indexPath.row == 1 { return 90 } else { return UITableView.automaticDimension }
        case 3:
            if SubjectType(rawValue: subjectType?.lowercased() ?? "") == SubjectType.theory {
                return UITableView.automaticDimension
            } else { return 0 }
        default: return UITableView.automaticDimension
        }
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self)) as! SubjectDetailsTableHeaderView
        if section == 1 {
            headerView.headerLabel.text = "Attendance"
        } else if section == 2 {
            headerView.headerLabel.text = "Lectures"
        } else if section == 3 {
            headerView.headerLabel.text = "Marks"
        } else {
            headerView.headerLabel.text = ""
        }
        return headerView
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { return 0 }
        else if section == 3 {
            if SubjectType(rawValue: subjectType?.lowercased() ?? "") == SubjectType.theory {
                return 35
            } else { return 0 }
        } else { return 35 }
    }
}
extension SubjectDetailsViewController: MFMailComposeViewControllerDelegate {
    func configureMailComposer(subject:String, body:String, recipients:String...)  -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(recipients)
        mailComposerVC.setSubject(subject)
        mailComposerVC.setMessageBody(body, isHTML: false)
        return mailComposerVC
    }
    
    func showSendMailErrorAlert()
    {
        UIAlertController.showAlert(alertTitle: "Could Not Send Email", message: "Your device could not send e-mail. Make sure you have an Active Internet Connection", alertActionTitle: "Ok", controller: self)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
            case MFMailComposeResult.cancelled:
                print("mail cancelled")
            case MFMailComposeResult.sent:
                print("mail sent")
            case MFMailComposeResult.failed:
                print("mail failed")
                UIAlertController.showAlert(alertTitle: "Could Not Send Email", message: "Fail to send your email. Make sure you have active Internet connection and setup Details correctly", alertActionTitle: "Ok", controller: self)
            case MFMailComposeResult.saved:
                print("mail saved to drafts")
            }
        self.dismiss(animated: true, completion: nil)
    }
}
extension SubjectDetailsViewController: SubjectInfoCellDelegate {
    func setupMailAction() {
        let mailComposeVC = self.configureMailComposer(subject: mailSubject, body: mailBody, recipients: self.facultyEmail ?? "")
        guard MFMailComposeViewController.canSendMail() else {
            self.showSendMailErrorAlert()
            return
        }
        mailComposeVC.modalPresentationStyle = .overFullScreen
        self.present(mailComposeVC, animated: true, completion: nil)
    }
    func showFacultyDetailsVC() {
        let facultyDetailViewController = FacultyDetailViewController()
        facultyDetailViewController.modalPresentationStyle = .overFullScreen
        //        facultyDetailViewController.transitioningDelegate = self
        facultyDetailViewController.facultyNameLabel.text = self.faculty?.capitalized
        facultyDetailViewController.email = self.facultyEmail
        facultyDetailViewController.phone = self.facultyContact
        self.present(facultyDetailViewController, animated: true, completion: nil)
    }
    func callFaculty() {
        if let url = URL(string: "tel://\(self.facultyContact ?? "")"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    func morePressed(cell: SubjectInfoTableViewCell) {
        let callFacultyAction = UIAlertAction(title: "Call Faculty",
                                             style: .default) { (action) in
                                                
                                                self.callFaculty()
        }
        let viewFacultyDetailsAction = UIAlertAction(title: "Faculty Details",
                                             style: .default) { (action) in
                                                self.showFacultyDetailsVC()
        }
//        let lodgeLeaveAction = UIAlertAction(title: "Lodge Leave",
//                                             style: .default) { (action) in
//                                                UIAlertController.showAlert(alertTitle: "Lodge Leave", message: "\nFeature Coming Soon.", alertActionTitle: "Ok", controller: self)
//        }
        let mailFacultyAction = UIAlertAction(title: "Mail Faculty",
                                              style: .default) { (action) in
                                                if self.facultyEmail != "--" && self.subjectCode != "--" {
                                                    self.setupMailAction()
                                                } else {
                                                    UIAlertController.showAlert(alertTitle: "Mail Faculty Failed", message: "Faculty Details Not Available", alertActionTitle: "Ok", controller: self)
                                                }
                                                
        }
        let feedbackAction = UIAlertAction(title: "Feedback",
                                           style: .default) { (action) in
                                            UIAlertController.showAlert(alertTitle: "Feedback", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
        }
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) { (action) in
                                            
        }
        
        // Create and configure the alert controller.
        let alertController = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alertController.addAction(viewFacultyDetailsAction)
        alertController.addAction(mailFacultyAction)
        alertController.addAction(callFacultyAction)
//        alertController.addAction(lodgeLeaveAction)
        alertController.addAction(feedbackAction)
        alertController.addAction(cancelAction)
        callFacultyAction.isEnabled = true
        viewFacultyDetailsAction.isEnabled = true
        mailFacultyAction.isEnabled = true
//        lodgeLeaveAction.isEnabled = false
        feedbackAction.isEnabled = false
        self.present(alertController, animated: true, completion: nil)
    }
    func disclosurePressed(cell: SubjectInfoTableViewCell) {
        let facultyDetailViewController = FacultyDetailViewController()
        facultyDetailViewController.modalPresentationStyle = .overFullScreen
        //        facultyDetailViewController.transitioningDelegate = self
        facultyDetailViewController.facultyNameLabel.text = self.faculty?.capitalized
        facultyDetailViewController.email = self.facultyEmail
        facultyDetailViewController.phone = self.facultyContact
        self.present(facultyDetailViewController, animated: true, completion: nil)
    }
    
}
extension SubjectDetailsViewController: AttendanceInfoCellDelegate {
    func animateAttendance(cell: AttendanceInfoTableViewCell) {
//        DispatchQueue.main.async {
//            UIView.animate(withDuration: 1.0) {
//                cell.ringProgressView.progress = Double(self.attendance/100)
//                cell.ringProgressView.shadowOpacity = 0
//                if self.attendance >= 75 {
//                    cell.ringProgressView.startColor = .systemGreenColor
//                } else {
//                    cell.ringProgressView.startColor = .systemRedColor
//                }
//                cell.contentView.layoutIfNeeded()
//            }
//        }
    }
    //    func potty(cell: AttendanceCollectionViewCell) {
    //        cell.tableView.register(AttendanceDetailsCell.self, forCellReuseIdentifier: NSStringFromClass(AttendanceDetailsCell.self))
    //        cell.tableView.dataSource = self as SubjectDetailViewController
    //        cell.tableView.delegate = self as SubjectDetailViewController
    //        cell.tableView.reloadDataWithLayout()
    //    }
}
extension SubjectDetailsViewController: TestAttendanceInfoCellDelegate {
    func animateAttendance(cell: TestAttendanceInfoTableViewCell) {
        DispatchQueue.main.async {
//            cell.shapeLayer.strokeEnd = 0.0
            cell.layoutIfNeeded()
            cell.shapeLayer.layoutIfNeeded()
//            cell.animatePulsatingLayer()
            UIView.animate(withDuration: 1.0) {
//                cell.shapeLayer.strokeEnd = CGFloat(self.attendance / 100)
                if let att = self.attendance {
                    
                
                if att > 75 {
                    cell.pulsatingLayer.fillColor = UIColor.systemGreenColor.withAlphaComponent(0.2).cgColor
                    cell.trackLayer.strokeColor = UIColor.systemGreenColor.withAlphaComponent(0.2).cgColor
                    cell.shapeLayer.strokeColor = UIColor.systemGreenColor.cgColor
                } else if att < 75 {
                    cell.pulsatingLayer.fillColor = UIColor.systemPinkColor.withAlphaComponent(0.2).cgColor
                    cell.trackLayer.strokeColor = UIColor.systemPinkColor.withAlphaComponent(0.2).cgColor
                    cell.shapeLayer.strokeColor = UIColor.systemPinkColor.cgColor
                } else {
                    cell.pulsatingLayer.fillColor = UIColor.systemYellowColor.withAlphaComponent(0.2).cgColor
                    cell.trackLayer.strokeColor = UIColor.systemYellowColor.withAlphaComponent(0.2).cgColor
                    cell.shapeLayer.strokeColor = UIColor.systemYellowColor.cgColor
                }
                }
                cell.contentView.layoutIfNeeded()
            }
        }
    }
}

