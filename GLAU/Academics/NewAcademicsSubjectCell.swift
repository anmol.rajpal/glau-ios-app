//
//  NewAcademicsSubjectCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 28/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class NewAcademicsSubjectCell: UITableViewCell {
    var subjectName:String? {
        didSet {
            subjectNameLabel.text = subjectName?.capitalized ?? ""
//            layoutIfNeeded()
        }
    }
    var subjectCode:String? {
        didSet {
            subjectCodeLabel.text = subjectCode?.uppercased() ?? ""
//            layoutIfNeeded()
        }
    }
    var subjectType:String? {
        didSet {
            subjectTypeLabel.text = subjectType?.uppercased() ?? ""
//            layoutIfNeeded()
        }
    }
    var attendance:String? {
        didSet {
            attendanceLabel.text = "\(attendance ?? "") %"
            let percentage:Float = (attendance! as NSString).floatValue
            attendanceLabel.backgroundColor = setUpAttendanceColor(attendance: percentage)
//            layoutIfNeeded()
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
//        setupSubjectViewShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
        override func layoutSubviews() {
            super.layoutSubviews()
            setupSubjectViewShadow()
    }
    func setupSubjectViewShadow() {
        let shadowPath = UIBezierPath(rect: subjectView.layer.bounds)
        subjectView.layer.shadowPath = shadowPath.cgPath
    }
    let subjectView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.shadowRadius = 5.0
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowOpacity = 0.3
        return view
    }()
    let topBarView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = UIColor.systemPurpleColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let subjectNameLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let subjectCodeLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout).bold()
        label.textColor = .gray
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let subjectTypeLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption2)
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.8
        label.layer.shadowOffset = CGSize.zero
        label.layer.shadowRadius = 5
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let attendanceLabel:UILabel = {
        let label = InsetLabel(3, 3, 3, 3)
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        label.textColor = .white
        label.backgroundColor = UIColor.gray
        label.clipsToBounds = true
        label.layer.cornerRadius = 7
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    func setUpViews() {
        contentView.backgroundColor = .clear
        subjectView.addSubview(topBarView)
        subjectView.addSubview(subjectTypeLabel)
        subjectView.addSubview(subjectNameLabel)
        subjectView.addSubview(subjectCodeLabel)
        subjectView.addSubview(attendanceLabel)
        contentView.addSubview(subjectView)
    }
    func setUpConstraints() {
        _ = subjectView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 7, leftConstant: 15, bottomConstant: 7, rightConstant: 15)
        
        _ = topBarView.anchor(subjectView.topAnchor, left: subjectView.leftAnchor, bottom: subjectTypeLabel.bottomAnchor, right: subjectView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -1, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = subjectTypeLabel.anchor(subjectView.topAnchor, left: nil, bottom: nil, right: subjectView.rightAnchor, topConstant: 1, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = subjectNameLabel.anchor(topBarView.bottomAnchor, left: subjectView.leftAnchor, bottom: nil, right: subjectView.rightAnchor, topConstant: 4, leftConstant: 15, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = subjectCodeLabel.anchor(subjectNameLabel.bottomAnchor, left: subjectView.leftAnchor, bottom: subjectView.bottomAnchor, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        attendanceLabel.centerYAnchor.constraint(equalTo: subjectCodeLabel.centerYAnchor).isActive = false
        _ = attendanceLabel.anchor(nil, left: nil, bottom: subjectView.bottomAnchor, right: subjectView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        attendanceLabel.widthAnchor.constraint(equalToConstant: 75).isActive = true
//        let bottomConstraint = attendanceLabel.bottomAnchor.constraint(equalTo: subjectView.bottomAnchor, constant: -10)
        let bottomConstraint = subjectView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -7)
        bottomConstraint.priority = UILayoutPriority(750)
        bottomConstraint.isActive = true
    }
}
