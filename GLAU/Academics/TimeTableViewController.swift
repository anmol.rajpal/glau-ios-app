//
//  TimeTableViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 13/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit
protocol TimeTableDelegateProtocol {
    func cancelButtonTapped()
}
extension TimeTableDelegateProtocol {
    func cancelButtonTapped() {
        
    }
}

class TimeTableViewController: UIViewController {
    let network = NetworkManager.shared
    var timeTable:[TimeTableCodable]?
    var delegate:TimeTableDelegateProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupNavBar()
//        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(handleCancelEvent))
        setUpViews()
        setUpConstraints()
        setUpDatePicker()
        tableView.dataSource = self
        tableView.delegate = self
        
        fetchTodayTimeTable()
        tableView.reloadDataWithLayout()
//        createPanGestureRecognizer(targetView: view)
        checkReachability()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        tableView.reloadData()
        checkReachability()
        setupStatusBarHelpers()
    }
    internal func setupStatusBarHelpers() {
        self.modalPresentationCapturesStatusBarAppearance = true
        self.setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .white
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel(message:String = "Cannot Connect to GLAU") {
//        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        fetchTodayTimeTable()
    }
    private func updateViews() {
        let isEmpty:Bool = !(timeTable?.count ?? 0 > 0)
        tableView.isHidden = isEmpty
        messageLabel.isHidden = !isEmpty
        retryButton.isHidden = !isEmpty
        if !isEmpty { tableView.reloadDataWithLayout() }
    }
//    func checkReachability() {
//        network.reachability.whenUnreachable = { _ in
//            self.dismiss(animated: true, completion: nil)
//        }
//    }

    func getWeekDayName(weekDay:Int) -> String {
        switch weekDay {
            case 0: return "Sunday"
            case 1: return "Monday"
            case 2: return "Tuesday"
            case 3: return "Wednesday"
            case 4: return "Thursday"
            case 5: return "Friday"
            case 6: return "Saturday"
            default: return ""
        }
    }
    func fetchTodayTimeTable() {
        let date = Date()
        datePicker.date = date
        setUpDateFields(date: date)
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        if let day = components.day, let month = components.month, let year = components.year {
            let date = "\(day).\(month).\(year)"
            fetchStudentTimeTable(date: date)
        }
    }
    func setUpDateFields(date:Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let weekDay = formatter.weekdaySymbols[Calendar.current.component(.weekday, from: date) - 1]
        let todayDate = Date()
        let dateString = formatter.string(from: date)
        let todayString = formatter.string(from: todayDate)
        if dateString == todayString {
            todayButton.isHidden = true
            dayLabel.text = "Today"
            formatter.dateFormat = "E, MMMM d, yyyy"
            datePickerTextField.text = formatter.string(from: date)
        } else {
            todayButton.isHidden = false
            dayLabel.text = weekDay
            formatter.dateFormat = "MMMM d, yyyy"
            datePickerTextField.text = formatter.string(from: date)
        }
    }
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
//        let maal = (view.safeAreaInsets.top + view.safeAreaInsets.bottom)
        let touchPoint = sender.location(in: self.view?.window)
        if sender.state == UIGestureRecognizer.State.began {
            
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    func createPanGestureRecognizer(targetView: UIView) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        targetView.addGestureRecognizer(panGesture)
    }
    @objc func handleCancelEvent() {
        
        delegate?.cancelButtonTapped()
//        guard let vc = self.presentingViewController as? TabBarController else { return }
//        (vc.selectedViewController as! UINavigationController).topViewController?.view.layer.shouldRasterize = false
        self.dismiss(animated: true, completion: nil)
    }
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let view = UIVisualEffectView(effect: blurEffect)
//        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        
        return view
    }()
    let cancelButton:UIButton = {
        let origImage = UIImage(named: "cancel")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(handleCancelEvent), for: .touchUpInside)
        return button
    }()
    let todayButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        button.tintColor = .white
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        button.setTitle("Today", for: UIControl.State.normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleTodayButtonEvent), for: .touchUpInside)
        return button
    }()
    @objc func handleTodayButtonEvent() {
        let date = Date()
        datePicker.date = date
        setUpDateFields(date: date)
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        if let day = components.day, let month = components.month, let year = components.year {
            let date = "\(day).\(month).\(year)"
            fetchStudentTimeTable(date: date)
        }
    }
    let nextButton:UIButton = {
        let origImage = UIImage(named: "next-page-60")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleNextButtonEvent), for: .touchUpInside)
        return button
    }()
    let previousButton:UIButton = {
        let origImage = UIImage(named: "previous-page-60")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handlePreviousButtonEvent), for: .touchUpInside)
        return button
    }()
    @objc func handleNextButtonEvent() {
        let currentDate = datePicker.date
        let nextDate = currentDate.add(days:1)
        setUpDateFields(date: nextDate!)
        let components = Calendar.current.dateComponents([.year, .month, .day], from: nextDate!)
        if let day = components.day, let month = components.month, let year = components.year {
            let date = "\(day).\(month).\(year)"
            fetchStudentTimeTable(date: date)
            datePicker.date = nextDate!
        }
    }
    @objc func handlePreviousButtonEvent() {
        let currentDate = datePicker.date
        let previousDate = currentDate.subtract(days:1)
        setUpDateFields(date: previousDate!)
        let components = Calendar.current.dateComponents([.year, .month, .day], from: previousDate!)
        if let day = components.day, let month = components.month, let year = components.year {
            let date = "\(day).\(month).\(year)"
            fetchStudentTimeTable(date: date)
            datePicker.date = previousDate!
        }
    }
    let gifImageView:UIImageView = {
        let imageView = getGifImageView("dogs", loopCount: loopCount.infinite.rawValue)
        imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        return imageView
    }()
    let infoView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
//        view.layer.shadowColor = UIColor.black.cgColor
//        view.layer.shadowOpacity = 1
//        view.layer.shadowOffset = CGSize.zero
//        view.layer.shadowRadius = 5
        view.isHidden = true
        return view
    }()
    let oopsLabel:UILabel = {
        let label = UILabel()
        label.text = "Sorry"
        label.font = UIFont.systemFont(ofSize: 30, weight: .light)
        return label
    }()
    let infoLabel:UILabel = {
        let label = UILabel()
        label.text = "No Data"
        label.textColor = UIColor.systemRedColor
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        return label
    }()
    let dateLabel:UILabel = {
        let label = UILabel()
        label.text = "December 7, 2018"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
        label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    let dayLabel:UILabel = {
        let label = UILabel()
        label.text = "Friday"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).bold()
        label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    let tableView : UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.register(TimeTableSubjectCell.self, forCellReuseIdentifier: NSStringFromClass(TimeTableSubjectCell.self))
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .clear
        tv.separatorStyle = .none
        tv.bounces = true
        tv.alwaysBounceVertical = true
        tv.clipsToBounds = true
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = true
        tv.layer.cornerRadius = 7
        tv.isHidden = true
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.estimatedRowHeight = 100.0
        tv.rowHeight = UITableView.automaticDimension
        return tv
    }()
    let datePickerTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageView.image = #imageLiteral(resourceName: "search")
        imageView.contentMode = .scaleAspectFit
        let leftPaddedView = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: 30))
        textField.rightViewMode = .always
        textField.leftViewMode = .always
        textField.leftView = leftPaddedView
        textField.rightView = imageView
        textField.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        _ = imageView.anchor(textField.topAnchor, left: nil, bottom: textField.bottomAnchor, right: textField.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 3, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
//        imageView.rightAnchor.constraint(equalTo: textField.rightAnchor, constant: -20).isActive = true
        textField.placeholder = "dd/MM/yyyy"
        textField.layer.borderColor = UIColor.glauThemeColor.cgColor
        textField.textColor = .black
        textField.backgroundColor = .white
        textField.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 7
        textField.textAlignment = .center
        
        return textField
    }()
    let datePicker = UIDatePicker()
    func setUpDatePicker() {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneDatePicker));
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        datePickerTextField.inputAccessoryView = toolbar
        datePickerTextField.inputView = datePicker
    }
    @objc func doneDatePicker(){
        self.view.endEditing(true)
        setUpDateFields(date: datePicker.date)
        let components = Calendar.current.dateComponents([.year, .month, .day], from: datePicker.date)
        if let day = components.day, let month = components.month, let year = components.year {
            let date = "\(day).\(month).\(year)"
            fetchStudentTimeTable(date: date)
        }
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    private func setUserInteraction(isEnabled:Bool) {
        todayButton.isUserInteractionEnabled = isEnabled
        previousButton.isUserInteractionEnabled = isEnabled
        nextButton.isUserInteractionEnabled = isEnabled
        datePickerTextField.isUserInteractionEnabled = isEnabled
        tableView.isUserInteractionEnabled = isEnabled
    }
    private func fetchStudentTimeTable(date:String) {
        let rollNo = UserDefaults.standard.getRollNo()
        spinner.startAnimating()
        self.setUserInteraction(isEnabled: false)
        self.cancelButton.isUserInteractionEnabled = true
        hideTableView(tableView: tableView)
        hideInfoView()
        TimeTableService.shared.fetch(rollNo: rollNo, date: date) { (data, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6, execute: {
                guard error == nil else {
                    self.timeTable?.removeAll()
                    self.hideTableView(tableView: self.tableView)
                    self.showInfoView(message: "Cannot Load TimeTable", isRetryButtonHidden: false)
                    self.spinner.stopAnimating()
                    print(error?.localizedDescription ?? "Error")
                    self.setUserInteraction(isEnabled: true)
                    return
                }
                let result = data?.count != 0 ? "Success" : "Failure"
                _ = "No Data"
                
                switch result {
                case ResultType.Failure.rawValue:
//                    print("Failure with message - \(message)")
                    self.timeTable?.removeAll()
                    self.hideTableView(tableView: self.tableView)
                    self.showInfoView()
                    self.spinner.stopAnimating()
                    self.setUserInteraction(isEnabled: true)
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.timeTable = data
                        self.tableView.reloadDataWithLayout()
                    }
                    self.showTableView(tableView: self.tableView)
                    self.tableView.reloadAndScrollToTop()
//                    self.tableView.scrollToTop()
                    self.spinner.stopAnimating()
                    self.setUserInteraction(isEnabled: true)
                default: break
                }
            })
        }
    }
    fileprivate func animateIn(cell: UITableViewCell, withDelay delay: TimeInterval) {
        let duration: TimeInterval = 0.3
        
        cell.alpha = 0.5
        cell.layer.transform = CATransform3DMakeScale(0.7, 0.0, 1)
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
    fileprivate func showTableView(tableView:UITableView) {
//        self.tableView.alpha = 0.0
//        let initialScale:CGFloat = 0.0
//        self.tableView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.tableView.isHidden = false
            
            tableView.layer.transform = CATransform3DIdentity
            tableView.alpha = 1.0
            
        }, completion: nil)
    }
    fileprivate func hideTableView(tableView:UITableView) {
//        self.infoView.alpha = 1.0
//        let initialScale:CGFloat = 1.0
//        self.infoView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//            tableView.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1.0)
            tableView.alpha = 0.0
        }, completion: nil)
    }
    fileprivate func showInfoView(message:String = "No Data", isRetryButtonHidden:Bool = true) {
        self.messageLabel.text = message
        self.retryButton.isHidden = isRetryButtonHidden
//        self.infoView.alpha = 0.0
//        let initialScale:CGFloat = 0.0
//        self.infoView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.infoView.isHidden = false
            self.infoView.alpha = 1.0
            self.infoView.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
    
    fileprivate func hideInfoView() {
//        self.infoView.alpha = 1.0
//        let initialScale:CGFloat = 1.0
//        self.infoView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.infoView.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1)
            self.infoView.alpha = 0.0
        }, completion: nil)
    }
//    fileprivate func hideInfoView() {
//        UIView.animate(withDuration: 0.2, delay: 0.1, options: UIView.AnimationOptions.curveLinear, animations: {
//            self.infoView.alpha = 0.0
//            self.infoView.layer.transform = CATransform3DMakeScale(1.0, 0.1, 0.0)
//            self.infoView.isHidden = true
//        }, completion: nil)
//    }
    func setUpViews() {
        view.addSubview(blurredEffectView)
        blurredEffectView.frame = self.view.bounds
        view.addSubview(todayButton)
        view.addSubview(cancelButton)
//        view.addSubview(dateLabel)
        
        view.addSubview(dayLabel)
        view.addSubview(previousButton)
        view.addSubview(nextButton)
        view.addSubview(datePickerTextField)
        view.addSubview(tableView)
        view.addSubview(infoView)
        view.addSubview(spinner)
        infoView.addSubview(messageLabel)
        infoView.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
        infoView.addSubview(gifImageView)
//        infoView.addSubview(oopsLabel)
//        infoView.addSubview(infoLabel)
        
    }
    func setUpConstraints() {
        todayButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        todayButton.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 15).isActive = true
        todayButton.layoutIfNeeded()
        let cancelBtnHeight = todayButton.frame.height
        _ = cancelButton.anchor(nil, left: nil, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: cancelBtnHeight, heightConstant: cancelBtnHeight)
        cancelButton.centerYAnchor.constraint(equalTo: todayButton.centerYAnchor).isActive = true
        
        dayLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 60).isActive = true
        dayLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = previousButton.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: nil
            , topConstant: 58, leftConstant: view.frame.width/5 - 5, bottomConstant: 0, rightConstant: 0, widthConstant: 35, heightConstant: 35)
        _ = nextButton.anchor(view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor
            , topConstant: 58, leftConstant: 0, bottomConstant: 0, rightConstant: view.frame.width/5 - 5, widthConstant: 35, heightConstant: 35)
        datePickerTextField.topAnchor.constraint(equalTo: dayLabel.bottomAnchor, constant: 10).isActive = true
        datePickerTextField.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 10).isActive = true
        datePickerTextField.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -10).isActive = true
        datePickerTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
//        datePickerTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        _ = tableView.anchorWithoutWH(top: datePickerTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
//        tableView.topAnchor.constraint(equalTo: datePickerTextField.bottomAnchor, constant: 10).isActive = true
//        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
//        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
//        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        
        
        
        _ = infoView.anchorWithoutWH(top: datePickerTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: retryButton.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 100, leftConstant: 15, bottomConstant: -20, rightConstant: 15)
        infoView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        infoView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        _ = gifImageView.anchor(infoView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 150, heightConstant: 150)
        gifImageView.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
//        _ = oopsLabel.anchor(gifImageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        oopsLabel.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
//        _ = infoLabel.anchor(oopsLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        infoLabel.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
        
        
        
//        messageLabel.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
        _ = messageLabel.anchorWithoutWH(top: gifImageView.bottomAnchor, left: infoView.leftAnchor, bottom: nil, right: infoView.rightAnchor, topConstant: 15, leftConstant: 20, bottomConstant: 0, rightConstant: 20)
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 5).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true

    }
}
extension TimeTableViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeTable?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TimeTableSubjectCell.self), for: indexPath) as! TimeTableSubjectCell
//        animateIn(cell: cell, withDelay: 0.0)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        let subjectData = timeTable?[indexPath.row]
        cell.timeTable = subjectData
//        cell.layoutIfNeeded()
//        cell.layer.borderWidth = 20
//        cell.layer.borderColor = tableView.backgroundColor?.cgColor
//        cell.layer.masksToBounds = false
//        cell.setNeedsUpdateConstraints()
//        cell.updateConstraintsIfNeeded()
//        cell.setNeedsLayout()
//        cell.layoutIfNeeded()
//        tableView.reloadDataWithLayout()
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
