//
//  AttendanceCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 27/09/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
//import MKRingProgressView
protocol AttendanceCellDelegate {
    func animateAttendance(cell:AttendanceCollectionViewCell)
//    func potty(cell:AttendanceCollectionViewCell)
}
class AttendanceCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {
    
    var delegate:AttendanceCellDelegate?
    var subjectCode = String()
    var attendanceDetails = [AttendanceDetailsCodable]()
//    var attendance:Float = 0
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setUpViews()
        setUpConstraints()
        configureTableView()
//        setUpRingProgressView()
        fetchAttendanceDetails()
        tableView.reloadDataWithLayout()
    }
    private func fetchAttendanceDetails() {
        let rollNo = UserDefaults.standard.getRollNo()
        AttendanceDetailsService.shared.fetch(rollNo: rollNo, subjectCode: subjectCode) { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    //                    self.updateViews()
                    //                    self.spinner.stopAnimating()
                    //                    self.timeTable.removeAll()
                    //                    self.hideTableView(tableView: self.tableView)
                    //                    self.showInfoView()
                    print(error?.localizedDescription ?? "Error")
                    return
                }
                let result = data?[0].result
                let message = data?[0].message
                
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(String(describing: message))")
                    //                    self.spinner.stopAnimating()
                    //                    self.timeTable.removeAll()
                    //                    self.hideTableView(tableView: self.tableView)
                //                    self.showInfoView()
                case ResultType.Success.rawValue:
                    
                    if let data = data {
                        self.attendanceDetails = data
                        self.tableView.reloadDataWithLayout()
//                        print(self.attendanceDetails)
                        //                        self.hideInfoView()
                        //                        self.showTableView(tableView: self.tableView)
                    }
                    //                    self.updateViews()
                    //                    self.spinner.stopAnimating()
                    
                default: break
                }
            }
        }
    }
    private func updateViews() {
        let hasData = attendanceDetails.count > 0
        
//        tableView.isHidden = !hasData
//        messageLabel.isHidden = hasData
//        retryButton.isHidden = hasData
        if hasData {
            tableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendanceDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AttendanceDetailsCell.self), for: indexPath) as! AttendanceDetailsCell
        cell.selectionStyle = .none
        let details = attendanceDetails[indexPath.row]
        cell.setUpAttendanceDetails(details: details)
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func setUpRingProgressView() {
        DispatchQueue.main.async {
//            self.contentView.addSubview(self.ringProgressView)
            self.animateRingProgressView()
        }
    }
    func animateRingProgressView() {
        //        delegate?.animateAttendance(cell: self)
    }
    
//    lazy var ringProgressView:RingProgressView = {
//        let view = RingProgressView(frame: CGRect(x: 0, y: 0, width: self.attendanceLabel.frame.width + 50, height: self.attendanceLabel.frame.width + 50))
//        view.startColor = .systemRedColor
//        view.endColor = .systemGreenColor
//        view.ringWidth = 15
//        view.progress = 0.0
//        view.shadowOpacity = 0.3
//        view.center = self.attendanceLabel.center
//        view.clipsToBounds = true
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
    let containerView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = .clear
        view.clipsToBounds = true
        return view
    }()
    let headerView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = UIColor.systemPurpleColor
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner
        return view
    }()
    let headerLabel:UILabel = {
        let label = UILabel()
        label.text = "Details"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        label.translatesAutoresizingMaskIntoConstraints = false
//        label.layer.shadowColor = UIColor.black.cgColor
//        label.layer.shadowOpacity = 0.8
//        label.layer.shadowOffset = CGSize.zero
//        label.layer.shadowRadius = 5
        return label
    }()
    let tableView : UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.bounces = true
        tv.alwaysBounceVertical = true
        tv.clipsToBounds = true
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = true
        tv.layer.cornerRadius = 10
        tv.isHidden = false
        tv.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
//        tv.backgroundColor = .red
        tv.layer.borderColor = UIColor.systemPurpleColor.cgColor
        tv.layer.borderWidth = 1
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.estimatedRowHeight = 50.0
        tv.rowHeight = UITableView.automaticDimension
        return tv
    }()
    let attendanceLabel:UILabel = {
//        let label = AnimatedLabel(22, 22, 50, 50)
//        label.decimalPoints = .two
//        label.count(from: self.attendance/2.0, to: attendance)
//        label.appendString = " %"
//        label.textColor = .white
//        if self.attendance < Config.requiredAttendance {
//            label.backgroundColor = UIColor.systemRedColor
//        } else if self.attendance > Config.requiredAttendance {
//            label.backgroundColor = UIColor.systemGreenColor
//        } else {
//            label.backgroundColor = UIColor.systemYellowColor
//        }
        let label = InsetLabel(10, 10, 30, 30)
        label.font = UIFont.systemFont(ofSize: 40, weight: UIFont.Weight.light)
        label.text = " %"
        label.textColor = .white
        label.textAlignment = .center
        label.clipsToBounds = true
        label.layer.cornerRadius = 10
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    func setAttributedAttendance(attendance:Float) {
        let attendanceString = String(attendance)
        let group = attendanceString.split(separator: ".")
        let valueString = group[0]
        let precisionString = group[1]
        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 35, weight: .light)])
        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15, weight: .light)])
        let percentageAttributedString = NSAttributedString(string: " %", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .bold)])
        let attendanceAttributedString = NSMutableAttributedString()
        if Int(precisionString) == 0 {
            attendanceAttributedString.append(valueAttributedString)
            attendanceAttributedString.append(percentageAttributedString)
        } else {
            attendanceAttributedString.append(valueAttributedString)
            attendanceAttributedString.append(NSAttributedString(string: "."))
            attendanceAttributedString.append(precisionAttributedString)
            attendanceAttributedString.append(percentageAttributedString)
        }
        attendanceLabel.attributedText = attendanceAttributedString
    }
    
    func configureTableView() {
        tableView.register(AttendanceDetailsCell.self, forCellReuseIdentifier: NSStringFromClass(AttendanceDetailsCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.scrollToBottom(animated: true)
        tableView.reloadDataWithLayout()
    }
    func setUpViews() {
        containerView.addSubview(attendanceLabel)
        headerView.addSubview(headerLabel)
        containerView.addSubview(headerView)
        containerView.addSubview(tableView)
        contentView.addSubview(containerView)
    }
    func setUpConstraints() {
//        attendanceLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        _ = containerView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: UIScreen.main.bounds.width)
        attendanceLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        attendanceLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 10).isActive = true
        attendanceLabel.rightAnchor.constraint(equalTo: containerView.centerXAnchor, constant: -10).isActive = true
        attendanceLabel.widthAnchor.constraint(equalToConstant: (containerView.bounds.width / 2) - 20).isActive = true
        _ = headerView.anchor(containerView.topAnchor, left: tableView.leftAnchor, bottom: nil, right: tableView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        _ = headerLabel.anchorWithoutWH(top: headerView.topAnchor, left: headerView.leftAnchor, bottom: headerView.bottomAnchor, right: nil, topConstant: 2, leftConstant: 15, bottomConstant: 2, rightConstant: 0)
        _ = tableView.anchor(headerView.bottomAnchor, left: containerView.centerXAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 140)
    }
}
