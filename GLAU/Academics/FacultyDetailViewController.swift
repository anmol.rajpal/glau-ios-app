
//
//  FacultyDetailViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 05/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
import MessageUI
class FacultyDetailViewController: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    var email:String? {
        didSet {
            emailTextView.text = email ?? ""
        }
    }
    var phone:String? {
        didSet {
            phoneTextView.text = phone ?? ""
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        setUpConstraints()
//        createPanGestureRecognizer(targetView: view)
    }
    lazy var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    
    @objc func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    func createPanGestureRecognizer(targetView: UIView) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerHandler(_:)))
        targetView.addGestureRecognizer(panGesture)
    }
    
    @objc func handleCancelEvent() {
        self.dismiss(animated: true, completion: nil)
    }
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let view = UIVisualEffectView(effect: blurEffect)
        view.clipsToBounds = true
        return view
    }()
    let cancelButton:UIButton = {
        let origImage = UIImage(named: "cancel")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = .systemPinkColor
        button.addTarget(self, action: #selector(handleCancelEvent), for: .touchUpInside)
        return button
    }()
    let imageView:UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "userIcon"))
        imageView.frame = CGRect.zero
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    let facultyNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 22, weight: .heavy)
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1).bold()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        return label
    }()
    let callButton:UIButton = {
        let origImage = UIImage(named: "phone")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setImage(tintedImage, for: UIControl.State.normal)
        button.contentEdgeInsets = buttonInsets
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .systemPinkColor
        button.layer.cornerRadius = 7
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(callFaculty), for: .touchUpInside)
        button.addAction(for: UIControl.Event.touchDragOutside, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        })
        button.addAction(for: UIControl.Event.touchDown, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        })
        return button
    }()
    let mailButton:UIButton = {
        let origImage = UIImage(named: "message")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setImage(tintedImage, for: UIControl.State.normal)
        button.contentEdgeInsets = buttonInsets
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .systemPinkColor
        button.layer.cornerRadius = 7
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(mailFaculty), for: .touchUpInside)
        button.addAction(for: UIControl.Event.touchDragOutside, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        })
        button.addAction(for: UIControl.Event.touchDown, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        })
        return button
    }()
    static let buttonInsets:UIEdgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 5, right: 15)
    let messageButton:UIButton = {
        let origImage = UIImage(named: "chat")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setImage(tintedImage, for: UIControl.State.normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.contentEdgeInsets = buttonInsets
        button.tintColor = .systemPinkColor
        button.layer.cornerRadius = 7
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(messageFaculty), for: .touchUpInside)
        button.addAction(for: UIControl.Event.touchDragOutside, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        })
        button.addAction(for: UIControl.Event.touchDown, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        })
        return button
    }()
    let feedbackButton:UIButton = {
        let origImage = UIImage(named: "feedback")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.setImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = .systemPinkColor
        button.layer.cornerRadius = 7
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        button.imageEdgeInsets = buttonInsets
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
//        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        button.isEnabled = false
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(feedbackFaculty), for: .touchUpInside)
        button.addAction(for: UIControl.Event.touchDragOutside, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        })
        button.addAction(for: UIControl.Event.touchDown, {
            button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
        })
        
        return button
    }()
    lazy var stackView:UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
//        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        stackView.axis = .horizontal
//        stackView.spacing = 50
        stackView.distribution = .equalSpacing
//        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    @objc func callFaculty() {
        callButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
//        UIAlertController.showAlert(alertTitle: "Call Faculty", message: "This feature is currently not supported on iOS", alertActionTitle: "Dismiss", controller: self)
        if let url = URL(string: "tel://\(self.phone ?? "")"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    @objc func mailFaculty() {
        mailButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        let mailComposeViewController = configureMailComposeViewController()
        guard MFMailComposeViewController.canSendMail() else {
            self.showSendMailErrorAlert()
            return
        }
        self.present(mailComposeViewController, animated: true, completion: nil)
    }
    @objc func messageFaculty() {
        messageButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
//        UIAlertController.showAlert(alertTitle: "Message Faculty", message: "This feature is currently not supported on iOS", alertActionTitle: "Dismiss", controller: self)
        let messageComposeVC = configureMessageComposeViewController()
        guard MFMessageComposeViewController.canSendText() else {
            self.showSendMessageErrorAlert()
            return
        }
        self.present(messageComposeVC, animated: true, completion: nil)
    }
    @objc func feedbackFaculty() {
        feedbackButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        UIAlertController.showAlert(alertTitle: "Feedback", message: "This feature is currently not supported on iOS", alertActionTitle: "Dismiss", controller: self)
    }
    func configureMailComposeViewController() -> MFMailComposeViewController
    {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients([email ?? ""])
//        mailComposerVC.setSubject("App Feedback")
        mailComposerVC.setMessageBody("Respected Sir/Mam,\n", isHTML: false)
        return mailComposerVC
    }
    
    func showSendMailErrorAlert()
    {
        UIAlertController.showAlert(alertTitle: "Could Not Send Email", message: "Your device could not send e-mail. Make sure you have an Active Internet Connection", alertActionTitle: "Ok", controller: self)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("mail cancelled")
        case MFMailComposeResult.sent.rawValue:
            print("mail sent")
        default: break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func configureMessageComposeViewController() -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        messageComposeVC.recipients = [self.phone ?? ""]
        return messageComposeVC
    }
    func showSendMessageErrorAlert()
    {
        UIAlertController.showAlert(alertTitle: "Could Not Send Message", message: "Your device could not send message. Make sure you have a Connection", alertActionTitle: "Ok", controller: self)
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    let emailLabel:UILabel = {
        let label = UILabel()
        label.text = "email"
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        label.adjustsFontForContentSizeCategory = true
        label.textColor = .black
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let emailTextView:UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.textAlignment = .left
        textView.isSelectable = true
        textView.backgroundColor = .clear
        textView.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        textView.adjustsFontForContentSizeCategory = true
        textView.textColor = UIColor.systemPinkColor
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.dataDetectorTypes = .all
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        return textView
    }()
    let phoneLabel:UILabel = {
        let label = UILabel()
        label.text = "phone"
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        label.adjustsFontForContentSizeCategory = true
        label.textColor = .black
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let phoneTextView:UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.textAlignment = .left
        textView.isSelectable = true
        textView.backgroundColor = .clear
        textView.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        textView.adjustsFontForContentSizeCategory = true
        textView.textColor = UIColor.systemPinkColor
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.dataDetectorTypes = .all
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        return textView
    }()
    let horizontalLine = HorizontalLine(color: UIColor.lightGray)
    func setUpViews() {
        blurredEffectView.frame = self.view.bounds
        view.addSubview(blurredEffectView)
        view.addSubview(cancelButton)
        view.addSubview(imageView)
        view.addSubview(facultyNameLabel)
        view.addSubview(stackView)
        stackView.addArrangedSubview(mailButton)
        stackView.addArrangedSubview(callButton)
        stackView.addArrangedSubview(messageButton)
        stackView.addArrangedSubview(feedbackButton)
        imageView.layer.cornerRadius = self.imageView.bounds.height/2
        view.addSubview(horizontalLine)
        view.addSubview(emailLabel)
        view.addSubview(emailTextView)
        view.addSubview(phoneLabel)
        view.addSubview(phoneTextView)
    }
    func setUpConstraints() {
        _ = cancelButton.anchor(view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 40, heightConstant: 40)
        _ = imageView.anchor(cancelButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.view.bounds.width/3, heightConstant: self.view.bounds.width/3)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = facultyNameLabel.anchor(imageView.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        facultyNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        _ = stackView.anchorWithoutWH(top: facultyNameLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        mailButton.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 15).isActive = true
        callButton.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 15).isActive = true
        messageButton.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 15).isActive = true
        feedbackButton.widthAnchor.constraint(equalToConstant: (view.bounds.width/4) - 15).isActive = true
        _ = horizontalLine.anchor(stackView.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0.5)
        _ = emailLabel.anchorWithoutWH(top: horizontalLine.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
        _ = emailTextView.anchorWithoutWH(top: emailLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 15, rightConstant: 15)
        _ = emailTextView.heightAnchor.constraint(equalToConstant: 25).isActive = false
        _ = phoneLabel.anchorWithoutWH(top: emailTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
        _ = phoneTextView.anchorWithoutWH(top: phoneLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 15, rightConstant: 15)
//        let fixedWidth = emailTextView.frame.size.width
//        let newSize = emailTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        emailTextView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
    }
}
