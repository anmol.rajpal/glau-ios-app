//
//  SubjectInfoCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 15/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit
protocol CustomCellDelegate: class {
    func morePressed(cell:SubjectInfoCollectionViewCell)
    func disclosurePressed(cell:SubjectInfoCollectionViewCell)
}
class SubjectInfoCollectionViewCell: UICollectionViewCell {
    var delegate:CustomCellDelegate?
    override func layoutSubviews() {
        super.layoutSubviews()
//        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        self.setUpViews()
        self.setUpConstraints()
    }
    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        layoutIfNeeded()
//        let layoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
//        layoutAttributes.bounds.size.height = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
//        return layoutAttributes
//    }
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        setNeedsLayout()
//        layoutIfNeeded()
//        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//        var frame = layoutAttributes.frame
//        frame.size.height = ceil(size.height)
//        layoutAttributes.frame = frame
//        return layoutAttributes
//    }
    //forces the system to do one layout pass
//    var isHeightCalculated: Bool = false
//    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        //Exhibit A - We need to cache our calculation to prevent a crash.
//        if !isHeightCalculated {
//            setNeedsLayout()
//            layoutIfNeeded()
//            let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//            var newFrame = layoutAttributes.frame
//            newFrame.size.width = CGFloat(ceilf(Float(size.width)))
//            layoutAttributes.frame = newFrame
//            isHeightCalculated = true
//        }
//        return layoutAttributes
//    }
    let containerView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = .clear
        view.clipsToBounds = true
        return view
    }()
    let topBarView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = UIColor.systemPurpleColor
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    let subjectTypeLabel:UILabel = {
        let label = UILabel()
        label.text = SubjectType.theory.rawValue.uppercased()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.8
        label.layer.shadowOffset = CGSize.zero
        label.layer.shadowRadius = 10
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let subjectNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 26, weight: .heavy)
        label.textColor = .black
        label.text = "Default Subject"
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let subjectCodeLabel:UILabel = {
        let label = UILabel()
        label.text = "CSE CODE"
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let facultyNameLabel:UILabel = {
        let label = UILabel()
        label.text = "Some Teacher"
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let facultyHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "(Faculty)"
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var actionButton:UIButton = {
        let origImage = UIImage(named: "more")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = UIColor.systemBlueColor
        button.backgroundColor = UIColor.white
        button.addTarget(self, action: #selector(showActions), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var facultyDisclosureButton:UIButton = {
        let origImage = UIImage(named: "info")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = UIColor.systemBlueColor
        button.backgroundColor = UIColor.white
        button.addTarget(self, action: #selector(showFacultyVC), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    @objc func showFacultyVC() {
        delegate?.disclosurePressed(cell: self)
    }
    @objc func showActions() {
        delegate?.morePressed(cell: self)
    }
//    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
//        let a  = bounds.size.width
//        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
//    }
    func setUpViews() {
//        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addSubview(topBarView)
        topBarView.addSubview(subjectTypeLabel)
        containerView.addSubview(subjectNameLabel)
        containerView.addSubview(facultyNameLabel)
        containerView.addSubview(facultyHeadingLabel)
        containerView.addSubview(actionButton)
        containerView.addSubview(facultyDisclosureButton)
        contentView.addSubview(containerView)
    }
    func setUpConstraints() {
        _ = containerView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: UIScreen.main.bounds.width)
        _ = topBarView.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: subjectTypeLabel.bottomAnchor, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -1, rightConstant: 0, widthConstant: containerView.bounds.width, heightConstant: 0)
        _ = subjectTypeLabel.anchorWithoutWH(top: topBarView.topAnchor, left: nil, bottom: nil, right: topBarView.rightAnchor, topConstant: 1, leftConstant: 0, bottomConstant: 0, rightConstant: 10)
        _ = subjectNameLabel.anchorWithoutWH(top: topBarView.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0)
        _ = facultyNameLabel.anchorWithoutWH(top: subjectNameLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0)
        _ = facultyHeadingLabel.anchorWithoutWH(top: facultyNameLabel.bottomAnchor, left: facultyNameLabel.leftAnchor, bottom: containerView.bottomAnchor, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 5, rightConstant: 0)
        _ = facultyDisclosureButton.anchor(nil, left: facultyHeadingLabel.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        facultyDisclosureButton.centerYAnchor.constraint(equalTo: facultyHeadingLabel.centerYAnchor).isActive = true
        _ = actionButton.anchor(nil, left: nil, bottom: nil, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 24, heightConstant: 24)
        actionButton.centerYAnchor.constraint(equalTo: facultyDisclosureButton.centerYAnchor).isActive = true
    }
}
