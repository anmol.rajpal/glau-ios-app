//
//  AcademicsSubjectsCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 10/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class AcademicsSubjectsCell: AcademicsSubjectsBaseCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    lazy var subjectView:UIView = {
        let view = UIView(frame: CGRect(x: AcademicsSubjectsBaseCell.kInnerMargin,
                                        y: AcademicsSubjectsBaseCell.kInnerMargin,
                                        width: bounds.width - (2 * AcademicsSubjectsBaseCell.kInnerMargin),
                                        height: AcademicsSubjectsBaseCell.cellHeight - (2 * AcademicsSubjectsBaseCell.kInnerMargin)))
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
        return view
    }()
    let topBarView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = UIColor.systemPurpleColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner
        return view
    }()
    let subjectNameLabel:UILabel = {
        let label = UILabel()
        label.text = "Computer Science & Engineering"
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        return label
    }()
    let subjectCodeLabel:UILabel = {
        let label = UILabel()
        label.text = "CSE-6002"
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .gray
        return label
    }()
    let subjectTypeLabel:UILabel = {
        let label = UILabel()
        label.text = SubjectType.theory.rawValue.uppercased()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.8
        label.layer.shadowOffset = CGSize.zero
        label.layer.shadowRadius = 5
        return label
    }()
    let attendanceLabel:UILabel = {
        let label = InsetLabel(3, 3, 3, 3)
        label.text = "89.96 %"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.light)
        label.textColor = .white
        label.backgroundColor = UIColor.systemRedColor
        label.clipsToBounds = true
        label.layer.cornerRadius = 7
        return label
    }()
    func setUpViews() {
        subjectView.addSubview(topBarView)
        subjectView.addSubview(subjectTypeLabel)
        subjectView.addSubview(subjectNameLabel)
        subjectView.addSubview(subjectCodeLabel)
        subjectView.addSubview(attendanceLabel)
        contentView.addSubview(subjectView)
    }
    func setUpConstraints() {
        _ = topBarView.anchor(subjectView.topAnchor, left: subjectView.leftAnchor, bottom: subjectTypeLabel.bottomAnchor, right: subjectView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -3, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = subjectNameLabel.anchor(topBarView.bottomAnchor, left: subjectView.leftAnchor, bottom: nil, right: subjectView.rightAnchor, topConstant: 4, leftConstant: 15, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: 0)
        _ = subjectCodeLabel.anchor(nil, left: subjectView.leftAnchor, bottom: subjectView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 15, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = subjectTypeLabel.anchor(subjectView.topAnchor, left: nil, bottom: nil, right: subjectView.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = attendanceLabel.anchor(nil, left: nil, bottom: subjectView.bottomAnchor, right: subjectView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        attendanceLabel.widthAnchor.constraint(equalToConstant: 75).isActive = true
    }
}
