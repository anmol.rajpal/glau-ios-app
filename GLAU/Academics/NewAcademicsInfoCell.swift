//
//  NewAcademicsInfoCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 28/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
protocol NewAcademicsInfoCellDelegate: class {
    func disclosurePressed(cell:NewAcademicsInfoCell)
    func infoBoxTapped(cell:NewAcademicsInfoCell)
}
class NewAcademicsInfoCell: UITableViewCell, CAAnimationDelegate {
    var classAdvisor:String? {
        didSet {
            setUpAttributedLabel(label: classAdvisorLabel, labelText: "Class Advisor:  ", valueText: classAdvisor?.capitalized ?? "")
//            layoutIfNeeded()
        }
    }
    weak var delegate:NewAcademicsInfoCellDelegate?
    var studentDetails:StudentDetailsCodable? {
        didSet {
            setUpCellData(details: studentDetails)
//            layoutIfNeeded()
//            self.setConstraint(isEnabled: false)

        }
    }
    func setConstraint(isEnabled:Bool) {
        let constraint = infoView.heightAnchor.constraint(equalToConstant: 0)
        constraint.priority = UILayoutPriority(750)
        constraint.isActive = isEnabled
//        layoutIfNeeded()
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setUpViews()
//        setUpConstraints()
//
//    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
//        addTouchEvent()
//        handleInfoBoxTouchDelegate()
//        layoutIfNeeded()
//        setConstraint(isEnabled: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        infoView.layoutIfNeeded()
        let shadowPath = UIBezierPath(rect: infoView.layer.bounds)
        infoView.layer.shadowPath = shadowPath.cgPath
        handleInfoBoxTouchDelegate()
        createGradientView()
        setupNotificationObservers()
    }
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    @objc private func handleEnterForeground() {
        createGradientView()
    }
    func cellWillDisplaySignalling() {
        createGradientView()
    }
    func setUpDetails(details:StudentDetailsCodable?) {
        let sectionStr = details?.section
        let sectionDetails = getSectionDetails(from: sectionStr)
        let section = sectionDetails.section
        let rollNo = sectionDetails.rollNo
        setAttributedLabel(label: nameLabel, labelText: "", valueText: details?.name?.capitalized ?? "--", valueTextFontSize: 18, valueTextFontWeight: .heavy, valueTextColor: .darkGray)
        setAttributedLabel(label: univRollNoLabel, labelText: "", valueText: details?.rollNo ?? "--", valueTextFontSize: 18, valueTextFontWeight: .heavy, valueTextColor: .lightGray)
        setAttributedLabel(label: semesterLabel, labelText: "Semester:  ", valueText: details?.semester ?? "--")
        setAttributedLabel(label: cpiLabel, labelText: "CPI:  ", valueText: details?.cPI ?? "--")
        setAttributedLabel(label: sectionLabel, labelText: "Section:  ", valueText: section ?? "--")
        setAttributedLabel(label: classRollNoLabel, labelText: "Roll No:  ", valueText: rollNo ?? "--")
        setAttributedLabel(label: classAdvisorLabel, labelText: "Class Advisor:  ", valueText: self.classAdvisor?.capitalized ?? "--")
        setAttributedLabel(label: statusLabel, labelText: "", valueText: details?.status ?? "--")
    }
    func setUpCellData(details:StudentDetailsCodable?) {
        let sectionStr = details?.section
        let sectionDetails = getSectionDetails(from: sectionStr)
        let section = sectionDetails.section
        let rollNo = sectionDetails.rollNo
        nameLabel.text = details?.name ?? "--"
//        setUpAttributedLabel(label: nameLabel, labelText: "", valueText: details?.name?.capitalized ?? "--", valueTextFontStyle: .title2, valueTextColor: .darkGray)
        setUpAttributedLabel(label: univRollNoLabel, labelText: "", valueText: details?.rollNo ?? "--", valueTextFontStyle: .title3, valueTextColor: .lightGray)
        setUpAttributedLabel(label: semesterLabel, labelText: "Semester:  ", valueText: details?.semester ?? "--")
        setUpAttributedLabel(label: cpiLabel, labelText: "CPI:  ", valueText: details?.cPI ?? "--")
        setUpAttributedLabel(label: sectionLabel, labelText: "Section:  ", valueText: section == "No" ? "--" : section ?? "--")
        setUpAttributedLabel(label: classRollNoLabel, labelText: "Roll No:  ", valueText: rollNo ?? "--")
        setUpAttributedLabel(label: classAdvisorLabel, labelText: "Class Advisor:  ", valueText: self.classAdvisor?.capitalized ?? "--")
        setUpAttributedLabel(label: statusLabel, labelText: "", valueText: details?.status ?? "--")
        setUpAttributedLabel(label: residentLabel, labelText: "Resident: ", valueText: details?.resident ?? "--")
    }
    func setUpAttributedLabel(label:UILabel, labelText:String, labelTextFontStyle:UIFont.TextStyle = .footnote, labelTextColor:UIColor = .gray, valueText:String, valueTextFontStyle:UIFont.TextStyle = .callout, valueTextColor:UIColor = .black) {
        let labelAttributedString = NSAttributedString(string: labelText, attributes: [NSAttributedString.Key.font:UIFont.preferredFont(forTextStyle: labelTextFontStyle), NSAttributedString.Key.foregroundColor:labelTextColor])
        let valueAttributedString = NSAttributedString(string: valueText, attributes: [NSAttributedString.Key.font:UIFont.preferredFont(forTextStyle: valueTextFontStyle), NSAttributedString.Key.foregroundColor:valueTextColor])
        let attributedString = NSMutableAttributedString()
        attributedString.append(labelAttributedString)
        attributedString.append(valueAttributedString)
        label.attributedText = attributedString
    }
    func setAttributedLabel(label:UILabel, labelText:String, labelTextFontSize:CGFloat = 14, labelTextFontWeight:UIFont.Weight = .medium, labelTextColor:UIColor = .gray, valueText:String, valueTextFontSize:CGFloat = 16, valueTextFontWeight:UIFont.Weight = .semibold, valueTextColor:UIColor = .black) {
        let labelAttributedString = NSAttributedString(string: labelText, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: labelTextFontSize, weight: labelTextFontWeight), NSAttributedString.Key.foregroundColor:labelTextColor])
        let valueAttributedString = NSAttributedString(string: valueText, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: valueTextFontSize, weight: valueTextFontWeight), NSAttributedString.Key.foregroundColor:valueTextColor])
        let attributedString = NSMutableAttributedString()
        attributedString.append(labelAttributedString)
        attributedString.append(valueAttributedString)
        label.attributedText = attributedString
    }
//    internal func setUpAttributedCpi(cpi:Float) {
//        let cpiString = String(format:"%.2f", cpi)
//        let group = cpiString.split(separator: ".")
//        let valueString = group[0]
//        let precisionString = group[1]
//        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 40, weight: .light)])
//        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 25, weight: .light)])
//        let cpiTitleAttributedString = NSAttributedString(string: "cpi".uppercased(), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 13, weight: .heavy), NSAttributedString.Key.foregroundColor:UIColor.lightGray])
//        let cpiAttributedString = NSMutableAttributedString()
//        cpiAttributedString.append(valueAttributedString)
//        cpiAttributedString.append(NSAttributedString(string: "."))
//        cpiAttributedString.append(precisionAttributedString)
//        cpiAttributedString.append(NSAttributedString(string: "\n"))
//        cpiAttributedString.append(cpiTitleAttributedString)
//        cpiLabel.attributedText = cpiAttributedString
//    }
//    func addTouchEvent() {
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(self.handleTouchEvent(_:)))
//        self.infoView.addGestureRecognizer(gestureRecognizer)
//    }
    func handleInfoBoxTouchDelegate() {
        delegate?.infoBoxTapped(cell: self)
    }
    let infoView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        view.layer.shadowRadius = 5.0
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowOpacity = 0.2
        view.layer.cornerRadius = 15
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(handleTouchEvent(_:)))
//        view.addGestureRecognizer(gestureRecognizer)
//        view.layer.borderWidth = 1
//        view.layer.borderColor = UIColor.glauThemeColor.cgColor
//        view.contentMode = .scaleAspectFit
//        view.backgroundColor = UIColor(patternImage: UIImage(named: "blabla.jpg")!)
        
        return view
    }()
    
    
    
    
    
    let gradient = CAGradientLayer()
    
    // list of array holding 2 colors
    var gradientSet = [[CGColor]]()
    // current gradient index
    var currentGradient: Int = 0
    
    // colors to be added to the set
    let colorOne = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1).cgColor
    let colorTwo = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor
    let colorFour = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1).cgColor
    let colorFive = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1).cgColor
    let colorSix = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
    /// Creates gradient view
    
    func createGradientView() {
        
        // overlap the colors and make it 3 sets of colors
        gradientSet.append([colorOne, colorTwo])
        gradientSet.append([colorTwo, colorFour])
//        gradientSet.append([colorThree, colorFour])
        gradientSet.append([colorFour, colorFive])
        gradientSet.append([colorFive, colorSix])
        gradientSet.append([colorSix, colorOne])
        
        // set the gradient size to be the entire screen
        gradient.frame = infoView.bounds
        gradient.colors = gradientSet[currentGradient]
        gradient.startPoint = CGPoint(x:0, y:0)
        gradient.endPoint = CGPoint(x:1, y:1)
        gradient.drawsAsynchronously = true
        
        infoView.layer.insertSublayer(gradient, at: 0)
        
        animateGradient()
    }
    
   
    
    func animateGradient() {
        // cycle through all the colors, feel free to add more to the set
        if currentGradient < gradientSet.count - 1 {
            currentGradient += 1
        } else {
            currentGradient = 0
        }
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 1.5
        gradientChangeAnimation.toValue = gradientSet[currentGradient]
        gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
        gradientChangeAnimation.isRemovedOnCompletion = false
        gradientChangeAnimation.delegate = self
        gradient.add(gradientChangeAnimation, forKey: "gradientChangeAnimation")
    }
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        // if our gradient animation ended animating, restart the animation by changing the color set
        if flag {
            gradient.colors = gradientSet[currentGradient]
            animateGradient()
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    let customImageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "blabla"))
        imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        imageView.contentMode = UIImageView.ContentMode.scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let view = UIVisualEffectView(effect: blurEffect)
        view.layer.cornerRadius = 15
        //        view.frame = self.view.bounds
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3).bold()
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let univRollNoLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .gray
        label.textAlignment = .right
        label.numberOfLines = 1
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let semesterLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 1
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let sectionLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 1
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let classAdvisorLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let residentLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let classRollNoLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 1
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let cpiLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 1
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let overallAttendanceLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 1
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let horizontalSeparator1 = HorizontalLine(color: UIColor.gray)
    let horizontalSeparator2 = HorizontalLine(color: UIColor.gray)
    let statusLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 1
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    
    let advisorHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "(Class Advisor)"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.adjustsFontForContentSizeCategory = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    lazy var facultyDisclosureButton:UIButton = {
        let origImage = UIImage(named: "info")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = UIColor.systemBlueColor
        button.backgroundColor = UIColor.clear
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showFacultyVC), for: .touchUpInside)
        return button
    }()
    @objc func showFacultyVC() {
        delegate?.disclosurePressed(cell: self)
    }
    func setUpViews() {
        
//        infoView.addSubview(blurredEffectView)
        infoView.addSubview(nameLabel)
        infoView.addSubview(univRollNoLabel)
        infoView.addSubview(horizontalSeparator1)
        infoView.addSubview(semesterLabel)
        infoView.addSubview(cpiLabel)
        infoView.addSubview(sectionLabel)
        infoView.addSubview(classRollNoLabel)
//        infoView.addSubview(classAdvisorLabel)
        infoView.addSubview(residentLabel)
//        infoView.addSubview(facultyDisclosureButton)
        infoView.addSubview(horizontalSeparator2)
        infoView.addSubview(statusLabel)
        contentView.addSubview(infoView)
//        infoView.addSubview(advisorHeadingLabel)
//        customImageView.frame = self.contentView.bounds
//        blurredEffectView.frame = self.infoView.bounds
    }
    func setUpConstraints() {
        _ = infoView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 10, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        _ = nameLabel.anchorWithoutWH(top: infoView.topAnchor, left: infoView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 20)
        _ = univRollNoLabel.anchorWithoutWH(top: infoView.topAnchor, left: nil, bottom: nil, right: infoView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 15)
        _ = horizontalSeparator1.anchor(nameLabel.bottomAnchor, left: infoView.leftAnchor, bottom: nil, right: infoView.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0.3)
        _ = semesterLabel.anchorWithoutWH(top: horizontalSeparator1.bottomAnchor, left: infoView.leftAnchor, bottom: nil, right: infoView.centerXAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0)
        _ = cpiLabel.anchorWithoutWH(top: nil, left: infoView.centerXAnchor, bottom: nil, right: infoView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        cpiLabel.centerYAnchor.constraint(equalTo: semesterLabel.centerYAnchor).isActive = true
        _ = sectionLabel.anchorWithoutWH(top: semesterLabel.bottomAnchor, left: infoView.leftAnchor, bottom: nil, right: infoView.centerXAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0)
        _ = classRollNoLabel.anchorWithoutWH(top: nil, left: infoView.centerXAnchor, bottom: nil, right: infoView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        classRollNoLabel.centerYAnchor.constraint(equalTo: sectionLabel.centerYAnchor).isActive = true
//        _ = classAdvisorLabel.anchorWithoutWH(top: sectionLabel.bottomAnchor, left: infoView.leftAnchor, bottom: nil, right: facultyDisclosureButton.leftAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 20)
        _ = residentLabel.anchorWithoutWH(top: sectionLabel.bottomAnchor, left: infoView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 20)
//        _ = facultyDisclosureButton.anchor(sectionLabel.bottomAnchor, left: nil, bottom: nil, right: infoView.rightAnchor, topConstant: 7, leftConstant: 20, bottomConstant: 0, rightConstant: 10, widthConstant: 28, heightConstant: 28)
        _ = horizontalSeparator2.anchor(residentLabel.bottomAnchor, left: infoView.leftAnchor, bottom: nil, right: infoView.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0.5)
        _ = statusLabel.anchorWithoutWH(top: horizontalSeparator2.bottomAnchor, left: nil, bottom: infoView.bottomAnchor, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 5, rightConstant: 0)
        statusLabel.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
//        let bottomConstraint = statusLabel.bottomAnchor.constraint(equalTo: infoView.bottomAnchor, constant: -10)
        let bottomConstraint = infoView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        bottomConstraint.priority = UILayoutPriority(750)
        bottomConstraint.isActive = true
//        _ = semesterHeadingLabel.anchor(infoView.topAnchor, left: infoView.leftAnchor, bottom: nil, right: infoView.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
//        _ = classAdvisorLabel.anchor(semesterHeadingLabel.bottomAnchor, left: customImageView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 2, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        _ = advisorHeadingLabel.anchor(classAdvisorLabel.bottomAnchor, left: classAdvisorLabel.leftAnchor, bottom: customImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        _ = facultyDisclosureButton.anchor(nil, left: classAdvisorLabel.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
//        facultyDisclosureButton.centerYAnchor.constraint(equalTo: classAdvisorLabel.centerYAnchor).isActive = true
        
        
//        _ = advisorHeadingLabel.anchorWithoutWH(top: classAdvisorLabel.bottomAnchor, left: classAdvisorLabel.leftAnchor, bottom: infoView.bottomAnchor, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 10, rightConstant: 0)
        
    }
}
