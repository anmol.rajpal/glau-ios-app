//
//  TestAttendanceInfoTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 19/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit
protocol TestAttendanceInfoCellDelegate: class {
    func animateAttendance(cell:TestAttendanceInfoTableViewCell)
}
class TestAttendanceInfoTableViewCell: UITableViewCell {
    weak var delegate:TestAttendanceInfoCellDelegate?
    var attendance:Float? {
        didSet {
            setAttributedAttendance(attendance: attendance ?? 0)
        }
    }
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        setUpViews()
//        setUpLayers()
//        setUpConstraints()
//        setupCircleLayers()
//        animateView()
//        setupNotificationObservers()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpLayers()
        setUpConstraints()
        setupCircleLayers()
        animateView()
        setupNotificationObservers()
    }
    func setAttributedAttendance(attendance:Float) {
        let attendanceString = String(attendance)
        let group = attendanceString.split(separator: ".")
        let valueString = group[0]
        let precisionString = group[1]
        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1).expanded()])
        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body).condensed()])
        let percentageAttributedString = NSAttributedString(string: " %", attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body).bold()])
        let attendanceAttributedString = NSMutableAttributedString()
        if Int(precisionString) == 0 {
            attendanceAttributedString.append(valueAttributedString)
            attendanceAttributedString.append(percentageAttributedString)
        } else {
            attendanceAttributedString.append(valueAttributedString)
            attendanceAttributedString.append(NSAttributedString(string: "."))
            attendanceAttributedString.append(precisionAttributedString)
            attendanceAttributedString.append(percentageAttributedString)
        }
        attendanceLabel.attributedText = attendanceAttributedString
    }
    let attendanceLabel:UILabel = {
        let label = UILabel()
//        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1).expanded()
        label.adjustsFontForContentSizeCategory = true
        label.textColor = .black
        label.textAlignment = .center
//        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.sizeToFit()
        return label
    }()
    lazy var pulsatingLayer:CAShapeLayer = {
        let layer = createCircleShapeLayer(strokeColor: .clear, fillColor: UIColor.systemGreenColor.withAlphaComponent(0.2))
        return layer
    }()
    lazy var trackLayer:CAShapeLayer = {
        let layer = createCircleShapeLayer(strokeColor: UIColor.systemGreenColor.withAlphaComponent(0.2), fillColor: .clear)
        return layer
    }()
    lazy var shapeLayer:CAShapeLayer = {
        let layer = createCircleShapeLayer(strokeColor: UIColor.systemGreenColor, fillColor: .clear)
        layer.strokeEnd = 0.0
        return layer
    }()
    func animateView() {
        delegate?.animateAttendance(cell: self)
    }
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    @objc private func handleEnterForeground() {
        animatePulsatingLayer()
    }
    
    func setupCircleLayers() {
        DispatchQueue.main.async {
            self.animatePulsatingLayer()
            self.shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
//            self.shapeLayer.strokeEnd = 0.6
            self.animateShapeLayer()
//            self.layoutIfNeeded()
        }
    }
    private func animateShapeLayer() {
//        let initial:CGFloat = 0
//        shapeLayer.strokeEnd = 0
//        UIView.animate(withDuration: 1.0) {
//            self.shapeLayer.strokeEnd = self.attendance / 100
//        }
        let stroke = CABasicAnimation(keyPath: "strokeEnd")
        stroke.duration = 1.0
        stroke.fromValue = 0
//        stroke.toValue = 1
        stroke.isRemovedOnCompletion = false
        stroke.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        stroke.fillMode = .forwards
        shapeLayer.strokeEnd = CGFloat(self.attendance ?? 0) / 100
        shapeLayer.add(stroke, forKey: "strokeEnd")
    }
    func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.3
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    private func createCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        let attendanceLabelWidth = attendanceLabel.frame.width
        let circularPath = UIBezierPath(arcCenter: .zero, radius: attendanceLabelWidth - (attendanceLabelWidth/6), startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = (attendanceLabelWidth / 6) + 3
        layer.fillColor = fillColor.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.position = contentView.center
        return layer
    }
    func setUpLayers() {
        DispatchQueue.main.async {
            self.contentView.layer.addSublayer(self.pulsatingLayer)
            self.contentView.layer.addSublayer(self.trackLayer)
            self.contentView.layer.addSublayer(self.shapeLayer)
        }
    }
    func setUpViews() {
        contentView.addSubview(attendanceLabel)
    }
    
    func setUpConstraints() {
        attendanceLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        attendanceLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        attendanceLabel.layoutIfNeeded()
        let safeMargin:CGFloat = attendanceLabel.frame.size.width
//        self.contentView.heightAnchor.constraint(equalToConstant: safeMargin * 2).isActive = true
//        let heightConstraint = attendanceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -safeMargin)
        let heightConstraint  = contentView.heightAnchor.constraint(equalToConstant: (safeMargin * 2) + 15)
        heightConstraint.priority = UILayoutPriority(999)
        heightConstraint.isActive = true
//        attendanceLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: safeMargin).isActive = true
        
        
//        attendanceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -safeMargin).isActive = true
//        contentView.bottomAnchor.constraint(equalTo: attendanceLabel.bottomAnchor, constant: safeMargin).isActive = true
//        let bottomConstraint = attendanceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -safeMargin)
//        bottomConstraint.priority = UILayoutPriority(999)
//        bottomConstraint.isActive = true
//        _ = attendanceLabel.anchor(contentView.topAnchor, left: nil, bottom: contentView.bottomAnchor, right: nil, topConstant: safeMargin, leftConstant: 0, bottomConstant: safeMargin, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}
