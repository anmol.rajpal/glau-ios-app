//
//  AttendanceDetailsTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit

class AttendanceDetailsTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var subjectCode:String? {
        willSet {
//            contentView.layoutIfNeeded()
//            spinner.layoutIfNeeded()
            spinner.center = contentView.center
            spinner.startAnimating()
        }
        didSet {
            if let code = subjectCode {
                fetchAttendanceDetails(subjectCode: code)
            }
        }
    }
    var attendanceDetails:[AttendanceDetailsCodable]?
    var attDetails:[AttendanceDetailsCodable]? = []
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
//        contentView.layoutIfNeeded()
//        fetchAttendanceDetails()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
//        layout.itemSize = UICollectionViewFlowLayout.automaticSize
//        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.contentInsetAdjustmentBehavior = .always
        cv.clipsToBounds = true
        cv.isPagingEnabled = false
        cv.showsHorizontalScrollIndicator = true
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.isHidden = false
        return cv
    }()
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
//        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = contentView.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .gray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    fileprivate func animateIn(cell: UICollectionViewCell, withDuration duration:TimeInterval, withDelay delay: TimeInterval) {
        let initialScale: CGFloat = 0.0
        cell.alpha = 0.0
        cell.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
    private func setupMessageLabel(message:String = "Unstable Internet Connection") {
        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        self.spinner.startAnimating()
        self.messageLabel.isHidden = true
        self.retryButton.isHidden = true
        fetchAttendanceDetails(subjectCode: self.subjectCode ?? "")
    }
    
    internal func configure(collectionView:UICollectionView) {
        collectionView.delegate = self
        collectionView.dataSource = self
    collectionView.register(AttendanceDetailsCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(AttendanceDetailsCollectionViewCell.self))
    }
    func setUpViews() {
        contentView.addSubview(collectionView)
        configure(collectionView: collectionView)
        contentView.addSubview(spinner)
        contentView.addSubview(messageLabel)
        contentView.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = collectionView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
//        messageLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        _ = messageLabel.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 20, bottomConstant: 0, rightConstant: 20)
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 5).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
//        retryButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 10).isActive = true
//        collectionView.heightAnchor.constraint(equalToConstant: 90).isActive = true
    }
    
    private func fetchAttendanceDetails(subjectCode:String) {
        let rollNo = UserDefaults.standard.getRollNo()
        AttendanceDetailsService.shared.fetch(rollNo: rollNo, subjectCode: subjectCode) { (data, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                self.attDetails?.removeAll()
                guard error == nil else {
                    self.updateViews()
                    self.messageLabel.text = "Cannot Load Attendance Details"
                    self.spinner.stopAnimating()
                    print(error?.localizedDescription ?? "Error")
                    return
                }
                if data?.count ?? 0 > 0 {
                    let result = data?[0].result
                    let message = data?[0].message
                    switch result {
                    case ResultType.Failure.rawValue:
                        print("Failure with message - \(String(describing: message))")
                        self.updateViews()
                        self.messageLabel.text = "Data Unavailable"
                        self.spinner.stopAnimating()
                    case ResultType.Success.rawValue:
                        if let data = data {
                            self.attendanceDetails = data
                            for i in data {
                                let details = i.details ?? "--"
                                let str = details.split(separator: ",")
                                for j in str {
                                    let message = i.message
                                    let result = i.result
                                    let lectureDate = i.lectureDate
                                    let sNo = i.sNo
                                    let detail = String(j).trimmingCharacters(in: .whitespacesAndNewlines)
                                    let element = AttendanceDetailsCodable(details: detail, lectureDate: lectureDate, message: message, result: result, sNo: sNo)
                                    self.attDetails?.append(element)
                                }
                            }
                            self.attDetails?.reverse()
                        }
                        self.updateViews()
                        self.spinner.stopAnimating()
                    default: break
                    }
                    
                } else {
                    self.updateViews()
                    self.messageLabel.text = "No Data"
                    self.spinner.stopAnimating()
                }
            })
        }
    }
    
    private func updateViews() {
        let isEmpty:Bool = attendanceDetails?.isEmpty ?? true
        collectionView.isHidden = isEmpty
        messageLabel.isHidden = !isEmpty
        retryButton.isHidden = !isEmpty
        if !isEmpty { collectionView.reloadData() }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset:CGFloat = 15
        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attDetails?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(AttendanceDetailsCollectionViewCell.self), for: indexPath) as! AttendanceDetailsCollectionViewCell
        animateIn(cell: cell, withDuration: 0.3, withDelay: 0.0)
        let details = attDetails?[indexPath.row]
        cell.attendanceDetails = details
        
//        collectionView.heightAnchor.constraint(equalToConstant: cell.contentView.frame.height).isActive = true
//        collectionView.reloadData()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 80)
    }
}
