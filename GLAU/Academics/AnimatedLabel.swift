//
//  AnimatedLabel.swift
//  GLA University
//
//  Created by Anmol Rajpal on 26/09/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import Foundation
import UIKit

enum CountingMethod {
    case easeInOut, easeIn, easeOut, linear
}

enum AnimationDuration {
    case laborious, plodding, strolling, brisk, instant
    
    var value: Double {
        switch self {
        case .laborious: return 20.0
        case .plodding: return 15.0
        case .strolling: return 8.0
        case .brisk: return 2.0
        case .instant: return 0.0
        }
    }
}

enum DecimalPoints {
    case zero, one, two, ridiculous
    
    var format: String {
        switch self {
        case .zero: return "%.0f"
        case .one: return "%.1f"
        case .two: return "%.2f"
        case .ridiculous: return "%f"
        }
    }
}

typealias OptionalCallback = (() -> Void)
typealias OptionalFormatBlock = (() -> String)

class AnimatedLabel: UILabel {
    var topInset:CGFloat!
    var bottomInset:CGFloat!
    var leftInset:CGFloat!
    var rightInset:CGFloat!
    init(_ topInset:CGFloat, _ bottomInset:CGFloat, _ leftInset:CGFloat, _ rightInset:CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.topInset = topInset
        self.bottomInset = bottomInset
        self.leftInset = leftInset
        self.rightInset = rightInset
    }
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)))
    }
    override public var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    var completion: OptionalCallback?
    var animationDuration: AnimationDuration = .brisk
    var decimalPoints: DecimalPoints = .zero
    var countingMethod: CountingMethod = .easeOut
    var customFormatBlock: OptionalFormatBlock?
    var prependString: String = ""
    var appendString: String = ""
    
    fileprivate var currentValue: Float {
        if progress >= totalTime { return destinationValue }
        return startingValue + (update(t: Float(progress / totalTime)) * (destinationValue - startingValue))
    }
    
    fileprivate var rate: Float = 0
    fileprivate var startingValue: Float = 0
    fileprivate var destinationValue: Float = 0
    fileprivate var progress: TimeInterval = 0
    fileprivate var lastUpdate: TimeInterval = 0
    fileprivate var totalTime: TimeInterval = 0
    fileprivate var easingRate: Float = 0
    fileprivate var timer: CADisplayLink?
    
    func count(from: Float, to: Float, duration: TimeInterval? = nil) {
        startingValue = from
        destinationValue = to
        timer?.invalidate()
        timer = nil
        
        if (duration == 0.0) {
            // No animation
            setTextValue(value: to, stringToPrepend: prependString, stringToAppend: appendString)
            completion?()
            return
        }
        
        easingRate = 3.0
        progress = 0.0
        totalTime = duration ?? animationDuration.value
        lastUpdate = Date.timeIntervalSinceReferenceDate
        rate = 3.0
        
        addDisplayLink()
    }
    
    func countFromCurrent(to: Float, duration: TimeInterval? = nil) {
        count(from: currentValue, to: to, duration: duration ?? nil)
    }
    
    func countFromZero(to: Float, duration: TimeInterval? = nil) {
        count(from: 0, to: to, duration: duration ?? nil)
    }
    
    func stop() {
        timer?.invalidate()
        timer = nil
        progress = totalTime
        completion?()
    }
    
    fileprivate func addDisplayLink() {
        timer = CADisplayLink(target: self, selector: #selector(self.updateValue(timer:)))
        timer?.add(to: .main, forMode: RunLoop.Mode.default)
        timer?.add(to: .main, forMode: RunLoop.Mode.tracking)
    }
    
    fileprivate func update(t: Float) -> Float {
        var t = t
        
        switch countingMethod {
        case .linear:
            return t
        case .easeIn:
            return powf(t, rate)
        case .easeInOut:
            var sign: Float = 1
            if Int(rate) % 2 == 0 { sign = -1 }
            t *= 2
            return t < 1 ? 0.5 * powf(t, rate) : (sign*0.5) * (powf(t-2, rate) + sign*2)
        case .easeOut:
            return 1.0-powf((1.0-t), rate);
        }
    }
    
    @objc fileprivate func updateValue(timer: Timer) {
        let now: TimeInterval = Date.timeIntervalSinceReferenceDate
        progress += now - lastUpdate
        lastUpdate = now
        
        if progress >= totalTime {
            self.timer?.invalidate()
            self.timer = nil
            progress = totalTime
        }
        
        setTextValue(value: currentValue, stringToPrepend: prependString, stringToAppend: appendString)
        if progress == totalTime { completion?() }
    }
    
    fileprivate func setTextValue(value: Float, stringToPrepend:String? = nil, stringToAppend:String? = nil) {
        prependString = stringToPrepend ?? ""
        appendString = stringToAppend ?? ""
        text = prependString + String(format: customFormatBlock?() ?? decimalPoints.format, value) + appendString
    }
    
}
