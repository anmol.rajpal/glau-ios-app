//
//  AttendanceInfoTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 17/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit
//import MKRingProgressView
protocol AttendanceInfoCellDelegate {
    func animateAttendance(cell:AttendanceInfoTableViewCell)
}
class AttendanceInfoTableViewCell: UITableViewCell {
    var delegate:AttendanceInfoCellDelegate?
    var subjectCode = String()
    var pulsatingLayer: CAShapeLayer!
    var attendanceDetails = [AttendanceDetailsCodable]()
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        
        setUpConstraints()
        setUpRingProgressView()
//        setupCircleLayers()
//        layoutIfNeeded()
//        self.setNeedsLayout()
//        self.setNeedsUpdateConstraints()
//        self.updateConstraintsIfNeeded()
//        self.updateConstraintsIfNeeded()
    }
    func setUpRingProgressView() {
//        DispatchQueue.main.async {
            self.animateRingProgressView()
//            self.ringProgressView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0).isActive = true
//            self.ringProgressView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        
//        }
    }
    func animateRingProgressView() {
        delegate?.animateAttendance(cell: self)
    }
    
//    lazy var ringProgressView:RingProgressView = {
//        let view = RingProgressView(frame: CGRect(x: 0, y: 0, width: self.attendanceLabel.frame.width + 50, height: self.attendanceLabel.frame.width + 50))
//        view.startColor = .systemRedColor
//        view.endColor = .systemGreenColor
//        view.ringWidth = 15
//        view.progress = 0.0
//        view.shadowOpacity = 0.3
////        view.center = self.attendanceLabel.center
//        view.clipsToBounds = true
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    let ringProgressView:RingProgressView = {
//        let view = RingProgressView(frame: CGRect(x: 50, y: 50, width: 100, height: 100))
//        view.startColor = .systemRedColor
//        view.endColor = .systemGreenColor
//        view.ringWidth = 15
//        view.progress = 0.0
//        view.shadowOpacity = 0.3
//        //        view.center = self.attendanceLabel.center
//        view.clipsToBounds = true
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
    let attendanceLabel:UILabel = {
        //        let label = AnimatedLabel(22, 22, 50, 50)
        //        label.decimalPoints = .two
        //        label.count(from: self.attendance/2.0, to: attendance)
        //        label.appendString = " %"
        //        label.textColor = .white
        //        if self.attendance < Config.requiredAttendance {
        //            label.backgroundColor = UIColor.systemRedColor
        //        } else if self.attendance > Config.requiredAttendance {
        //            label.backgroundColor = UIColor.systemGreenColor
        //        } else {
        //            label.backgroundColor = UIColor.systemYellowColor
        //        }
//        let label = InsetLabel(10, 10, 30, 30)
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 40, weight: UIFont.Weight.light)
        label.text = " %"
        label.textColor = .black
        label.textAlignment = .center
        label.clipsToBounds = true
        label.layer.cornerRadius = 10
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    func setAttributedAttendance(attendance:Float) {
        let attendanceString = String(attendance)
        let group = attendanceString.split(separator: ".")
        let valueString = group[0]
        let precisionString = group[1]
        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 35, weight: .light)])
        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15, weight: .light)])
        let percentageAttributedString = NSAttributedString(string: " %", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .bold)])
        let attendanceAttributedString = NSMutableAttributedString()
        if Int(precisionString) == 0 {
            attendanceAttributedString.append(valueAttributedString)
            attendanceAttributedString.append(percentageAttributedString)
        } else {
            attendanceAttributedString.append(valueAttributedString)
            attendanceAttributedString.append(NSAttributedString(string: "."))
            attendanceAttributedString.append(precisionAttributedString)
            attendanceAttributedString.append(percentageAttributedString)
        }
        attendanceLabel.attributedText = attendanceAttributedString
    }
    
//    func configureTableView() {
//        tableView.register(AttendanceDetailsCell.self, forCellReuseIdentifier: NSStringFromClass(AttendanceDetailsCell.self))
//        tableView.dataSource = self
//        tableView.delegate = self
//        tableView.scrollToBottom(animated: true)
//        tableView.reloadDataWithLayout()
//    }
    private func setupCircleLayers() {
        pulsatingLayer = createCircleShapeLayer(strokeColor: .clear, fillColor: UIColor.purple)
        contentView.layer.addSublayer(pulsatingLayer)
        animatePulsatingLayer()
        
//        let trackLayer = createCircleShapeLayer(strokeColor: .blue, fillColor: .yellow)
//        contentView.layer.addSublayer(trackLayer)
        
//        shapeLayer = createCircleShapeLayer(strokeColor: .outlineStrokeColor, fillColor: .clear)
//
//        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
//        shapeLayer.strokeEnd = 0
//        view.layer.addSublayer(shapeLayer)
    }
    private func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.toValue = 1.3
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    private func createCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 50, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = 10
        layer.fillColor = fillColor.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        layer.position = contentView.center
        return layer
    }
    func setUpViews() {
        contentView.addSubview(attendanceLabel)
//        contentView.addSubview(ringProgressView)
//        ringProgressView.center = attendanceLabel.center
    }
    func setUpConstraints() {
        
//        attendanceLabel.centerXAnchor.constraint(equalTo: ringProgressView.centerXAnchor).isActive = true
//        attendanceLabel.centerYAnchor.constraint(equalTo: ringProgressView.centerYAnchor).isActive = true
//        attendanceLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 50).isActive = true
//        attendanceLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
//        let width = attendanceLabel.frame.width
//        print("Width - \(width)")
//        let safeMargin:CGFloat = 45
//        attendanceLabel.topAnchor.constraint(equalTo: ringProgressView.topAnchor, constant: safeMargin).isActive = true
//        attendanceLabel.leftAnchor.constraint(equalTo: ringProgressView.leftAnchor, constant: safeMargin).isActive = true
//        attendanceLabel.bottomAnchor.constraint(equalTo: ringProgressView.bottomAnchor, constant: -safeMargin).isActive = true
//        attendanceLabel.rightAnchor.constraint(equalTo: ringProgressView.rightAnchor, constant: -safeMargin).isActive = true
        
        
//        ringProgressView.topAnchor.constraint(equalTo: attendanceLabel.topAnchor, constant: -safeMargin).isActive = true
//        ringProgressView.bottomAnchor.constraint(equalTo: attendanceLabel.bottomAnchor, constant: safeMargin).isActive = true
//        ringProgressView.leftAnchor.constraint(equalTo: attendanceLabel.leftAnchor, constant: -safeMargin).isActive = true
//        ringProgressView.rightAnchor.constraint(equalTo: attendanceLabel.rightAnchor, constant: safeMargin).isActive = true
//        ringProgressView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
//        ringProgressView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
//        ringProgressView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
//        ringProgressView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
//        contentView.heightAnchor.constraint(equalToConstant: ringProgressView.bounds.height).isActive = true
//        contentView.translatesAutoresizingMaskIntoConstraints = false
//        contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
//        contentView.topAnchor.constraint(equalTo: ringProgressView.topAnchor, constant: 0).isActive = true
//        contentView.bottomAnchor.constraint(equalTo: ringProgressView.bottomAnchor, constant: 0).isActive = true
//        contentView.heightAnchor.constraint(equalToConstant: ringProgressView.frame.height).isActive = true
//        ringProgressView.heightAnchor.constraint(equalToConstant: 20).isActive = true
//        ringProgressView.widthAnchor.constraint(equalToConstant: 20).isActive = true
//        ringProgressView.widthAnchor.constraint(equalToConstant: attendanceLabel.frame.width + 200).isActive = true
    }
}
