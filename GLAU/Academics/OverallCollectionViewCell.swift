////
////  OverallCollectionViewCell.swift
////  GLA University
////
////  Created by Anmol Rajpal on 15/10/18.
////  Copyright © 2018 Natovi. All rights reserved.
////
//
//import UIKit
////import MKRingProgressView
//protocol OverallCellDelegate {
//    func animateAttendance(cell:OverallCollectionViewCell)
//}
//class OverallCollectionViewCell: UICollectionViewCell {
//    static let cellHeight:CGFloat = 215
//    let padding:CGFloat = 20
//    var delegate:OverallCellDelegate?
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setUpViews()
//        setUpConstraints()
//        setUpRingProgressView()
//    }
//    let overallAttendanceLabel:UILabel = {
//        let label = UILabel()
//        label.text = "100.0 %"
//        label.textColor = .black
//        label.textAlignment = .center
//        label.numberOfLines = 2
//        label.clipsToBounds = true
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
//    let overallAttendanceTitleLabel:UILabel = {
//        let label = UILabel()
//        label.text = "attendance".uppercased()
//        label.font = UIFont.systemFont(ofSize: 13, weight: .heavy)
//        label.textColor = .darkGray
//        label.textAlignment = .center
//        label.clipsToBounds = true
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
//    let cpiLabel:UILabel = {
//        let label = UILabel()
//        label.text = "7.25"
//        label.textColor = .black
//        label.textAlignment = .center
//        label.numberOfLines = 2
//        label.clipsToBounds = true
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
//    let cpiTitleLabel:UILabel = {
//        let label = UILabel()
//        label.text = "cpi".uppercased()
//        label.font = UIFont.systemFont(ofSize: 13, weight: .heavy)
//        label.textColor = .darkGray
//        label.textAlignment = .center
//        label.clipsToBounds = true
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
//    lazy var cpiRingProgressView:RingProgressView = {
//        let view = RingProgressView(frame: CGRect(x: 0, y: 0, width: self.overallAttendanceLabel.frame.width - padding, height: self.overallAttendanceLabel.frame.width - padding))
//        view.startColor = .systemRedColor
//        view.endColor = .systemGreenColor
//        view.ringWidth = 15
//        view.progress = 0.0
//        view.shadowOpacity = 0.3
//        view.center = self.cpiLabel.center
//        view.clipsToBounds = true
//        return view
//    }()
//    lazy var overallAttendanceRingProgressView:RingProgressView = {
//        let view = RingProgressView(frame: CGRect(x: 0, y: 0, width: self.overallAttendanceLabel.frame.width - padding, height: self.overallAttendanceLabel.frame.width - padding))
//        view.startColor = .systemRedColor
//        view.endColor = .systemGreenColor
//        view.ringWidth = 15
//        view.progress = 0.0
//        view.shadowOpacity = 0.3
//        view.center = self.overallAttendanceLabel.center
//        view.clipsToBounds = true
//        return view
//    }()
//    let stackView:UIStackView = {
//        let stackView = UIStackView()
//        stackView.translatesAutoresizingMaskIntoConstraints = false
//        //        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
//        stackView.axis = .horizontal
////                stackView.spacing = 50
//        stackView.distribution = .fillEqually
////        stackView.isLayoutMarginsRelativeArrangement = true
//        return stackView
//    }()
//    func animateRingProgressView() {
//        delegate?.animateAttendance(cell: self)
//    }
//    func setUpRingProgressView() {
//        DispatchQueue.main.async {
//            self.contentView.addSubview(self.cpiRingProgressView)
//            self.contentView.addSubview(self.overallAttendanceRingProgressView)
//            self.animateRingProgressView()
//        }
//    }
//    internal func setAttributedCpi(cpi:Double) {
//        let cpiString = String(format:"%.2f", cpi)
//        let group = cpiString.split(separator: ".")
//        let valueString = group[0]
//        let precisionString = group[1]
//        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 40, weight: .light)])
//        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 25, weight: .light)])
//        let cpiTitleAttributedString = NSAttributedString(string: "cpi".uppercased(), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 13, weight: .heavy), NSAttributedString.Key.foregroundColor:UIColor.lightGray])
//        let cpiAttributedString = NSMutableAttributedString()
//        cpiAttributedString.append(valueAttributedString)
//        cpiAttributedString.append(NSAttributedString(string: "."))
//        cpiAttributedString.append(precisionAttributedString)
//        cpiAttributedString.append(NSAttributedString(string: "\n"))
//        cpiAttributedString.append(cpiTitleAttributedString)
//        cpiLabel.attributedText = cpiAttributedString
//    }
//    internal func setAttributedAttendance(attendance:Float) {
//        let attendanceString = String(format:"%.2f", attendance)
//        let group = attendanceString.split(separator: ".")
//        let valueString = group[0]
//        let precisionString = group[1]
//        let valueAttributedString = NSAttributedString(string: String(valueString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 40, weight: .light)])
//        let precisionAttributedString = NSAttributedString(string: String(precisionString), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15, weight: .light)])
//        let percentageAttributedString = NSAttributedString(string: " %", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .bold)])
//        let attendanceTitleAttributedString = NSAttributedString(string: "attendance".uppercased(), attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 13, weight: .heavy), NSAttributedString.Key.foregroundColor:UIColor.lightGray])
//        let attendanceAttributedString = NSMutableAttributedString()
//        if Int(precisionString) == 0 {
//            attendanceAttributedString.append(valueAttributedString)
//            attendanceAttributedString.append(percentageAttributedString)
//        } else {
//            attendanceAttributedString.append(valueAttributedString)
//            attendanceAttributedString.append(NSAttributedString(string: "."))
//            attendanceAttributedString.append(precisionAttributedString)
//            attendanceAttributedString.append(percentageAttributedString)
//            attendanceAttributedString.append(NSAttributedString(string: "\n"))
//            attendanceAttributedString.append(attendanceTitleAttributedString)
//        }
//        overallAttendanceLabel.attributedText = attendanceAttributedString
//    }
//    func setUpViews() {
//        contentView.addSubview(stackView)
////        contentView.addSubview(cpiTitleLabel)
////        contentView.addSubview(overallAttendanceTitleLabel)
//        stackView.addArrangedSubview(cpiLabel)
////        stackView.addArrangedSubview(cpiTitleLabel)
//        stackView.addArrangedSubview(overallAttendanceLabel)
////        stackView.addArrangedSubview(overallAttendanceTitleLabel)
//    }
//    func setUpConstraints() {
////        cpiLabel.widthAnchor.constraint(equalToConstant: (contentView.bounds.width/2) - 40).isActive = true
////        overallAttendanceLabel.widthAnchor.constraint(equalToConstant: (contentView.bounds.width/2) - 40).isActive = true
////        cpiLabel.centerYAnchor.constraint(equalTo: stackView.centerYAnchor).isActive = true
////        overallAttendanceLabel.centerYAnchor.constraint(equalTo: stackView.centerYAnchor).isActive = true
//        _ = stackView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
////        cpiTitleLabel.topAnchor.constraint(equalTo: cpiLabel.bottomAnchor, constant: 5).isActive = true
////        overallAttendanceTitleLabel.topAnchor.constraint(equalTo: overallAttendanceLabel.bottomAnchor, constant: 5).isActive = true
////        cpiTitleLabel.centerXAnchor.constraint(equalTo: cpiLabel.centerXAnchor).isActive = true
////        overallAttendanceTitleLabel.centerXAnchor.constraint(equalTo: overallAttendanceLabel.centerXAnchor).isActive = true
//    }
//}
