//
//  SubjectDetailViewControllerWaste.swift
//  GLA University
//
//  Created by Anmol Rajpal on 27/09/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import Foundation


/*
 let subjectTitleLabel:UILabel = {
 let label = UILabel()
 label.font = UIFont.systemFont(ofSize: 30, weight: .heavy)
 label.textColor = .black
 label.text = "Engineering Subject Title, To be rendered"
 label.numberOfLines = 0
 return label
 }()
 let subjectCodeLabel:UILabel = {
 let label = UILabel()
 label.text = "CSE 6002"
 label.textColor = .gray
 label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
 label.numberOfLines = 1
 
 return label
 }()
 let facultyNameLabel:UILabel = {
 let label = UILabel()
 label.text = "Some Teacher"
 label.textColor = UIColor.darkGray
 label.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
 label.numberOfLines = 1
 
 return label
 }()
 
 let imageView:UIImageView = {
 let imageView = UIImageView(image: #imageLiteral(resourceName: "blabla"))
 imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
 imageView.layer.cornerRadius = 15
 imageView.clipsToBounds = true
 return imageView
 }()
 let attendanceView:UIView = {
 let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
 view.layer.cornerRadius = 15
 view.backgroundColor = UIColor.white
 view.layer.shadowColor = UIColor.black.cgColor
 view.layer.shadowOpacity = 0.7
 view.layer.shadowOffset = CGSize.zero
 view.layer.shadowRadius = 3
 return view
 }()
 let statisticsHeadingLabel:UILabel = {
 let label = UILabel()
 label.text = "Statistics"
 label.font = UIFont.systemFont(ofSize: 25, weight: .heavy)
 label.textColor = UIColor.darkText
 label.numberOfLines = 1
 
 return label
 }()
 let attendanceHeadingLabel:UILabel = {
 let label = UILabel()
 label.text = "Attendance"
 label.textColor = UIColor.darkGray
 label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
 label.numberOfLines = 1
 
 return label
 }()
 let attendanceLabel:UILabel = {
 let label = InsetLabel(10, 10, 30, 30)
 label.text = "89.96 %"
 label.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.light)
 label.textColor = .white
 label.backgroundColor = UIColor.systemGreenColor
 label.clipsToBounds = true
 label.layer.cornerRadius = 7
 return label
 }()
 let lecturesHeadingLabel:UILabel = {
 let label = UILabel()
 label.text = "Lectures"
 label.textColor = UIColor.darkGray
 label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
 label.numberOfLines = 1
 
 return label
 }()
 let attendedLecturesLabel:UILabel = {
 let label = UILabel()
 label.text = "Attended - 20"
 label.textColor = .black
 label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
 label.numberOfLines = 1
 
 return label
 }()
 let heldLecturesLabel:UILabel = {
 let label = UILabel()
 label.text = "Held - 20"
 label.textColor = .black
 label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
 label.numberOfLines = 1
 return label
 }()
 let marksHeadingLabel:UILabel = {
 let label = UILabel()
 label.text = "Marks"
 label.textColor = UIColor.darkGray
 label.font = UIFont.systemFont(ofSize: 22, weight: .medium)
 label.numberOfLines = 1
 
 return label
 }()
 let mid1HeadingLabel:UILabel = {
 let label = UILabel()
 label.text = "MidTerm-I"
 label.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
 label.numberOfLines = 1
 return label
 }()
 let mid2HeadingLabel:UILabel = {
 let label = UILabel()
 label.text = "MidTerm-II"
 label.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
 label.numberOfLines = 1
 return label
 }()
 let endTermHeadingLabel:UILabel = {
 let label = UILabel()
 label.text = "EndTerm"
 label.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
 label.numberOfLines = 1
 return label
 }()
 let mid1MarksLabel:UILabel = {
 let label = InsetLabel(5, 5, 30, 30)
 label.text = "- -"
 label.textColor = .white
 label.layer.cornerRadius = 15
 label.backgroundColor = .lightGray
 label.font = UIFont.systemFont(ofSize: 18, weight: .light)
 label.numberOfLines = 1
 label.clipsToBounds = true
 return label
 }()
 let mid2MarksLabel:UILabel = {
 let label = InsetLabel(5, 5, 30, 30)
 label.text = "15"
 label.textColor = .white
 label.layer.cornerRadius = 15
 label.backgroundColor = .systemRedColor
 label.font = UIFont.systemFont(ofSize: 18, weight: .light)
 label.numberOfLines = 1
 label.clipsToBounds = true
 return label
 }()
 let endTermMarksLabel:UILabel = {
 let label = InsetLabel(5, 5, 30, 30)
 label.text = "- -"
 label.textColor = .white
 label.layer.cornerRadius = 15
 label.backgroundColor = .lightGray
 label.font = UIFont.systemFont(ofSize: 18, weight: .light)
 label.numberOfLines = 1
 label.clipsToBounds = true
 return label
 }()
 let hSeperatorLine1 = HorizontalLine(color: .lightGray)
 let hSeperatorLine2 = HorizontalLine(color: .lightGray)
 let hSeperatorLine3 = HorizontalLine(color: .lightGray)
 //    let hSeperatorLine4 = HorizontalLine(color: .lightGray)
 let vSeperatorLine1 = VerticalLine(color: .lightGray)
 */


/*
 view.addSubview(subjectTitleLabel)
 view.addSubview(subjectCodeLabel)
 view.addSubview(facultyNameLabel)
 view.addSubview(attendanceView)
 
 view.addSubview(hSeperatorLine1)
 view.addSubview(hSeperatorLine2)
 view.addSubview(hSeperatorLine3)
 
 view.addSubview(vSeperatorLine1)
 
 attendanceView.addSubview(statisticsHeadingLabel)
 attendanceView.addSubview(attendanceHeadingLabel)
 attendanceView.addSubview(attendanceLabel)
 attendanceView.addSubview(lecturesHeadingLabel)
 attendanceView.addSubview(attendedLecturesLabel)
 attendanceView.addSubview(heldLecturesLabel)
 attendanceView.addSubview(marksHeadingLabel)
 attendanceView.addSubview(mid1HeadingLabel)
 attendanceView.addSubview(mid2HeadingLabel)
 attendanceView.addSubview(endTermHeadingLabel)
 attendanceView.addSubview(mid1MarksLabel)
 attendanceView.addSubview(mid2MarksLabel)
 attendanceView.addSubview(endTermMarksLabel)
 */



/*
_ = subjectTitleLabel.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 80, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
_ = subjectCodeLabel.anchor(subjectTitleLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
_ = facultyNameLabel.anchor(subjectTitleLabel.bottomAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
_ = attendanceView.anchor(subjectCodeLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 45, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 380)

attendanceView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
_ = statisticsHeadingLabel.anchor(attendanceView.topAnchor, left: attendanceView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
_ = attendanceHeadingLabel.anchor(statisticsHeadingLabel.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
_ = attendanceLabel.anchor(attendanceHeadingLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
attendanceLabel.centerXAnchor.constraint(equalTo: attendanceView.centerXAnchor).isActive = true
_ = lecturesHeadingLabel.anchor(hSeperatorLine2.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
_ = attendedLecturesLabel.anchor(lecturesHeadingLabel.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
_ = heldLecturesLabel.anchor(lecturesHeadingLabel.bottomAnchor, left: vSeperatorLine1.rightAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 40, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        _ = hSeperatorLine1.anchor(attendanceHeadingLabel.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: attendanceView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
_ = vSeperatorLine1.anchor(lecturesHeadingLabel.bottomAnchor, left: nil, bottom: attendedLecturesLabel.bottomAnchor, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: vSeperatorWidth, heightConstant: 0)
vSeperatorLine1.centerXAnchor.constraint(equalTo: attendanceView.centerXAnchor).isActive = true
_ = hSeperatorLine1.anchor(subjectCodeLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: hSeperatorHeight)
_ = hSeperatorLine2.anchor(attendanceLabel.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: attendanceView.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: hSeperatorHeight)
_ = hSeperatorLine3.anchor(attendedLecturesLabel.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: attendanceView.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: hSeperatorHeight)
_ = marksHeadingLabel.anchor(hSeperatorLine3.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
_ = mid1HeadingLabel.anchor(marksHeadingLabel.bottomAnchor, left: attendanceView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
_ = mid1MarksLabel.anchor(mid1HeadingLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
mid1MarksLabel.centerXAnchor.constraint(equalTo: mid1HeadingLabel.centerXAnchor).isActive = true
_ = mid2HeadingLabel.anchor(marksHeadingLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
mid2HeadingLabel.centerXAnchor.constraint(equalTo: attendanceView.centerXAnchor).isActive = true
_ = mid2MarksLabel.anchor(mid2HeadingLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
mid2MarksLabel.centerXAnchor.constraint(equalTo: mid2HeadingLabel.centerXAnchor).isActive = true
_ = endTermHeadingLabel.anchor(marksHeadingLabel.bottomAnchor, left: nil, bottom: nil, right: attendanceView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
_ = endTermMarksLabel.anchor(endTermHeadingLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
endTermMarksLabel.centerXAnchor.constraint(equalTo: endTermHeadingLabel.centerXAnchor).isActive = true
*/
