//
//  LecturesCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 27/09/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class LecturesCollectionViewCell: UICollectionViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        setNeedsLayout()
//        layoutIfNeeded()
//        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//        var frame = layoutAttributes.frame
//        frame.size.height = ceil(size.height)
//        layoutAttributes.frame = frame
//        return layoutAttributes
//    }
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        layoutIfNeeded()
//        let layoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
//        layoutAttributes.bounds.size.height = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
//        return layoutAttributes
//    }
    let lecturesHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Lectures"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 25, weight: .heavy)
        label.numberOfLines = 1
        
        return label
    }()
    let attendedLecturesView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemBlueColor
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let heldLecturesView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemBlueColor
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let attendedLecturesValueLabel:UILabel = {
        let label = UILabel()
        label.text = "20"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let heldLecturesValueLabel:UILabel = {
        let label = UILabel()
        label.text = "25"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let attendedLecturesLabel:UILabel = {
        let label = UILabel()
        label.text = "Attended"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let heldLecturesLabel:UILabel = {
        let label = UILabel()
        label.text = "Held"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let hSeperatorLine5 = HorizontalLine(color: .white)
    let hSeperatorLine6 = HorizontalLine(color: .white)
    func setUpViews() {
        attendedLecturesView.addSubview(attendedLecturesValueLabel)
        attendedLecturesView.addSubview(hSeperatorLine5)
        attendedLecturesView.addSubview(attendedLecturesLabel)
        heldLecturesView.addSubview(heldLecturesValueLabel)
        heldLecturesView.addSubview(hSeperatorLine6)
        heldLecturesView.addSubview(heldLecturesLabel)
        contentView.addSubview(attendedLecturesView)
        contentView.addSubview(heldLecturesView)
//        contentView.addSubview(lecturesHeadingLabel)
    }
    func setUpConstraints() {
//        _ = lecturesHeadingLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = attendedLecturesView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 10, bottomConstant: 0, rightConstant: 0)
        attendedLecturesView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 2) - 15).isActive = true
        _ = attendedLecturesValueLabel.anchor(attendedLecturesView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        attendedLecturesValueLabel.centerXAnchor.constraint(equalTo: attendedLecturesView.centerXAnchor).isActive = true
        _ = hSeperatorLine5.anchor(attendedLecturesValueLabel.bottomAnchor, left: attendedLecturesView.leftAnchor, bottom: nil, right: attendedLecturesView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = attendedLecturesLabel.anchor(hSeperatorLine5.bottomAnchor, left: nil, bottom: attendedLecturesView.bottomAnchor, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        attendedLecturesLabel.centerXAnchor.constraint(equalTo: attendedLecturesView.centerXAnchor).isActive = true
        
        _ = heldLecturesView.anchorWithoutWH(top: contentView.topAnchor, left: nil, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 10)
        heldLecturesView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 2) - 15).isActive = true
        _ = heldLecturesValueLabel.anchor(heldLecturesView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        heldLecturesValueLabel.centerXAnchor.constraint(equalTo: heldLecturesView.centerXAnchor).isActive = true
        _ = hSeperatorLine6.anchor(heldLecturesValueLabel.bottomAnchor, left: heldLecturesView.leftAnchor, bottom: nil, right: heldLecturesView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = heldLecturesLabel.anchor(hSeperatorLine6.bottomAnchor, left: nil, bottom: heldLecturesView.bottomAnchor, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        heldLecturesLabel.centerXAnchor.constraint(equalTo: heldLecturesView.centerXAnchor).isActive = true
    }
}
