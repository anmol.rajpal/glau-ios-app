//
//  SubjectInfoTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 17/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit
protocol SubjectInfoCellDelegate: class {
    func morePressed(cell:SubjectInfoTableViewCell)
    func disclosurePressed(cell:SubjectInfoTableViewCell)
}
class SubjectInfoTableViewCell: UITableViewCell {
    weak var delegate:SubjectInfoCellDelegate?
    var subjectName:String? {
        didSet {
            subjectNameLabel.text = subjectName?.capitalized ?? ""
        }
    }
    var facultyName:String? {
        didSet {
            facultyNameLabel.text = facultyName?.capitalized ?? ""
        }
    }
    var subjectType:String? {
        didSet {
            subjectTypeLabel.text = subjectType?.uppercased() ?? ""
        }
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setUpViews()
//        setUpConstraints()
//    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let topBarView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemPurpleColor
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.clipsToBounds = true
        return view
    }()
    let subjectTypeLabel:UILabel = {
        let label = UILabel()
        label.text = SubjectType.theory.rawValue.uppercased()
        label.textColor = .white
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        label.numberOfLines = 1
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.8
        label.layer.shadowOffset = CGSize.zero
        label.layer.shadowRadius = 10
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let subjectNameLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1).condensed()
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.textAlignment = .left
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let subjectCodeLabel:UILabel = {
        let label = UILabel()
        label.text = "CSE CODE"
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let facultyNameLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let facultyHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "(Faculty)"
        label.textColor = UIColor.darkGray
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var actionButton:UIButton = {
        let origImage = UIImage(named: "more")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = UIColor.systemBlueColor
        button.backgroundColor = UIColor.white
        button.addTarget(self, action: #selector(showActions), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    lazy var facultyDisclosureButton:UIButton = {
        let origImage = UIImage(named: "info")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = UIColor.systemBlueColor
        button.backgroundColor = UIColor.white
        button.addTarget(self, action: #selector(showFacultyVC), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    @objc func showFacultyVC() {
        delegate?.disclosurePressed(cell: self)
    }
    @objc func showActions() {
        delegate?.morePressed(cell: self)
    }
    //    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
    //        let a  = bounds.size.width
    //        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    //    }
    func setUpViews() {
        //        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(topBarView)
        topBarView.addSubview(subjectTypeLabel)
        contentView.addSubview(subjectNameLabel)
        contentView.addSubview(facultyNameLabel)
        contentView.addSubview(facultyHeadingLabel)
        contentView.addSubview(actionButton)
//        contentView.addSubview(facultyDisclosureButton)
    }
    func setUpConstraints() {
        let leftRightMargin:CGFloat = 15
        _ = topBarView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: subjectTypeLabel.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -1, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = subjectTypeLabel.anchorWithoutWH(top: topBarView.topAnchor, left: nil, bottom: nil, right: topBarView.rightAnchor, topConstant: 1, leftConstant: 0, bottomConstant: 0, rightConstant: 10)
        _ = subjectNameLabel.anchorWithoutWH(top: topBarView.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = facultyNameLabel.anchorWithoutWH(top: subjectNameLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = facultyHeadingLabel.anchorWithoutWH(top: facultyNameLabel.bottomAnchor, left: facultyNameLabel.leftAnchor, bottom: contentView.bottomAnchor, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 10, rightConstant: 0)
//        _ = facultyDisclosureButton.anchor(nil, left: facultyHeadingLabel.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 25, heightConstant: 25)
//        facultyDisclosureButton.centerYAnchor.constraint(equalTo: facultyHeadingLabel.centerYAnchor).isActive = true
        _ = actionButton.anchor(nil, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 24, heightConstant: 24)
        actionButton.centerYAnchor.constraint(equalTo: facultyHeadingLabel.centerYAnchor).isActive = true
//        let b = facultyHeadingLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
//        b.priority = UILayoutPriority(999)
//        b.isActive = true
    }
}
