//
//  TimeTableSubjectCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 07/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class TimeTableSubjectCell: UITableViewCell {
    var timeTable:TimeTableCodable? {
        didSet {
            setUpTimeTableCell(subjectData: timeTable)
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setUpTimeTableCell(subjectData:TimeTableCodable?) {
        lectureNoLabel.text = "Lecture - \(subjectData?.lectureNo ?? "--")"
        subjectNameLabel.text = subjectData?.subjectName?.capitalized
        timingLabel.text =  subjectData?.timing
        let details = subjectData?.details
        let components = details?.components(separatedBy: "/")
        let subjectCode = components?[0]
        let subjectType = components?[1]
        let blockNo = components?[2] ?? "--"
        let roomNo = components?[3] ?? "--"
        subjectCodeLabel.text = subjectCode
        subjectTypeLabel.text = subjectType?.uppercased()
        locationLabel.text = "\(roomNo) in \(blockNo)"
        facultyNameLabel.text = subjectData?.facultyName?.capitalized
    }
    private let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        view.layer.cornerRadius = 10;
//        view.layer.borderWidth = 0.5
//        view.layer.borderColor = UIColor.gray.cgColor
        return view
    }()
    private let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let view = UIVisualEffectView(effect: blurEffect)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        return view
    }()
    let topBarView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.backgroundColor = UIColor.systemBlueColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
//        view.layer.masksToBounds = true
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner
        return view
    }()
    let lectureNoLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        return label
    }()
    let subjectTypeLabel:UILabel = {
        let label = UILabel()
        label.text = SubjectType.theory.rawValue.uppercased()
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)
        label.textColor = .white
//        label.layer.shadowColor = UIColor.black.cgColor
//        label.layer.shadowOpacity = 0.8
//        label.layer.shadowOffset = CGSize.zero
//        label.layer.shadowRadius = 5
        return label
    }()
    let subjectNameLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 21, weight: .medium)
        return label
    }()
    let subjectCodeLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .gray
        return label
    }()
    let timingLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .heavy)
        return label
    }()
    let facultyNameLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return label
    }()
    let locationLabel:UILabel = {
        let label = UILabel()
        label.text = "--"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.textColor = .gray
        return label
    }()
    func setUpViews() {
//        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
        contentView.backgroundColor = .clear
        containerView.addSubview(blurredEffectView)
        containerView.addSubview(topBarView)
        containerView.addSubview(lectureNoLabel)
        containerView.addSubview(subjectTypeLabel)
        containerView.addSubview(subjectNameLabel)
        containerView.addSubview(subjectCodeLabel)
//        containerView.addSubview(facultyNameLabel)
        containerView.addSubview(timingLabel)
        containerView.addSubview(locationLabel)
        contentView.addSubview(containerView)
    }
    func setUpConstraints() {
        let margin:CGFloat = 10
        _ = containerView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 7.5, leftConstant: margin, bottomConstant: 7.5, rightConstant: margin)
        _ = blurredEffectView.anchorWithoutWH(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        _ = topBarView.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: subjectTypeLabel.bottomAnchor, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -2, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = lectureNoLabel.anchorWithoutWH(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 15, bottomConstant: 0, rightConstant: 0)
        _ = subjectTypeLabel.anchor(containerView.topAnchor, left: nil, bottom: nil, right: containerView.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = subjectNameLabel.anchorWithoutWH(top: topBarView.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 10)
        _ = subjectCodeLabel.anchorWithoutWH(top: subjectNameLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 0)
        _ = timingLabel.anchorWithoutWH(top: subjectCodeLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 0)
        _ = locationLabel.anchorWithoutWH(top: timingLabel.bottomAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 5, rightConstant: 15)
        let bottomConstraint = locationLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -margin)
        bottomConstraint.priority = UILayoutPriority(999)
        bottomConstraint.isActive = false
//        _ = facultyNameLabel.anchorWithoutWH(top: detailsLabel.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 15, rightConstant: 15)
    }
}
