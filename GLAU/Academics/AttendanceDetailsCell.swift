//
//  AttendanceDetailsCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 09/12/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class AttendanceDetailsCell: UITableViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    var serialNo = String()
    var date = String()
    var info = String()
    func setAttributedDate(dateString:String) -> String? {
        if let date = getDateFromString(dateString: dateString, dateFormat: NSObject.DateFormatType.dateType1) {
        let str = getStringFromDate(date: date, dateFormat: NSObject.DateFormatType.dateType2)
        return str
        }
        return nil
    }
    func setUpAttendanceDetails(details:AttendanceDetailsCodable) {
        self.serialNo = details.sNo ?? "--"
        self.date = details.lectureDate ?? "--"
        self.info = details.details ?? "--"
        serialNoLabel.text = serialNo
        dateLabel.text = setAttributedDate(dateString: date) ?? "--"
        detailsLabel.text = info
    }
    let serialNoLabel:UILabel = {
        let label = UILabel()
        label.text = "1"
        
//        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        return label
    }()
    let dateLabel:UILabel = {
        let label = UILabel()
        label.text = "Nov 6"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
//        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        return label
    }()
    let detailsLabel:UILabel = {
        let label = UILabel()
        label.text = "Lec 5 (P)"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
//        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        return label
    }()
    func setUpViews() {
        contentView.addSubview(serialNoLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(detailsLabel)
    }
    func setUpConstraints() {
        serialNoLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
//        serialNoLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true
        serialNoLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
//        serialNoLabel.rightAnchor.constraint(equalTo: dateLabel.leftAnchor, constant: 10).isActive = true
        serialNoLabel.widthAnchor.constraint(equalToConstant: 20).isActive = true
//        dateLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true
        dateLabel.leftAnchor.constraint(equalTo: serialNoLabel.rightAnchor, constant: 5).isActive = true
//        detailsLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        detailsLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 5).isActive = true
        detailsLabel.leftAnchor.constraint(equalTo: dateLabel.leftAnchor, constant: 0).isActive = true
        detailsLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -5).isActive = true
        detailsLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
    }
}
