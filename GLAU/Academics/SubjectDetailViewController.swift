//
//  SubjectDetailViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 10/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class SubjectDetailViewController: UIViewController {
    var attendanceDetails = [AttendanceDetailsCodable]()
    var subjectName = String()
    var subjectCode = String()
    var faculty = String()
    var attendance = Float()
    var attendedLectures = Int()
    var heldLectures = Int()
    var mod1Marks = String()
    var mod2Marks = String()
    var mod3Marks = String()
    var subjectType = String()
    var facultyEmail = String()
    func setUpDetails(details:AcademicDetails) {
        self.subjectName = details.subjectName ?? "- -"
        self.subjectCode = details.subjectCode ?? "- -"
        self.faculty = details.faculty ?? "- -"
        self.attendance = (details.percentage! as NSString).floatValue
        self.attendedLectures = (details.attend! as NSString).integerValue
        self.heldLectures = (details.held! as NSString).integerValue
        self.mod1Marks = details.m1Mark ?? "- -"
        self.mod2Marks = details.m2Mark ?? "- -"
        self.mod3Marks = details.endMark ?? "- -"
        self.subjectType = details.subjectType ?? "--"
        self.facultyEmail = details.facultyEmail ?? "--"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setUpNavBarItems()
        view.backgroundColor = .white
        navigationItem.title = subjectCode
        setUpViews()
        configure(collectionView: collectionView)
        setUpConstraints()
        collectionView.reloadDataWithLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        fetchAttendanceDetails()
    }
    
   
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        collectionView.reloadData()
//
//    }
    override var previewActionItems: [UIPreviewActionItem] {
        let lodgeLeaveAction = UIPreviewAction(title: "Lodge Leave", style: .default) { (action, viewController) -> Void in
            print("Lodge Leave Request Feature Coming Soon")
        }
        let shareAction = UIPreviewAction(title: "Share", style: .default) { (action, viewController) in
            print("Share feature coming soon")
        }
        let feedbackAction = UIPreviewAction(title: "Feedback", style: .default) { (action, viewController) in
            print("Feedback feature coming soon")
        }
        let reportAction = UIPreviewAction(title: "Report", style: .destructive) { (action, viewController) -> Void in
            print("Report Action coming Soon")
        }
        let previewActionItems:Array = [lodgeLeaveAction, shareAction, feedbackAction, reportAction]
        return previewActionItems
    }
    func setUpNavBarItems() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(share))
    }
    @objc func share() {
        let textToShare = "Welcome to the Future!"
        if let myWebsite = NSURL(string: "https://www.natovi.com") {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    lazy var collectionViewFlowLayout:UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
//        layout.itemSize = UICollectionViewFlowLayout.automaticSize
//        let width = UIScreen.main.bounds.size.width
//        let width = collectionView.frame.width
        let width = view.frame.width
        layout.estimatedItemSize = CGSize(width: width, height: 40)
//        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        return layout
    }()
    lazy var collectionView : UICollectionView = {
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .vertical
//        layout.itemSize = UICollectionViewFlowLayout.automaticSize
//        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewFlowLayout)
//        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceVertical = true
//        cv.clipsToBounds = true
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        return cv
    }()
    func setUpViews() {
        view.addSubview(collectionView)
    }
    func setUpConstraints() {
        _ = collectionView.anchorWithoutWH(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 10, rightConstant: 0)
    }
    
}

//extension SubjectDetailViewController: AttendanceCellDelegate {
//    func animateAttendance(cell: AttendanceCollectionViewCell) {
//        DispatchQueue.main.async {
//            UIView.animate(withDuration: 1.0) {
//                cell.ringProgressView.progress = Double(self.attendance/100)
//                cell.ringProgressView.shadowOpacity = 0
//                if self.attendance >= 75 {
//                    cell.ringProgressView.startColor = .systemGreenColor
//                } else {
//                    cell.ringProgressView.startColor = .systemRedColor
//                }
//            }
//        }
//    }
////    func potty(cell: AttendanceCollectionViewCell) {
////        cell.tableView.register(AttendanceDetailsCell.self, forCellReuseIdentifier: NSStringFromClass(AttendanceDetailsCell.self))
////        cell.tableView.dataSource = self as SubjectDetailViewController
////        cell.tableView.delegate = self as SubjectDetailViewController
////        cell.tableView.reloadDataWithLayout()
////    }
//}
extension SubjectDetailViewController: CustomCellDelegate {
    func morePressed(cell: SubjectInfoCollectionViewCell) {
        let lodgeLeaveAction = UIAlertAction(title: "Lodge Leave",
                                          style: .default) { (action) in
                                            UIAlertController.showAlert(alertTitle: "Lodge Leave", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
        }
        let mailFacultyAction = UIAlertAction(title: "Mail Faculty",
                                             style: .default) { (action) in
                                                UIAlertController.showAlert(alertTitle: "Call Faculty", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
        }
        let feedbackAction = UIAlertAction(title: "Feedback",
                                             style: .default) { (action) in
                                                UIAlertController.showAlert(alertTitle: "Feedback", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
        }
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel) { (action) in
                                            
        }
        
        // Create and configure the alert controller.
        let alert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.addAction(lodgeLeaveAction)
        alert.addAction(mailFacultyAction)
        alert.addAction(feedbackAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func disclosurePressed(cell: SubjectInfoCollectionViewCell) {
        let facultyDetailViewController = FacultyDetailViewController()
        facultyDetailViewController.modalPresentationStyle = .overFullScreen
//        facultyDetailViewController.transitioningDelegate = self
        facultyDetailViewController.facultyNameLabel.text = self.faculty.capitalized
        facultyDetailViewController.email = facultyEmail
        self.present(facultyDetailViewController, animated: true, completion: nil)
    }
    
}

class HalfSizePresentationController : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            guard let theView = containerView else { return CGRect.zero }
            return CGRect(x: 0, y: theView.bounds.height/2, width: theView.bounds.width, height: theView.bounds.height/2)
        }
    }
}
//extension SubjectDetailViewController: UIViewControllerTransitioningDelegate {
//    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
//        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
//    }
//}
//extension SubjectDetailViewController : UIViewControllerTransitioningDelegate {
//    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
//        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
//    }
//}
//extension SubjectDetailViewController:UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 8
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(AttendanceDetailsCell.self), for: indexPath) as! AttendanceDetailsCell
//        cell.selectionStyle = .none
////        let details = attendanceDetails[indexPath.row]
////        print(details)
////        cell.setUpAttendanceDetails(details: details)
//        cell.dateLabel.text = "8 Nov, 2018"
//        tableView.reloadDataWithLayout()
//        return cell
//    }
//
//
//}
extension SubjectDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - Configuration
   
    internal func configure(collectionView: UICollectionView) {
        collectionView.register(SubjectsHeaderCollectionViewCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: NSStringFromClass(SubjectsHeaderCollectionViewCell.self))
        collectionView.register(SubjectDetailFooterCollectionViewCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: NSStringFromClass(SubjectDetailFooterCollectionViewCell.self))
        collectionView.register(SubjectInfoCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(SubjectInfoCollectionViewCell.self))
        collectionView.register(AttendanceCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(AttendanceCollectionViewCell.self))
        collectionView.register(LecturesCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(LecturesCollectionViewCell.self))
        collectionView.register(MarksCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(MarksCollectionViewCell.self))
        collectionView.register(AssignmentsCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(AssignmentsCollectionViewCell.self))
        collectionView.register(SyllabusCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(SyllabusCollectionViewCell.self))
        collectionView.register(hmmCollectionViewCell.self, forCellWithReuseIdentifier: "i")
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "listCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        
//        let l = UICollectionViewFlowLayout()
//        l.scrollDirection = .vertical
//        l.estimatedItemSize = CGSize(width: collectionView.frame.width, height: 100)
//        collectionView.collectionViewLayout = l
        
       
    }
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if(kind == UICollectionView.elementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(SubjectsHeaderCollectionViewCell.self), for: indexPath) as! SubjectsHeaderCollectionViewCell
            if indexPath.section == 1 {
                headerView.headerLabel.text = "Attendance"
            } else if indexPath.section == 2 {
                headerView.headerLabel.text = "Lectures"
            } else if indexPath.section == 3 {
                headerView.headerLabel.text = "Marks"
            } else {
                headerView.headerLabel.text = ""
            }
            
            return headerView
        } else {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(SubjectDetailFooterCollectionViewCell.self), for: indexPath) as! SubjectDetailFooterCollectionViewCell
            if indexPath.section == 0 {
                return footerView
            }
        }
        fatalError()
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(SubjectInfoCollectionViewCell.self), for: indexPath) as! SubjectInfoCollectionViewCell
            cell.delegate = self
            cell.subjectTypeLabel.text = subjectType.uppercased()
            cell.subjectNameLabel.text = subjectName.capitalized
            cell.facultyNameLabel.text = faculty.capitalized
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(AttendanceCollectionViewCell.self), for: indexPath) as! AttendanceCollectionViewCell
            cell.attendanceLabel.backgroundColor = setUpAttendanceColor(attendance: attendance)
//            cell.delegate = self
            cell.setAttributedAttendance(attendance: attendance)
//            print(self.attendanceDetails.count)
//            cell.attendanceDetails = attendanceDetails
            cell.subjectCode = subjectCode
//            cell.tableView.reloadDataWithLayout()
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(LecturesCollectionViewCell.self), for: indexPath) as! LecturesCollectionViewCell
            cell.attendedLecturesValueLabel.text = String(attendedLectures)
            cell.heldLecturesValueLabel.text = String(heldLectures)
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(MarksCollectionViewCell.self), for: indexPath) as! MarksCollectionViewCell
            cell.module1MarksLabel.text = mod1Marks
            cell.module2MarksLabel.text = mod2Marks
            cell.module3MarksLabel.text = mod3Marks
            return cell
        default: let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCell", for: indexPath)
            return cell
        }
    }
    
  
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize.zero
        } else {
            return CGSize(width: self.view.frame.width, height: 35)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize(width: self.view.frame.width, height: 20)
        } else {
            return CGSize.zero
        }
    }
    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let staticSize = CGSize(width: collectionView.bounds.width, height: 140)
//        return staticSize
//    }
        /*
        if indexPath.section == 0 {
            return CGSize(width: collectionView.bounds.width, height: 140)
        } else if indexPath.section == 1 {
            return CGSize(width: collectionView.bounds.width, height: 150)
        } else {
            return CGSize(width: collectionView.bounds.width, height: 120)
        }
        */
        return UICollectionViewFlowLayout.automaticSize
    }
    */
}
