//
//  NewAcademicsViewController.swift
//  GLAU
//
//  Created by Anmol Rajpal on 28/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
import MessageUI
class NewAcademicsViewController: UIViewController {
    var academicDetails:[AcademicDetails]? = []
    var studentDetails:StudentDetailsCodable?
    var advisor:String?
    var advisorEmail:String?
    var advisorContact:String?
    var overallAttendance:Float?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        checkReachability()
        setupNavBar()
        setUpNavBarItems()
        setUpViews()
        setUpConstraints()
        setUpTableView()
//        fetchData()
        fetchAllServices()
        StoreKitHelper.checkAndAskForReview()
        if(traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: tableView)
        }
//        view.layoutIfNeeded()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkReachability()
        setupNavBar()
        reloadAnimation()
        self.navigationItem.title = "Academics"
    }
    func reloadAnimation() {
        guard studentDetails != nil else { return }
        let indexPath = IndexPath(row: 0, section: 0)
//        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
//        tableView.layoutIfNeeded()
//        tableView.endUpdates()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let selectionIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectionIndexPath, animated: animated)
        }
    }
    func setUpTableView() {
        tableView.layoutIfNeeded()
        tableView.refreshControl = self.refreshControl
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .white
        tableView.register(SubjectDetailsTableHeaderView.self, forHeaderFooterViewReuseIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self))
        tableView.register(NewAcademicsInfoCell.self, forCellReuseIdentifier: NSStringFromClass(NewAcademicsInfoCell.self))
        tableView.register(NewAcademicsSubjectCell.self, forCellReuseIdentifier: NSStringFromClass(NewAcademicsSubjectCell.self))
//        tableView.reloadDataWithLayout()
    }
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        //        refreshControl.backgroundColor = UIColor.glauThemeColor
        
        refreshControl.tintColor = UIColor.glauThemeColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        return refreshControl
    }()
    lazy var tableView : UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .white
        tv.bounces = true
        tv.alwaysBounceVertical = true
        tv.clipsToBounds = true
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = true
        tv.separatorStyle = .none
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        tv.isHidden = false
        tv.tableFooterView = UIView(frame: CGRect.zero)
//        tv.estimatedRowHeight = 100
//        tv.rowHeight = UITableView.automaticDimension
        tv.alpha = 0
        return tv
    }()
    func setUpNavBarItems() {
//        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Advisor", style: .plain, target: self, action: #selector(handleLeftBarButtonItem))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "TimeTable", style: .plain, target: self, action: #selector(handleRightBarButtonItem))
        
    }
    func addLeftNavigationBarItem() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Advisor", style: .plain, target: self, action: #selector(handleLeftBarButtonItem))
    }
    func removeLeftNavigationBarItem() {
        navigationItem.leftBarButtonItem = nil
    }
    @objc func handleLeftBarButtonItem() {
//        self.view.layer.shouldRasterize = true
        let facultyDetailViewController = FacultyDetailViewController()
        facultyDetailViewController.modalPresentationStyle = .overFullScreen
        facultyDetailViewController.facultyNameLabel.text = self.advisor?.capitalized
        facultyDetailViewController.email = self.advisorEmail
        facultyDetailViewController.phone = self.advisorContact
        self.present(facultyDetailViewController, animated: true, completion: nil)
    }
    @objc func handleRightBarButtonItem() {
        self.view.layer.shouldRasterize = true
        let timeTableViewController = TimeTableViewController()
        timeTableViewController.delegate = self
        timeTableViewController.modalPresentationStyle = .overFullScreen
        self.present(timeTableViewController, animated: true, completion: nil)
    }
    func showTimeTable() {
        let timeTableViewController = TimeTableViewController()
        timeTableViewController.modalPresentationStyle = .overFullScreen
        present(timeTableViewController, animated: true, completion: nil)
    }
    fileprivate func showTableView() {
//        tableView.alpha = 0
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//            self.tableView.isHidden = false
            self.tableView.alpha = 1.0
            self.tableView.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
    fileprivate func hideTableView() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.tableView.alpha = 0.0
        }, completion: nil)
    }
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel(message:String = "Cannot Connect to GLAU") {
        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
//        spinner.startAnimating()
        if !tableView.isDragging {
//            fetchData()
            print("Table view isn't dragging")
        }
        if refreshControl.isRefreshing == true {
            self.removeLeftNavigationBarItem()
            let dispatchTime:DispatchTime = .now()
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                self.fetchAllServices()
            }
        }
    }
    var isLoaded:Bool = false
    func reloadSection(section:Int) {
//        let lecturesIndexPath = IndexPath(row: 1, section: 1)
//        self.tableView.reloadSections(IndexSet(integer: section), with: .fade)
//        self.tableView.beginUpdates()
        self.tableView.reloadSections(IndexSet(integer: section), with: .automatic)
//        self.tableView.setNeedsLayout()
//        self.tableView.layoutIfNeeded()
//        self.tableView.reloadSectionIndexTitles()
//        self.tableView.endUpdates()
    }
    func reloadRow(row:Int, section:Int) {
        let indexPath = IndexPath(row: row, section: section)
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
//        self.tableView.layoutIfNeeded()
        self.tableView.endUpdates()
    }
    func reload() {
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: self.tableView.indexPathsForVisibleRows ?? [IndexPath(row: 0, section: 1)], with: .none)
        self.tableView.endUpdates()
    }
    func reloadRows(indexPaths:[IndexPath], section:Int) {
        
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: indexPaths, with: UITableView.RowAnimation.automatic)
        self.tableView.endUpdates()
    }
    private func fetchAllServices() {
        fetchAcademicDetails()
        fetchAcademicSubjects()
    }
    private func fetchAcademicDetails() {
        let rollNo = UserDefaults.standard.getRollNo()
        StudentDetailsService.shared.fetch(rollNo: rollNo) { (data, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.7, execute: {
                
            
                guard error == nil else {
                    print(error?.localizedDescription ?? "Some Error Occured")
                    self.spinner.stopAnimating()
                    self.refreshControl.endRefreshing()
                    self.showAlert()
                    return
                }
                
                let result = data?.result
                let message = data?.message
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    self.spinner.stopAnimating()
                    self.refreshControl.endRefreshing()
                case ResultType.Success.rawValue:
                    if let data = data {
                        
                        self.studentDetails = data
                        self.reloadSection(section: 0)
//                        self.tableView.beginUpdates()
//                        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.none)
//
//                        self.tableView.endUpdates()
//                        self.fetchAcademicSubjects()
//                        UIView.animate(withDuration: 1.0, animations: {
//                            self.reloadRow(row: 0, section: 0)
//                        })
//                        self.reloadRow(row: 0, section: 0)
//                        self.tableView.layoutIfNeeded()
                    }
                default: break
                }
            })
        }
    }
    private func showAlert() {
        let alertVC = UIAlertController(title: "Connection Error", message: "There was a problem connecting with server.", preferredStyle: UIAlertController.Style.alert)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { (_) -> Void in
            UIView.animate(withDuration: 0.3, animations: {
                self.tableView.alpha = 0
            })
            self.spinner.startAnimating()
            self.fetchAllServices()
        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertVC.addAction(retryAction)
        DispatchQueue.main.async {
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    private func fetchAcademicSubjects() {
        let rollNo = UserDefaults.standard.getRollNo()
        AcademicDetailsService.shared.fetchAcademicDetails(rollNo) { (data, error) in
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
                
                guard error == nil else {
                    print(error?.localizedDescription ?? "Error Occured")
                    self.academicDetails?.removeAll()
                    self.spinner.stopAnimating()
                    self.refreshControl.endRefreshing()
                    self.showAlert()
//                    self.hideTableView()
//                    self.messageLabel.isHidden = false
//                    self.retryButton.isHidden = false
//                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?[0].result
                let message = data?[0].message
                
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    self.academicDetails?.removeAll()
                    UIView.animate(withDuration: 0.3, animations: {
                        self.tableView.alpha = 1
                    })
                    self.spinner.stopAnimating()
                    self.refreshControl.endRefreshing()
//                    self.hideTableView()
//                    self.messageLabel.text = "Cannot load Subjects"
//                    self.messageLabel.isHidden = false
//                    self.retryButton.isHidden = false
//                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        var total:Float = 0
                        self.academicDetails = data
                        if let adv = data[0].advisor {
                            self.advisor = adv
//                            self.reloadRow(row: 0, section: 0)
                        }
                        
                        self.advisorEmail = data[0].advisorEmail
                        self.advisorContact = data[0].advisorContact
                        for i in data {
                            total += (i.percentage! as NSString).floatValue
                        }
                        self.overallAttendance = total/Float(data.count)
                       
//                        self.reloadRows(indexPaths: arr, section: 1)
                        UIView.animate(withDuration: 0.3, animations: {
                            self.tableView.alpha = 1
                        })
                        
                        self.reloadSection(section: 1)
                        self.addLeftNavigationBarItem()
//                        self.tableView.reloadData()
//                        self.fetchAcademicDetails()
//                        self.reload()
//                        self.tableView.layoutIfNeeded()
                        self.spinner.stopAnimating()
                        self.refreshControl.endRefreshing()
                    }
//                    self.showTableView()
                    
//                    self.spinner.stopAnimating()
                default: break
                }
            })
        }
    }
    private func fetchData() {
//        spinner.startAnimating()
        self.hideTableView()
        self.messageLabel.isHidden = true
        self.retryButton.isHidden = true
        let rollNo = UserDefaults.standard.getRollNo()
        AcademicDetailsService.shared.fetchAcademicDetails(rollNo) { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    self.hideTableView()
                    self.messageLabel.isHidden = false
                    self.retryButton.isHidden = false
                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?[0].result
                let message = data?[0].message
                
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
//                    let alertVC = UIAlertController(title: message, message: "Something went wrong. Please login again.", preferredStyle: UIAlertController.Style.alert)
//                    alertVC.addAction(UIAlertAction(title: "Log in", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
//                        self.handleSignOut()
//                    })
//                    self.present(alertVC, animated: true, completion: nil)
                    self.hideTableView()
                    self.messageLabel.text = "Cannot load Subjects"
                    self.messageLabel.isHidden = false
                    self.retryButton.isHidden = false
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        var total:Float = 0
                        self.academicDetails = data
                        self.advisor = data[0].advisor
                        self.advisorEmail = data[0].advisorEmail
                        for i in data {
                            total += (i.percentage! as NSString).floatValue
                        }
                        self.overallAttendance = total/Float(data.count)
                        self.tableView.reloadDataWithLayout()
                    }
                    self.showTableView()
                    self.refreshControl.endRefreshing()
                    self.spinner.stopAnimating()
                default: break
                }
            }
        }
        StudentDetailsService.shared.fetch(rollNo: rollNo) { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print(error?.localizedDescription ?? "Some Error Occured")
                    self.hideTableView()
                    self.messageLabel.isHidden = false
                    self.retryButton.isHidden = false
//                    self.spinner.stopAnimating()
                    return
                }

                let result = data?.result
                let message = data?.message
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    self.hideTableView()
                    self.messageLabel.text = "Cannot load Details"
                    self.messageLabel.isHidden = false
                    self.retryButton.isHidden = false
//                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.studentDetails = data
                    }
                    self.isLoaded = true
//                    self.updateViews()
                    self.tableView.reloadDataWithLayout()
//                    self.spinner.stopAnimating()
                default: break
                }
            }
        }
    }
    var cellHeights: [IndexPath : CGFloat] = [:]
    func setUpViews() {
        view.addSubview(tableView)
        view.addSubview(spinner)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = tableView.anchorWithoutWH(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
}
extension NewAcademicsViewController: UITableViewDelegate, UITableViewDataSource {
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
////        refreshControl.programaticallyBeginRefreshing(in: tableView)
//        if refreshControl.isRefreshing == true {
//            UIView.animate(withDuration: 0.4) {
////                self.tableView.alpha = 0
//            }
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
//                self.fetchAllServices()
//            }
//            let dispatchTime:DispatchTime = .now() + 0.2
//            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
//                self.navigationController?.navigationBar.isTranslucent = true
//                self.navigationController?.navigationBar.isTranslucent = false
//                UIView.animate(withDuration: 0.4) {
//
////                    self.tableView.alpha = 1
//                    let offsetPoint = CGPoint.init(x: 0, y: -self.refreshControl.frame.size.height)
//                    let offset = CGPoint(x: 0, y: -100)
////                    self.tableView.setContentOffset(offset, animated: true)
//                }
//
//                self.refreshControl.endRefreshing()
//            }
//        }
//    }
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if refreshControl.isRefreshing == true {
//            let dispatchTime:DispatchTime = .now() + 0.5
//            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
//                self.fetchAllServices()
//            }
//        }
//    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            case 0: return (studentDetails?.rollNo?.isEmpty == true) ? 0 : 1
            case 1: return academicDetails?.count ?? 0
            default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(NewAcademicsInfoCell.self), for: indexPath) as! NewAcademicsInfoCell
            cell.delegate = self
            cell.selectionStyle = .none
            cell.backgroundColor = .white
            if !(self.studentDetails?.rollNo?.isEmpty ?? true) {
                if let ad = self.advisor {
                    cell.classAdvisor = ad
//                    self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.automatic)
                }
                cell.studentDetails = self.studentDetails
//                cell.layoutIfNeeded()
//                print(cell.frame.height)
            } else {
                cell.isHidden = true
            }
//            cell.setUpDetails(details: self.studentDetails)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(NewAcademicsSubjectCell.self), for: indexPath) as! NewAcademicsSubjectCell
            cell.selectionStyle = .none
            cell.backgroundColor = .white
//            cell.accessoryType = .disclosureIndicator
            if !(academicDetails?.isEmpty ?? true) {
                let subject = academicDetails?[indexPath.row]
                cell.subjectType = subject?.subjectType
                cell.subjectName = subject?.subjectName
                cell.subjectCode = subject?.subjectCode
                cell.attendance = subject?.percentage
//                print(cell.frame.height)
//                cell.layoutIfNeeded()
            } else {
                cell.isHidden = true
            }
//            cell.subjectNameLabel.text = subject.subjectName?.capitalized
//            cell.subjectCodeLabel.text = subject.subjectCode
//            cell.subjectTypeLabel.text = subject.subjectType?.uppercased()
//            cell.attendanceLabel.text = ("\(subject.percentage ?? "-") %")
//            let attendance:Float = (subject.percentage! as NSString).floatValue
//            cell.attendanceLabel.backgroundColor = setUpAttendanceColor(attendance: attendance)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            guard !(self.academicDetails?.isEmpty ?? true) else { return }
            let subjectDetailViewController = SubjectDetailsViewController()
            let details = academicDetails?[indexPath.row]
//            subjectDetailViewController.setUpDetails(details: details)
            subjectDetailViewController.details = details
            subjectDetailViewController.view.backgroundColor = .white
            subjectDetailViewController.view.layoutIfNeeded()
            self.show(subjectDetailViewController, sender: self)
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self)) as! SubjectDetailsTableHeaderView
//        if section == 0 {
//            headerView.headerLabel.text = "Semester Info"
//        } else
//        headerView.layoutIfNeeded()
//        headerView.frame.size.width = self.view.frame.width
        if section == 1 {
            headerView.headerTitle = "Subjects"
        } else {
            headerView.headerTitle = ""
        }
        return headerView
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
//        if section == 0 {
//            return 0
//        } else {
//            return 35
//        }
//    }
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        headerHeights[section] = view.frame.size.height
//    }
//    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
//        return headerHeights[section] ?? 35.0
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            if (self.academicDetails?.isEmpty ?? true) {
                    return 0
                } else {
                    return 35
                }
        default: return 0
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.section == 0 {
//            if let c = cell as? NewAcademicsInfoCell {
//                print("turrr")
//                c.cellWillDisplaySignalling()
//            }
//        }
        cellHeights[indexPath] = cell.frame.size.height
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
////        return UITableView.automaticDimension
////        if indexPath.section == 0 {
////            if (self.studentDetails?.rollNo?.isEmpty ?? true) { return 0 } else { return UITableView.automaticDimension }
////        } else {
////            if self.academicDetails?.isEmpty ?? true { return 0 } else { return UITableView.automaticDimension }
////        }
//    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 0 {
//            return 190
//        } else {
//            return 100
//        }
//        if indexPath.section == 0 {
//            return 190
//        }
        return cellHeights[indexPath] ?? 0.0
    }
    
}
extension NewAcademicsViewController: NewAcademicsInfoCellDelegate {
    func infoBoxTapped(cell: NewAcademicsInfoCell) {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:  #selector(self.handleTouchEvent(_:)))
        cell.infoView.addGestureRecognizer(gestureRecognizer)
    }
    @objc func handleTouchEvent(_ sender:UITapGestureRecognizer){
        let profileVC = ProfileTableViewController()
        tabBarController?.selectedIndex = 2
        (tabBarController?.selectedViewController as! UINavigationController).popToRootViewController(animated: false)
        tabBarController?.selectedViewController?.show(profileVC, sender: self)
    }
    func disclosurePressed(cell: NewAcademicsInfoCell) {
        let facultyDetailViewController = FacultyDetailViewController()
        facultyDetailViewController.modalPresentationStyle = .overFullScreen
        facultyDetailViewController.facultyNameLabel.text = self.advisor?.capitalized
        facultyDetailViewController.email = self.advisorEmail
        facultyDetailViewController.phone = self.advisorContact
        self.present(facultyDetailViewController, animated: true, completion: nil)
    }
}
extension NewAcademicsViewController: SubjectDetailsViewControllerDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    func callFaculty(phoneNo: String) {
        if let url = URL(string: "tel://\(phoneNo)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    func messageFaculty(phoneNo: String) {
        guard MFMessageComposeViewController.canSendText() else {
            self.showSendMessageErrorAlert()
            return
        }
        let messageComposeVC = configureMessageComposeViewController(phoneNo: phoneNo)
        messageComposeVC.modalPresentationStyle = .overFullScreen
        self.present(messageComposeVC, animated: true, completion: nil)
    }
    func configureMessageComposeViewController(phoneNo:String) -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        messageComposeVC.recipients = [phoneNo]
//        messageComposeVC.modalPresentationStyle = .overFullScreen
        return messageComposeVC
    }
    func showSendMessageErrorAlert()
    {
        UIAlertController.showAlert(alertTitle: "Could Not Send Message", message: "Your device could not send message. Make sure you have a Connection", alertActionTitle: "Ok", controller: self)
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case .sent:
            print("Message Sent")
        case .failed:
            print("Message Failed")
        case .cancelled:
            print("Message Cancelled")
        }
        self.dismiss(animated: true, completion: nil)
    }
    func lodgeLeave() {
        UIAlertController.showAlert(alertTitle: "Lodge Leave", message: "Lodging leave request is currently not supported on iOS", alertActionTitle: "Dismiss", controller: self)
    }
    
    func feedbackFaculty(facultyName: String, facultyEmail: String) {
        UIAlertController.showAlert(alertTitle: "Feedback", message: "This feature is currently not supported on iOS", alertActionTitle: "Dismiss", controller: self)
    }

    func setupMailAction(subject: String, body: String, facultyEmail: String) {
        MFMailComposeViewController.setupMailComposer(subject: subject, body: body, recipients: facultyEmail, delegate: self, controller: self)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    func showFacultyDetailsVC(facultyName: String, facultyEmail: String, facultyContact:String) {
        let facultyDetailViewController = FacultyDetailViewController()
        facultyDetailViewController.modalPresentationStyle = .overFullScreen
        facultyDetailViewController.facultyNameLabel.text = facultyName.capitalized
        facultyDetailViewController.email = facultyEmail
        facultyDetailViewController.phone = facultyContact
        self.present(facultyDetailViewController, animated: true, completion: nil)
    }
    func handleAction(_ controller: SubjectDetailsViewController, actionTitle: SubjectDetailsViewController.PreviewActionsTitle, for previewedController: UIViewController) {
        switch actionTitle {
            case .mailFaculty:
                print("Mail Faculty Called")
            case .viewFacultyDetails:
                print("View Faculty Details Called")
            case .callFaculty:
                print("Call Faculty Called")
            case .lodgeLeave:
                print("Lodge Leave Called")
            case .feedbackFaculty:
                print("Feedback Faculty Called")
            case .messageFaculty:
                print("Message Faculty Called")
        }
    }
}

extension NewAcademicsViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let subjectDetailsViewController = SubjectDetailsViewController()
        guard let indexPath = tableView.indexPathForRow(at: location) else { return nil }
        if indexPath.section == 1 {
            guard let cell = tableView.cellForRow(at: indexPath) else { return nil }
            //        subjectDetailViewController.preferredContentSize = CGSize(width: 0, height: 300)
            let detail = academicDetails?[indexPath.row]
//            subjectDetailsViewController.setUpDetails(details: detail)
            subjectDetailsViewController.details = detail
            previewingContext.sourceRect = cell.frame
            subjectDetailsViewController.view.layoutIfNeeded()
            subjectDetailsViewController.delegate = self
            return subjectDetailsViewController
        }
        return nil
    }
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }
    
}
extension NewAcademicsViewController:TimeTableDelegateProtocol {
    func cancelButtonTapped() {
        self.view.layer.shouldRasterize = false
    }
}
