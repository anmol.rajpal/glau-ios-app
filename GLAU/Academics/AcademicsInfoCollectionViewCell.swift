//
//  AcademicsInfoCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 02/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
//import MKRingProgressView
protocol AcademicsInfoCellDelegate: class {
    func disclosurePressed(cell:AcademicsInfoCollectionViewCell)
}
class AcademicsInfoCollectionViewCell: UICollectionViewCell {
    static let cellHeight:CGFloat = 110
    var delegate:AcademicsInfoCellDelegate?
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.cornerRadius = 15
        setUpViews()
        setUpConstraints()
    }
//    override func layoutSublayers(of layer: CALayer) {
//        super.layoutSublayers(of: layer)
//        DispatchQueue.main.async {
//            //            self.setUpAttendanceLayer()
//            self.contentView.addSubview(self.ringProgressView)
//            self.animate()
//        }
//    }
//    func setUpRingProgressView() {
//        DispatchQueue.main.async {
//            self.contentView.addSubview(self.ringProgressView)
//            self.animateRingProgressView()
//        }
//    }
    func setUpViews() {
        
        contentView.addSubview(imageView)
        imageView.addSubview(blurredEffectView)
//        contentView.addSubview(overallAttendanceLabel)
//        ringProgressView.center = overallAttendanceLabel.center
        contentView.addSubview(classAdvisorLabel)
        contentView.addSubview(semesterHeadingLabel)
//        contentView.addSubview(subjectsHeadingLabel)
        contentView.addSubview(facultyDisclosureButton)
        contentView.addSubview(advisorHeadingLabel)
        imageView.frame = self.contentView.bounds
        blurredEffectView.frame = self.imageView.bounds
    }
    let overallAttendanceLabel:UILabel = {
        let label = UILabel()
        label.text = "100.0 %"
        label.textAlignment = .center
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
//    lazy var ringProgressView:RingProgressView = {
//        let view = RingProgressView(frame: CGRect(x: 0, y: 0, width: self.overallAttendanceLabel.frame.width + 30, height: self.overallAttendanceLabel.frame.width + 30))
//        view.startColor = .systemRedColor
//        view.endColor = .systemGreenColor
//        view.ringWidth = 7
//        view.progress = 0.0
//        view.shadowOpacity = 0.3
//        view.center = self.overallAttendanceLabel.center
//        view.clipsToBounds = true
//        return view
//    }()
//    func animateRingProgressView() {
//        delegate?.animateAttendance(cell: self)
//    }
    
    func setUpConstraints() {
//        overallAttendanceLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -30).isActive = true
//        overallAttendanceLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        _ = imageView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = semesterHeadingLabel.anchor(imageView.topAnchor, left: imageView.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = classAdvisorLabel.anchor(nil, left: imageView.leftAnchor, bottom: advisorHeadingLabel.topAnchor, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 2, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = advisorHeadingLabel.anchor(nil, left: classAdvisorLabel.leftAnchor, bottom: imageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = facultyDisclosureButton.anchor(nil, left: classAdvisorLabel.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        facultyDisclosureButton.centerYAnchor.constraint(equalTo: classAdvisorLabel.centerYAnchor).isActive = true
    }
    
    let imageView:UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "blabla"))
        imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        return imageView
    }()
    let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let view = UIVisualEffectView(effect: blurEffect)
        view.layer.cornerRadius = 15
        //        view.frame = self.view.bounds
        view.clipsToBounds = true
        
        return view
    }()
    let semesterHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Semester - VII"
        label.font = UIFont.systemFont(ofSize: 25, weight: .heavy)
        label.textColor = .black
        label.numberOfLines = 1
        return label
    }()
    let classAdvisorLabel:UILabel = {
        let label = UILabel()
        label.text = "Class Advisor"
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.textColor = .black
        label.numberOfLines = 1
        return label
    }()
    let advisorHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "(Class Advisor)"
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        return label
    }()
    lazy var facultyDisclosureButton:UIButton = {
        let origImage = UIImage(named: "info")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = UIColor.systemBlueColor
        button.backgroundColor = UIColor.clear
        button.addTarget(self, action: #selector(showFacultyVC), for: .touchUpInside)
        return button
    }()
    @objc func showFacultyVC() {
        delegate?.disclosurePressed(cell: self)
    }
}
