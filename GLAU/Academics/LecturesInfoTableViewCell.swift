//
//  LecturesInfoTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit

class LecturesInfoTableViewCell: UITableViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let attendedLecturesView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemBlueColor
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let heldLecturesView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemBlueColor
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let attendedLecturesValueLabel:UILabel = {
        let label = UILabel()
        label.text = "20"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let heldLecturesValueLabel:UILabel = {
        let label = UILabel()
        label.text = "25"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let attendedLecturesLabel:UILabel = {
        let label = UILabel()
        label.text = "Attended"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let heldLecturesLabel:UILabel = {
        let label = UILabel()
        label.text = "Held"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let suggestionLabel:UILabel = {
        let label = UILabel()
        label.text = "Suggestion"
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let hSeperatorLine5 = HorizontalLine(color: .white)
    let hSeperatorLine6 = HorizontalLine(color: .white)
    func canLeaveLecture(attended:Int, held:Int, requiredAttendance:Int = 75) -> Bool {
        return (attended * 100) / held >= requiredAttendance
    }
    func needLecture(attended:Int, held:Int, requiredAttendance:Int = 75) -> Bool {
        return (attended * 100) / held < requiredAttendance
    }
    func canLeaveNextLecture(attended:Int, held:Int, requiredAttendance:Float = 75) -> Bool {
        return (Float(attended) * 100) / Float(held + 1) >= requiredAttendance
    }
    func numLecturesCanLeave(attended:Int, held:Int) -> Int {
        var count = 0, h = held
        while canLeaveNextLecture(attended: attended, held: h) {
            h = h + 1
            count = count + 1
        }
        return count
    }
    func numLecturesRequired(attended:Int, held:Int) -> Int {
        var count = 0, a = attended, h = held
        let requiredAttendance:Float = 75
        while (Float(a) * 100) / Float(h) < requiredAttendance {
            a = a + 1
            h = h + 1
            count = count + 1
        }
        return count
    }
    func setUpViews() {
        contentView.addSubview(suggestionLabel)
        attendedLecturesView.addSubview(attendedLecturesValueLabel)
        attendedLecturesView.addSubview(hSeperatorLine5)
        attendedLecturesView.addSubview(attendedLecturesLabel)
        heldLecturesView.addSubview(heldLecturesValueLabel)
        heldLecturesView.addSubview(hSeperatorLine6)
        heldLecturesView.addSubview(heldLecturesLabel)
        contentView.addSubview(attendedLecturesView)
        contentView.addSubview(heldLecturesView)
    }
    func setUpConstraints() {
        let leftRightMargin:CGFloat = 15
//        contentView.bottomAnchor.constraint(equalTo: attendedLecturesView.bottomAnchor, constant: 0).isActive = true
        _ = suggestionLabel.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = attendedLecturesView.anchorWithoutWH(top: suggestionLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: leftRightMargin, bottomConstant: 10, rightConstant: 0)
        let attendedViewbottomConstraint = attendedLecturesView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        attendedViewbottomConstraint.priority = UILayoutPriority(999)
        attendedViewbottomConstraint.isActive = true
        attendedLecturesView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 2) - 17.5).isActive = true
        attendedLecturesValueLabel.topAnchor.constraint(equalTo: attendedLecturesView.topAnchor, constant: 10).isActive = true
        attendedLecturesValueLabel.centerXAnchor.constraint(equalTo: attendedLecturesView.centerXAnchor).isActive = true
        _ = hSeperatorLine5.anchor(attendedLecturesValueLabel.bottomAnchor, left: attendedLecturesView.leftAnchor, bottom: nil, right: attendedLecturesView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        attendedLecturesLabel.topAnchor.constraint(equalTo: hSeperatorLine5.bottomAnchor, constant: 5).isActive = true
        attendedLecturesLabel.bottomAnchor.constraint(equalTo: attendedLecturesView.bottomAnchor, constant: -5).isActive = true
        attendedLecturesLabel.centerXAnchor.constraint(equalTo: attendedLecturesView.centerXAnchor).isActive = true
        
        _ = heldLecturesView.anchorWithoutWH(top: suggestionLabel.bottomAnchor, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 10, rightConstant: leftRightMargin)
        let heldViewbottomConstraint = heldLecturesView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        heldViewbottomConstraint.priority = UILayoutPriority(999)
        heldViewbottomConstraint.isActive = true
        heldLecturesView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 2) - 17.5).isActive = true
        heldLecturesValueLabel.topAnchor.constraint(equalTo: heldLecturesView.topAnchor, constant: 10).isActive = true
        heldLecturesValueLabel.centerXAnchor.constraint(equalTo: heldLecturesView.centerXAnchor).isActive = true
        _ = hSeperatorLine6.anchor(heldLecturesValueLabel.bottomAnchor, left: heldLecturesView.leftAnchor, bottom: nil, right: heldLecturesView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        heldLecturesLabel.topAnchor.constraint(equalTo: hSeperatorLine6.bottomAnchor, constant: 5).isActive = true
        heldLecturesLabel.bottomAnchor.constraint(equalTo: heldLecturesView.bottomAnchor, constant: -5).isActive = true
        heldLecturesLabel.centerXAnchor.constraint(equalTo: heldLecturesView.centerXAnchor).isActive = true
    }
}
