//
//  AcademicsViewController.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 23/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class AcademicsViewController: UIViewController {
    var canRefresh = true
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setUpNavBarItems()
        setUpViews()
        setUpConstraints()
        configure(collectionView: collectionView)
        if(traitCollection.forceTouchCapability == .available){
            registerForPreviewing(with: self, sourceView: collectionView)
        }
        fetchAcademicDetails()
        
    }
    func reload() {
        collectionView.reloadData()
        collectionView.scrollsToTop = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchAcademicDetails()
    }
    var academicDetails = [AcademicDetails]()
    var advisor:String?
    var advisorEmail:String?
    var overallAttendance:Float?
    var cpi:Double?
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
//        refreshControl.backgroundColor = UIColor.glauThemeColor
        
        refreshControl.tintColor = UIColor.glauThemeColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        return refreshControl
    }()
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
//        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel() {
        messageLabel.isHidden = true
        messageLabel.text = "Unstable Internet Connection"
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    
    // MARK: - Actions
    
    @objc private func refreshData(_ sender: Any) {
        if !collectionView.isDragging {
            fetchAcademicDetails()
        }
    }
    
    private func updateViews() {
        let hasData = academicDetails.count > 0
        collectionView.isHidden = !hasData
        messageLabel.isHidden = hasData
        retryButton.isHidden = hasData
//        self.navigationController?.navigationBar.isHidden = !hasData
        if hasData {
            collectionView.reloadData()
        }
    }
    func setUpNavBarItems() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "TimeTable", style: .plain, target: self, action: #selector(handleRightBarButtonItem))
    }
    @objc func handleRightBarButtonItem() {
        let timeTableViewController = TimeTableViewController()
        timeTableViewController.modalPresentationStyle = .overFullScreen
        present(timeTableViewController, animated: true, completion: nil)
    }
    func showTimeTable() {
        let timeTableViewController = TimeTableViewController()
        timeTableViewController.modalPresentationStyle = .overFullScreen
        present(timeTableViewController, animated: true, completion: nil)
    }
    private func handleSignOut() {
        DispatchQueue.main.async {
            UserDefaults.standard.setIsLoggedIn(value: false)
            UserDefaults.clearUserData()
            self.tabBarController?.selectedIndex = 0
            let loginController = LoginController()
            self.tabBarController?.present(loginController, animated: true, completion: nil)
        }
    }
    var studentDetails:StudentDetailsCodable?
    private func fetchAcademicDetails() {
        let rollNo = UserDefaults.standard.getRollNo()
        AcademicDetailsService.shared.fetchAcademicDetails(rollNo) { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    self.updateViews()
                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?[0].result
                let message = data?[0].message
                
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    let alertVC = UIAlertController(title: message, message: "Something went wrong. Please login again.", preferredStyle: UIAlertController.Style.alert)
                    alertVC.addAction(UIAlertAction(title: "Log in", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
                            self.handleSignOut()
                        })
                    self.present(alertVC, animated: true, completion: nil)
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        var total:Float = 0
                        self.academicDetails = data
                        self.advisor = data[0].advisor
                        self.advisorEmail = data[0].advisorEmail
                        for i in data {
                            total += (i.percentage! as NSString).floatValue
                        }
                        self.overallAttendance = total/Float(data.count)
                        self.cpi = 7.25
                    }
                    self.updateViews()
                    self.refreshControl.endRefreshing()
                    self.spinner.stopAnimating()
                default: break
                }
            }
        }
        StudentDetailsService.shared.fetch(rollNo: rollNo) { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print(error?.localizedDescription ?? "Some Error Occured")
                    self.updateViews()
                    self.spinner.stopAnimating()
                    return
                }
                
                let result = data?.result
                let message = data?.message
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure with message - \(message ?? "")")
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.studentDetails = data
//                        let sectionStr = data.section ?? "NA ( NA )"
//                        let sectionDetails = getSectionDetails(from: sectionStr)
//                        let section = sectionDetails.section
//                        let rollNo = sectionDetails.rollNo
//                        self.dict = [
//                            "Univ. Roll No." : self.studentDetails?.rollNo ?? "",
//                            "Name" : self.studentDetails?.name ?? "",
//                            "Father's Name" : self.studentDetails?.fatherName ?? "",
//                            "Semester" : self.studentDetails?.semester ?? "",
//                            "Section" : section,
//                            "Class Roll No.": rollNo,
//                            "Course" : self.studentDetails?.course ?? "",
//                            "Branch" : self.studentDetails?.branch ?? "--",
//                            "CPI" : self.studentDetails?.cPI ?? "",
//                            "Status" : self.studentDetails?.status ?? "",
//                            "Nature" : self.studentDetails?.nature ?? "",
//                            "Resident" : self.studentDetails?.resident ?? "",
//                            "Placed" : self.studentDetails?.placed ?? ""
//                        ]
                    }
                    self.updateViews()
                    self.spinner.stopAnimating()
                default: break
                }
            }
        }
    }
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.bounces = true
        cv.alwaysBounceVertical = true
        cv.clipsToBounds = true
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = true
        cv.backgroundColor = .clear
        cv.isHidden = true
        if #available(iOS 10.0, *) {
            cv.refreshControl = refreshControl
        } else {
            cv.addSubview(refreshControl)
        }
        return cv
    }()
    func setUpViews() {
        view.addSubview(collectionView)
        view.addSubview(spinner)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = collectionView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
}
extension AcademicsViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let subjectDetailViewController = SubjectDetailViewController()
        guard let indexPath = collectionView.indexPathForItem(at: location) else { return nil }
        if indexPath.section == 1 {
            guard let cell = collectionView.cellForItem(at: indexPath) else { return nil }
    //        subjectDetailViewController.preferredContentSize = CGSize(width: 0, height: 300)
            let detail = academicDetails[indexPath.row]
            subjectDetailViewController.setUpDetails(details: detail)
            previewingContext.sourceRect = cell.frame
            return subjectDetailViewController
        }
       return nil
    }
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }
    
}

extension AcademicsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // MARK: - Configuration
    
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.contentOffset.y > -100 {
//            self.refreshControl.beginRefreshing()
//        }
//    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if refreshControl.isRefreshing == true {
            let dispatchTime:DispatchTime = .now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                self.fetchAcademicDetails()
            }
        }
    }
    internal func configure(collectionView: UICollectionView) {
        collectionView.register(SubjectsHeaderCollectionViewCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: NSStringFromClass(SubjectsHeaderCollectionViewCell.self))
        collectionView.register(AcademicsInfoCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(AcademicsInfoCollectionViewCell.self))
//        collectionView.register(OverallCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(OverallCollectionViewCell.self))
        collectionView.register(AcademicsSubjectsCell.self, forCellWithReuseIdentifier: NSStringFromClass(AcademicsSubjectsCell.self))
        collectionView.dataSource = self
        collectionView.delegate = self
//        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return (academicDetails.count)
        } else {
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if(kind == UICollectionView.elementKindSectionHeader) {
//            if indexPath.section == 1 {
//                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(SubjectsHeaderCollectionViewCell.self), for: indexPath) as! SubjectsHeaderCollectionViewCell
//                headerView.headerLabel.text = "Overall".capitalized
//                return headerView
//            } else
            if indexPath.section == 1 {
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: NSStringFromClass(SubjectsHeaderCollectionViewCell.self), for: indexPath) as! SubjectsHeaderCollectionViewCell
                headerView.headerLabel.text = "Subjects".capitalized
                return headerView
            }
        }
        fatalError()
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(AcademicsInfoCollectionViewCell.self), for: indexPath) as! AcademicsInfoCollectionViewCell
//            cell.isUserInteractionEnabled = false
            cell.classAdvisorLabel.text = advisor?.capitalized
            cell.delegate = self
            return cell
//        } else if indexPath.section == 1 {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(OverallCollectionViewCell.self), for: indexPath) as! OverallCollectionViewCell
//            cell.delegate = self
//            cell.setAttributedCpi(cpi: cpi ?? 0)
//            cell.setAttributedAttendance(attendance: overallAttendance ?? 0)
//            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(AcademicsSubjectsCell.self), for: indexPath) as! AcademicsSubjectsCell
            let subject = academicDetails[indexPath.row]
            cell.subjectNameLabel.text = subject.subjectName?.capitalized
            cell.subjectCodeLabel.text = subject.subjectCode
            cell.subjectTypeLabel.text = subject.subjectType?.uppercased()
            cell.attendanceLabel.text = ("\(subject.percentage ?? "-") %")
            let attendance:Float = (subject.percentage! as NSString).floatValue
            cell.attendanceLabel.backgroundColor = setUpAttendanceColor(attendance: attendance)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let subjectDetailViewController = SubjectDetailsViewController()
            let details = academicDetails[indexPath.row]
            subjectDetailViewController.details = details
//            subjectDetailViewController.setUpDetails(details: details)
            self.show(subjectDetailViewController, sender: self)
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize.zero
        }
        return CGSize(width: self.view.frame.width, height: 40)
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: collectionView.bounds.width, height: AcademicsInfoCollectionViewCell.cellHeight)
//        } else if indexPath.section == 1 {
//            return CGSize(width: collectionView.bounds.width, height: OverallCollectionViewCell.cellHeight)
        } else {
            return CGSize(width: collectionView.bounds.width, height: AcademicsSubjectsBaseCell.cellHeight)
        }
    }
}
//extension AcademicsViewController: OverallCellDelegate {
//    func animateAttendance(cell: OverallCollectionViewCell) {
//        DispatchQueue.main.async {
//            UIView.animate(withDuration: 1.0) {
//                let cpi = self.cpi ?? 0
//                let overallAttendance = self.overallAttendance ?? 0
//                cell.cpiRingProgressView.progress = Double(cpi)/10
//                cell.overallAttendanceRingProgressView.progress = Double(overallAttendance)/100
//                cell.overallAttendanceRingProgressView.shadowOpacity = 0
//                cell.cpiRingProgressView.shadowOpacity = 0
//                if overallAttendance >= 75 {
//                    cell.overallAttendanceRingProgressView.startColor = .systemGreenColor
//                    cell.overallAttendanceRingProgressView.endColor = .systemGreenColor
//                } else {
//                    cell.overallAttendanceRingProgressView.startColor = .systemRedColor
//                    cell.overallAttendanceRingProgressView.endColor = .systemRedColor
//                }
//                if cpi >= 6 {
//                    cell.cpiRingProgressView.startColor = .systemGreenColor
//                    cell.cpiRingProgressView.endColor = .systemGreenColor
//                } else {
//                    cell.cpiRingProgressView.startColor = .systemRedColor
//                    cell.cpiRingProgressView.endColor = .systemRedColor
//                }
//            }
//        }
//    }
//}
extension AcademicsViewController: AcademicsInfoCellDelegate {
    func disclosurePressed(cell: AcademicsInfoCollectionViewCell) {
        let facultyDetailViewController = FacultyDetailViewController()
        facultyDetailViewController.modalPresentationStyle = .overFullScreen
        facultyDetailViewController.facultyNameLabel.text = self.advisor?.capitalized
        facultyDetailViewController.email = self.advisorEmail
        self.present(facultyDetailViewController, animated: true, completion: nil)
    }
}
