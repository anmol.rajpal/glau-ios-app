//
//  MarksInfoTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/01/19.
//  Copyright © 2019 natovi. All rights reserved.
//

import UIKit

class MarksInfoTableViewCell: UITableViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let module1ContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemPinkColor
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let module2ContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemPinkColor
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let module3ContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemPinkColor
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let module1Label:UILabel = {
        let label = UILabel()
        label.text = "Mod-I"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let module2Label:UILabel = {
        let label = UILabel()
        label.text = "Mod-II"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let module3Label:UILabel = {
        let label = UILabel()
        label.text = "Mod-III"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let module1MarksLabel:UILabel = {
        let label = UILabel()
        label.text = "- -"
        label.textColor = .white
        label.layer.cornerRadius = 15
        //        label.backgroundColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        //        label.clipsToBounds = true
        return label
    }()
    let module2MarksLabel:UILabel = {
        let label = UILabel()
        label.text = "15"
        label.textColor = .white
        label.layer.cornerRadius = 15
        //        label.backgroundColor = .systemRedColor
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        //        label.clipsToBounds = true
        return label
    }()
    let module3MarksLabel:UILabel = {
        let label = UILabel()
        label.text = "- -"
        label.textColor = .white
        label.layer.cornerRadius = 15
        //        label.backgroundColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        //        label.clipsToBounds = true
        return label
    }()
    let hSeperatorLine7 = HorizontalLine(color: .white)
    let hSeperatorLine8 = HorizontalLine(color: .white)
    let hSeperatorLine9 = HorizontalLine(color: .white)
    func setUpViews() {
        //        contentView.addSubview(marksHeadingLabel)
        
        module1ContainerView.addSubview(module1MarksLabel)
        module1ContainerView.addSubview(hSeperatorLine7)
        module1ContainerView.addSubview(module1Label)
        
        module2ContainerView.addSubview(module2MarksLabel)
        module2ContainerView.addSubview(hSeperatorLine8)
        module2ContainerView.addSubview(module2Label)
        
        module3ContainerView.addSubview(module3MarksLabel)
        module3ContainerView.addSubview(hSeperatorLine9)
        module3ContainerView.addSubview(module3Label)
        
        contentView.addSubview(module1ContainerView)
        contentView.addSubview(module2ContainerView)
        contentView.addSubview(module3ContainerView)
    }
    func setUpConstraints() {
        //        _ = marksHeadingLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        //Module-I Constraints
        _ = module1ContainerView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: module1Label.bottomAnchor, right: nil, topConstant: 15, leftConstant: 10, bottomConstant: -10, rightConstant: 0)
        module1ContainerView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 3) - 10).isActive = true
        _ = module1MarksLabel.anchor(module1ContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module1MarksLabel.centerXAnchor.constraint(equalTo: module1ContainerView.centerXAnchor).isActive = true
        _ = hSeperatorLine7.anchor(module1MarksLabel.bottomAnchor, left: module1ContainerView.leftAnchor, bottom: nil, right: module1ContainerView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = module1Label.anchor(hSeperatorLine7.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module1Label.centerXAnchor.constraint(equalTo: module1ContainerView.centerXAnchor).isActive = true
        
        //Module-II Constraints
        _ = module2ContainerView.anchorWithoutWH(top: contentView.topAnchor, left: nil, bottom: module2Label.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: -10, rightConstant: 0)
        module2ContainerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        module2ContainerView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 3) - 10).isActive = true
        _ = module2MarksLabel.anchor(module2ContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module2MarksLabel.centerXAnchor.constraint(equalTo: module2ContainerView.centerXAnchor).isActive = true
        _ = hSeperatorLine8.anchor(module2MarksLabel.bottomAnchor, left: module2ContainerView.leftAnchor, bottom: nil, right: module2ContainerView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = module2Label.anchor(hSeperatorLine8.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module2Label.centerXAnchor.constraint(equalTo: module2ContainerView.centerXAnchor).isActive = true
        
        //Module-III Constraints
        _ = module3ContainerView.anchorWithoutWH(top: contentView.topAnchor, left: nil, bottom: module3Label.bottomAnchor, right: contentView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: -10, rightConstant: 10)
        module3ContainerView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 3) - 10).isActive = true
        _ = module3MarksLabel.anchor(module3ContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module3MarksLabel.centerXAnchor.constraint(equalTo: module3ContainerView.centerXAnchor).isActive = true
        _ = hSeperatorLine9.anchor(module3MarksLabel.bottomAnchor, left: module3ContainerView.leftAnchor, bottom: nil, right: module3ContainerView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = module3Label.anchor(hSeperatorLine9.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module3Label.centerXAnchor.constraint(equalTo: module3ContainerView.centerXAnchor).isActive = true
        
    }
}
