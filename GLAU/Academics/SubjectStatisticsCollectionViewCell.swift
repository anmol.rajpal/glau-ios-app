//
//  SubjectStatisticsCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 15/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class SubjectStatisticsCollectionViewCell: UICollectionViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpCons()
    }
    //forces the system to do one layout pass
    var isHeightCalculated: Bool = false
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        //Exhibit A - We need to cache our calculation to prevent a crash.
        if !isHeightCalculated {
            setNeedsLayout()
            layoutIfNeeded()
            let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
            var newFrame = layoutAttributes.frame
            newFrame.size.width = CGFloat(ceilf(Float(size.width)))
            layoutAttributes.frame = newFrame
            isHeightCalculated = true
        }
        return layoutAttributes
    }
    let statisticsHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Statistics"
        label.font = UIFont.systemFont(ofSize: 25, weight: .heavy)
        label.textColor = UIColor.darkText
        label.numberOfLines = 1
        
        return label
    }()
    let attendanceHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Attendance"
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.numberOfLines = 1
        
        return label
    }()
    let attendanceLabel:UILabel = {
        let label = AnimatedLabel(20, 20, 50, 50)
//        label.text = "89.96 %"
        label.decimalPoints = .two
        let attendance:Float = 75
        label.count(from: attendance/2.0, to: attendance)
        label.appendString = " %"
        label.font = UIFont.systemFont(ofSize: 40, weight: UIFont.Weight.light)
        label.textColor = .white
        if attendance < Config.requiredAttendance {
            label.backgroundColor = UIColor.systemRedColor
        } else if attendance > Config.requiredAttendance {
            label.backgroundColor = UIColor.systemGreenColor
        } else {
            label.backgroundColor = UIColor.systemYellowColor
        }
        label.clipsToBounds = true
        label.layer.cornerRadius = 15
        return label
    }()
    let lecturesHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Lectures"
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.numberOfLines = 1
        
        return label
    }()
    let attendedLecturesView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemBlueColor
        view.layer.cornerRadius = 15
        return view
    }()
    let heldLecturesView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemBlueColor
        view.layer.cornerRadius = 15
        return view
    }()
    let attendedLecturesValueLabel:UILabel = {
        let label = UILabel()
        label.text = "20"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        return label
    }()
    let heldLecturesValueLabel:UILabel = {
        let label = UILabel()
        label.text = "25"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        label.numberOfLines = 1
        return label
    }()
    let attendedLecturesLabel:UILabel = {
        let label = UILabel()
        label.text = "Attended"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        
        return label
    }()
    let heldLecturesLabel:UILabel = {
        let label = UILabel()
        label.text = "Held"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 1
        return label
    }()
    let marksHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Marks"
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        label.numberOfLines = 1
        
        return label
    }()
    let module1ContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemPinkColor
        view.layer.cornerRadius = 15
        return view
    }()
    let module2ContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemPinkColor
        view.layer.cornerRadius = 15
        return view
    }()
    let module3ContainerView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.systemPinkColor
        view.layer.cornerRadius = 15
        return view
    }()
    let module1Label:UILabel = {
        let label = UILabel()
        label.text = "Module-I"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 17, weight: .light)
        label.numberOfLines = 1
        return label
    }()
    let module2Label:UILabel = {
        let label = UILabel()
        label.text = "Module-II"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 17, weight: .light)
        label.numberOfLines = 1
        return label
    }()
    let module3Label:UILabel = {
        let label = UILabel()
        label.text = "Module-III"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 17, weight: .light)
        label.numberOfLines = 1
        return label
    }()
    let module1MarksLabel:UILabel = {
        let label = UILabel()
        label.text = "- -"
        label.textColor = .white
        label.layer.cornerRadius = 15
//        label.backgroundColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        label.numberOfLines = 1
//        label.clipsToBounds = true
        return label
    }()
    let module2MarksLabel:UILabel = {
        let label = UILabel()
        label.text = "15"
        label.textColor = .white
        label.layer.cornerRadius = 15
//        label.backgroundColor = .systemRedColor
        label.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        label.numberOfLines = 1
//        label.clipsToBounds = true
        return label
    }()
    let module3MarksLabel:UILabel = {
        let label = UILabel()
        label.text = "- -"
        label.textColor = .white
        label.layer.cornerRadius = 15
//        label.backgroundColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        label.numberOfLines = 1
//        label.clipsToBounds = true
        return label
    }()
    let hSeperatorLine1 = HorizontalLine(color: .lightGray)
    let hSeperatorLine2 = HorizontalLine(color: .lightGray)
    let hSeperatorLine3 = HorizontalLine(color: .lightGray)
    let hSeperatorLine4 = HorizontalLine(color: .lightGray)
    let hSeperatorLine5 = HorizontalLine(color: .white)
    let hSeperatorLine6 = HorizontalLine(color: .white)
    let hSeperatorLine7 = HorizontalLine(color: .white)
    let hSeperatorLine8 = HorizontalLine(color: .white)
    let hSeperatorLine9 = HorizontalLine(color: .white)
    let vSeperatorLine1 = VerticalLine(color: .lightGray)
    func setUpViews() {
        contentView.addSubview(hSeperatorLine1)
        contentView.addSubview(hSeperatorLine2)
        contentView.addSubview(hSeperatorLine3)
        contentView.addSubview(hSeperatorLine4)
//        contentView.addSubview(vSeperatorLine1)
        contentView.addSubview(statisticsHeadingLabel)
        contentView.addSubview(attendanceHeadingLabel)
        contentView.addSubview(attendanceLabel)
        contentView.addSubview(lecturesHeadingLabel)
        
        attendedLecturesView.addSubview(attendedLecturesValueLabel)
        attendedLecturesView.addSubview(hSeperatorLine5)
        attendedLecturesView.addSubview(attendedLecturesLabel)
        heldLecturesView.addSubview(heldLecturesValueLabel)
        heldLecturesView.addSubview(hSeperatorLine6)
        heldLecturesView.addSubview(heldLecturesLabel)
        contentView.addSubview(attendedLecturesView)
        contentView.addSubview(heldLecturesView)
        
        contentView.addSubview(marksHeadingLabel)
        
        module1ContainerView.addSubview(module1MarksLabel)
        module1ContainerView.addSubview(hSeperatorLine7)
        module1ContainerView.addSubview(module1Label)
        
        module2ContainerView.addSubview(module2MarksLabel)
        module2ContainerView.addSubview(hSeperatorLine8)
        module2ContainerView.addSubview(module2Label)
        
        module3ContainerView.addSubview(module3MarksLabel)
        module3ContainerView.addSubview(hSeperatorLine9)
        module3ContainerView.addSubview(module3Label)
        
        contentView.addSubview(module1ContainerView)
        contentView.addSubview(module2ContainerView)
        contentView.addSubview(module3ContainerView)
    }
    func setUpCons() {
//        _ = vSeperatorLine1.anchor(lecturesHeadingLabel.bottomAnchor, left: nil, bottom: attendedLecturesLabel.bottomAnchor, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0.5, heightConstant: 0)
//        vSeperatorLine1.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        _ = hSeperatorLine1.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 15, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0.5)
        _ = hSeperatorLine2.anchor(attendanceLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0.5)
        _ = hSeperatorLine3.anchor(attendedLecturesLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0.5)
        _ = hSeperatorLine4.anchor(attendedLecturesLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0.5)
        
        _ = statisticsHeadingLabel.anchor(hSeperatorLine1.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = attendanceHeadingLabel.anchor(statisticsHeadingLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = attendanceLabel.anchor(attendanceHeadingLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        attendanceLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        _ = lecturesHeadingLabel.anchor(hSeperatorLine2.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = attendedLecturesView.anchorWithoutWH(top: lecturesHeadingLabel.bottomAnchor, left: contentView.leftAnchor, bottom: attendedLecturesLabel.bottomAnchor, right: nil, topConstant: 15, leftConstant: 10, bottomConstant: -10, rightConstant: 0)
        attendedLecturesView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 2) - 15).isActive = true
        _ = attendedLecturesValueLabel.anchor(attendedLecturesView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        attendedLecturesValueLabel.centerXAnchor.constraint(equalTo: attendedLecturesView.centerXAnchor).isActive = true
        _ = hSeperatorLine5.anchor(attendedLecturesValueLabel.bottomAnchor, left: attendedLecturesView.leftAnchor, bottom: nil, right: attendedLecturesView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = attendedLecturesLabel.anchor(hSeperatorLine5.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        attendedLecturesLabel.centerXAnchor.constraint(equalTo: attendedLecturesView.centerXAnchor).isActive = true
        
        _ = heldLecturesView.anchorWithoutWH(top: lecturesHeadingLabel.bottomAnchor, left: nil, bottom: heldLecturesLabel.bottomAnchor, right: contentView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: -10, rightConstant: 10)
        heldLecturesView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 2) - 15).isActive = true
        _ = heldLecturesValueLabel.anchor(heldLecturesView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        heldLecturesValueLabel.centerXAnchor.constraint(equalTo: heldLecturesView.centerXAnchor).isActive = true
        _ = hSeperatorLine6.anchor(heldLecturesValueLabel.bottomAnchor, left: heldLecturesView.leftAnchor, bottom: nil, right: heldLecturesView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = heldLecturesLabel.anchor(hSeperatorLine6.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        heldLecturesLabel.centerXAnchor.constraint(equalTo: heldLecturesView.centerXAnchor).isActive = true
        
        
        _ = marksHeadingLabel.anchor(hSeperatorLine3.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = module1ContainerView.anchorWithoutWH(top: marksHeadingLabel.bottomAnchor, left: contentView.leftAnchor, bottom: module1Label.bottomAnchor, right: nil, topConstant: 15, leftConstant: 10, bottomConstant: -10, rightConstant: 0)
        module1ContainerView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 3) - 10).isActive = true
        _ = module1MarksLabel.anchor(module1ContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module1MarksLabel.centerXAnchor.constraint(equalTo: module1ContainerView.centerXAnchor).isActive = true
        _ = hSeperatorLine7.anchor(module1MarksLabel.bottomAnchor, left: module1ContainerView.leftAnchor, bottom: nil, right: module1ContainerView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = module1Label.anchor(hSeperatorLine7.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module1Label.centerXAnchor.constraint(equalTo: module1ContainerView.centerXAnchor).isActive = true
        
        _ = module2ContainerView.anchorWithoutWH(top: marksHeadingLabel.bottomAnchor, left: nil, bottom: module2Label.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: -10, rightConstant: 0)
        module2ContainerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        module2ContainerView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 3) - 10).isActive = true
        _ = module2MarksLabel.anchor(module2ContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module2MarksLabel.centerXAnchor.constraint(equalTo: module2ContainerView.centerXAnchor).isActive = true
        _ = hSeperatorLine8.anchor(module2MarksLabel.bottomAnchor, left: module2ContainerView.leftAnchor, bottom: nil, right: module2ContainerView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = module2Label.anchor(hSeperatorLine8.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module2Label.centerXAnchor.constraint(equalTo: module2ContainerView.centerXAnchor).isActive = true
        
        _ = module3ContainerView.anchorWithoutWH(top: marksHeadingLabel.bottomAnchor, left: nil, bottom: module3Label.bottomAnchor, right: contentView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: -10, rightConstant: 10)
        module3ContainerView.widthAnchor.constraint(equalToConstant: (contentView.bounds.width / 3) - 10).isActive = true
        _ = module3MarksLabel.anchor(module3ContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module3MarksLabel.centerXAnchor.constraint(equalTo: module3ContainerView.centerXAnchor).isActive = true
        _ = hSeperatorLine9.anchor(module3MarksLabel.bottomAnchor, left: module3ContainerView.leftAnchor, bottom: nil, right: module3ContainerView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = module3Label.anchor(hSeperatorLine9.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        module3Label.centerXAnchor.constraint(equalTo: module3ContainerView.centerXAnchor).isActive = true
        
        
        
        
    }
}
