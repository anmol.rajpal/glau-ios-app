//
//  AssignmentsCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 15/09/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class AssignmentsCollectionViewCell: UICollectionViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let assignmentsHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Assignments"
        label.font = UIFont.systemFont(ofSize: 25, weight: .heavy)
        label.textColor = UIColor.darkText
        label.numberOfLines = 1
        
        return label
    }()
    let pendingHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Pending"
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.numberOfLines = 1
        
        return label
    }()
    let totalHeadingLabel:UILabel = {
        let label = UILabel()
        label.text = "Total"
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.numberOfLines = 1
        
        return label
    }()
    let pendingAssignmentsLabel:UILabel = {
        let label = InsetLabel(2, 2, 12, 12)
        label.text = "5"
        label.textColor = .white
        label.layer.cornerRadius = 15
        label.backgroundColor = .red
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        label.numberOfLines = 1
        label.clipsToBounds = true
        return label
    }()
    let totalAssignmentsLabel:UILabel = {
        let label = InsetLabel(5, 5, 20, 20)
        label.text = "12"
        label.textColor = .white
        label.layer.cornerRadius = 15
        label.backgroundColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        label.numberOfLines = 1
        label.clipsToBounds = true
        return label
    }()
    let attendanceLabel:UILabel = {
        let label = InsetLabel(10, 10, 30, 30)
        label.text = "89.96 %"
        label.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.light)
        label.textColor = .white
        label.backgroundColor = UIColor.systemGreenColor
        label.clipsToBounds = true
        label.layer.cornerRadius = 7
        return label
    }()
    let hSeperatorLine1 = HorizontalLine(color: .lightGray)
    let vSeperatorLine1 = VerticalLine(color: .lightGray)
    func setUpViews() {
        contentView.addSubview(hSeperatorLine1)
//        contentView.addSubview(vSeperatorLine1)
        contentView.addSubview(assignmentsHeadingLabel)
        contentView.addSubview(pendingHeadingLabel)
//        contentView.addSubview(totalHeadingLabel)
        contentView.addSubview(pendingAssignmentsLabel)
//        contentView.addSubview(totalAssignmentsLabel)
    }
    func setUpConstraints() {
        _ = hSeperatorLine1.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 15, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0.5)
//        _ = vSeperatorLine1.anchor(assignmentsHeadingLabel.bottomAnchor, left: nil, bottom: pendingAssignmentsLabel.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0.5, heightConstant: 0)
//        vSeperatorLine1.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        _ = assignmentsHeadingLabel.anchor(hSeperatorLine1.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = pendingHeadingLabel.anchor(assignmentsHeadingLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 30, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = pendingAssignmentsLabel.anchor(assignmentsHeadingLabel.bottomAnchor, left: pendingHeadingLabel.rightAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        pendingAssignmentsLabel.centerXAnchor.constraint(equalTo: pendingHeadingLabel.centerXAnchor).isActive = true
//        _ = totalHeadingLabel.anchor(assignmentsHeadingLabel.bottomAnchor, left: nil, bottom: nil, right: totalAssignmentsLabel.leftAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
//        _ = totalAssignmentsLabel.anchor(assignmentsHeadingLabel.bottomAnchor, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
//        totalAssignmentsLabel.centerXAnchor.constraint(equalTo: totalHeadingLabel.centerXAnchor).isActive = true
    }
}
