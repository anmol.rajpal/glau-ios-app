//
//  MarksTableViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 23/09/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class MarksTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpViews()
        setUpConstraints()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let attendanceLabel:UILabel = {
        let label = InsetLabel(10, 10, 30, 30)
        label.text = "89.96 %"
        label.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.light)
        label.textColor = .white
        label.backgroundColor = UIColor.systemGreenColor
        label.clipsToBounds = true
        label.layer.cornerRadius = 7
        return label
    }()
    func setUpViews() {
        contentView.addSubview(attendanceLabel)
    }
    func setUpConstraints() {
        _ = attendanceLabel.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        attendanceLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    }

}
