//
//  SubDetailViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 23/09/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class SubDetailViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        view.backgroundColor = .white
        navigationItem.title = "CSE 6002"
        self.view.addSubview(tableView)
    }
    lazy var tableView:UITableView = {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        let tv = UITableView(frame: CGRect(x:0, y:0, width:screenWidth, height:screenHeight), style: UITableView.Style.plain)
        tv.dataSource = self
        tv.delegate = self
        tv.estimatedRowHeight = 50.0
        tv.rowHeight = UITableView.automaticDimension
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.separatorColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.6)
        tv.register(UITableViewCell.self, forCellReuseIdentifier: "cell0")
        tv.register(AttendanceTableViewCell.self, forCellReuseIdentifier: "cell1")
        tv.register(LecturesTableViewCell.self, forCellReuseIdentifier: "cell2")
        tv.register(MarksTableViewCell.self, forCellReuseIdentifier: "cell3")
        tv.register(UITableViewCell.self, forCellReuseIdentifier: "defaultCell")
        return tv
    }()
}
let sections = ["","Attendance","Lectures","Marks","Assignments"]
extension SubDetailViewController:UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return nil
        } else {
            return sections[section]
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
//        header.backgroundView?.backgroundColor = .white
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.numberOfLines = 0
        header.textLabel?.lineBreakMode = .byWordWrapping
        header.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath)
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell0", for: indexPath)
            cell.textLabel?.text = "Engineering Subject Title, To be Rendered"
            cell.textLabel?.font = UIFont.systemFont(ofSize: 22, weight: .heavy)
            cell.textLabel?.numberOfLines = 0
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! AttendanceTableViewCell
            cell.selectionStyle = .none
            cell.backgroundColor = tableView.separatorColor
//            cell.backgroundView?.backgroundColor = tableView.separatorColor
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! LecturesTableViewCell
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! MarksTableViewCell
            cell.selectionStyle = .none
            return cell
        default: return cell
        }
    }
}
