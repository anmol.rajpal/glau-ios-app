//
//  UserDefaults+Helpers.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 22/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import Foundation
extension UserDefaults {
    
    enum UserDefaultsKeys: String {
        case isLoggedIn
        case rollNo
        case userType
        case password
        case appLaunchCount
    }
    var appLaunchCount:Int? {
        get {
            return integer(forKey: UserDefaultsKeys.appLaunchCount.rawValue)
        }
        set (count) {
            set(count!, forKey: UserDefaultsKeys.appLaunchCount.rawValue)
            synchronize()
        }
    }
    func setIsLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        synchronize()
    }
    
    func isLoggedIn() -> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func setRollNo(rollNo: String) {
        set(rollNo, forKey: UserDefaultsKeys.rollNo.rawValue)
    }
    func getRollNo() -> String {
        return UserDefaults.standard.string(forKey: UserDefaultsKeys.rollNo.rawValue) ?? ""
    }
    func setPassword(password: String) {
        set(password, forKey: UserDefaultsKeys.password.rawValue)
    }
    func getPassword() -> String? {
        return UserDefaults.standard.string(forKey: UserDefaultsKeys.password.rawValue)
    }
    func setUserType(type:UserType) {
        set(type.rawValue, forKey: UserDefaultsKeys.userType.rawValue)
//        print(type.rawValue)
    }
    func getUserType() -> UserType {
        return UserType(rawValue: UserDefaults.standard.string(forKey: UserDefaultsKeys.userType.rawValue)!) ?? .Student
    }
    static func clearUserData() {
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.rollNo.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.password.rawValue)
//        UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.userType.rawValue)
    }
}
