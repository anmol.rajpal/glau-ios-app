//
//  TabBarController.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 23/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit
extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return (navigation.visibleViewController?.topMostViewController())!
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}
class TabBarController: UITabBarController {
    let network = NetworkManager.shared
    var shortcutType:AppDelegate.ShortcutType?
    var notificationType:AppDelegate.NotificationType?
    var newsId:String?
    var isLoaded:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
//        checkReachability()
//        self.viewControllers = nil
//        setUp()
//        authenticate()
//        setupNotificationObservers()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.viewControllers = nil
        setUp()
//        setUpTabBarViewControllers()
        
        if NetworkManager.isReachable() {
            authenticate()
        } else {
            setViewsState(isHidden: false)
            NetworkManager.shared.reachability.whenReachable = {_ in
                self.authenticate()
            }
        }
    }
    func processNotification(type: AppDelegate.NotificationType?) {
        switch type {
        case .News?:
            self.selectedIndex = 1
            return
        default: break
        }
    }
    func processNewsNotification(newsId:String?) {
        if let newsid = newsId {
            self.selectedIndex = 0
            let navigationController = self.selectedViewController as! UINavigationController
            let destinationViewController = navigationController.viewControllers[0] as! HomeViewController
            navigationController.popToViewController(destinationViewController, animated: true)
            destinationViewController.processNewsNotification(newsId: newsid)
        }
    }
    func processShortcut(type:AppDelegate.ShortcutType?) {
        switch type {
        case .showAttendance?:
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            self.selectedIndex = 1
            (self.selectedViewController as! UINavigationController).popToRootViewController(animated: true)
            return
        case .showTimeTable?:
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            let timeTableVC = TimeTableViewController()
            timeTableVC.modalPresentationStyle = .overFullScreen
            self.selectedIndex = 1
            self.selectedViewController?.present(timeTableVC, animated: true, completion: nil)
            return
        case .showUserProfile?:
            
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            let profileVC = ProfileTableViewController()
            profileVC.view.backgroundColor = .white
            profileVC.tableView.backgroundColor = .white
            self.selectedIndex = 2
            (self.selectedViewController as! UINavigationController).popToRootViewController(animated: false)
            self.selectedViewController?.show(profileVC, sender: self)
            return
        default: break
        }
    }
    
    
    func setUp() {
//        isReachable()
        setUpViews()
        setUpConstraints()
//        self.authenticate()
    }
    func isReachable() {
        if NetworkManager.isUnreachable() {
            self.showOfflineViewController()
        }
    }
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    @objc private func handleEnterForeground() {
        if NetworkManager.isReachable() {
            reAuthenticate()
        }
    }
    func reAuthenticate() {
        if isLoggedIn() {
            let rollNo = UserDefaults.standard.getRollNo()
            guard let password = UserDefaults.standard.getPassword() else {
                let alertVC = UIAlertController(title: "Authentication Error", message: "Please sign in again.", preferredStyle: UIAlertController.Style.alert)
                DispatchQueue.main.async {
                    alertVC.addAction(UIAlertAction(title: "Sign in", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
                        let loginController = LoginController()
                        UserDefaults.standard.setIsLoggedIn(value: false)
                        UserDefaults.clearUserData()
                        DispatchQueue.main.async {
                            self.present(loginController, animated: false, completion: nil)
                        }
                    })
                    self.present(alertVC, animated: true, completion: nil)
                }
                return
            }
            
            UserAuthenticationService.shared.fetch(rollNo, password) { (data, error) in
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    
                    
                    guard error == nil else {
                        let alertVC = UIAlertController(title: "Connection Error", message: "Request timed out. Please try again.", preferredStyle: UIAlertController.Style.alert)
                        alertVC.addAction(UIAlertAction(title: "Sign Out", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
                            let loginController = LoginController()
                            UserDefaults.standard.setIsLoggedIn(value: false)
                            UserDefaults.clearUserData()
                            DispatchQueue.main.async {
                                self.present(loginController, animated: false, completion: nil)
                            }
                        })
                        alertVC.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default) { (action:UIAlertAction) in
                            self.retry()
                        })
                        self.present(alertVC, animated: true, completion: nil)
                        return
                    }
                    
                    let result = data?.result
                    _ = data?.message
                    switch result {
                    case ResultType.Failure.rawValue:
                        let alertVC = UIAlertController(title: "Authentication Error", message: "Please sign in again.", preferredStyle: UIAlertController.Style.alert)
                        alertVC.addAction(UIAlertAction(title: "Sign in", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
                            let loginController = LoginController()
                            UserDefaults.standard.setIsLoggedIn(value: false)
                            UserDefaults.clearUserData()
                            DispatchQueue.main.async {
                                self.present(loginController, animated: false, completion: nil)
                            }
                        })
                        self.present(alertVC, animated: true, completion: nil)
                        
                    case ResultType.Success.rawValue:
                        print("")
                    default: break
                    }
                })
            }
        } else {
            let loginController = LoginController()
            UserDefaults.standard.setIsLoggedIn(value: false)
            UserDefaults.clearUserData()
            DispatchQueue.main.async {
                self.present(loginController, animated: false, completion: nil)
            }
            //            DispatchQueue.main.async {
            //                print("hahahha")
            //                self.showLoginController()
            //            }
        }
    }
    func setUpViews() {
        view.addSubview(connectivityLabel)
        view.addSubview(settingsButton)
        view.addSubview(spinner)
        view.backgroundColor = .white
        
    }
    private func setUpConstraints() {
        _ = connectivityLabel.anchorWithoutWH(top: nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: view.frame.height / 10, leftConstant: 20, bottomConstant: 0, rightConstant: 20)
        connectivityLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        settingsButton.topAnchor.constraint(equalTo: connectivityLabel.bottomAnchor, constant: 15).isActive = true
        settingsButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    fileprivate func isLoggedIn() -> Bool {
        return UserDefaults.standard.isLoggedIn()
    }
    
    private func setViewsState(isHidden:Bool = true) {
        connectivityLabel.isHidden = isHidden
        settingsButton.isHidden = isHidden
    }
    let connectivityLabel:UILabel = {
        let label = UILabel()
        label.text = "Turn on Mobile Data or Wifi to Access GLAU"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.largeTitle)
        label.textColor = .gray
        label.textAlignment = .center
        label.numberOfLines = 0
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.isHidden = true
        return label
    }()
    let settingsButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.roundedRect)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Settings", for: UIControl.State.normal)
        button.setTitleColor(UIColor.gray, for: UIControl.State.normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.cornerRadius = 8
        button.backgroundColor = .clear
        button.isHidden = true
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 15, bottom: 5, right: 15)
        button.addTarget(self, action: #selector(launchSettings), for: UIControl.Event.touchUpInside)
        return button
    }()
    @objc func launchSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                
            })
        }
    }
//    override var preferredStatusBarStyle : UIStatusBarStyle {
//        return .lightContent
//    }
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        return indicator
    }()
    
    func signOut() {
        DispatchQueue.main.async {
            guard let window = UIApplication.shared.keyWindow else {
                return
            }
            UIView.transition(with: window, duration: 0.2, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                window.rootViewController = LoginController()
            }, completion: { completed in
                UserDefaults.standard.setIsLoggedIn(value: false)
                UserDefaults.clearUserData()
                
            })
            //            UIApplication.shared.keyWindow?.rootViewController = TabBarController()
            //            let loginController = LoginController()
            //            self.tabBarController?.present(loginController, animated: true, completion: {
            //
            //
            //            })
        }
    }
    func handleSignOut() {
//        DispatchQueue.main.async {
            self.showLoginController()
//        }
//        showLoginController()
    }
    func retry() {
        authenticate()
    }
    func authenticate() {
        setViewsState()
        self.spinner.startAnimating()
//        viewControllers = []
        if isLoggedIn() {
            let rollNo = UserDefaults.standard.getRollNo()
            guard let password = UserDefaults.standard.getPassword() else {
                self.spinner.stopAnimating()
                let alertVC = UIAlertController(title: "Authentication Error", message: "Please sign in again.", preferredStyle: UIAlertController.Style.alert)
                DispatchQueue.main.async {
                    alertVC.addAction(UIAlertAction(title: "Sign in", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
                    self.handleSignOut()
                })
                    self.present(alertVC, animated: true, completion: nil)
                }
                return
            }
            
            UserAuthenticationService.shared.fetch(rollNo, password) { (data, error) in
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
                    
                
                    guard error == nil else {
                        self.spinner.stopAnimating()
                        let alertVC = UIAlertController(title: "Connection Error", message: "Request timed out. Please try again.", preferredStyle: UIAlertController.Style.alert)
                        alertVC.addAction(UIAlertAction(title: "Sign Out", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
                            let loginController = LoginController()
                            UserDefaults.standard.setIsLoggedIn(value: false)
                            UserDefaults.clearUserData()
                            DispatchQueue.main.async {
                                self.present(loginController, animated: true, completion: nil)
                            }
                        })
                        alertVC.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default) { (action:UIAlertAction) in
                            self.retry()
                        })
                        self.present(alertVC, animated: true, completion: nil)
                        return
                    }
                    
                    let result = data?.result
                    _ = data?.message
                    switch result {
                    case ResultType.Failure.rawValue:
                        self.spinner.stopAnimating()
                        let alertVC = UIAlertController(title: "Authentication Error", message: "Please sign in again.", preferredStyle: UIAlertController.Style.alert)
                        alertVC.addAction(UIAlertAction(title: "Sign in", style: UIAlertAction.Style.destructive) { (action:UIAlertAction) in
                            let loginController = LoginController()
                            UserDefaults.standard.setIsLoggedIn(value: false)
                            UserDefaults.clearUserData()
                            DispatchQueue.main.async {
                                self.present(loginController, animated: true, completion: nil)
                            }
                        })
                        self.present(alertVC, animated: true, completion: nil)
                        
                    case ResultType.Success.rawValue:
                        self.spinner.stopAnimating()
                        self.setUpTabBarViewControllers()
                    default:
                        break
                    }
                })
            }
        } else {
            self.spinner.stopAnimating()
            let loginController = LoginController()
            UserDefaults.standard.setIsLoggedIn(value: false)
            UserDefaults.clearUserData()
            DispatchQueue.main.async {
                self.present(loginController, animated: false, completion: nil)
            }
//            DispatchQueue.main.async {
//                print("hahahha")
//                self.showLoginController()
//            }
        }
    }
    func showLoginController() {
        let loginController = LoginController()
        UserDefaults.standard.setIsLoggedIn(value: false)
        UserDefaults.clearUserData()
        tabBarController?.present(loginController, animated: true, completion: nil)
        //        present(loginController, animated: false, completion: {
        //            UserDefaults.standard.setIsLoggedIn(value: false)
        //            UserDefaults.clearUserData()
        //        })
    }
    var verified = false
    
    func setUpTabBarViewControllers() {
//        self.view.alpha = 1.0
        let homeViewController = UINavigationController(rootViewController: HomeViewController())
//        let eventsViewController = UINavigationController(rootViewController: EventsViewController())
        let academicsViewController = UINavigationController(rootViewController: NewAcademicsViewController())
        let preferencesViewController = UINavigationController(rootViewController: PreferencesTableViewController())
        homeViewController.tabBarItem = UITabBarItem(title: "Home", image: #imageLiteral(resourceName: "home"), tag: 0)
//        eventsViewController.tabBarItem = UITabBarItem(title: "Events", image: #imageLiteral(resourceName: "events"), tag: 1)
        academicsViewController.tabBarItem = UITabBarItem(title: "Academics", image: #imageLiteral(resourceName: "academics"), tag: 1)
        preferencesViewController.tabBarItem = UITabBarItem(title: "Preferences", image: #imageLiteral(resourceName: "preferences"), tag: 2)
        self.tabBar.tintColor = UIColor.glauThemeColor
        let viewControllersList = [homeViewController, academicsViewController, preferencesViewController]
        viewControllers = viewControllersList
        isLoaded = true
        self.processShortcut(type: self.shortcutType)
//        AppDelegate().registerForPushNotifications()
//        self.processNotification(type: self.notificationType)
        self.processNewsNotification(newsId: self.newsId)
        self.shortcutType = nil
        self.newsId = nil
    }
}
