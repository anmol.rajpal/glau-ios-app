//
//  ArticleCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit
protocol ArticleCellDelegate {
//    func articlePressed(cell:ArticleCollectionViewCell)
}
class ArticleCollectionViewCell: UICollectionViewCell {
    static let viewHeight:CGFloat = 120
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let articleView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.layer.cornerRadius = 15
        view.backgroundColor = UIColor.systemPurpleColor
        //        view.clipsToBounds = true
        return view
    }()
    
    let titleLabel:UILabel = {
        let label = UILabel()
        label.text = "This is the best article of the day. The title is long usually. So it should be expanded to 3 lines approximately. Content to be displayed after opening the article."
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 3
        return label
    }()
    
    /*
     let contentLabel:UILabel = {
     let label = UILabel()
     label.text = "sdasd sdf s df sd f sadf a f sdf as dfasdf asa df asf sadf  asdf sda fa sdf as df asdf as dfa sdf sdaf as df asdf as df  fads fa sd asg ds f asdfasdfasdf asdf asd fsad fas df as fasf"
     label.textColor = .white
     label.font = UIFont.systemFont(ofSize: 18, weight: .light)
     label.lineBreakMode = .byTruncatingTail
     label.numberOfLines = 2
     return label
     }()
     */
    let authorLabel:UILabel = {
        let label = UILabel()
        label.text = "- "+"Crappy Author"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        return label
    }()
    let sourceLabel:UILabel = {
        let label = UILabel()
        label.text = "Source: TOI"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(articleView)
        articleView.addSubview(titleLabel)
        articleView.addSubview(authorLabel)
        articleView.addSubview(sourceLabel)
    }
    func setUpConstraints() {
        _ = articleView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        articleView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        _ = titleLabel.anchor(articleView.topAnchor, left: articleView.leftAnchor, bottom: nil, right: articleView.rightAnchor, topConstant: 15, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = authorLabel.anchor(nil, left: nil, bottom: articleView.bottomAnchor, right: articleView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        _ = sourceLabel.anchor(nil, left: articleView.leftAnchor, bottom: articleView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 15, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}
