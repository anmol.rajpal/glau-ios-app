//
//  MainNavigationController.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 22/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
//        if isLoggedIn() {
//            //assume user is logged in
////            var tabBar:TabBarController
////            let tabBarController = TabBarController()
//            let rootViewController = UIApplication.shared.keyWindow?.rootViewController
//            guard let mainNavigationController = rootViewController as? TabBarController else { return }
//            mainNavigationController.tabBarController?.selectedIndex = 1
//
//        } else {
//            perform(#selector(showLoginController), with: nil, afterDelay: 0.01)
//        }
    }
    
    fileprivate func isLoggedIn() -> Bool {
        return UserDefaults.standard.isLoggedIn()
    }
    
    @objc func showLoginController() {
        let loginController = LoginController()
        present(loginController, animated: true, completion: {
            //perhaps we'll do something here later
        })
    }
}
