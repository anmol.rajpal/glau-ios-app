//
//  ReusableFooterCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class ReusableFooterCell: UICollectionViewCell {
    internal static let viewHeight: CGFloat = 10
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let hSeperator = HorizontalLine(color: .lightGray)
    func setUpViews() {
        contentView.addSubview(hSeperator)
    }
    func setUpConstraints() {
        _ = hSeperator.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0.5)
    }
}
