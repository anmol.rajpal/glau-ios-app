//
//  TodaySectionHeaderCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class TodaySectionHeaderCell: UICollectionViewCell {
    internal static let viewHeight: CGFloat = 65
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let todayLabel:UILabel = {
        let label = UILabel()
        label.text = "Today"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 24, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    let dateLabel:UILabel = {
        let label = UILabel()
        label.text = "SUNDAY 14 OCTOBER"
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 14, weight: .heavy)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(todayLabel)
        contentView.addSubview(dateLabel)
    }
    func setUpConstraints() {
        _ = dateLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = todayLabel.anchor(dateLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}
