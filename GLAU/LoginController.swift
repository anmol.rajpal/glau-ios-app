//
//  LoginController.swift
//  glau ios app
//
//  Created by Anmol Rajpal on 22/07/18.
//  Copyright © 2018 Protomile. All rights reserved.
//
import Foundation
import UIKit
import SwiftSoup
import LocalAuthentication

class SpinnerViewController:UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        view.alpha = 0.5
        view.addSubview(spinner)
    }
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.whiteLarge
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        return indicator
    }()
    func start() {
        spinner.startAnimating()
    }
    func stop() {
        self.dismiss(animated: false, completion: nil)
        spinner.stopAnimating()
    }
}
class LoginController: UIViewController {
    let sp = SpinnerViewController()
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        authenticateUserUsingTouchId()
//        observeKeyboardNotifications()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        observeKeyboardNotifications()
        hideKeyboardWhenTappedAround()
        setUpViews()
        setUpConstraints()
        idTextField.delegate = self
        passwordTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        print("hmm")
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.keyboardHide()
//        self.view.endEditing(true)
//        idTextField.resignFirstResponder()
//        passwordTextField.resignFirstResponder()
    }
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        self.keyboardHide()
//    }
    let spinner:UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
//        spinner.color = .white
        
        spinner.backgroundColor = UIColor.glauThemeColor
//        spinner.style = UIActivityIndicatorView.Style.whiteLarge
        spinner.hidesWhenStopped = true
        spinner.clipsToBounds = true
        spinner.translatesAutoresizingMaskIntoConstraints = false
        return spinner
    }()
   
    func startSpinner() {
//        sp.modalPresentationStyle = .overFullScreen
//        self.present(sp, animated: false, completion: nil)
//        sp.start()
        spinner.startAnimating()
        self.view.isUserInteractionEnabled = false
//        self.view.alpha = 0.5
    }
    func stopSpinner() {
//        sp.stop()
        spinner.stopAnimating()
//        self.view.alpha = 1.0
        self.view.isUserInteractionEnabled = true
    }
    let logoImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        return imageView
    }()
    
    let idTextField: LeftPaddedTextField = {
        let textField = LeftPaddedTextField()
        textField.placeholder = "University Roll No."
        textField.clearButtonMode = .always
        textField.clearButtonMode = .whileEditing
        textField.layer.borderColor = UIColor.glauThemeColor.cgColor
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 15
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.textContentType = UITextContentType.username
        textField.returnKeyType = UIReturnKeyType.next
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.addTarget(self, action: #selector(idFieldDidReturn(textField:)), for: UIControl.Event.editingDidEndOnExit)
        return textField
    }()
    
    let passwordTextField: LeftPaddedTextField = {
        let textField = LeftPaddedTextField()
        textField.placeholder = "Password"
        textField.clearButtonMode = .always
        textField.clearButtonMode = .whileEditing
        textField.layer.borderColor = UIColor.glauThemeColor.cgColor
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 15
        textField.isSecureTextEntry = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textContentType = UITextContentType.password
        textField.returnKeyType = UIReturnKeyType.go
        textField.addTarget(self, action: #selector(passwordFieldDidReturn(textField:)), for: UIControl.Event.editingDidEndOnExit)
        return textField
    }()
    @objc func idFieldDidReturn(textField: UITextField!) {
//        textField.resignFirstResponder()
        self.passwordTextField.becomeFirstResponder()
    }
    @objc func passwordFieldDidReturn(textField: UITextField!) {
        textField.resignFirstResponder()
        self.login()
    }
    lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.glauThemeColor
        button.setTitle("LOG IN", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .heavy)
//        button.layer.shadowColor = UIColor.black.cgColor
//        button.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
//        button.layer.masksToBounds = false
//        button.layer.shadowRadius = 2.0
//        button.layer.shadowOpacity = 0.5
//        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 15
        button.setTitleColor(.white, for: .normal)
        button.clipsToBounds = true
//        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    let skipButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Skip", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        button.setTitleColor(UIColor.glauThemeColor, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(handleSkipAction), for: UIControl.Event.touchUpInside)
        button.isHidden = true
        return button
    }()
    @objc func handleSkipAction() {
        let homeVC = GuestHomeViewController()
        self.show(homeVC, sender: self)
    }
    @objc func handleLogin() {
        self.login()
    }
    @objc func handleStudentLogin() {
        self.login()
    }
    @objc func handleFacultyLogin() {
        UIAlertController.showAlert(alertTitle: "Invalid Data", message: "Unable to process your request", alertActionTitle: "Dismiss", controller: self)
    }
    func setUpViewsForUserType(userType:UserType) {
        switch userType {
        case .Student:
            self.idTextField.placeholder = "Roll No#"
            loginButton.alpha = 1
            loginButton.isUserInteractionEnabled = true
        case .Faculty:
            self.idTextField.placeholder = "Faculty ID"
            loginButton.alpha = 0.4
            loginButton.isUserInteractionEnabled = false
        case .Parent:
            self.idTextField.placeholder = "Parent ID"
            loginButton.alpha = 0.4
            loginButton.isUserInteractionEnabled = false
        }
    }
    @objc func indexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            setUpViewsForUserType(userType: .Student)
        case 1:
            setUpViewsForUserType(userType: .Faculty)
        case 2:
            setUpViewsForUserType(userType: .Parent)
        default:
            break
        }
    }
    let segmentedControl:UISegmentedControl = {
        let items = [UserType.Student.rawValue, UserType.Faculty.rawValue, UserType.Parent.rawValue]
        let control = UISegmentedControl(items: items)
        control.selectedSegmentIndex = 0
        control.layer.cornerRadius = 5.0

        control.backgroundColor = .white
        control.tintColor = .glauThemeColor
        control.addTarget(self, action: #selector(indexChanged(_:)), for: .valueChanged)
        control.translatesAutoresizingMaskIntoConstraints = false
        return control
    }()
    func setUpViews() {
        
//        view.addSubview(segmentedControl)
        view.addSubview(skipButton)
        view.addSubview(logoImageView)
        view.addSubview(idTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        loginButton.addSubview(spinner)
    }
    func setUpConstraints() {
        _ = skipButton.anchorWithoutWH(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 20)
        
//        skipButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        _ = logoImageView.anchor(nil, left: nil, bottom: idTextField.topAnchor, right: nil, topConstant: -230, leftConstant: 0, bottomConstant: 25, rightConstant: 0, widthConstant: 140, heightConstant: 140)
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
//        segmentedControl.bottomAnchor.constraint(equalTo: idTextField.topAnchor, constant: -25).isActive = true
//        segmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = idTextField.anchor(nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 32, bottomConstant: 0, rightConstant: 32, widthConstant: 0, heightConstant: 50)
        idTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        idTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        _ = passwordTextField.anchor(idTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 16, leftConstant: 32, bottomConstant: 0, rightConstant: 32, widthConstant: 0, heightConstant: 50)
        
        _ = loginButton.anchor(passwordTextField.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 16, leftConstant: 100, bottomConstant: 0, rightConstant: 100, widthConstant: 0, heightConstant: 45)
        spinner.centerXAnchor.constraint(equalTo: loginButton.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: loginButton.centerYAnchor).isActive = true
        spinner.widthAnchor.constraint(equalTo: loginButton.widthAnchor).isActive = true
        spinner.heightAnchor.constraint(equalTo: loginButton.heightAnchor).isActive = true
    }

    fileprivate func observeKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardHide() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    
    @objc func keyboardShow() {
        let iPhoneKeyboardHeight:CGFloat = 100
        let iPadKeyboardHeight:CGFloat = 100
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            let y: CGFloat = UIDevice.current.orientation.isLandscape ? -iPadKeyboardHeight : -iPhoneKeyboardHeight
            self.view.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    /*
    fileprivate func saveAccountDetailsToKeychain(account: String, password: String) {
        guard account.isEmpty, password.isEmpty else { return }
        UserDefaults.standard.set(account, forKey: "lastAccessedUserName")
        
        let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: account, accessGroup: KeychainConfiguration.accessGroup)
        do {
            try passwordItem.savePassword(password)
            print("Password Saved")
        } catch {
            print("Error saving password")
        }
    }
    fileprivate func authenticateUserUsingTouchId() {
        print("Function called")
        let context = LAContext()
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthentication, error: nil) {
            self.evaulateTocuhIdAuthenticity(context: context)
            print("evaulateTocuhIdAuthenticity")
        }
    }
    func evaulateTocuhIdAuthenticity(context: LAContext) {
        print("evaulateTocuhIdAuthenticity called")
        guard let lastAccessedUserName = UserDefaults.standard.object(forKey: "lastAccessedUserName") as? String else { return }
        print("last accessed user name -> \(lastAccessedUserName)")
        context.evaluatePolicy(LAPolicy.deviceOwnerAuthentication, localizedReason: lastAccessedUserName) { (authSuccessful, authError) in
            if authSuccessful {
                print("authSuccessful")
                self.loadPasswordFromKeychainAndAuthenticateUser(lastAccessedUserName)
            } else {
                print("Nops")
                if let error = authError as? LAError {
                    self.showError(error: error)
                }
            }
        }
    }
    func showError(error: LAError) {
        var message: String = ""
        switch error.code {
        case LAError.authenticationFailed:
            message = "Authentication was not successful because the user failed to provide valid credentials. Please enter password to login."
            break
        case LAError.userCancel:
            message = "Authentication was canceled by the user"
            break
        case LAError.userFallback:
            message = "Authentication was canceled because the user tapped the fallback button"
            break
        case LAError.biometryNotEnrolled:
            message = "Authentication could not start because Touch ID has no enrolled fingers."
            break
        case LAError.biometryNotAvailable:
            message = "Biometry Not Available"
            break
        case LAError.passcodeNotSet:
            message = "Passcode is not set on the device."
            break
        case LAError.systemCancel:
            message = "Authentication was canceled by system"
            break
        default:
            message = error.localizedDescription
            break
        }
        UIAlertController.showAlert(alertTitle: "Error", message: message, alertActionTitle: "Ok", controller: self)
    }
    fileprivate func loadPasswordFromKeychainAndAuthenticateUser(_ account: String) {
        print("Load passwordfromke and authenrasd called")
        guard !account.isEmpty else { return }
        let passwordItem = KeychainPasswordItem(service:   KeychainConfiguration.serviceName, account: account, accessGroup: KeychainConfiguration.accessGroup)
        do {
            let storedPassword = try passwordItem.readPassword()
            print(storedPassword)
            authenticateAndFetch(account, storedPassword)
        } catch KeychainPasswordItem.KeychainError.noPassword {
            print("No saved password")
        } catch {
            print("Unhandled error")
        }
    }
    */
    func login() {
        view.endEditing(true)
        let rollNo = idTextField.text!, password = passwordTextField.text!
        guard rollNo != "" && password != "" else {
            DispatchQueue.main.async {
                UIAlertController.showAlert(alertTitle: "Fields Empty", message: "Required Fields are Empty", alertActionTitle: "Dismiss", controller: self)
            }
            return
        }
        startSpinner()
        authenticate(rollNo, password)
    }
    func abc() {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        
        
        //                     let snapshot = (window.snapshotView(afterScreenUpdates: true))!
        //                    window.addSubview(snapshot)
        //                    UIView.animate(withDuration: 0.5, animations: {
        //                         UIApplication.shared.keyWindow?.rootViewController = TabBarController()
        ////                        snapshot.layer.transform = CATransform3DMakeScale(0, 1.5, 0)
        //                        let a = CATransition()
        //                        a.subtype = CATransitionSubtype.fromTop
        //                        a.duration = CFTimeInterval(exactly: 1.0)!
        //                        window.layer.add(a, forKey: "a")
        //
        //                    }, completion: { (status) in
        //                        self.keyboardHide()
        ////                        snapshot.removeFromSuperview()
        //                    })
        //                    let s = window.snapshotView(afterScreenUpdates: true)+
        //                    window.alpha = 0.0
        //                    UIView.transition(with: window, duration: 0.2, options: .transitionCrossDissolve, animations: {
        ////                        snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
        //                        window.resignFirstResponder()
        //                        window.alpha = 1.0
        //                        window.rootViewController = TabBarController()
        //                    }, completion: { completed in
        //
        //
        //
        //                        window.removeFromSuperview()
        //                    })
        let tbc = TabBarController()
        tbc.view.alpha = 0
        UIView.animate(withDuration: 0.3, delay: 0.1, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            window.resignFirstResponder()
            self.view.alpha = 0.0
            window.rootViewController = tbc
            tbc.view.alpha = 1
            //                        tbc.view.alpha = 1.0
            //                        window.layer.transform = CATransform3DIdentity
        }, completion: nil)
        //                    self.dismiss(animated: false, completion: {
        //                        UserDefaults.standard.setRollNo(rollNo: rollNo)
        //                        UserDefaults.standard.setIsLoggedIn(value: true)
        //
        //                    })
    }
   
    func authenticate(_ rollNo:String, _ password:String) {
        UserAuthenticationService.shared.fetch(rollNo, password) { (jsonObjectData, error) in
            guard error == nil else {
                if let err = error {
                    switch err {
                    case .FailedRequest:
                        DispatchQueue.main.async {
                            self.stopSpinner()
                            UIAlertController.showAlert(alertTitle: "Request Timed Out", message: "Please try again later", alertActionTitle: "Ok", controller: self)
                        }
                    case .InvalidResponse:
                        DispatchQueue.main.async {
                            self.stopSpinner()
                            UIAlertController.showAlert(alertTitle: "Invalid Response", message: "Please try again", alertActionTitle: "Ok", controller: self)
                        }
                    case .Unknown:
                        DispatchQueue.main.async {
                            self.stopSpinner()
                            UIAlertController.showAlert(alertTitle: "Some Error Occured", message: "An unknown error occured. Please try again later.", alertActionTitle: "Ok", controller: self)
                        }
                    }
                }
                return
            }
            
//                guard let dict = try JSONSerialization.jsonObject(with: jsonObjectData!, options: []) as? [String: Any] else {
//                    print("Error, JSON error")
//                    return
//                }
            
            let result = jsonObjectData?.result
            let message = jsonObjectData?.message
            switch result {
            case ResultType.Failure.rawValue:
                print("Failure. Unable to login with message \(message ?? "- -")")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.stopSpinner()
                    UIAlertController.showAlert(alertTitle: "Login Failed", message: message ?? "", alertActionTitle: "Try Again", controller: self)
                })

            case ResultType.Success.rawValue:
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    UserDefaults.standard.setIsLoggedIn(value: true)
                    UserDefaults.standard.setRollNo(rollNo: rollNo)
                    UserDefaults.standard.setPassword(password: password)
                    UserDefaults.standard.setUserType(type: UserType.Student)
                    self.stopSpinner()
                    
                    //self.saveAccountDetailsToKeychain(account: rollNo, password: password)
//                    self.tabBarController?.viewControllers = nil
//                    self.tabBarController?.view.isHidden = true
//                    self.tabBarController?.selectedViewController?.view.isHidden = true
                    self.dismiss(animated: true, completion: nil)
                })
            default:
                print("Invalid Case")
                break
            }
        }
    }
    
    
}
class LeftPaddedTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width + 10, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width + 10, height: bounds.height)
    }
    
}
extension LoginController:UITextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        return true
    }
}
