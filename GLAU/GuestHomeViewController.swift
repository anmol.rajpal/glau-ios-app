//
//  GuestHomeViewController.swift
//  GLAU
//
//  Created by Anmol Rajpal on 24/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class GuestHomeViewController: UIViewController {
    var news:NewsServiceCodable?
    var newsArr:[NewsServiceCodable.NewsOfTheDay]? = []
    var combinedServices:CombinedServicesCodable?
    var errorLoadingTimeTable:Bool?
    var isTimeTableEmpty:Bool?
    var isTimeTableLoaded:Bool?
    var isArticlesServiceEnabled:Bool = false
    var isQuotesServiceEnabled:Bool = false
    var isFactsServiceEnabled:Bool = false
    var isRiddlesServiceEnabled:Bool = false
    var isTipsServiceEnabled:Bool = false
    var isNewsServiceEnabled:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        checkReachability()
        setupNavBar()
        setUpTableView()
        setUpViews()
        setUpConstraints()
        fetchNewsOfTheDay()
        fetchCombinedServicesOfTheDay()
        //        loadTableViewWithAnimation()
        tableView.reloadDataWithLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkReachability()
        setupNavBar()
        self.navigationItem.title = "Home"
        //        fetchNewsOfTheDay()
        //        view.layoutIfNeeded()
        //        tableView.reloadDataWithLayout()
    }
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //        setupNavBar()
    //    }
    //    override func viewDidDisappear(_ animated: Bool) {
    //        super.viewDidDisappear(animated)
    //        setupNavBar()
    //    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //        fetchNewsOfTheDay()
        
        //        loadTableViewWithAnimation()
        if let selectionIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectionIndexPath, animated: animated)
        }
    }
    func loadTableViewWithAnimation() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.tableView.reloadDataWithLayout()
            })
        }
    }
    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .white
        tableView.register(SubjectDetailsTableHeaderView.self, forHeaderFooterViewReuseIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self))
        tableView.register(GuestMenuItemsCell.self, forCellReuseIdentifier: NSStringFromClass(GuestMenuItemsCell.self))
        tableView.register(GreetingTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(GreetingTableViewCell.self))
        tableView.register(TodayNewsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayNewsTableViewCell.self))
        tableView.register(TodayQuoteTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayQuoteTableViewCell.self))
        tableView.register(TodayFactTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayFactTableViewCell.self))
        tableView.register(TodayTipTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayTipTableViewCell.self))
        tableView.register(TodayArticleTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayArticleTableViewCell.self))
        tableView.register(TodayRiddleTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayRiddleTableViewCell.self))
    }
    let loginButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Login", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        button.setTitleColor(UIColor.glauThemeColor, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(handleLoginAction), for: UIControl.Event.touchUpInside)
        return button
    }()
    @objc func handleLoginAction() {
        self.dismiss(animated: true, completion: nil)
    }
    let tableView : UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .white
        tv.bounces = true
        tv.alwaysBounceVertical = true
        tv.clipsToBounds = true
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = true
        tv.separatorStyle = .none
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        tv.isHidden = false
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.estimatedRowHeight = 10.0
        tv.rowHeight = UITableView.automaticDimension
        return tv
    }()
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel(message:String = "Cannot Connect to GLAU") {
        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        
    }
    
    func getDayTimeText() -> String {
        let morningTexts = ["Good Morning!", "Rise & Shine", "It's a new Day", "Have a great Day", "Morning!"]
        let noonTexts = ["Good Noon!", "Noon!"]
        let afterNoonTexts = ["Good Afternoon!", "Have a wonderful Afternoon", "Hope your afternoon's going well!", "Afternoon!"]
        let eveningTexts = ["Good Evening!", "Hope your evening's going well!", "Have a wonderful Evening"]
        let nightTexts = ["Good Night!", "Sleep Well!"]
        let timeOfDay = getTimeOfDay()
        switch timeOfDay {
        case .Morning: return morningTexts.randomElement()!
        case .Noon: return noonTexts.randomElement()!
        case .Afternoon: return afterNoonTexts.randomElement()!
        case .Evening: return eveningTexts.randomElement()!
        case .Night: return nightTexts.randomElement()!
        }
    }
    
    func getTodayDateString() -> String {
        return getStringFromDate(date: Date(), dateFormat: NSObject.DateFormatType.MMMMdEEEE).uppercased()
    }
    var todayDateString:String {
        get {
            return self.getTodayDateString()
        }
    }
    var dayTimeText:String {
        get {
            return getDayTimeText()
        }
    }
    private func fetchCombinedServicesOfTheDay() {
        CombinedServices.shared.fetch { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print(error?.localizedDescription ?? "Error")
                    return
                }
                //                let result = data?.count != 0 ? "Success" : "Failure"
                //                _ = "No Data"
                //                let dataCount = data?.count ?? -1
                
                let result = data?.result
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure")
                case ResultType.Success.rawValue:
                    if let data = data {
                        
                        self.combinedServices = data
                        
                        self.isArticlesServiceEnabled = ((self.combinedServices?.articlesService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isQuotesServiceEnabled = ((self.combinedServices?.quotesService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isTipsServiceEnabled = ((self.combinedServices?.tipsService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isFactsServiceEnabled = ((self.combinedServices?.factsService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isRiddlesServiceEnabled = ((self.combinedServices?.riddlesService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.tableView.reloadDataWithLayout()
                    }
                default: break
                }
            }
        }
    }
    let homeLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Home"
        label.textColor = .black
        label.numberOfLines = 1
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body).bold()
        label.textAlignment = .center
        return label
    }()
    private func fetchNewsOfTheDay() {
        NewsService.shared.fetch { (data, error) in
            self.newsArr?.removeAll()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
                
            
                guard error == nil else {
                    print(error?.localizedDescription ?? "Error")
                    return
                }
                //                let result = data?.count != 0 ? "Success" : "Failure"
                //                _ = "No Data"
                //                let dataCount = data?.count ?? -1
                
                let result = data?.result
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure")
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.news = data
                        self.newsArr = data.newsOfTheDay
                        self.tableView.reloadDataWithLayout()
//                        self.animate()
                        //                        UIView.animate(withDuration: 0.2, animations: {
                        //                            self.tableView.reloadDataWithLayout()
                        //                        })
                        //                        self.tableView.reloadDataWithLayout()
                    }
                default: break
                }
            })
        }
    }
    func setUpViews() {
        view.backgroundColor = .white
        view.addSubview(loginButton)
        view.addSubview(homeLabel)
        view.addSubview(tableView)
        view.addSubview(spinner)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
        //        setupMessageLabel()
        //        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = loginButton.anchorWithoutWH(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: nil, topConstant: 13, leftConstant: 15, bottomConstant: 0, rightConstant: 0)
        homeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        homeLabel.centerYAnchor.constraint(equalTo: loginButton.centerYAnchor).isActive = true
        _ = tableView.anchorWithoutWH(top: loginButton.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
}
extension GuestHomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func shit(cell:UITableViewCell) {
        cell.alpha = 1
        
        let anchor = cell.heightAnchor.constraint(equalToConstant: 0)
        anchor.priority = UILayoutPriority(999)
        anchor.isActive = true
        //        cell.frame = CGRect.zero
        //        cell.layoutIfNeeded()
        //        cell.layer.transform = CATransform3DMakeScale(1.0, 0.01, 1.0)
        UIView.animate(withDuration: 2.0, delay: 1.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            cell.alpha = 1
            //            cell.layer.transform = CATransform3DIdentity
            //            anchor.constant = 100
            cell.layoutIfNeeded()
        }, completion: nil)
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let duration:TimeInterval = 0.2
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(GuestMenuItemsCell.self), for: indexPath) as! GuestMenuItemsCell
            cell.delegate = self
            cell.selectionStyle = .none
//            DispatchQueue.main.async {
//                UIView.animate(withDuration: duration, animations: {
//                    tableView.beginUpdates()
//                    tableView.endUpdates()
//                })
//            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(GreetingTableViewCell.self), for: indexPath) as! GreetingTableViewCell
            cell.selectionStyle = .none
//            DispatchQueue.main.async {
//                UIView.animate(withDuration: duration, animations: {
//                    tableView.beginUpdates()
//                    tableView.endUpdates()
//                })
//            }
            cell.dateLabel.text = self.todayDateString
            cell.greetingLabel.text = self.dayTimeText
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayNewsTableViewCell.self), for: indexPath) as! TodayNewsTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.newsDetails = self.news?.newsOfTheDay
            cell.layoutIfNeeded()
//            cell.contentView.frame.height = 0
//            if newsArr?.count ?? 0 > 0 {
//                DispatchQueue.main.async {
////                    cell.frame.size.height = 0
//                    UIView.animate(withDuration: duration, animations: {
//                        self.tableView.beginUpdates()
//                        cell.newsDetails = self.news?.newsOfTheDay
////                        cell.layoutIfNeeded()
//                        self.tableView.endUpdates()
//                    })
//                }
//            }
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayQuoteTableViewCell.self), for: indexPath) as! TodayQuoteTableViewCell
            cell.selectionStyle = .none
//            shit(cell: cell)
//            cell.alpha = 0.5
//            cell.layer.transform = CATransform3DMakeScale(1.0, 0.0, 1)
            
            if isQuotesServiceEnabled {
//                DispatchQueue.main.async {
//                    UIView.animate(withDuration: duration) {
//                        tableView.beginUpdates()
                        cell.quoteDetails = self.combinedServices?.quotesService?.quoteOfTheDay
//                        tableView.endUpdates()
//                    }
//                }
//                cell.quoteDetails = self.combinedServices?.quotesService?.quoteOfTheDay
            }
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayFactTableViewCell.self), for: indexPath) as! TodayFactTableViewCell
            cell.selectionStyle = .none
            if isFactsServiceEnabled {
//                DispatchQueue.main.async {
//                    UIView.animate(withDuration: duration) {
//                        tableView.beginUpdates()
//                        cell.factDetails = self.combinedServices?.factsService?.factOfTheDay
//                        tableView.endUpdates()
//                    }
//                }
                cell.factDetails = self.combinedServices?.factsService?.factOfTheDay
            }
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayArticleTableViewCell.self), for: indexPath) as! TodayArticleTableViewCell
            
            if isArticlesServiceEnabled {
//                DispatchQueue.main.async {
//                    UIView.animate(withDuration: duration) {
//                        tableView.beginUpdates()
//                        cell.articleDetails = self.combinedServices?.articlesService?.articleOfTheDay
//                        tableView.endUpdates()
//                    }
//                }
                cell.articleDetails = self.combinedServices?.articlesService?.articleOfTheDay
            }
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayTipTableViewCell.self), for: indexPath) as! TodayTipTableViewCell
            cell.selectionStyle = .none
            if isTipsServiceEnabled {
//                DispatchQueue.main.async {
//                    UIView.animate(withDuration: duration) {
//                        tableView.beginUpdates()
//                        cell.tipDetails = self.combinedServices?.tipsService?.tipOfTheDay
//                        tableView.endUpdates()
//                    }
//                }
                cell.tipDetails = self.combinedServices?.tipsService?.tipOfTheDay
            }
        
            return cell
        
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayRiddleTableViewCell.self), for: indexPath) as! TodayRiddleTableViewCell
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 5 {
            let articleDetailsVC = ArticleDetailsViewController()
            articleDetailsVC.articleDetails = self.combinedServices?.articlesService?.articleOfTheDay
            articleDetailsVC.view.backgroundColor = .white
            self.navigationController?.pushViewController(articleDetailsVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self)) as! SubjectDetailsTableHeaderView
        //        if section == 0 {
        //            headerView.headerLabel.text = "Semester Info"
        //        } else
        switch section {
        case 0: headerView.headerLabel.text = "Menu"
        case 1: headerView.headerLabel.text = "Today"
        case 2: headerView.headerLabel.text = "News"
        case 3: headerView.headerLabel.text = "Quote of the Day"
        case 4: headerView.headerLabel.text = "Fact of the Day"
        case 5: headerView.headerLabel.text = "Article of the Day"
        case 6: headerView.headerLabel.text = "Tip"
        //            case 8: headerView.headerLabel.text = "Riddle"
        default: headerView.headerLabel.text = ""
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        animate()
        let defaultHeight:CGFloat = 35
        switch section {
        case 0: return 0
        case 2: if self.news?.newsOfTheDay?.isEmpty ?? true { return 0 } else { return defaultHeight }
        case 3: if !isQuotesServiceEnabled { return 0 } else { return defaultHeight }
        case 4: if !isFactsServiceEnabled { return 0 } else { return defaultHeight }
        case 5: if !isArticlesServiceEnabled { return 0 } else { return defaultHeight }
        case 6: if !isTipsServiceEnabled { return 0 } else { return defaultHeight }
        default: return defaultHeight
        }
    }
    func animate() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            })
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        animate()
        switch indexPath.section {
        case 0:
            let numOfItemsInRow = 3
            let itemsCount = 1
            let itemHeight = MenuItemsTableViewCell.itemHeight
            let ceil = itemsCount / numOfItemsInRow
            if itemsCount % numOfItemsInRow == 0 {
                return CGFloat(ceil) * (itemHeight + 10)
            } else {
                return CGFloat(ceil + 1) * (itemHeight + 10)
            }
        case 2:
            if self.news?.newsOfTheDay?.isEmpty ?? true { return 0 } else {
                return UITableView.automaticDimension
            }
        default: return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 10
    }
}

extension GuestHomeViewController:GuestMenuItemsCellDelegate {
    func itemPressed(cell: GuestMenuItemsCell, indexPath: IndexPath, menuCell:MenuCollectionViewCell) {
        switch indexPath.row {
        case 0:
            let evc = EventCalendarViewController()
            evc.modalPresentationStyle = .overFullScreen
            self.present(evc, animated: true, completion: nil)
        default: break
        }
        UIView.animate(withDuration: 0.3, delay: 1.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            menuCell.menuButton.alpha = 1
            menuCell.menuButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        }, completion: nil)
    }
}
extension GuestHomeViewController:TodayNewsCellDelegate {
    func scroll(cell: TodayNewsTableViewCell) {
        print("shit")
    }
    
    func newsPressed(cell: TodayNewsTableViewCell, newsDetails: NewsServiceCodable.NewsOfTheDay?) {
        let newsDetailVC = NewsDetailViewController()
        newsDetailVC.newsDetails = newsDetails
        newsDetailVC.view.backgroundColor = .white
        self.navigationController?.pushViewController(newsDetailVC, animated: true)
    }
}
