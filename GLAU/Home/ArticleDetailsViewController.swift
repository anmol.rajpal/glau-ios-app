//
//  ArticleDetailsViewController.swift
//  GLAU
//
//  Created by Anmol Rajpal on 04/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
extension UIScrollView {
    func scrollViewToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
}
class ArticleDetailsViewController: UIViewController {
    var articleDetails:CombinedServicesCodable.ArticlesService.ArticleOfTheDay? {
        didSet {
            articleTitleLabel.text = articleDetails?.title ?? ""
            articleAuthorLabel.text = "- \(articleDetails?.author ?? "")"
            articleContentLabel.text = articleDetails?.content ?? ""
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        navigationItem.title = "Article Details"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        setUpViews()
        setUpConstraints()
//        scrollView.scrollViewToBottom(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkReachability()
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    let scrollView:UIScrollView = {
        let view = UIScrollView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = true
        view.alwaysBounceVertical = true
        view.bounces = true
        return view
    }()
    let contentView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let articleTitleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1).condensed()
        //        label.textColor = .black
        label.adjustsFontForContentSizeCategory = true
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .left
        label.sizeToFit()
        return label
    }()
    let articleContentLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        label.adjustsFontForContentSizeCategory = true
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
//        label.textAlignment = .left
        return label
    }()
    let articleAuthorLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    let sourceLabel:UILabel = {
        let label = UILabel()
        label.text = "Source: Natovi"
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    let publisherLabel:UILabel = {
        let label = UILabel()
        label.text = "Publisher: Anmol Rajpal"
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        view.backgroundColor = .white
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(articleTitleLabel)
        contentView.addSubview(articleContentLabel)
        contentView.addSubview(articleAuthorLabel)
        
    }
    func setUpConstraints() {
        _ = scrollView.anchorWithoutWH(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        _ = contentView.anchorWithoutWH(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        _ = articleTitleLabel.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 20)
        _ = articleContentLabel.anchorWithoutWH(top: articleTitleLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 20)
        _ = articleAuthorLabel.anchorWithoutWH(top: articleContentLabel.bottomAnchor, left: nil, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 15, rightConstant: 20)
    }
}
