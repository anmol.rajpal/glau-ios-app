//
//  NewsDetailViewController.swift
//  GLA University
//
//  Created by Anmol Rajpal on 15/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    var newsDetails:NewsServiceCodable.NewsOfTheDay? {
        willSet {
            view.layoutIfNeeded()
        }
        didSet {
            newsTitleLabel.text = newsDetails?.title ?? "--"
            newsContentLabel.text = newsDetails?.content ?? "--"
            publisherLabel.text = "- \(newsDetails?.publisherDetails?.publisherName ?? "--")"
            
            setupMetaLabel(source: newsDetails?.source ?? "", publishingDate: newsDetails?.publishingDate ?? "", publishingTime: newsDetails?.publishingTime ?? "")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        checkReachability()
        setupNavBar()
        navigationItem.title = "News Details"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        setUpViews()
        setUpConstraints()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkReachability()
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    func setupMetaLabel(source:String, publishingDate:String, publishingTime:String) {
        let publishingDateTextAttributes = [
            NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        ]
        let publishingTimeTextAttributes = [
            NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        ]
        let sourceTextAttributes = [
            NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        ]
        let publishingDateTextAttributedString = NSMutableAttributedString(string: publishingDate, attributes: publishingDateTextAttributes)
        let publishingTimeTextAttributedString = NSAttributedString(string: publishingTime, attributes: publishingTimeTextAttributes)
        let sourceTextAttributedString = NSAttributedString(string: source, attributes: sourceTextAttributes)
        let metaAttributedString = NSMutableAttributedString()
        metaAttributedString.append(NSAttributedString(string: "Published on "))
        metaAttributedString.append(publishingDateTextAttributedString)
        metaAttributedString.append(NSAttributedString(string: " at "))
        metaAttributedString.append(publishingTimeTextAttributedString)
        metaAttributedString.append(NSAttributedString(string: " | Source: "))
        metaAttributedString.append(sourceTextAttributedString)
        newsMetaLabel.attributedText = metaAttributedString
    }
    let scrollView:UIScrollView = {
        let view = UIScrollView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.showsVerticalScrollIndicator = true
        view.alwaysBounceVertical = true
        view.bounces = true
        return view
    }()
    let contentView:UIView = {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    let newsTitleLabel:UILabel = {
        let label = UILabel()
//        label.textColor = .white
        label.font = UIFont(name: CustomFonts.FuturaCondensedMedium.rawValue, size: 30)
        label.adjustsFontForContentSizeCategory = true
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let newsMetaLabel:UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1)
        label.adjustsFontForContentSizeCategory = true
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }()
    let newsContentLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
        label.adjustsFontForContentSizeCategory = true
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .left
        label.sizeToFit()
        return label
    }()
    let newsAuthorLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        label.adjustsFontForContentSizeCategory = true
        label.textAlignment = .right
        label.sizeToFit()
        return label
    }()
    let sourceLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    let publisherLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout).bold()
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 1
        label.textAlignment = .right
        label.sizeToFit()
        return label
    }()
    func setUpViews() {
        view.backgroundColor = .white
        contentView.addSubview(newsTitleLabel)
        contentView.addSubview(newsMetaLabel)
        contentView.addSubview(newsContentLabel)
        contentView.addSubview(publisherLabel)
        scrollView.addSubview(contentView)
        view.addSubview(scrollView)
    }
    func setUpConstraints() {
        let leftRightMargin:CGFloat = 15
        _ = scrollView.anchorWithoutWH(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        _ = contentView.anchorWithoutWH(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        _ = newsTitleLabel.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = newsMetaLabel.anchorWithoutWH(top: newsTitleLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 15, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = newsContentLabel.anchorWithoutWH(top: newsMetaLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 15, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = publisherLabel.anchorWithoutWH(top: newsContentLabel.bottomAnchor, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 5, rightConstant: leftRightMargin)
    }
}
