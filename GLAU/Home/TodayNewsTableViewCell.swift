//
//  TodayNewsTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
protocol TodayNewsCellDelegate {
    func newsPressed(cell:TodayNewsTableViewCell, newsDetails: NewsServiceCodable.NewsOfTheDay?)
}
class TodayNewsTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var delegate:TodayNewsCellDelegate?
    var isLoaded:Bool = false
    var newsDetails:[NewsServiceCodable.NewsOfTheDay]? {
        willSet {
//            collectionView.reloadData()
//            collectionView.layoutIfNeeded()
//            let h = self.contentView.heightAnchor.constraint(equalToConstant: 0)
//            h.priority = UILayoutPriority(999)
//            h.isActive = true
//            print("Will set content view height - \(self.contentView.frame.height)")
//            print("Will set height - \(collectionView.frame.height)")
            
            
        }
        didSet {
//            collectionView.reloadData()
            self.enableConstraint(constant: NewsCell.viewHeight + 10, true)
            collectionView.reloadData()
//            collectionView.layoutIfNeeded()
//            self.collectionView.frame.size.height = 0
//            UIView.animate(withDuration: 0.2) {
//                self.collectionView.frame.size.height = NewsCell.viewHeight
//                self.enableConstraint(constant: NewsCell.viewHeight, true)
//            }
//            self.isLoaded = true
//            self.layoutIfNeeded()
        }
    }
    func enableConstraint(constant:CGFloat = 0, _ bool:Bool) {
        let h = self.collectionView.heightAnchor.constraint(equalToConstant: constant)
        h.priority = UILayoutPriority(999)
        h.isActive = bool
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setUpViews()
//        setUpConstraints()
//        setUpCollectionView()
//        self.layoutIfNeeded()
//        collectionView.layoutIfNeeded()
//        let h = self.collectionView.heightAnchor.constraint(equalToConstant: NewsCell.viewHeight + 10)
//        h.priority = UILayoutPriority(999)
//        h.isActive = true
//    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
        setUpCollectionView()
        
//        self.layoutIfNeeded()
//        collectionView.layoutIfNeeded()
//        let h = self.collectionView.heightAnchor.constraint(equalToConstant: NewsCell.viewHeight + 40)
//        h.priority = UILayoutPriority(999)
//        h.isActive = true
//        //        abc()
//        self.layoutIfNeeded()
//        collectionView.layoutIfNeeded()

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var needsScroll:Bool? {
        didSet {
            print("did need scroll called")
            if needsScroll == true {
                print("scroll true")
                self.callFromNotification()
            } else {
                print("scroll false")
            }
        }
    }
    func callFromNotification() {
//        self.delegate?.scroll(cell: self)
    }
    func handleNews(indexPath:IndexPath) {
        self.collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        self.delegate?.newsPressed(cell: self, newsDetails: self.newsDetails?[indexPath.row])
    }
    private var indexOfCellBeforeDragging = 0
    private var itemWidth:CGFloat {
        let inset = calculateSectionInset()
        return collectionView.frame.width - inset * 2
    }
    let interItemSpacing:CGFloat = 7.5
    func calculateSectionInset() -> CGFloat {
        return 15
    }
    func setUpCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: NSStringFromClass(NewsCell.self))
//        collectionView.reloadData()
//        collectionView.layoutIfNeeded()
    }
    let headerLabel:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "News"
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).bold()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let separatorLine = HorizontalLine(color: UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0))
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.contentInsetAdjustmentBehavior = .always
//        cv.clipsToBounds = true
        cv.isPagingEnabled = false
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.isHidden = false
        return cv
    }()
    func setUpViews() {
        contentView.addSubview(separatorLine)
        contentView.addSubview(headerLabel)
        contentView.addSubview(collectionView)
    }
    func setUpConstraints() {
        let leftRightMargin:CGFloat = 15
        _ = separatorLine.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        _ = headerLabel.anchorWithoutWH(top: separatorLine.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = collectionView.anchorWithoutWH(top: headerLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
//        _ = collectionView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        let b = self.collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        b.priority = UILayoutPriority(750)
        b.isActive = true
        let h = self.collectionView.heightAnchor.constraint(equalToConstant: 0)
        h.priority = UILayoutPriority(999)
        h.isActive = false
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsDetails?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(NewsCell.self), for: indexPath) as! NewsCell
        let newsCellData = self.newsDetails?[indexPath.row]
        cell.newsDetails = newsCellData
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.newsPressed(cell: self, newsDetails: self.newsDetails?[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return interItemSpacing
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let inset = calculateSectionInset()
        let iw = collectionView.frame.width - inset * 2
//        let iw:CGFloat = 374
        return CGSize(width: iw, height: NewsCell.viewHeight)
//        print(self.isLoaded)
////        print("Count -> \(self.newsDetails?.count)")
//        if !isLoaded {
//            return CGSize.zero
//        } else {
//            return CGSize(width: iw, height: NewsCell.viewHeight)
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = calculateSectionInset()
        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    private func indexOfMajorCell() -> Int {
        let itemWidth = self.itemWidth
        let xContentOffset = collectionView.contentOffset.x
        let proportionalOffset = xContentOffset / itemWidth
        let index = Int(round(proportionalOffset))
        let numberOfItems = collectionView.numberOfItems(inSection: 0)
        let safeIndex = max(0, min(numberOfItems - 1, index))
        return safeIndex
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //*
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        
        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        
        // calculate conditions:
        let dataSourceCount = collectionView.numberOfItems(inSection: 0)
        let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        if didUseSwipeToSkipCell {
            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = (itemWidth + interItemSpacing) * CGFloat(snapToIndex)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)
        } else {
            targetContentOffset.pointee = scrollView.contentOffset
            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}
