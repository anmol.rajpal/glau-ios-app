//
//  TodayQuoteTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class TodayQuoteTableViewCell: UITableViewCell {
    var quoteDetails:CombinedServicesCodable.QuotesService.QuoteOfTheDay? {
        didSet {
            
            quoteLabel.text = "\"" + (quoteDetails?.quote ?? "--") + "\""
            quoterLabel.text = "- " + (quoteDetails?.quoter ?? "--")
            layoutIfNeeded()
//            setUpQuoteData(quote: quoteDetails?.quote ?? "--", quoter: quoteDetails?.quoter ?? "--")
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setUpQuoteData(quote:String, quoter:String) {
        let quotationMarkAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 40, weight: UIFont.Weight.bold),
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        let quoteTextAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.light),
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        let quotationMarkAttributedString = NSMutableAttributedString(string: "\u{275D} ", attributes: quotationMarkAttributes)
        let quoteTextAttributedString = NSAttributedString(string: quote, attributes: quoteTextAttributes)
        let quoteString = NSMutableAttributedString()
        quoteString.append(quotationMarkAttributedString)
        quoteString.append(quoteTextAttributedString)
        quoteLabel.attributedText = quoteString
        quoterLabel.text = "- \(quoter)"
    }
    let headerLabel:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Quote of the Day"
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).bold()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let separatorLine = HorizontalLine(color: UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0))
    let quoteLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: CustomFonts.BodoniBook.rawValue, size: 23)
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    let quoterLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.textAlignment = .right
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(separatorLine)
        contentView.addSubview(headerLabel)
        contentView.addSubview(quoteLabel)
        contentView.addSubview(quoterLabel)
    }
    func setUpConstraints() {
        let leftRightMargin:CGFloat = 15
        _ = separatorLine.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        _ = headerLabel.anchorWithoutWH(top: separatorLine.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = quoteLabel.anchorWithoutWH(top: headerLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = quoterLabel.anchorWithoutWH(top: quoteLabel.bottomAnchor, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 10, rightConstant: leftRightMargin)
        let b = quoterLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        b.priority = UILayoutPriority(750)
        b.isActive = true
    }
}
