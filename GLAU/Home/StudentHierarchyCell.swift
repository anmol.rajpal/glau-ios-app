//
//  StudentHierarchyCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class StudentHierarchyCell: UITableViewCell {
    var studentHierarchy:StudentHierarchyCodable? {
        didSet {
            nameLabel.text = studentHierarchy?.name ?? "--"
            postLabel.text = studentHierarchy?.post ?? "--"
//            print(studentHierarchy)
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImage.layer.cornerRadius = profileImage.frame.width/2
        self.layoutIfNeeded()
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private let profileImage:UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "userIcon")
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 3
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.lightGray.cgColor
//        imageView.layer.opacity = 0.5
//        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    let verticalLine = VerticalLine(color: UIColor.darkGray)
    let horizontalLine = HorizontalLine(color: UIColor.darkGray)
    private let nameLabel:UILabel = {
        let label = UILabel()
        label.text = "Anmol Rajpal"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }()
    private let postLabel:UILabel = {
        let label = UILabel()
        label.text = "Vice Chancellor"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
        label.textColor = .gray
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }()
    fileprivate func setUpViews() {
//        contentView.backgroundColor = .red
        contentView.addSubview(profileImage)
        contentView.addSubview(nameLabel)
        contentView.addSubview(postLabel)
//        contentView.addSubview(horizontalLine)
        contentView.addSubview(verticalLine)
    }
    fileprivate func setUpConstraints() {
        _ = verticalLine.anchor(contentView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 2.5, heightConstant: 50)
        verticalLine.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        profileImage.topAnchor.constraint(equalTo: verticalLine.bottomAnchor, constant: 0).isActive = true
        profileImage.widthAnchor.constraint(equalToConstant: self.contentView.frame.width / 2.5).isActive = true
        profileImage.heightAnchor.constraint(equalToConstant: self.contentView.frame.width / 2.5).isActive = true
        profileImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        _ = nameLabel.anchorWithoutWH(top: profileImage.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
        _ = postLabel.anchorWithoutWH(top: nameLabel.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 15, rightConstant: 15)
//        _ = horizontalLine.anchor(postLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0.5)
//        _ = verticalLine.anchor(horizontalLine.bottomAnchor, left: nil, bottom: contentView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 2, heightConstant: 50)
//        verticalLine.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    }
}
