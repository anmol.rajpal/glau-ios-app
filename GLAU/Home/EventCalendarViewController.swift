//
//  EventCalendarViewController.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class EventCalendarViewController: UIViewController {
    var eventCalendar = [EventCalendarCodable]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        setUpConstraints()
        setupTableView()
        fetchEventCalendar()
    }
    private func setupTableView() {
        tableView.register(EventCalendarCell.self, forCellReuseIdentifier: NSStringFromClass(EventCalendarCell.self))
        tableView.delegate = self
        tableView.dataSource = self
    }
    private let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let view = UIVisualEffectView(effect: blurEffect)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    private let cancelButton:UIButton = {
        let origImage = UIImage(named: "cancel")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setBackgroundImage(tintedImage, for: UIControl.State.normal)
        button.tintColor = .systemPinkColor
        button.addTarget(self, action: #selector(handleCancelEvent), for: .touchUpInside)
        return button
    }()
    @objc func handleCancelEvent() {
        self.dismiss(animated: true, completion: nil)
    }
    private let tableView : UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .clear
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = true
        tv.isHidden = true
        tv.separatorStyle = .none
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.estimatedRowHeight = 100.0
        tv.rowHeight = UITableView.automaticDimension
        return tv
    }()
    private let infoView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.layer.cornerRadius = 15
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.isHidden = true
        return view
    }()
    private lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .white
        return indicator
    }()
    private let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    fileprivate func setupMessageLabel(message:String = "Cannot Connect to GLAU") {
        //        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    fileprivate func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
        fetchEventCalendar()
    }
    private let headingLabel:UILabel = {
        let label = UILabel()
        label.text = "Calendar"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.largeTitle).bold()
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 1
        label.textAlignment = .left
        //        label.textColor = .black
        label.sizeToFit()
        return label
    }()
    fileprivate func animateIn(cell: UITableViewCell, withDelay delay: TimeInterval) {
        let duration: TimeInterval = 0.3
        
        cell.alpha = 0.5
        cell.layer.transform = CATransform3DMakeScale(0.7, 0.0, 1)
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
    fileprivate func showTableView(tableView:UITableView) {
        //        self.tableView.alpha = 0.0
        //        let initialScale:CGFloat = 0.0
        //        self.tableView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.tableView.isHidden = false
            
            tableView.layer.transform = CATransform3DIdentity
            tableView.alpha = 1.0
            
        }, completion: nil)
    }
    fileprivate func hideTableView(tableView:UITableView) {
        //        self.infoView.alpha = 1.0
        //        let initialScale:CGFloat = 1.0
        //        self.infoView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            //            tableView.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1.0)
            tableView.alpha = 0.0
        }, completion: nil)
    }
    fileprivate func showInfoView(message:String = "No Data", isRetryButtonHidden:Bool = true) {
        self.messageLabel.text = message
        self.retryButton.isHidden = isRetryButtonHidden
        //        self.infoView.alpha = 0.0
        //        let initialScale:CGFloat = 0.0
        //        self.infoView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.infoView.isHidden = false
            self.infoView.alpha = 1.0
            self.infoView.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
    
    fileprivate func hideInfoView() {
        //        self.infoView.alpha = 1.0
        //        let initialScale:CGFloat = 1.0
        //        self.infoView.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            self.infoView.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1)
            self.infoView.alpha = 0.0
        }, completion: nil)
    }
    //    fileprivate func hideInfoView() {
    //        UIView.animate(withDuration: 0.2, delay: 0.1, options: UIView.AnimationOptions.curveLinear, animations: {
    //            self.infoView.alpha = 0.0
    //            self.infoView.layer.transform = CATransform3DMakeScale(1.0, 0.1, 0.0)
    //            self.infoView.isHidden = true
    //        }, completion: nil)
    //    }
    
    private func fetchEventCalendar() {
        spinner.startAnimating()
        hideTableView(tableView: tableView)
        hideInfoView()
        EventCalendarService.shared.fetch(date: "") { (data, error) in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                guard error == nil else {
                    self.eventCalendar.removeAll()
                    self.hideTableView(tableView: self.tableView)
                    self.showInfoView(message: "Cannot Load TimeTable", isRetryButtonHidden: false)
                    self.spinner.stopAnimating()
                    print(error?.localizedDescription ?? "Error")
                    return
                }
                let result = data?.count != 0 ? "Success" : "Failure"
                _ = "No Data"
                
                switch result {
                case ResultType.Failure.rawValue:
                    //                    print("Failure with message - \(message)")
                    self.eventCalendar.removeAll()
                    self.hideTableView(tableView: self.tableView)
                    self.showInfoView()
                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.eventCalendar = data
                        self.tableView.reloadDataWithLayout()
                    }
                    self.showTableView(tableView: self.tableView)
                    self.spinner.stopAnimating()
                default: break
                }
            })
        }
    }
    private func setUpViews() {
        view.addSubview(blurredEffectView)
        blurredEffectView.frame = self.view.bounds
        view.addSubview(headingLabel)
        view.addSubview(cancelButton)
        view.addSubview(tableView)
        view.addSubview(infoView)
        view.addSubview(spinner)
        infoView.addSubview(messageLabel)
        infoView.addSubview(retryButton)
        setupMessageLabel()
        setupActivityIndicatorView()
    }
    private func setUpConstraints() {
        _ = headingLabel.anchorWithoutWH(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: cancelButton.leftAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 10)
        _ = cancelButton.anchor(nil, left: nil, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: headingLabel.frame.height, heightConstant: headingLabel.frame.height)
        cancelButton.centerYAnchor.constraint(equalTo: headingLabel.centerYAnchor).isActive = true
        _ = tableView.anchorWithoutWH(top: headingLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        infoView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 15).isActive = true
        infoView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 15).isActive = true
        //        infoView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        //        infoView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        _ = messageLabel.anchorWithoutWH(top: infoView.topAnchor, left: infoView.leftAnchor, bottom: nil, right: infoView.rightAnchor, topConstant: 50, leftConstant: 20, bottomConstant: 0, rightConstant: 20)
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 5).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: infoView.centerXAnchor).isActive = true
        retryButton.bottomAnchor.constraint(equalTo: infoView.bottomAnchor, constant: 50).isActive = true
    }
}
extension EventCalendarViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventCalendar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(EventCalendarCell.self), for: indexPath) as! EventCalendarCell
        //        animateIn(cell: cell, withDelay: 0.0)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        let eventDetails = eventCalendar[indexPath.row]
        cell.eventDetails = eventDetails
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
