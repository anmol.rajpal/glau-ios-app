//
//  TodayLecturesTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class TodayLecturesTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func animateHeight() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1.5, animations: {
                let h = self.collectionView.heightAnchor.constraint(equalToConstant: TodayLecturesTableViewCell.itemHeight + 10)
                h.priority = UILayoutPriority(999)
                h.isActive = true
            })
        }
    }
    var isLoaded:Bool = false
    func enableConstraint(constant:CGFloat = 0, _ bool:Bool) {
        let h = self.collectionView.heightAnchor.constraint(equalToConstant: constant)
        h.priority = UILayoutPriority(999)
        h.isActive = bool
    }
    var timeTable:[TimeTableCodable]? {
        didSet {
            
            self.enableConstraint(constant: TodayLecturesTableViewCell.itemHeight + 10, true)
            collectionView.reloadData()
//            self.collectionView.frame.size.height = 0
//            UIView.animate(withDuration: 0.2) {
//                self.collectionView.frame.size.height = TodayLecturesTableViewCell.itemHeight + 10
//                self.enableConstraint(constant: TodayLecturesTableViewCell.itemHeight + 10, true)
//            }
//            self.isLoaded = true
//            collectionView.reloadData()
//            self.layoutIfNeeded()
//           animateHeight()
//            let h = self.collectionView.heightAnchor.constraint(equalToConstant: TodayLecturesTableViewCell.itemHeight + 10)
//            h.priority = UILayoutPriority(999)
//            h.isActive = true
        }
    }
    var isTimeTableEmpty:Bool?
    var isErrorLoadingTimeTable:Bool?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
        setUpCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateViews() {
        let isEmpty = !(self.timeTable?.count ?? 0 > 0)
        collectionView.isHidden = isEmpty
        messageLabel.isHidden = !isEmpty
//        retryButton.isHidden = !isEmpty
        if !isEmpty {
            collectionView.reloadData()
            spinner.stopAnimating()
        }
    }
    func setUpCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(TodayLectureCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(TodayLectureCollectionViewCell.self))
//        collectionView.reloadData()
    }
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        //        layout.itemSize = UICollectionViewFlowLayout.automaticSize
        //        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.contentInsetAdjustmentBehavior = .always
        cv.clipsToBounds = true
        cv.isPagingEnabled = false
        cv.showsHorizontalScrollIndicator = true
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.isHidden = false
        return cv
    }()
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.backgroundColor = .black
        indicator.center = contentView.center
        indicator.startAnimating()
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .gray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    let headerLabel:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Lectures"
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).bold()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let separatorLine = HorizontalLine(color: UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0))
    fileprivate func animateIn(cell: UICollectionViewCell, withDuration duration:TimeInterval, withDelay delay: TimeInterval) {
        let initialScale: CGFloat = 0.0
        cell.alpha = 0.0
        cell.layer.transform = CATransform3DMakeScale(initialScale, initialScale, 1)
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
        }, completion: nil)
    }
    private func setupMessageLabel(message:String = "No Data Available") {
        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    private func setupActivityIndicatorView() {
//        spinner.center = contentView.center
        spinner.startAnimating()
    }
    @objc private func refreshData(_ sender: Any) {
//        fetchAttendanceDetails()
    }
    func setUpViews() {
        contentView.addSubview(separatorLine)
        contentView.addSubview(headerLabel)
        contentView.addSubview(collectionView)
        
    }
    func setUpConstraints() {
//        spinner.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
//        spinner.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        let leftRightMargin:CGFloat = 15
        _ = separatorLine.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        _ = headerLabel.anchorWithoutWH(top: separatorLine.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = collectionView.anchorWithoutWH(top: headerLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        let b = self.collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        b.priority = UILayoutPriority(750)
        b.isActive = true
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset:CGFloat = 15
        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeTable?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(TodayLectureCollectionViewCell.self), for: indexPath) as! TodayLectureCollectionViewCell
        let timeTableData = self.timeTable?[indexPath.row]
//        cell.setUpCellData(timeTable: timeTableData)
        cell.timeTable = timeTableData
        animateIn(cell: cell, withDuration: 0.4, withDelay: 0.1)
//        self.updateViews()
        
        //        collectionView.heightAnchor.constraint(equalToConstant: cell.contentView.frame.height).isActive = true
        //        collectionView.reloadData()
        return cell
    }
    static let itemHeight:CGFloat = 135
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let inset:CGFloat = 20
//        let itemWidth = (self.collectionView.frame.width - inset * 4) / 3
        let itemWidth:CGFloat = 110
        return CGSize(width: itemWidth, height: TodayLecturesTableViewCell.itemHeight)
    }
}
