//
//  TodayArticleTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
class TodayArticleTableViewCell: UITableViewCell {
    var articleDetails:CombinedServicesCodable.ArticlesService.ArticleOfTheDay? {
        didSet {
            articleTitleLabel.text = articleDetails?.title ?? "--"
            articleAuthorLabel.text = "- \(articleDetails?.author ?? "--")"
            articleContentLabel.text = articleDetails?.content ?? "--"
//            setUpContentField(articleContent: articleDetails?.content ?? "--")
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setUpContentField(articleContent:String) {
        let articleContentLabelTextAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
        ]
        let appendTextAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium),
            NSAttributedString.Key.foregroundColor : UIColor.lightGray
        ]
        let appendTextAttributedString = NSMutableAttributedString(string: " Tap to read more", attributes: appendTextAttributes)
        let articleLabelTextAttributedString = NSAttributedString(string: articleContent, attributes: articleContentLabelTextAttributes)
        
        let articleString = NSMutableAttributedString()
        articleString.append(articleLabelTextAttributedString)
        articleString.append(appendTextAttributedString)
        articleContentLabel.attributedText = articleString
    }
    let appendLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Tap to read more"
        label.textColor = .darkGray
        label.numberOfLines = 1
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        label.adjustsFontForContentSizeCategory = true
        label.textAlignment = .left
        label.sizeToFit()
        return label
    }()
    let articleTitleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3).condensed()
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.textColor = .gray
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    let articleContentLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()
    let articleAuthorLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1).bold()
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.numberOfLines = 1
        label.textAlignment = .right
        return label
    }()
    let headerLabel:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Article of the Day"
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).bold()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let separatorLine = HorizontalLine(color: UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0))
    let sourceLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    let publisherLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(separatorLine)
        contentView.addSubview(headerLabel)
        contentView.addSubview(articleTitleLabel)
        contentView.addSubview(articleContentLabel)
        contentView.addSubview(appendLabel)
        contentView.addSubview(articleAuthorLabel)
    }
    func setUpConstraints() {
        let leftRightMargin:CGFloat = 15
        _ = separatorLine.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        _ = headerLabel.anchorWithoutWH(top: separatorLine.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = articleTitleLabel.anchorWithoutWH(top: headerLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = articleContentLabel.anchorWithoutWH(top: articleTitleLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = appendLabel.anchorWithoutWH(top: articleContentLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = articleAuthorLabel.anchorWithoutWH(top: appendLabel.bottomAnchor, left: nil, bottom: nil, right: contentView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 5, rightConstant: leftRightMargin)
        let b = articleAuthorLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5)
        b.priority = UILayoutPriority(750)
        b.isActive = true
    }
}
