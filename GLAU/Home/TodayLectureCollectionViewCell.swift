//
//  TodayLectureCollectionViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 01/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class TodayLectureCollectionViewCell: UICollectionViewCell {
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setUpViews()
//        setUpConstraints()
//    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var timeTable:TimeTableCodable? {
        didSet {
            let details = timeTable?.details
            let components = details?.components(separatedBy: "/")
            let subjectCode = components?[0]
            let subjectType = components?[1]
            let blockNo = components?[2] ?? "--"
            let roomNo = components?[3] ?? "--"
            let timing = timeTable?.timing
            let timingComponents = timing?.components(separatedBy: "-")
            let startTime = timingComponents?[0] ?? "--"
            subjectTypeLabel.text = subjectType?.uppercased()
            subjectCodeLabel.text = subjectCode
            startTimeLabel.text = startTime
            locationLabel.text = "\(roomNo)\nin \(blockNo)"
        }
    }
    func setUpCellData(timeTable:TimeTableCodable?) {
        let details = timeTable?.details
        let components = details?.components(separatedBy: "/")
        let subjectCode = components?[0]
        let subjectType = components?[1]
        let blockNo = components?[2] ?? "--"
        let roomNo = components?[3] ?? "--"
        let timing = timeTable?.timing
        let timingComponents = timing?.components(separatedBy: "-")
        let startTime = timingComponents?[0] ?? "--"
//        let endTime = timingComponents?[1] ?? "--"
//        print("Start Time - \(startTime) \n End Time - \(endTime)")
        subjectTypeLabel.text = subjectType?.uppercased()
        subjectCodeLabel.text = subjectCode
        startTimeLabel.text = startTime
        locationLabel.text = "\(roomNo)\nin \(blockNo)"
    }
    let subjectTypeLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.light)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let horizontalSeparator = HorizontalLine(color: UIColor.white)
    let subjectCodeLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
        label.textColor = .white
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontForContentSizeCategory = true
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        label.textAlignment = .center
        return label
    }()
    let startTimeLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        return label
    }()
    let endTimeLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .heavy)
        return label
    }()
    let locationLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.lineBreakMode = .byTruncatingTail
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.light)
        return label
    }()
    func setUpViews() {
        contentView.backgroundColor = UIColor.systemTealColor
        contentView.layer.cornerRadius = 10
        contentView.addSubview(subjectTypeLabel)
        contentView.addSubview(horizontalSeparator)
        contentView.addSubview(subjectCodeLabel)
        contentView.addSubview(startTimeLabel)
//        contentView.addSubview(endTimeLabel)
        contentView.addSubview(locationLabel)
    }
    func setUpConstraints() {
        _ = subjectTypeLabel.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 2, leftConstant: 5, bottomConstant: 0, rightConstant: 5)
        _ = horizontalSeparator.anchor(subjectTypeLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 2, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        _ = subjectCodeLabel.anchorWithoutWH(top: horizontalSeparator.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 5, bottomConstant: 0, rightConstant: 5)
        _ = startTimeLabel.anchorWithoutWH(top: subjectCodeLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        _ = locationLabel.anchorWithoutWH(top: nil, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5)
    }
}
