//
//  NewsCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class NewsCell: UICollectionViewCell {
    static let viewHeight:CGFloat = 95
    var newsDetails:NewsServiceCodable.NewsOfTheDay? {
        didSet {
            newsLabel.text = newsDetails?.title ?? "--"
            sourceLabel.text = newsDetails?.source ?? "--"
            timeLabel.text = newsDetails?.publishingTime ?? "--"
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
        setUpConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let newsLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont(name: CustomFonts.FuturaCondensedMedium.rawValue, size: 22)
        label.adjustsFontForContentSizeCategory = true
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    let sourceLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let timeLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    func setUpViews() {
        contentView.layer.cornerRadius = 10
        contentView.backgroundColor = UIColor.systemPinkColor
        contentView.addSubview(newsLabel)
        contentView.addSubview(sourceLabel)
        contentView.addSubview(timeLabel)
    }
    func setUpConstraints() {
        _ = newsLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        _ = sourceLabel.anchor(nil, left: nil, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = timeLabel.anchorWithoutWH(top: nil, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 10, rightConstant: 0)
    }
}
