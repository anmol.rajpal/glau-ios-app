//
//  EventCalendarCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 18/02/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class EventCalendarCell: UITableViewCell {
    var eventDetails:EventCalendarCodable? {
        willSet {
            contentView.backgroundColor = .clear
        }
        didSet {
            setupCellData(details: eventDetails)
            self.layoutIfNeeded()
        }
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        blurredEffectView.frame = containerView.bounds
//        self.layoutIfNeeded()
//    }
    func setupCellData(details:EventCalendarCodable?) {
        eventTypeLabel.text = details?.eventType ?? "--"
        captionLabel.text = details?.caption ?? "--"
        descriptionLabel.text = details?.desc ?? "--"
        startTimeLabel.text = "Start - \(details?.start ?? "--")"
        endTimeLabel.text = "End - \(details?.end ?? "--")"
        venueLabel.text = details?.venue ?? "--"
        departmentLabel.text = details?.department ?? "--"
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        view.layer.cornerRadius = 15;
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.gray.cgColor
        return view
    }()
    private let blurredEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let view = UIVisualEffectView(effect: blurEffect)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 15
        return view
    }()
    static let color:UIColor = .black
    let verticalLine = VerticalLine(color: UIColor.darkGray)
    let horizontalLine = HorizontalLine(color: UIColor.darkGray)
    private let captionLabel:UILabel = {
        let label = UILabel()
        label.text = "Event Caption"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1).condensed()
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = EventCalendarCell.color
        return label
    }()
    private let eventTypeLabel:UILabel = {
        let label = UILabel()
        label.text = "Event Type"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 1
        label.textAlignment = .right
        label.sizeToFit()
        label.textColor = EventCalendarCell.color
        return label
    }()
    private let descriptionLabel:UILabel = {
        let label = UILabel()
        label.text = "Event Description"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = EventCalendarCell.color
        return label
    }()
    private let departmentLabel:UILabel = {
        let label = UILabel()
        label.text = "Department"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.footnote)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = EventCalendarCell.color
        return label
    }()
    private let venueLabel:UILabel = {
        let label = UILabel()
        label.text = "Event Venue"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = EventCalendarCell.color
        return label
    }()
    private let startTimeLabel:UILabel = {
        let label = UILabel()
        label.text = "March 30, 2019"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 1
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = EventCalendarCell.color
        return label
    }()
    private let endTimeLabel:UILabel = {
        let label = UILabel()
        label.text = "March 31, 2019"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 1
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = EventCalendarCell.color
        return label
    }()
    
    fileprivate func setUpViews() {
        //        contentView.backgroundColor = .red
//        blurredEffectView.frame = self.containerView.bounds
        containerView.addSubview(blurredEffectView)
        containerView.addSubview(eventTypeLabel)
        containerView.addSubview(captionLabel)
        containerView.addSubview(descriptionLabel)
        containerView.addSubview(startTimeLabel)
        containerView.addSubview(endTimeLabel)
        containerView.addSubview(venueLabel)
        containerView.addSubview(departmentLabel)
        contentView.addSubview(containerView)
    }
    fileprivate func setUpConstraints() {
        let margin:CGFloat = 10
        _ = containerView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 7.5, leftConstant: 15, bottomConstant: 7.5, rightConstant: 15)
        _ = blurredEffectView.anchorWithoutWH(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        _ = eventTypeLabel.anchorWithoutWH(top: containerView.topAnchor, left: nil, bottom: nil, right: containerView.rightAnchor, topConstant: margin, leftConstant: 0, bottomConstant: 0, rightConstant: margin)
        _ = captionLabel.anchorWithoutWH(top: eventTypeLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: margin, leftConstant: margin, bottomConstant: 0, rightConstant: margin)
        _ = descriptionLabel.anchorWithoutWH(top: captionLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: margin, leftConstant: margin, bottomConstant: 0, rightConstant: margin)
        _ = startTimeLabel.anchorWithoutWH(top: descriptionLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: margin, leftConstant: margin, bottomConstant: 0, rightConstant: margin)
        _ = endTimeLabel.anchorWithoutWH(top: startTimeLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: margin, leftConstant: margin, bottomConstant: 0, rightConstant: margin)
        _ = venueLabel.anchorWithoutWH(top: endTimeLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: margin, leftConstant: margin, bottomConstant: 0, rightConstant: margin)
        _ = departmentLabel.anchorWithoutWH(top: venueLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: margin, leftConstant: margin, bottomConstant: 0, rightConstant: margin)
        let bottomConstraint = departmentLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -margin)
        bottomConstraint.priority = UILayoutPriority(999)
        bottomConstraint.isActive = true
//        _ = postLabel.anchorWithoutWH(top: nameLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 15, rightConstant: 15)
//        _ = horizontalLine.anchor(postLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0.5)
//        _ = verticalLine.anchor(horizontalLine.bottomAnchor, left: nil, bottom: contentView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 2, heightConstant: 50)
//        verticalLine.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    }
}
