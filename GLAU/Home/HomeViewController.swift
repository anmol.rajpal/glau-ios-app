//
//  HomeViewController.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
extension UIRefreshControl {
    func programaticallyBeginRefreshing(in tableView: UITableView) {
        beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -frame.size.height)
        tableView.setContentOffset(offsetPoint, animated: true)
    }
}
class HomeViewController: UIViewController {
    var newsOfTheDay:[NewsServiceCodable.NewsOfTheDay]?
    var timeTable:[TimeTableCodable]? = []
    var news:NewsServiceCodable?
    var combinedServices:CombinedServicesCodable?
    var errorLoadingTimeTable:Bool?
    var isTimeTableEmpty:Bool?
    var isTimeTableLoaded:Bool?
    var isArticlesServiceEnabled:Bool = false
    var isQuotesServiceEnabled:Bool = false
    var isFactsServiceEnabled:Bool = false
    var isRiddlesServiceEnabled:Bool = false
    var isTipsServiceEnabled:Bool = false
    var isNewsServiceEnabled:Bool = false
    var newsIndexPath:IndexPath?
    var needsScroll = false
    override func viewDidLoad() {
        super.viewDidLoad()
        checkReachability()
        setupNavBar()
        setUpViews()
        setUpConstraints()
        setUpTableView()
        AppUpdater.shared.showUpdate(withConfirmation: true)
        fetchAllServices()
        
//        tableView.reloadDataWithLayout()
    }
    private func fetchAllServices(isDragging:Bool = false) {
        if !isDragging {
            spinner.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                UIView.animate(withDuration: 0.3, animations: {
                    self.tableView.alpha = 1
                    self.spinner.stopAnimating()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                })
            }
        }
//        isDragging ? spinner.stopAnimating() : spinner.startAnimating()
        fetchTodayTimeTable()
        fetchNewsOfTheDay()
        fetchCombinedServicesOfTheDay()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkReachability()
        setupNavBar()
        self.navigationItem.title = "Home"
//        fetchTodayTimeTable()
//        fetchNewsOfTheDay()
//        fetchCombinedServicesOfTheDay()
//        fetchNewsOfTheDay()
//        view.layoutIfNeeded()
//        tableView.reloadDataWithLayout()
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        setupNavBar()
//    }
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        setupNavBar()
//    }
    func processNewsNotification(newsId:String) {
        NewsService.shared.fetch { (data, error) in
            DispatchQueue.main.async {
                guard error == nil else {
                    print(error?.localizedDescription ?? "Error")
                    self.newsOfTheDay?.removeAll()
                    self.tableView.isUserInteractionEnabled = true
                    return
                }
                let result = data?.result
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure of news")
                    self.newsOfTheDay?.removeAll()
                    self.tableView.isUserInteractionEnabled = true
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.news = data
                        self.newsOfTheDay = data.newsOfTheDay
                        self.isNewsServiceEnabled = data.serviceDetails?.serviceState == ServiceState.Normal.rawValue
                        
                        self.refreshControl.endRefreshing()
                        self.tableView.isUserInteractionEnabled = true
                        if let newsItems = self.newsOfTheDay {
                            for newsItem in newsItems {
                                if let id = newsItem.newsId {
                                    if id == newsId {
                                        let index = newsItems.firstIndex(where: { (item) -> Bool in
                                            item.newsId == id
                                        })
                                        let ip = IndexPath(item: index!, section: 0)
                                        self.newsIndexPath = ip
                                        self.newz()
                                        break
                                    }
                                }
                            }
                        }
                    }
                default: break
                }
            }
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        fetchNewsOfTheDay()
        
//        loadTableViewWithAnimation()
        if let selectionIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectionIndexPath, animated: animated)
        }
    }
    func loadTableViewWithAnimation() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.tableView.reloadDataWithLayout()
            })
        }
    }
    func reloadRows(row:Int, section:Int) {
        let indexPath = IndexPath(row: row, section: section)
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        self.tableView.endUpdates()
    }
    func setUpTableView() {
        tableView.layoutIfNeeded()
        tableView.refreshControl = self.refreshControl
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .white
        tableView.register(SubjectDetailsTableHeaderView.self, forHeaderFooterViewReuseIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self))
        tableView.register(MenuItemsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(MenuItemsTableViewCell.self))
        tableView.register(GreetingTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(GreetingTableViewCell.self))
        tableView.register(TodayLecturesTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayLecturesTableViewCell.self))
        tableView.register(TodayNewsTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayNewsTableViewCell.self))
        tableView.register(TodayQuoteTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayQuoteTableViewCell.self))
        tableView.register(TodayFactTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayFactTableViewCell.self))
        tableView.register(TodayTipTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayTipTableViewCell.self))
        tableView.register(TodayArticleTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayArticleTableViewCell.self))
        tableView.register(TodayRiddleTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(TodayRiddleTableViewCell.self))
    }
    let tableView : UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .white
        tv.bounces = true
        tv.alwaysBounceVertical = true
        tv.clipsToBounds = true
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = true
        tv.separatorStyle = .none
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        tv.isHidden = false
        tv.tableFooterView = UIView(frame: CGRect.zero)
        tv.alpha = 0
        return tv
    }()
    lazy var spinner:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        indicator.backgroundColor = .black
        return indicator
    }()
    let messageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let retryButton:UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.isHidden = true
        let refreshImage = UIImage(named: "refresh")
        button.imageView?.contentMode = .scaleToFill
        button.setTitle("Tap to Retry", for: UIControl.State.normal)
        button.setImage(refreshImage, for: UIControl.State.normal)
        button.imageEdgeInsets.right = -20
        button.semanticContentAttribute = .forceRightToLeft
        //        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(refreshData(_:)), for: .touchUpInside)
        return button
    }()
    private func setupMessageLabel(message:String = "Cannot Connect to GLAU") {
        messageLabel.isHidden = true
        messageLabel.text = message
    }
    
    private func setupActivityIndicatorView() {
        spinner.startAnimating()
    }
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.glauThemeColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        return refreshControl
    }()
    @objc private func refreshData(_ sender: Any) {
        if !tableView.isDragging {
            fetchAllServices()
            
        }
//        refreshControl.programaticallyBeginRefreshing(in: tableView)
//        fetchAllServices()
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
//            self.refreshControl.endRefreshing()
//        }
//        if refreshControl.isRefreshing == true {
//            let offset = self.tableView.contentOffset
//
////            self.refreshControl.endRefreshing()
//            self.refreshControl.beginRefreshing()
//
//            let dispatchTime:DispatchTime = .now() + 1.5
//            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
//                self.fetchAllServices()
////                self.tableView.contentOffset = offset
//                self.refreshControl.endRefreshing()
//            }
//        }
    }
    
    func getDayTimeText() -> String {
        let morningTexts = ["Good Morning!", "Rise & Shine", "It's a new Day", "Have a great Day", "Morning!"]
        let noonTexts = ["Good Noon!", "Noon!"]
        let afterNoonTexts = ["Good Afternoon!", "Have a wonderful Afternoon", "Hope your afternoon's going well!", "Afternoon!"]
        let eveningTexts = ["Good Evening!", "Hope your evening's going well!", "Have a wonderful Evening"]
        let nightTexts = ["Good Night!", "Sleep Well!"]
        let timeOfDay = getTimeOfDay()
        switch timeOfDay {
            case .Morning: return morningTexts.randomElement()!
            case .Noon: return noonTexts.randomElement()!
            case .Afternoon: return afterNoonTexts.randomElement()!
            case .Evening: return eveningTexts.randomElement()!
            case .Night: return nightTexts.randomElement()!
        }
    }
    func getTodayDateString() -> String {
        return getStringFromDate(date: Date(), dateFormat: NSObject.DateFormatType.MMMMdEEEE).uppercased()
    }
    
    func lectures() {
        let lecturesIndexPath = IndexPath(row: 1, section: 1)
//        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [lecturesIndexPath], with: UITableView.RowAnimation.none)
//        self.refreshControl.endRefreshing()
        self.tableView.layoutIfNeeded()
//        self.tableView.endUpdates()
    }
    func newz() {
        let newsIndexPath = IndexPath(row: 2, section: 1)
//        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [newsIndexPath], with: UITableView.RowAnimation.none)
//        self.refreshControl.endRefreshing()
        self.tableView.layoutIfNeeded()
        
//        self.tableView.endUpdates()
    }
    func combo() {
        let quoteIndexPath = IndexPath(row: 3, section: 1)
        let factIndexPath = IndexPath(row: 4, section: 1)
        let articleIndexPath = IndexPath(row: 5, section: 1)
        let tipIndexPath = IndexPath(row: 6, section: 1)
//        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [quoteIndexPath, factIndexPath, articleIndexPath, tipIndexPath], with: UITableView.RowAnimation.none)
//        self.refreshControl.endRefreshing()
        self.tableView.layoutIfNeeded()
//        self.tableView.endUpdates()
    }
    func fetchTodayTimeTable() {
        let date = Date()
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        if let day = components.day, let month = components.month, let year = components.year {
            let date = "\(day).\(month).\(year)"
            fetchStudentTimeTable(date: date)
        }
    }
    private func reloadNews() {
        fetchNewsOfTheDay()
    }
    private func reloadCombinedServices() {
        fetchCombinedServicesOfTheDay()
    }
    private func reloadTimeTable() {
        fetchTodayTimeTable()
    }
   
//    var newsReloadAttempts = 0
    func fetchNewsOfTheDay() {
        NewsService.shared.fetch { (data, error) in
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
//                self.refreshControl.endRefreshing()
                guard error == nil else {
                    print(error?.localizedDescription ?? "Error")
                    self.newsOfTheDay?.removeAll()
//                    if self.newsReloadAttempts != 10 {
//                        self.newsReloadAttempts = self.newsReloadAttempts + 1
//                        self.reloadNews()
//                    }
                    self.tableView.isUserInteractionEnabled = true
                    return
                }
//                let result = data?.count != 0 ? "Success" : "Failure"
//                _ = "No Data"
//                let dataCount = data?.count ?? -1
                
                let result = data?.result
                switch result {
                case ResultType.Failure.rawValue:
                    print("Failure of news")
                    self.newsOfTheDay?.removeAll()
                    self.tableView.isUserInteractionEnabled = true
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.news = data
                        self.newsOfTheDay = data.newsOfTheDay
                        self.isNewsServiceEnabled = data.serviceDetails?.serviceState == ServiceState.Normal.rawValue
                        self.newz()
                        self.refreshControl.endRefreshing()
                        self.tableView.isUserInteractionEnabled = true
//                        self.reloadRows(row: 0, section: 3)
//                        self.tableView.reloadSections(IndexSet(integer: 1), with: UITableView.RowAnimation.fade)
//                        self.tableView.reloadData()
//                        self.tableView.reloadDataWithLayout()
//                        UIView.animate(withDuration: 0.2, animations: {
//                            self.tableView.reloadDataWithLayout()
//                        })
//                        self.tableView.reloadDataWithLayout()
                    }
                default: break
                }
            })
        }
    }
    private func fetchCombinedServicesOfTheDay() {
        CombinedServices.shared.fetch { (data, error) in
           
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
//                self.refreshControl.endRefreshing()
                guard error == nil else {
                    print(error?.localizedDescription ?? "Error")
                    self.tableView.isUserInteractionEnabled = true
                    return
                }
                //                let result = data?.count != 0 ? "Success" : "Failure"
                //                _ = "No Data"
                //                let dataCount = data?.count ?? -1
                
                let result = data?.result
                switch result {
                case ResultType.Failure.rawValue:
                    self.tableView.isUserInteractionEnabled = true
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.combinedServices = data
                        
                        self.isArticlesServiceEnabled = ((self.combinedServices?.articlesService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isQuotesServiceEnabled = ((self.combinedServices?.quotesService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isTipsServiceEnabled = ((self.combinedServices?.tipsService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isFactsServiceEnabled = ((self.combinedServices?.factsService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.isRiddlesServiceEnabled = ((self.combinedServices?.riddlesService?.serviceDetails?.serviceState) == ServiceState.Normal.rawValue)
                        self.combo()
                        self.refreshControl.endRefreshing()
                        self.tableView.isUserInteractionEnabled = true
                        //                        self.tableView.reloadDataWithLayout()
                        //                        self.tableView.reloadData()
                        //                        self.tableView.reloadSections(IndexSet(integer: 1), with: UITableView.RowAnimation.fade)
                        //                        self.reloadRows(row: 0, section: 1)
                    }
                default: break
                }
            })
        }
    }
    private func fetchStudentTimeTable(date:String) {
        let rollNo = UserDefaults.standard.getRollNo()
//        spinner.startAnimating()
//        hideTableView(tableView: tableView)
//        hideInfoView()
        TimeTableService.shared.fetch(rollNo: rollNo, date: date) { (data, error) in
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5 , execute: {
//                self.refreshControl.endRefreshing()
                guard error == nil else {
                    
//                    self.timeTable.removeAll()
//                    self.hideTableView(tableView: self.tableView)
//                    self.showInfoView(message: "Cannot Load TimeTable", isRetryButtonHidden: false)
//                    self.spinner.stopAnimating()
                    self.timeTable?.removeAll()
                    self.errorLoadingTimeTable = true
                    print(error?.localizedDescription ?? "Error")
                    self.tableView.isUserInteractionEnabled = true
                    return
                }
                let result = data?.count != 0 ? "Success" : "Failure"
                _ = "No Data"
                
                switch result {
                case ResultType.Failure.rawValue:
                    self.timeTable?.removeAll()
                    self.isTimeTableEmpty = true
                    self.tableView.isUserInteractionEnabled = true
//                    self.hideTableView(tableView: self.tableView)
//                    self.showInfoView()
//                    self.spinner.stopAnimating()
                case ResultType.Success.rawValue:
                    if let data = data {
                        self.timeTable = data
                        self.isTimeTableLoaded = true
                        self.isTimeTableEmpty = false
                        self.lectures()
                        self.refreshControl.endRefreshing()
                        self.tableView.isUserInteractionEnabled = true
//                        self.reloadRows(row: 0, section: 2)
//                        self.tableView.reloadSections(IndexSet(integer: 1), with: UITableView.RowAnimation.fade)
//                        self.tableView.reloadData()
//                        print(self.timeTable)
//                    self.tableView.reloadDataWithLayout()
                    }
                    
//                    self.showTableView(tableView: self.tableView)
//                    self.tableView.reloadAndScrollToTop()
                    //                    self.tableView.scrollToTop()
//                    self.spinner.stopAnimating()
                default: break
                }
            })
        }
    }
    func setUpViews() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        view.addSubview(spinner)
        view.addSubview(messageLabel)
        view.addSubview(retryButton)
//        setupMessageLabel()
//        setupActivityIndicatorView()
    }
    func setUpConstraints() {
        _ = tableView.anchorWithoutWH(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        retryButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    var todayDateString:String {
        get {
            return self.getTodayDateString()
        }
    }
    var dayTimeText:String {
        get {
            return getDayTimeText()
        }
    }
}
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if refreshControl.isRefreshing == true {
//            UIView.animate(withDuration: 0.4) {
//                self.tableView.alpha = 0
//            }
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {
//                self.fetchAllServices()
//            }
//            let dispatchTime:DispatchTime = .now() + 1.5
//            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
//                self.navigationController?.navigationBar.isTranslucent = true
//                self.navigationController?.navigationBar.isTranslucent = false
//                UIView.animate(withDuration: 0.4) {
//                    self.tableView.alpha = 1
//                }
//                self.refreshControl.endRefreshing()
//
//            }
//        }
//    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if refreshControl.isRefreshing == true {
            tableView.isUserInteractionEnabled = false
            let dispatchTime:DispatchTime = .now()
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                self.fetchAllServices(isDragging: true)
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 7
        }
        return 1
    }
    func shit(cell:UITableViewCell) {
        cell.alpha = 1
        
        let anchor = cell.heightAnchor.constraint(equalToConstant: 0)
        anchor.priority = UILayoutPriority(999)
        anchor.isActive = true
//        cell.frame = CGRect.zero
//        cell.layoutIfNeeded()
//        cell.layer.transform = CATransform3DMakeScale(1.0, 0.01, 1.0)
        UIView.animate(withDuration: 2.0, delay: 1.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            cell.alpha = 1
//            cell.layer.transform = CATransform3DIdentity
//            anchor.constant = 100
            cell.layoutIfNeeded()
        }, completion: nil)
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        DispatchQueue.main.async {
//            UIView.animate(withDuration: 1.0) {
//                self.tableView.beginUpdates()
//                self.tableView.endUpdates()
//            }
//        }
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(MenuItemsTableViewCell.self), for: indexPath) as! MenuItemsTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        } else {
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(GreetingTableViewCell.self), for: indexPath) as! GreetingTableViewCell
                cell.selectionStyle = .none
                cell.dateLabel.text = self.todayDateString
                cell.greetingLabel.text = self.dayTimeText
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayLecturesTableViewCell.self), for: indexPath) as! TodayLecturesTableViewCell
                cell.selectionStyle = .none
                if !(isTimeTableEmpty ?? true) {
                    cell.timeTable = self.timeTable
                    cell.isErrorLoadingTimeTable = self.errorLoadingTimeTable
                    cell.isTimeTableEmpty = self.isTimeTableEmpty
//                    cell.layoutIfNeeded()
                } else {
                    cell.isHidden = true
                }
                
                
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayNewsTableViewCell.self), for: indexPath) as! TodayNewsTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                
                if ((newsOfTheDay?.isEmpty ?? true) || !isNewsServiceEnabled) {
                    cell.isHidden = true
                    
//                    cell.layoutIfNeeded()
                } else {
                    cell.newsDetails = self.newsOfTheDay
                    if let ip = self.newsIndexPath {
                        cell.handleNews(indexPath: ip)
                        self.newsIndexPath = nil
                    }
                }
                return cell
            case 5:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayQuoteTableViewCell.self), for: indexPath) as! TodayQuoteTableViewCell
                cell.selectionStyle = .none
                if isQuotesServiceEnabled {
                    cell.quoteDetails = self.combinedServices?.quotesService?.quoteOfTheDay
                } else {
//                    cell.frame.size.height = 0
//                    cell.contentView.frame.size.height = 0
//                    cell.heightAnchor.constraint(equalToConstant: 0).isActive = true
                    cell.isHidden = true
                    
                }
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayFactTableViewCell.self), for: indexPath) as! TodayFactTableViewCell
                cell.selectionStyle = .none
                if isFactsServiceEnabled {
                    cell.factDetails = self.combinedServices?.factsService?.factOfTheDay
                } else {
                    cell.isHidden = true
                }
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayArticleTableViewCell.self), for: indexPath) as! TodayArticleTableViewCell
                if isArticlesServiceEnabled {
                    cell.articleDetails = self.combinedServices?.articlesService?.articleOfTheDay
                } else {
                    cell.isHidden = true
                }
                return cell
            case 6:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayTipTableViewCell.self), for: indexPath) as! TodayTipTableViewCell
                cell.selectionStyle = .none
                if isTipsServiceEnabled {
                    cell.tipDetails = self.combinedServices?.tipsService?.tipOfTheDay
                } else {
                    cell.isHidden = true
                }
                return cell
            
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayRiddleTableViewCell.self), for: indexPath) as! TodayRiddleTableViewCell
                cell.selectionStyle = .none
                return cell
            }
        }
//        switch indexPath.section {
//        case 0:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(MenuItemsTableViewCell.self), for: indexPath) as! MenuItemsTableViewCell
//            cell.delegate = self
//            cell.selectionStyle = .none
//            return cell
//        case 1:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(GreetingTableViewCell.self), for: indexPath) as! GreetingTableViewCell
//            cell.selectionStyle = .none
//            cell.dateLabel.text = self.todayDateString
//            cell.greetingLabel.text = self.dayTimeText
//            return cell
//        case 2:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayLecturesTableViewCell.self), for: indexPath) as! TodayLecturesTableViewCell
//            cell.selectionStyle = .none
//            cell.timeTable = self.timeTable
//            cell.isErrorLoadingTimeTable = self.errorLoadingTimeTable
//            cell.isTimeTableEmpty = self.isTimeTableEmpty
//
//
//            return cell
//        case 3:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayNewsTableViewCell.self), for: indexPath) as! TodayNewsTableViewCell
//            cell.selectionStyle = .none
//            cell.delegate = self
//            cell.newsDetails = self.news?.newsOfTheDay
//            return cell
//        case 4:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayQuoteTableViewCell.self), for: indexPath) as! TodayQuoteTableViewCell
//            cell.selectionStyle = .none
//            if isQuotesServiceEnabled {
//                cell.quoteDetails = self.combinedServices?.quotesService?.quoteOfTheDay
//            }
//            return cell
//        case 5:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayFactTableViewCell.self), for: indexPath) as! TodayFactTableViewCell
//            cell.selectionStyle = .none
//            if isFactsServiceEnabled {
//                cell.factDetails = self.combinedServices?.factsService?.factOfTheDay
//            }
//            return cell
//        case 7:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayTipTableViewCell.self), for: indexPath) as! TodayTipTableViewCell
//            cell.selectionStyle = .none
//            if isTipsServiceEnabled {
//                cell.tipDetails = self.combinedServices?.tipsService?.tipOfTheDay
//            }
//            return cell
//        case 6:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayArticleTableViewCell.self), for: indexPath) as! TodayArticleTableViewCell
//            if isArticlesServiceEnabled {
//                cell.articleDetails = self.combinedServices?.articlesService?.articleOfTheDay
//            }
//            return cell
//        default:
//            let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(TodayRiddleTableViewCell.self), for: indexPath) as! TodayRiddleTableViewCell
//            cell.selectionStyle = .none
//            return cell
//        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 3 {
                let articleDetailsVC = ArticleDetailsViewController()
                articleDetailsVC.articleDetails = self.combinedServices?.articlesService?.articleOfTheDay
                articleDetailsVC.view.backgroundColor = .white
                self.navigationController?.pushViewController(articleDetailsVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(SubjectDetailsTableHeaderView.self)) as! SubjectDetailsTableHeaderView
        if section == 1 {
            headerView.headerLabel.text = "Today"
        } else {
            headerView.headerLabel.text = ""
        }
//        switch section {
//            case 1: headerView.headerLabel.text = "Today"
////            case 2: headerView.headerLabel.text = "Lectures"
////            case 3: headerView.headerLabel.text = "News"
////            case 4: headerView.headerLabel.text = "Quote of the Day"
////            case 5: headerView.headerLabel.text = "Fact of the Day"
////            case 7: headerView.headerLabel.text = "Tip"
////            case 6: headerView.headerLabel.text = "Article of the Day"
////            case 8: headerView.headerLabel.text = "Riddle"
//        default: headerView.headerLabel.text = ""
//        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let defaultHeight:CGFloat = 35
        if section == 0 {
            return 0
        } else {
            return defaultHeight
        }
//        switch section {
//        case 0: return 0
////        case 2: if self.timeTable.isEmpty { return 0 } else { return defaultHeight }
////        case 3: if self.news?.newsOfTheDay?.isEmpty ?? true { return 0 } else { return defaultHeight }
////        case 4: if !isQuotesServiceEnabled { return 0 } else { return defaultHeight }
////        case 5: if !isFactsServiceEnabled { return 0 } else { return defaultHeight }
////        case 6: if !isArticlesServiceEnabled { return 0 } else { return defaultHeight }
////        case 7: if !isTipsServiceEnabled { return 0 } else { return defaultHeight }
//        default: return defaultHeight
//        }
//        return defaultHeight
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            let numOfItemsInRow = 3
            let itemsCount = 3
            let itemHeight = MenuItemsTableViewCell.itemHeight
            let ceil = itemsCount / numOfItemsInRow
            if itemsCount % numOfItemsInRow == 0 {
                return CGFloat(ceil) * (itemHeight + 5)
            } else {
                return CGFloat(ceil + 1) * (itemHeight + 5)
            }
        } else {
//            if indexPath.row == 3 {
//                if !isQuotesServiceEnabled { return 0 } else {
//                    return UITableView.automaticDimension
//                }
//            } else {
//                return UITableView.automaticDimension
//            }
            switch indexPath.row {
                case 1:
                    if self.timeTable?.isEmpty ?? true { return 0 } else {
                        return UITableView.automaticDimension
                    }
                case 2:
                    if (self.newsOfTheDay?.isEmpty ?? true) || !isNewsServiceEnabled { return 0 } else {
                        return UITableView.automaticDimension
                    }
                case 3:
                    if !isQuotesServiceEnabled { return 0 } else {
                        return UITableView.automaticDimension
                    }
                case 4:
                    if !isFactsServiceEnabled { return 0 } else {
                        return UITableView.automaticDimension
                    }
                case 5:
                    if !isArticlesServiceEnabled { return 0 } else {
                        return UITableView.automaticDimension
                    }
                case 6:
                    if !isTipsServiceEnabled { return 0 } else {
                        return UITableView.automaticDimension
                    }
                default: return UITableView.automaticDimension
            }
        }
//        switch indexPath.section {
//        case 0:
//            let numOfItemsInRow = 3
//            let itemsCount = 8
//            let itemHeight = MenuItemsTableViewCell.itemHeight
//            let ceil = itemsCount / numOfItemsInRow
//            if itemsCount % numOfItemsInRow == 0 {
//                return CGFloat(ceil) * (itemHeight + 5)
//            } else {
//                return CGFloat(ceil + 1) * (itemHeight + 5)
//            }
//        case 2:
//            if self.timeTable.isEmpty { return 0 } else {
//                return UITableView.automaticDimension
//            }
//        case 3:
//            if self.news?.newsOfTheDay?.isEmpty ?? true { return 0 } else {
//                return UITableView.automaticDimension
//            }
////        case 4:
////            if !isQuotesServiceEnabled { return 0 } else {
////                return UITableView.automaticDimension
////            }
////        case 5:
////            if !isFactsServiceEnabled { return 0 } else {
////                return UITableView.automaticDimension
////            }
////        case 6:
////            if !isArticlesServiceEnabled { return 0 } else {
////                return UITableView.automaticDimension
////            }
////        case 7:
////            if !isTipsServiceEnabled { return 0 } else {
////                return UITableView.automaticDimension
////            }
//        default: return UITableView.automaticDimension
//        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

extension HomeViewController:MenuItemsCellDelegate {
    func itemPressed(cell: MenuItemsTableViewCell, indexPath: IndexPath, menuCell:MenuCollectionViewCell) {
        switch indexPath.row {
            case 0:
                let shc = StudentHierarchyViewController()
                shc.modalPresentationStyle = .overFullScreen
                self.present(shc, animated: true, completion: nil)
            case 1:
                let evc = EventCalendarViewController()
                evc.modalPresentationStyle = .overFullScreen
                self.present(evc, animated: true, completion: nil)
            case 2:
                let timeTableVC = TimeTableViewController()
                timeTableVC.modalPresentationStyle = .overFullScreen
                self.present(timeTableVC, animated: true, completion: nil)
//            case 0:
//                UIAlertController.showAlert(alertTitle: "Nearby Service", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
//            case 1:
//                let shc = StudentHierarchyViewController()
//                shc.modalPresentationStyle = .overFullScreen
//                self.present(shc, animated: true, completion: nil)
//            case 2:
//                UIAlertController.showAlert(alertTitle: "Resources Service", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
//            case 3:
//                UIAlertController.showAlert(alertTitle: "Emergency Service", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
//            case 4:
//                UIAlertController.showAlert(alertTitle: "Libraries Service", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
//            case 5:
//                UIAlertController.showAlert(alertTitle: "Ask Service", message: "Feature Coming Soon.", alertActionTitle: "Ok", controller: self)
//            case 6:
//                let evc = EventCalendarViewController()
//                evc.modalPresentationStyle = .overFullScreen
//                self.present(evc, animated: true, completion: nil)
//            case 7:
//                let timeTableVC = TimeTableViewController()
//                timeTableVC.modalPresentationStyle = .overFullScreen
//                self.present(timeTableVC, animated: true, completion: nil)
            default: break
        }
        UIView.animate(withDuration: 0.3, delay: 1.0, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            menuCell.menuButton.alpha = 1
            menuCell.menuButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        }, completion: nil)
    }
}
extension HomeViewController:TodayNewsCellDelegate {
    func newsPressed(cell: TodayNewsTableViewCell, newsDetails: NewsServiceCodable.NewsOfTheDay?) {
        let newsDetailVC = NewsDetailViewController()
        newsDetailVC.newsDetails = newsDetails
        newsDetailVC.view.backgroundColor = .white
        self.navigationController?.pushViewController(newsDetailVC, animated: true)
    }
}
