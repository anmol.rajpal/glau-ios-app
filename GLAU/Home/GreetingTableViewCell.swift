//
//  GreetingTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class GreetingTableViewCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let dateLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1)
        label.adjustsFontForContentSizeCategory = true
        label.textColor = UIColor.lightGray
        label.numberOfLines = 1
        label.sizeToFit()
        return label
    }()
    let greetingLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).condensed()
        label.adjustsFontForContentSizeCategory = true
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }()
    func setUpViews() {
        contentView.addSubview(dateLabel)
        contentView.addSubview(greetingLabel)
    }
    func setUpConstraints() {
        _ = dateLabel.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 3, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
        _ = greetingLabel.anchorWithoutWH(top: dateLabel.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 10, leftConstant: 30, bottomConstant: 20, rightConstant: 30)
    }
}
