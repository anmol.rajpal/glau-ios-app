//
//  TodayFactTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit

class TodayFactTableViewCell: UITableViewCell {
    var factDetails:CombinedServicesCodable.FactsService.FactOfTheDay? {
        didSet {
            setUpCellData(fact: factDetails?.fact ?? "--")
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
        setUpConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setUpCellData(fact:String) {
        let prependTextAttributes = [
            NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3).condensed(),
            NSAttributedString.Key.foregroundColor : UIColor.lightGray
        ]
        let factLabelTextAttributes = [
            NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
//            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        let prependTextAttributedString = NSMutableAttributedString(string: "Do you know?\n", attributes: prependTextAttributes)
        let factLabelTextAttributedString = NSAttributedString(string: fact, attributes: factLabelTextAttributes)
        let factString = NSMutableAttributedString()
        factString.append(prependTextAttributedString)
        factString.append(factLabelTextAttributedString)
        factLabel.attributedText = factString
    }
    let headerLabel:UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Fact of the Day"
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title2).bold()
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        return label
    }()
    let separatorLine = HorizontalLine(color: UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0))
    let factLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontForContentSizeCategory = true
        label.sizeToFit()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    let sourceLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    let publisherLabel:UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.heavy)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(separatorLine)
        contentView.addSubview(headerLabel)
        contentView.addSubview(factLabel)
//        contentView.addSubview(sourceLabel)
//        contentView.addSubview(publisherLabel)
    }
    func setUpConstraints() {
        let leftRightMargin:CGFloat = 15
        _ = separatorLine.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        _ = headerLabel.anchorWithoutWH(top: separatorLine.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: leftRightMargin, bottomConstant: 0, rightConstant: leftRightMargin)
        _ = factLabel.anchorWithoutWH(top: headerLabel.bottomAnchor, left: contentView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 0, leftConstant: leftRightMargin, bottomConstant: 10, rightConstant: leftRightMargin)
        let b = factLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        b.priority = UILayoutPriority(750)
        b.isActive = true
//        _ = sourceLabel.anchorWithoutWH(top: factLabel.bottomAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: publisherLabel.leftAnchor, topConstant: 10, leftConstant: 20, bottomConstant: 5, rightConstant: 20)
//        _ = publisherLabel.anchorWithoutWH(top: factLabel.bottomAnchor, left: nil, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 5, rightConstant: 20)
    }
}
