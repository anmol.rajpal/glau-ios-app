//
//  MenuItemsTableViewCell.swift
//  GLAU
//
//  Created by Anmol Rajpal on 31/01/19.
//  Copyright © 2019 Natovi. All rights reserved.
//

import UIKit
protocol MenuItemsCellDelegate {
    func itemPressed(cell:MenuItemsTableViewCell, indexPath:IndexPath, menuCell:MenuCollectionViewCell)
}
class MenuItemsTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var delegate:MenuItemsCellDelegate?
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
        setUpCollectionView()
    }
    func setUpCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(DropdownGridMenuCell.self, forCellWithReuseIdentifier: NSStringFromClass(DropdownGridMenuCell.self))
        collectionView.register(MenuCollectionViewCell.self, forCellWithReuseIdentifier: NSStringFromClass(MenuCollectionViewCell.self))
        collectionView.layoutIfNeeded()
    }
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = true
        cv.contentInsetAdjustmentBehavior = .always
        cv.clipsToBounds = true
        cv.alwaysBounceVertical = true
        
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.isHidden = false
        return cv
    }()
    let containerView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 5.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowOpacity = 0.35
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let items: [DropdownGridMenuItem] = [
        
        DropdownGridMenuItem(text: "Map", image: UIImage(named: "map")!, selected: false),
        DropdownGridMenuItem(text: "Directory", image: UIImage(named: "directory")!, selected: false),
        DropdownGridMenuItem(text: "Resources", image: UIImage(named: "resources")!, selected: false),
        DropdownGridMenuItem(text: "Emergency", image: UIImage(named: "emergency")!, selected: false),
        DropdownGridMenuItem(text: "Dining", image: UIImage(named: "dining")!, selected: false),
        DropdownGridMenuItem(text: "Libraries", image: UIImage(named: "libraries")!, selected: false),
        DropdownGridMenuItem(text: "Ask", image: UIImage(named: "ask")!, selected: false),
        DropdownGridMenuItem(text: "Calendar", image: UIImage(named: "calendar")!, selected: false)
    ]
    let menuItems: [MenuItemButton] = [
//        MenuItemButton(text: "Nearby", image: UIImage(named: "map")!),
        MenuItemButton(text: "Hierarchy", image: UIImage(named: "directory")!),
//        MenuItemButton(text: "Resources", image: UIImage(named: "resources")!),
//        MenuItemButton(text: "Emergency", image: UIImage(named: "emergency")!),
////        MenuItemButton(text: "Dining", image: UIImage(named: "dining")!),
//        MenuItemButton(text: "Libraries", image: UIImage(named: "libraries")!),
//        MenuItemButton(text: "Ask", image: UIImage(named: "ask")!),
        MenuItemButton(text: "Calendar", image: UIImage(named: "calendar")!),
        MenuItemButton(text: "TimeTable", image: UIImage(named: "timetable")!)
    ]
    func setUpViews() {
        contentView.addSubview(collectionView)
    }
    func setUpConstraints() {
        _ = collectionView.anchorWithoutWH(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
//        let topConstraint = collectionView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0)
//        topConstraint.priority = UILayoutPriority(999)
//        topConstraint.isActive = true
//        let bottomConstraint = collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
//        bottomConstraint.priority = UILayoutPriority(999)
//        bottomConstraint.isActive = true
    }
    
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuItems.count
    }
    static let itemHeight:CGFloat = 80
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(MenuCollectionViewCell.self), for: indexPath) as! MenuCollectionViewCell
        
        let item = self.menuItems[indexPath.row]
        cell.configureButton(item: item)
        cell.menuButton.addAction(for: UIControl.Event.touchUpInside) {
            cell.menuButton.alpha = 0.9
            cell.menuButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
//            cell.menuButton.alpha = 1
//            cell.menuButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
            self.delegate?.itemPressed(cell: self, indexPath: indexPath, menuCell: cell)
        }
        cell.menuButton.addAction(for: UIControl.Event.touchDown) {
            UIView.animate(withDuration: 0.1, animations: {
                cell.menuButton.alpha = 0.9
                cell.menuButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            })
        }
        cell.menuButton.addAction(for: UIControl.Event.touchDragOutside) {
            UIView.animate(withDuration: 0, animations: {
                cell.menuButton.alpha = 1
                cell.menuButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
            })
        }
        cell.menuButton.addAction(for: UIControl.Event.touchCancel) {
            UIView.animate(withDuration: 0, animations: {
                cell.menuButton.alpha = 1
                cell.menuButton.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
            })
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.size.width / 3), height: MenuItemsTableViewCell.itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    }
}
