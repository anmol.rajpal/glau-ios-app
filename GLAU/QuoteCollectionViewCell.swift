//
//  QuoteCollectionViewCell.swift
//  GLA University
//
//  Created by Anmol Rajpal on 14/10/18.
//  Copyright © 2018 Natovi. All rights reserved.
//

import UIKit

class QuoteCollectionViewCell: UICollectionViewCell {
    static let viewHeight:CGFloat = 120
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViews()
        setUpConstraints()
    }
    let quoteView:UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        view.layer.cornerRadius = 15
        view.backgroundColor = UIColor.systemBlueColor
        //        view.clipsToBounds = true
        return view
    }()
    func setUpQuoteData(quote:String, quoter:String) {
        let quotationMarkAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 40, weight: UIFont.Weight.bold),
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        let quoteTextAttributes = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.light),
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        let quotationMarkAttributedString = NSMutableAttributedString(string: "\u{275D} ", attributes: quotationMarkAttributes)
        let quoteTextAttributedString = NSAttributedString(string: quote, attributes: quoteTextAttributes)
        let quoteString = NSMutableAttributedString()
        quoteString.append(quotationMarkAttributedString)
        quoteString.append(quoteTextAttributedString)
        quoteLabel.attributedText = quoteString
        quoterLabel.text = "- \(quoter)"
    }
    let quoteLabel:UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    let quoterLabel:UILabel = {
        let label = UILabel()
        label.text = "- "+"Quoter"
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
        label.numberOfLines = 1
        return label
    }()
    func setUpViews() {
        contentView.addSubview(quoteView)
        quoteView.addSubview(quoteLabel)
        quoteView.addSubview(quoterLabel)
    }
    func setUpConstraints() {
         _ = quoteView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = quoteLabel.anchor(quoteView.topAnchor, left: quoteView.leftAnchor, bottom: nil, right: quoteView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = quoterLabel.anchor(nil, left: nil, bottom: quoteView.bottomAnchor, right: quoteView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 15, widthConstant: 0, heightConstant: 0)
    }
}
